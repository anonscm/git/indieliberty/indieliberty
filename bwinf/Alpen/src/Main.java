import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.graphics.PaletteData;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;


public class Main {
	
	public static final int length = 100;
	public static final int height = length / 2;
	
	public static final int zoom = 10;

	public static void main(String[] args) {
		
		boolean acclivityDirection = true;
		int[] gebirge = new int[length];
		gebirge[0] = 0;
		
	//	int maxHeight = 5;
	//	boolean maxHeightReached = false;
	//	int minHeight = 1;
	//	boolean minHeightReached = true;
		
		Display display = new Display();
		Image gebirgeImage = new Image(display, length*zoom, height*zoom);
		GC gcGebirgeImage = new GC(gebirgeImage);
		
		for(int i=1;i<length;i++){
			if(gebirge[i-1] == (length - i) ) // ab jetzt runtergehen um auf 0 zu kommen.
				gebirge[i] = gebirge[i-1] - 1;
			else if(gebirge[i-1] == (length - i) -1) { // ab jetzt nicht mehr hochgehen um auf 0 zu kommen.
				int acclivity = (int)(Math.random()); // 0 oder 1
				gebirge[i] = gebirge[i-1] - acclivity;
			}
			else if(gebirge[i-1] == 0) {	
				int acclivity = (int) (Math.random() * 2) ; // 0, 1
				gebirge[i] = gebirge[i-1] + acclivity;				
			} else {
				int acclivity = (int) (Math.random() * 3) -1 ; // -1, 0, 1
				gebirge[i] = gebirge[i-1] + acclivity;
			}			
			System.out.print(gebirge[i] + ", ");
		}	
	/*		int acclivity = (int)(Math.random() * 2);

			if(acclivityDirection)
				gebirge[i] = gebirge[i-1]+acclivity;
			
			if(!acclivityDirection)
				gebirge[i] = gebirge[i-1]-acclivity;
			
			if(gebirge[i] == maxHeight)
				acclivityDirection = false;
			
			if(gebirge[i] == minHeight)
				acclivityDirection = true;
			
			
			if(gebirge[i-1] == (100-i)){
				//gcGebirgeImage.drawLine((i-1)*12, (5-gebirge[i-1])*20, i*12, (5-gebirge[i])*20);
				gcGebirgeImage.drawLine((i-1)*12, (5-gebirge[i-1])*20, 100*12, 5*20);
				break;
			
			else
				gcGebirgeImage.drawLine((i-1)*12, (5-gebirge[i-1])*20, i*12, (5-gebirge[i])*20);			
		} */
		for(int i = 0; i < length-1; i++)
			gcGebirgeImage.drawLine(i*zoom, zoom * (height - gebirge[i]), zoom*(i+1), zoom*(height - gebirge[i+1]) );			
			
		PaletteData palette = gebirgeImage.getImageData().palette;
		int lastPoint = -1;
		int snowLine = 40+(int)Math.floor(Math.random() * 10);
		
	/*	for(int i=0;i<1200;i++){
			int colorCurrentPixel = palette.getRGB(gebirgeImage.getImageData().getPixel(i, snowLine)).green;
			int colorNextPixel = 0;
			if(i+1 < 1200)
				colorNextPixel = palette.getRGB(gebirgeImage.getImageData().getPixel(i+1, snowLine)).green;
			
			if((lastPoint == -1) && (colorCurrentPixel == 0) && (colorNextPixel != 0)){
				lastPoint = i;
			}
			else if((colorCurrentPixel == 0) || ((i == 1199 ) && (lastPoint != -1) )){
				gcGebirgeImage.drawLine(lastPoint, snowLine, i, snowLine);
				int distance = i-lastPoint;
				boolean direction = false;
				lastPoint = -1;
				/*
				 * snowLine = 40+(int)Math.floor(Math.random() * 10);
				 * i += 4;
				 */
			/*	
			}
				
		} */
		/*
		gcGebirgeImage.setForeground(new Color(display, 33,185, 215));
		ImageData imageDataTmp = gebirgeImage.getImageData();
		PaletteData paletteTmp = imageDataTmp.palette;
		
		for(int i=0;i<800;i++){
			for(int j=0;j<160;j++){
				System.out.println(i+" : "+j);
				if(paletteTmp.getRGB(imageDataTmp.getPixel(i, j)).green == 0){
					gcGebirgeImage.drawLine(i, 0, i, j-1);
					break;
				}
					
			}
		}
		
		gcGebirgeImage.setForeground(new Color(display, 86,86, 86));
		
		for(int i=0;i<800;i++){
			for(int j=159;j>0;j--){
				System.out.println(i+" : "+j);
				if(paletteTmp.getRGB(imageDataTmp.getPixel(i, j)).green == 0){
					gcGebirgeImage.drawLine(i, 160, i, j);
					break;
				}
					
			}
		}*/
		
		
		gcGebirgeImage.dispose();
		Shell shell = new Shell(display);
		shell.setBounds(0,0,length*zoom+20,height*zoom+50);
		Label gebirgeImageLabel = new Label(shell, SWT.BORDER);
		gebirgeImageLabel.setImage(gebirgeImage);
		gebirgeImageLabel.setBounds(10,10,length*zoom,height*zoom);
		
		shell.open();
	
		while(!shell.isDisposed())
			display.readAndDispatch();
		
		display.dispose();
		
	}

}
