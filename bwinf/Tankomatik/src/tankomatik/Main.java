package tankomatik;

public class Main {

	/**
	 * @param args
	 */
	public static final int customersPerHour = 60; 
	
	public static final int Zeit = 2400;

	/*
	 * Anteil von Diesel, Super und SuperPlus Fahrzeugen. Quelle http://de.wikipedia.org/wiki/Autos.
	 * Da es keine klare Angabe für die Anzahl von SuperPlus-Kunden gibt, gehen wir davon aus, dass 
	 * 1/6 der Super-Kunden zu SuperPlus greifen, bzw der Rest um mit geraden Zahlen zu arbeiten.
	 * Von den PKW in Deutschland, die mit klassischen, fossilen Brennstoffen 
	 * angetrieben werden, fallen dann folgende Anteile an:
	 * Diesel:		10/46 
	 * Super:		36/46
	 * SuperPlus:	1/6 der superkunden.
	 */
	
	/*
	 * Anteil Benzinsorten
	 */
	public static final float dieselPart = 10f/46f;
	public static final float superPart = 36f/46f;
	
	
	/*
	 * Kunden
	 */
	public static final int stammkunden = (int)(customersPerHour*0.4f);
	public static final int angebotskunden = (int)(customersPerHour*0.6f);

	public static int dieselCustomers;
	public static int superCustomers;
	public static int superPlusCustomers;
	
	
	public static void main(String[] args) {
		
		System.out.println("Tankomatik gestartet\n" +
				"_______");
		
		dieselCustomers = (int)((angebotskunden * dieselPart));
		superCustomers = (int)(angebotskunden * superPart);
		superPlusCustomers = (angebotskunden - (dieselCustomers+superCustomers));
		
		int dieselStammCustomers = (int)(((stammkunden/2) * dieselPart));
		int superStammCustomers = (int)((stammkunden/2)  * superPart);
		int superPlusStammCustomers = ((stammkunden/2)  - ((dieselStammCustomers)+(superStammCustomers)));
		
		System.out.println("Angebotskunden "+(dieselCustomers+superCustomers+superPlusCustomers));
		System.out.println("Stammkunden "+(dieselStammCustomers+superStammCustomers+superPlusStammCustomers)*2);
		
		Spotmarkt.getInstance();
		
		for(int i=0; i<Zeit;i++){
			setPrices(i);
			if(Scholl.getInstance().getDieselPrice() < Asso.getInstance().getDieselPrice()){
				Scholl.getInstance().addDieselCustomers(dieselCustomers);
				Scholl.getInstance().addSuperCustomers(superCustomers);
				Scholl.getInstance().addSuperPlusCustomers(superPlusCustomers);
			}else{
				Asso.getInstance().addDieselCustomers(dieselCustomers);
				Asso.getInstance().addSuperCustomers(superCustomers);
				Asso.getInstance().addSuperPlusCustomers(superPlusCustomers);
			}
			
			Asso.getInstance().addDieselCustomers(dieselStammCustomers);
			Asso.getInstance().addSuperCustomers(superStammCustomers);
			Asso.getInstance().addSuperPlusCustomers(superPlusStammCustomers);
			
			Scholl.getInstance().addDieselCustomers(dieselStammCustomers);
			Scholl.getInstance().addSuperCustomers(superStammCustomers);
			Scholl.getInstance().addSuperPlusCustomers(superPlusStammCustomers);

			System.out.println("Kunden Scholl: " + Scholl.getInstance().customersPerHour);
			System.out.println("Kunden Asso: " + Asso.getInstance().customersPerHour);
			
		}
		System.out.println("\n\nAnzahl an Kunden Scholl: "+Scholl.getInstance().getCustomers());
		System.out.println("Anzahl an Kunden Asso: "+Asso.getInstance().getCustomers());
		System.out.println("Gewinn Scholl: "+Scholl.getInstance().gewinn);
		System.out.println("Gewinn Asso: "+Asso.getInstance().gewinn);
		
	}
	
	static void setPrices(int hour){
		System.out.println("Gewinn Scholl: " + Scholl.getInstance().berechneGewinn());
		System.out.println("Gewinn Asso: " + Asso.getInstance().berechneGewinn());
		
		System.out.println("\n"+hour+". Stunde\n------");
		Spotmarkt.getInstance().setNewPrices(hour);
		Scholl.getInstance().setPrices(hour);
		Asso.getInstance().setPrices(hour);
	}
	
	

}
