package tankomatik;


public class Spotmarkt {
	
	public static Spotmarkt spotmarkt;
	/*
	 * Realistisches Minimum in Euro für die drei Kraftstoffe
	 */
	public static final float lowestPrice = 1.30f;
	public static final float basicMultiplicator = 15;

	
	float diesel;
	float superBleifrei;
	float superPlusBleifrei;
	
	public Spotmarkt(){
		/*	
		 * Erzeugung der ersten Tagespreise durch Addition des minimalen Preises mit einer 
		 * Zufallszahl zwischen 0 und 15 Cent, was dirch die Variable 
		 * basicMultiplicator eingestellt werden kann.
		 */ 
		
		System.out.println("Erzeuge erste Preise...");
		
		diesel = lowestPrice+(float)Math.floor(Math.random() * basicMultiplicator)/100;
		superBleifrei = lowestPrice+(float)Math.floor(Math.random() * basicMultiplicator)/100;
		// superPlus wird auf Basis von Super berechnet, da superPlus immer teuerer als Super sein soll
		superPlusBleifrei = superBleifrei+(float)Math.floor(Math.random() * (basicMultiplicator)/2)/100;
	}
	
	public void setNewPrices(int hour){
		/*
		 * Generiere einen Faktor zwischen 0.99 und 1.01, um aus dem vorrausgegangenem Preis einen neuen zu 
		 * erzeugen, um eine für 6 Stunden realistische Preisentwicklung zu simulieren. 
		 */
		if((hour != 0) && ((hour % 6) == 0)){
			System.out.println("Neue Preise!");
			float factor = 0.99f+(float)((Math.random() * 2f)/100);
			diesel *= factor;
			superBleifrei *= factor;
			superPlusBleifrei *= factor;
		}

	}
	
	public float getDieselPrice(){
		return diesel;
	}
	
	public float getSuperBleifreiPrice(){
		return superBleifrei;
	}
	
	public float getSuperPlusBleifreiPrice(){
		return superPlusBleifrei;
	}
	
	public static Spotmarkt getInstance(){
		if(spotmarkt == null)
			spotmarkt = new Spotmarkt();
		return spotmarkt;
	}

}
