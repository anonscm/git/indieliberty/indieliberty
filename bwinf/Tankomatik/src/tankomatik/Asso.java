package tankomatik;
public class Asso {
	public static Asso asso;

	float dieselPrice;

	float superPrice;

	float superPlusPrice;

	int customersPerHour = 0;
	int customers = 0;

	int dieselCustomers = 0;
	int superCustomers = 0;
	int superPlusCustomers = 0;

	float einkaufspreisDiesel;
	float einkaufspreisSuper;
	float einkaufspreisSuperPlus;

	
	float gewinn = 0;

	public Asso() {
		dieselPrice = Spotmarkt.getInstance().getDieselPrice();
		superPrice = Spotmarkt.getInstance().getSuperBleifreiPrice();
		superPlusPrice = Spotmarkt.getInstance()
				.getSuperPlusBleifreiPrice();
	}

	public void addDieselCustomers(int number) {
		dieselCustomers += number;
		customersPerHour += number;
		customers += number;
		
	}

	public void addSuperCustomers(int number) {
		superCustomers += number;
		customersPerHour += number;
		customers += number;
	}

	public void addSuperPlusCustomers(int number) {
		superPlusCustomers += number;
		customersPerHour += number;
		customers += number;
	}

	public float getPriceAddition() {
		if (customersPerHour > 35)
			return +0.005f;
		if (customersPerHour < 25 && customersPerHour != 0)
			return -0.015f;
		else
			return 0.0f;
	}
	// Gewinn-Berechnung:
	public float berechneGewinn() {
		float stundenGewinn = 0;
		stundenGewinn += 100f *(dieselPrice - einkaufspreisDiesel) * (int)(customersPerHour * Main.dieselPart);
		stundenGewinn += 100f *(superPrice - einkaufspreisSuper) * (int)(customersPerHour * Main.superPart);
		stundenGewinn += 100f *(superPlusPrice - einkaufspreisSuperPlus) * (customersPerHour - (int)(customersPerHour * Main.dieselPart) - (int)(customersPerHour * Main.superPart));
		
		gewinn += stundenGewinn;
		return stundenGewinn;
	}
	
	/* 
	 * Preisberechnung fuer Asso:
	 */
	public void setPrices(int hour) {
			if ((hour % 6) == 0) {
				einkaufspreisDiesel = Spotmarkt.getInstance().getDieselPrice();
				einkaufspreisSuper = Spotmarkt.getInstance().getSuperBleifreiPrice();
				einkaufspreisSuperPlus = Spotmarkt.getInstance().getSuperPlusBleifreiPrice();
			}
			
			// je nach Kunden-Anzahl den Preis erhoehen/senken
			dieselPrice += getPriceAddition();
			superPrice += getPriceAddition();
			superPlusPrice += getPriceAddition();

			// falls der Preis unter dem aktuellen Einkaufspreis liegen wuerde, fuer den Einkaufspreis verkaufen...
			if (einkaufspreisDiesel > dieselPrice)
				dieselPrice = einkaufspreisDiesel;
			if (einkaufspreisSuper > superPrice)
				superPrice = einkaufspreisSuper;
			if (einkaufspreisSuperPlus > superPlusPrice)
				superPlusPrice = einkaufspreisSuperPlus;

			System.out.println("Asso:\n" +
					"\nPreisveränderung: "+ getPriceAddition() + "\n" +
					"Diesel: " + dieselPrice 
					+ "\nSuper: " + superPrice 
					+ "\nSuperPlus: "+ superPlusPrice + "\n");
			
			customersPerHour = 0;

	}

	public float getDieselPrice() {
		return dieselPrice;
	}

	public float getSuperPlusBleifreiPrice() {
		return superPlusPrice;
	}

	public float getSuperBleifreiPrice() {
		return superPrice;
	}

	public int getCustomers() {
		return customers;
	}

	public static Asso getInstance() {
		if (asso == null)
			asso = new Asso();
		return asso;
	}

}
