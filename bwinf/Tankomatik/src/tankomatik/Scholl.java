package tankomatik;

public class Scholl {

	public static Scholl scholl;
	
	float dieselPrice;
	float superBleifreiPrice;
	float superPlusBleifreiPrice;
	
	int customersPerHour = 0;
	int customers = 0;
	
	int dieselCustomers = 0;
	int superCustomers = 0;
	int superPlusCustomers = 0;
	
	float gewinn = 0;
	
	public void addDieselCustomers(int number) {
		dieselCustomers += number;
		customersPerHour += number;
		customers += number;
	}
	
	public void addSuperCustomers(int number) {
		superCustomers += number;
		customersPerHour += number;
		customers += number;
	}
	
	public void addSuperPlusCustomers(int number) {
		superPlusCustomers += number;
		customersPerHour += number;
		customers += number;
	}
	
	//Gewinn-Berechnung:
	public float berechneGewinn() {
		float stundenGewinn = customersPerHour * 5f;
		gewinn += stundenGewinn;
		return stundenGewinn;
	}
	
	/* Preisberechnung fuer Scholl:
	 * Preise sind genau 5c ueber dem Einkaufspreis.
	 */
	public void setPrices(int hour){
		if((hour % 6) == 0){
			dieselPrice = Spotmarkt.getInstance().getDieselPrice() + 0.05f;
			superBleifreiPrice = Spotmarkt.getInstance().getSuperBleifreiPrice() + 0.05f;
			superPlusBleifreiPrice = Spotmarkt.getInstance().getSuperPlusBleifreiPrice() + 0.05f;
		}
		System.out.println("Scholl:\n" +
				"Diesel: "+dieselPrice+
			     "\nSuper: "+superBleifreiPrice+
			     "\nSuperPlus: "+superPlusBleifreiPrice+"\n");

		
		customersPerHour = 0;
			
	}
	
	public float getDieselPrice(){
		return dieselPrice;
	}
	
	public float getSuperPlusBleifreiPrice(){
		return superPlusBleifreiPrice;
	}
	
	public float getSuperBleifreiPrice(){
		return superBleifreiPrice;
	}
	
	public int getCustomers(){
		return customers;
	}
	
	public static Scholl getInstance(){
		if(scholl == null)
			scholl = new Scholl();
		return scholl;
	}
}
