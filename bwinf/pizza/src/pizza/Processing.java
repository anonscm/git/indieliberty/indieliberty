package pizza;

import org.eclipse.swt.SWTException;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.graphics.ImageLoader;

import java.io.File;
import java.io.FileNotFoundException;
import java.math.*;
import java.util.ArrayList;

public class Processing {

	static Processing processing;

	ArrayList<String> components = new ArrayList<String>();

	ArrayList<Image> componentImage = new ArrayList<Image>();

	int componentCount = 0;

	ImageData pizzaImageData;

	public Processing() {
		pizzaImageData = new ImageData("pizza_blank.png");
	}

	/*
	 * Komponenten zur Belagsliste hinzufügen, wenn es sich nicht um
	 * einen Oberbegriff handelt.
	 */
	public void addComponents(String component) {

		
		if (!(component.contains("Fleisch") || component.contains("Fisch")
				|| component.contains("Vegetarisch")
				|| component.contains("Fisch") 
				|| component.contains("Käse") 
				|| component.contains("Gewürze"))) {
			if (!components.contains(component)) {
				components.add(component);
				System.out.println(component.substring(10,
						component.length() - 1)
						+ " Hinzugefügt");
			} else {
				components.remove(components.indexOf(component));
				System.out.println(component.substring(10,
						component.length() - 1)
						+ " Entfernt");
			}

			componentCount = 0;

			attachComponents();
		}

	}

	/*
	 * Laden der gewünschten Bilder der Zutaten von der Festplatte
	 */
	public int loadComponentImages() {
		componentImage.clear();
		if (components.size() != 0) {
			for (int i = 0; i < components.size(); i++) {
				if (new File("components/"
						+ components.get(i).substring(10,
								components.get(i).length() - 1) + ".png")
						.exists()) {
					componentImage.add(new Image(Gui.getInstance().display,
							new ImageData("components/"
									+ components.get(i).substring(10,
											components.get(i).length() - 1)
									+ ".png")));
				} else {
					components.remove(i);
				}
			}
			return components.size();
		} else
			return components.size();
	}

	/*
	 * Wählen der aktuellen Zutat, um gleichmäßige Verteilung zu erreichen
	 */
	public Image getCurrentComponent() {
		if (componentCount >= components.size()) {
			componentCount = 0;
		}
		componentCount++;
		return componentImage.get(componentCount - 1);
	}

	/*
	 * Zeichnen der Zutaten auf die Pizza Vorlage
	 */
	public void attachComponents() {
		Image pizzaImage = new Image(Gui.getInstance().display, pizzaImageData);
		GC pizzaGC = new GC(pizzaImage);

		
		/*
		 * Die Pizza ist auf drei Ringe aufgeteilt. Um auch bei viele Komponenten noch eine geeignete Menge der einzelnen
		 * Zutaten zu erhalten. Bei 26 freien Plätzen wäre bei voller Auslastung immerhon jede Komponente noch zwei mal 
		 * vorhanden. Die Position ergibt sich aus der Anzahl der Komponenten, die auf den Ringen Platz finden soll 
		 * (13, 8, 5), bei gegeben Entfernung zum Mittelpunkt der Pizza (175px, 100px, 45px) errechnet man somit anhand der
		 * Winkelbeziehungen die korrekte Position des Platzes vom Mittelpunkt der Pizza entfernt. 
		 * Dieser Vorgang benötigt zwar bei jedem platzieren Rechenleistung, ist jedoch deutlich eleganter, als die Position
		 * von vorn herein hart zu coden.
		 */
		Image currentComponent = null;
		if (loadComponentImages() > 0) {
			for (int i = 0; i < 13; i++) {
				int angle = 27 * i;
				double cosinus = Math.cos(Math.toRadians(angle)) * 175;
				int x = (int) cosinus;

				double sinus = Math.sin(Math.toRadians(angle)) * 175;
				int y = (int) sinus;

				currentComponent = getCurrentComponent();
				pizzaGC.drawImage(currentComponent, 250 - x
						- currentComponent.getBounds().width / 2, 250 - y
						- currentComponent.getBounds().height / 2);

			}

			for (int i = 0; i < 8; i++) {
				int angle = 45 * i;
				double cosinus = Math.cos(Math.toRadians(angle)) * 110;
				int x = (int) cosinus;

				double sinus = Math.sin(Math.toRadians(angle)) * 110;
				int y = (int) sinus;

				currentComponent = getCurrentComponent();
				pizzaGC.drawImage(currentComponent, 250 - x
						- currentComponent.getBounds().width / 2, 250 - y
						- currentComponent.getBounds().height / 2);
			}

			for (int i = 0; i < 5; i++) {
				int angle = 72 * i;
				double cosinus = Math.cos(Math.toRadians(angle)) * 55;
				int x = (int) cosinus;

				double sinus = Math.sin(Math.toRadians(angle)) * 55;
				int y = (int) sinus;

				currentComponent = getCurrentComponent();
				pizzaGC.drawImage(currentComponent, 250 - x
						- currentComponent.getBounds().width / 2, 250 - y
						- currentComponent.getBounds().height / 2);
			}

		}
		
		/*
		 * Käse als letzten Layer über die Pizza legen
		 */
		Image cheeseImage = new Image(Gui.getInstance().display, new ImageData(
				"cheese.png"));
		pizzaGC.drawImage(cheeseImage, 0, 0);
		pizzaGC.dispose();
		Gui.getInstance().attachPizzaImage(pizzaImage);
	}

	public static Processing getInstance() {
		if (processing == null)
			processing = new Processing();
		return processing;
	}
}