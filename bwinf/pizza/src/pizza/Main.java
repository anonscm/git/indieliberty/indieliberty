package pizza;

public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		System.out.println("Pizza generator started\n" +
				"------");
		
		//Gui initalisieren
		Gui.getInstance().initGui();
		
		//Prozess Klasse Instanzieren
		Processing processing = new Processing();
		
		//Eigentliches Programm starten
		Gui.getInstance().runGui();
		
	}

}
