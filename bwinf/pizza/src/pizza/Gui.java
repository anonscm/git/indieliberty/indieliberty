package pizza;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;

public class Gui {
	
	static Gui gui;
	
	public Display display;
	public Shell shell;
	
	public Label pizzaLabel;
	
	public Gui(){
		
	}
	
	public void initGui(){
		display = new Display();
		shell = new Shell(display);
		
		shell.setSize(800, 600);
		shell.setText("Pizza generator");
		
		GridLayout gridLayout = new GridLayout(9, false);
		shell.setLayout(gridLayout);
		
		
		pizzaLabel = new Label(shell, SWT.BORDER);
		GridData pizzaLabelGridData = new GridData(SWT.FILL, SWT.FILL, true, true, 5, 1);
		pizzaLabel.setLayoutData(pizzaLabelGridData);
	
		
		ImageData pizzaImageData = new ImageData("pizza_blank.png");
		Image pizzaImage = new Image(display, pizzaImageData);
		pizzaLabel.setSize(pizzaImage.getBounds().width, pizzaImage.getBounds().height);
		pizzaLabel.setImage(pizzaImage);
		
		Composite controlComposite = new Composite(shell, SWT.BORDER);
		GridData controlCompositeGridData = new GridData(SWT.FILL, SWT.FILL, true, true, 2, 1);
		controlCompositeGridData.widthHint = 770-pizzaImage.getBounds().width;
		controlComposite.setLayoutData(controlCompositeGridData);

		
		GridLayout controlCompositeGridLayout = new GridLayout(1, true);
		controlComposite.setLayout(controlCompositeGridLayout);
		
		final Tree tree = new Tree (controlComposite, SWT.CHECK);
		GridData treeGridData = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
		tree.setLayoutData(treeGridData);
		
		Button finishButton = new Button(controlComposite, SWT.PUSH);
		GridData finishButtonGridData = new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1);
		finishButtonGridData.minimumHeight = 150;
		finishButton.setLayoutData(finishButtonGridData);
		finishButton.setText("Bestellen!!!");
		finishButton.addListener (SWT.Selection, new Listener () {
			public void handleEvent (Event event) {
				Shell dialog = new Shell(shell);
				dialog.setSize(250, 400);
				GridLayout gridLayout2 = new GridLayout(1, false);
				dialog.setLayout(gridLayout2);
				dialog.setText("Bestellung aufgeben");
				
				Label headLabel = new Label(dialog, SWT.NONE);
				GridData gridData1 = new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1);
				gridData1.minimumHeight = 300;
				headLabel.setLayoutData(gridData1);
				headLabel.setText("Pizza mit folgenden Komponenten\n" +
						"wird bestellt\n" +
						"______");
				List componentList = new List(dialog, SWT.None);
				componentList.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
				Button closeButton = new Button(dialog, SWT.NONE);
				GridData gridData2 = new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1);
				gridData2.minimumHeight = 150;
				closeButton.setLayoutData(gridData2);
				closeButton.setText("Bestellen!!!");
				closeButton.addListener (SWT.Selection, new Listener () {
					public void handleEvent (Event event) {
							System.out.println("-------\n" +
									"Bestellung mit folgenden Zutaten aufgegeben:");
							for(int i=0; i<Processing.getInstance().components.size();i++){
								System.out.println(" -"+Processing.getInstance().components.get(i).substring(10,
										Processing.getInstance().components.get(i).length() - 1));
							}
							System.exit(0);
					}
				});
				
				for(int i=0; i<Processing.getInstance().components.size();i++){
					componentList.add(Processing.getInstance().components.get(i).substring(10,
							Processing.getInstance().components.get(i).length() - 1));
				}
				
				dialog.open();
				
			}
		});
		/*
		 * Listener um Veränderungen der Zutatenliste zu erkennen
		 */
		tree.addListener (SWT.Selection, new Listener () {
			public void handleEvent (Event event) {
				if(event.detail == SWT.CHECK){
						Processing.getInstance().addComponents(event.item+"");
				}
			}
		});

		
		TreeItem fleischTreeItem = new TreeItem(tree, 0);
		fleischTreeItem.setText("Fleisch");
			TreeItem salami = new TreeItem(fleischTreeItem, 0);
			salami.setText("Salami");
			TreeItem schinken = new TreeItem(fleischTreeItem, 0);
			schinken.setText("Schinken");
			TreeItem hackfleisch = new TreeItem(fleischTreeItem, 0);
			hackfleisch.setText("Hackfleisch");
		
		
		

		TreeItem fischTreeItem = new TreeItem(tree, 0);
		fischTreeItem.setText("Fisch");
			TreeItem thunfisch = new TreeItem(fischTreeItem, 0);
			thunfisch.setText("Thunfisch");
			TreeItem sardellen = new TreeItem(fischTreeItem, 0);
			sardellen.setText("Sardellen");
		
		TreeItem vegetarischTreeItem = new TreeItem(tree, 0);
		vegetarischTreeItem.setText("Vegetarisch");
			TreeItem tomaten = new TreeItem(vegetarischTreeItem, 0);
			tomaten.setText("Tomaten");
			TreeItem zwiebeln = new TreeItem(vegetarischTreeItem, 0);
			zwiebeln.setText("Zwiebeln");
			TreeItem champignon = new TreeItem(vegetarischTreeItem, 0);
			champignon.setText("Champignon");
			TreeItem peperoni = new TreeItem(vegetarischTreeItem, 0);
			peperoni.setText("Pepperoni");
			TreeItem oliven = new TreeItem(vegetarischTreeItem, 0);
			oliven.setText("Oliven");
			TreeItem ananas = new TreeItem(vegetarischTreeItem, 0);
			ananas.setText("Annanas");
			
		TreeItem kaeseTreeItem = new TreeItem(tree, 0);
		kaeseTreeItem.setText("Käse");
			TreeItem mozarella = new TreeItem(kaeseTreeItem, 0);
			mozarella.setText("Mozzarella");
			TreeItem ricotta = new TreeItem(kaeseTreeItem, 0);
			ricotta.setText("Ricotta");

		
		TreeItem gewuerzTreeItem = new TreeItem(tree, 0);
		gewuerzTreeItem.setText("Gewürze");
			TreeItem basilikum = new TreeItem(gewuerzTreeItem, 0);
			basilikum.setText("Basilikum");
			TreeItem oregano = new TreeItem(gewuerzTreeItem, 0);
			oregano.setText("Oregano");
			TreeItem knoblauch = new TreeItem(gewuerzTreeItem, 0);
			knoblauch.setText("Knoblauch");
			TreeItem pfeffer = new TreeItem(gewuerzTreeItem, 0);
			pfeffer.setText("Pfeffer");
				
	}
	
	public void runGui(){
		shell.open();
		
		while(!shell.isDisposed()){
			display.readAndDispatch();
		}
		display.dispose();
	}
	
	
	public void attachPizzaImage(Image image){
		pizzaLabel.setImage(image);
	}
	
	
	public static Gui getInstance(){
		if(gui == null)
			gui = new Gui();
		return gui;
	}
}
