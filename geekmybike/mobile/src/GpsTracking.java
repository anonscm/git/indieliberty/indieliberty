package src;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.StringTokenizer;


public class GpsTracking extends Thread{
	String time;
	String latitude;
	String longtitude;
	String quality;
	String satellites;
	String height;
	
	public static GpsTracking gpsTracking;
	static boolean status = true;
	boolean gpsStatus = false;
	
	public void run(){
		FileInputStream inputStream = null;
		try {
			inputStream = new FileInputStream("/dev/pts/0");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		byte[] input = new byte[2000];
		GpsProtocolation gpsProtocolation = new GpsProtocolation();
		int count = 0;
		
		while(status){
			try {
				inputStream.read(input);
			} catch (IOException e) {
			// TODO Auto-generated catch block
				e.printStackTrace();
			}
			String s = new String(input);

			String gpgga = null;
			int idx = 0; int tokenCount;
			String words[] = new String [500];
			StringTokenizer st = new StringTokenizer(s);
			tokenCount = st.countTokens();
			while (st.hasMoreTokens()) // make sure there is stuff to get
			{ words[idx] = st.nextToken(); idx++; }
			for (idx=0;idx<tokenCount; idx++)
				{ }//System.out.println(words[idx]); System.out.println(); }
			for (int i = 0; i < tokenCount; i++){
				if(words[i].contains("$GPGGA")){
					//System.out.println(words[i]);
					gpgga = words[i];
				}
			}
			String[] parts = gpgga.split(",");
			time = parts[1];
			latitude = parts[2];
			longtitude = parts[4];
	    	quality = parts[6];
	    	satellites = parts[7];
	    	height = parts[9];
	    	
	    	count++;
	    	
	    	if(quality.equals("0")){
	    		Gui.getInstance().setGpsData("Searching for Sattelites\n" +
	    				"trys: "+count);
	    		gpsStatus = false;
	    		if(gpsStatus == true){
	    			Gui.getInstance().setGpsIcon(false);
	    			gpsStatus = false;
	    		}
	    	}
	    	else{
	    		Gui.getInstance().setGpsData("Sattelites: "+satellites+"\n"+
	    				"Longtitude: "+longtitude+"\n"+
	    				"Latitude: "+latitude+"\n"+
	    				"Waypoints "+count);
	    		if(gpsStatus == false){
	    			Gui.getInstance().setGpsIcon(true);
	    			gpsStatus = true;
	    		}
	    	}
	    	
	    	
	 
	    	GpsProtocolation.getInstance().protocollate(latitude, longtitude, height, time, ""+SensorData.getInstance().getSpeed());
	  
	    	
	    /*	System.out.println("Time: "+time);
	    	System.out.println("Latitude: "+latitude);
	    	System.out.println("Longtitude: "+longtitude);
	    	System.out.println("Quality: "+quality);
	 	   	System.out.println("satellites: "+satellites);
	 	   	System.out.println("Height: "+height);   */
		}
	}
	public String getLatitude(){
		return latitude;
	}
	
	public String getLongtitude(){
		return longtitude;
	}
	
	public String getTime(){
		return time;
	}
	
	public int getHeight(){
		if(!(height == null) && !(height.length() == 0)){
			float tmp = Float.parseFloat(height);
			int intTmp = (int)tmp;
			return intTmp;
		}
		else 
			return 1;
	}
	
	public static void stopTracking(){
		status = false;
	}
	
	public static GpsTracking getInstance(){
		if(gpsTracking == null){
			gpsTracking = new GpsTracking();
		}
		return gpsTracking;
	}

}
