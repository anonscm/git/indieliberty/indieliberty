package src;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class GpsProtocolation {
	
	static GpsProtocolation gpsProtocolation;
	
	ArrayList <String> latitude = new ArrayList<String>();
	ArrayList <String> longtitude = new ArrayList<String>(); 
	ArrayList <String> height = new ArrayList<String>(); 
	ArrayList <String> time = new ArrayList<String>(); 
	ArrayList <String> speed = new ArrayList<String>();
	int count = 0;
	
	public void protocollate(String latitudetmp, String longtitudetmp, String heighttmp, String timetmp, String speedtmp){
		latitude.add(latitudetmp);
		longtitude.add(longtitudetmp);
		height.add(heighttmp);
		time.add(timetmp);
		speed.add(speedtmp);
		count++;
	}
	
	public void writetodisk(String path) throws IOException{
		System.out.println(latitude);
		File a = new File(path);
		FileWriter fw = new FileWriter(a);
		BufferedWriter bw = new BufferedWriter(fw); 
		for(int i=0; i<count;i++){
		    bw.write(i+" "+time.get(i)+" "+latitude.get(i)+" "+longtitude.get(i)+" "+height.get(i)+" "+speed.get(i)+"\n");
		}
	    bw.close();
	}
	
	public void emptyGpsLog(){
		latitude.clear();
		longtitude.clear();
		time.clear();
		height.clear();
		count = 0;
	}
	
	public static GpsProtocolation getInstance(){
		if(gpsProtocolation == null){
			gpsProtocolation = new GpsProtocolation();
		}
		return gpsProtocolation;
	}
	

}
