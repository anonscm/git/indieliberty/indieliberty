package src;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Spinner;

public class BikeProperties {
	public static BikeProperties bikeProperties;
	
	int diameter = 62;
	int impulsNumber = 18;
	
	public void bikePropertiesDialog(){
		final Shell dialog = new Shell(Display.getCurrent());
		
		dialog.setText("Bike properties");
		GridLayout gridLayout = new GridLayout(6, false);
		dialog.setLayout(gridLayout);
		
		GridData labelGridData = new GridData(SWT.FILL, SWT.FILL, true, false, 4, 1);
		Label diameterLabel = new Label(dialog, SWT.NONE);
		diameterLabel.setText("Diameter in cm: ");
		diameterLabel.setLayoutData(labelGridData);
		
		GridData spinnerGridData = new GridData(SWT.FILL, SWT.FILL, true, false, 2, 1);
		final Spinner diameterSpinner = new Spinner (dialog, SWT.BORDER);
		diameterSpinner.setMinimum(1);
		diameterSpinner.setMaximum(80);
		if(diameter > 0)
			diameterSpinner.setSelection(diameter);
		else
			diameterSpinner.setSelection(50);
		diameterSpinner.setIncrement(1);
		diameterSpinner.setPageIncrement(100);
		diameterSpinner.setLayoutData(spinnerGridData);
		
		Label impulsLabel = new Label(dialog, SWT.NONE);
		impulsLabel.setText("Number of impulses: ");
		impulsLabel.setLayoutData(labelGridData);
		
		final Spinner impulsSpinner = new Spinner (dialog, SWT.BORDER);
		impulsSpinner.setMinimum(1);
		impulsSpinner.setMaximum(30);
		if(impulsNumber > 0)
			impulsSpinner.setSelection(impulsNumber);
		else
			impulsSpinner.setSelection(18);
		impulsSpinner.setIncrement(1);
		impulsSpinner.setPageIncrement(100);
		impulsSpinner.setLayoutData(spinnerGridData);
		
		
		GridData buttonGridData = new GridData(SWT.FILL, SWT.FILL, true, false, 3, 1);
		
		Button save = new Button(dialog, SWT.PUSH);
		save.setText("Save");
		save.setLayoutData(buttonGridData);
		
		Button cancel = new Button(dialog, SWT.PUSH);
		cancel.setText("Cancel");
		cancel.setLayoutData(buttonGridData);
		
		save.addListener (SWT.Selection, new Listener () {
			public void handleEvent (Event e) {
				diameter = diameterSpinner.getSelection();
				impulsNumber = impulsSpinner.getSelection();
				dialog.dispose();
			}
		});
		
		cancel.addListener (SWT.Selection, new Listener () {
			public void handleEvent (Event e) {
				dialog.dispose();
			}
		});
		dialog.pack();
		dialog.setSize(400, 300);
		dialog.open();
		dialog.setFullScreen(Gui.getInstance().getShell().getFullScreen());
	    while (!dialog.isDisposed()) {
	      if (!Display.getCurrent().readAndDispatch())
	    	  Display.getCurrent().sleep();
	    }
	    SensorData.getInstance().setProperties();
	    dialog.dispose();
	}
	
	public static BikeProperties getInstance(){
		if(bikeProperties == null){
			bikeProperties = new BikeProperties();
		}
		return bikeProperties;
	}
}
