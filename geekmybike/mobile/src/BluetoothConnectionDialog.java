package src;


import icommand.nxt.comm.NXTCommand;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.swt.widgets.Text;


public class BluetoothConnectionDialog {
	public String mac = "";
	
	Shell dialog;
	Text hostEntry;
	Text portEntry;
	Text passwordEntry;
	Text btPasswordEntry;
	List list;
	
	public BluetoothConnectionDialog(Shell parent) {		
		dialog = new Shell(Display.getCurrent(), SWT.DIALOG_TRIM | SWT.APPLICATION_MODAL);
		dialog.setText ("Bluetooth connection");
		dialog.setSize (350, 410);
		GridLayout dialogLayout = new GridLayout(2, false);
		dialogLayout.horizontalSpacing = 20;
		dialogLayout.verticalSpacing = 20;
		dialog.setLayout(dialogLayout);
		
		
		final TabFolder tabFolder = new TabFolder (dialog, SWT.NONE);
		TabItem bluetooth = new TabItem (tabFolder, SWT.NONE);
		bluetooth.setText("Bluetooth");
		bluetooth.addListener(SWT.Selection, bluetoothListener);
		
		
		//BluetoothConfiguration
		
		Composite bluetoothConfiguration = new Composite(tabFolder, SWT.NONE);
		bluetoothConfiguration.setLayout(new GridLayout(2, false));
		bluetoothConfiguration.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, true, 2, 1));
		
		Composite buttonComposite = new Composite(bluetoothConfiguration, SWT.NONE);
		buttonComposite.setLayout(new GridLayout(2, false));
		buttonComposite.setLayoutData(new GridData(SWT.FILL, SWT.RIGHT, true, true, 2, 1));
		
/*		Composite passwordComposite = new Composite(bluetoothConfiguration, SWT.NONE);
		passwordComposite.setLayout(new GridLayout(2, false));
		passwordComposite.setLayoutData(new GridData(SWT.FILL, SWT.RIGHT, true, true, 2, 1)); */
		
		list = new List (bluetoothConfiguration, SWT.BORDER | SWT.MULTI | SWT.V_SCROLL);
		GridData bluetoothConfigurationGridData = new GridData(SWT.RIGHT, SWT.BOTTOM, false, false);
		bluetoothConfigurationGridData.widthHint = 300;
		bluetoothConfigurationGridData.heightHint = 250;
		list.setLayoutData(bluetoothConfigurationGridData);
		
		Button bluetoothConnectButton = new Button(buttonComposite, SWT.PUSH);
		GridData bluetoothConnectButtonGridData = new GridData(SWT.RIGHT, SWT.BOTTOM, false, false);
		bluetoothConnectButtonGridData.widthHint = 150;
		bluetoothConnectButtonGridData.heightHint = 50;
		bluetoothConnectButton.setLayoutData(bluetoothConnectButtonGridData);
		bluetoothConnectButton.setText("Connect");
		bluetoothConnectButton.addListener(SWT.Selection, bluetoothConnectButtonListener);
		
		Button refreshButton = new Button(buttonComposite, SWT.PUSH);
		GridData refreshButtonGridData = new GridData(SWT.RIGHT, SWT.BOTTOM, false, false);
		refreshButtonGridData.widthHint = 150;
		refreshButtonGridData.heightHint = 50;
		refreshButton.setLayoutData(refreshButtonGridData);
		refreshButton.setText("Refresh");
		refreshButton.addListener(SWT.Selection, bluetoothListener);

		
		bluetooth.setControl(bluetoothConfiguration);
		
		
		//dialog.pack();
		open();
	}
	
	Listener bluetoothConnectButtonListener = new Listener(){
		public void handleEvent(Event arg0){
			String [] test = list.getSelection();
			
			for(int i = 1; i < 18; i++){
				mac = mac+test[0].charAt(i);
			}
			File a = new File("icommand.properties");
			FileWriter fw;
			try {
				fw = new FileWriter(a);
				BufferedWriter bw = new BufferedWriter(fw); 
				bw.write("nxtcomm.type=bluecove");
				bw.write("\nnxt.btaddress="+mac);
				bw.write("\nnxtcomm.type=bluez");
				bw.write("\nbluez.address="+mac);
			    bw.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
		    NXTCommand.open();
		    Gui.getInstance().setBtIcon(true);
			SensorData.getInstance().start();
			SensorData.getInstance().address = mac;
		    dialog.dispose();
			
		}
	};

	Listener cancelListener = new Listener() {
		public void handleEvent(Event arg0) {
			dialog.dispose();
		}
	};
	
	private void open() {
		dialog.open();
			Display display = Display.getDefault();
		while (!dialog.isDisposed())
			display.readAndDispatch();
	}
	
	Listener bluetoothListener = new Listener(){
		public void handleEvent(Event arg0) {
			System.out.println("BLuetoothListener");
			btsearch();
		}	
	};
	
	Process proc = null;
	private void btsearch() {
		new Thread(new Runnable() {
			public void run() {
					String first = "";
					System.out.println("Searching for Bluetooth devices");
					try {	
						clearListAsync();
						setNewContentAsync("Searching...");
						proc = Runtime.getRuntime().exec( "hcitool scan --refresh" );
					}  
					catch (IOException e) {			
						e.printStackTrace();
					} 
					BufferedReader in = new BufferedReader(new InputStreamReader(proc.getInputStream()));
					String [] test = new String[100];
					int i = 0;
					try {
						first = in.readLine();					
					} 
					catch (IOException e1) {
						e1.printStackTrace();
					}
					System.out.println(first);		
					System.out.println("Search finished");
					while(true){
						try {
							test[i] = in.readLine();
						} 
						catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						if(test[i] == null) {
							if(i == 0){
								setNewContentAsync("Nothing found");
							}
							break;
						}
						i++;
					} 
					clearListAsync();
					for(int e=0; e < i; e++){ 
						if(test[e] != null){
							setNewContentAsync(test[e]);
							System.out.println(test[e]);
						} 					
					}
					if(first == null){
						setNewContentAsync("No powered bluetooth");
						setNewContentAsync("device found.");
					}
			}			
		}).start();
	}
	
	public void setNewContent(String string) {
		list.add(string);
	}
	
	public void setNewContentAsync(final String string) {
		Display.getDefault().asyncExec(new Runnable() {
			public void run() {
				setNewContent(string);
			}			
		});
	}

	public void clearListAsync() {
		Display.getDefault().asyncExec(new Runnable() {
			public void run() {
				list.removeAll();
			}			
		});
	}
}
