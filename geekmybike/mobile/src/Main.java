package src;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;

public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		//GpsTracking gpstracking = new GpsTracking();
		//gpstracking.start();
		GpsTracking.getInstance().start();
		//TrackingDialog.getInstance().test();
		
		Gui.getInstance().initGUI();
		Gui.getInstance().runGUI();
	 }

}
