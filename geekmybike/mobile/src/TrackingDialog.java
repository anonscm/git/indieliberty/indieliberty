package src;

import java.io.IOException;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

public class TrackingDialog {
	Display display;
	Shell shell;
	Label time;
	static TrackingDialog trackingdialog;
	Label longtitude;
	Label latitude; 
	Label quality; 
	Label satalites;  
	Label height; 
	Label count;
	Button stop;
	
	public void test(){
		display = new Display();
		shell = new Shell(display);
		shell.setSize(300,300);
		time = new Label(shell, SWT.BEGINNING);
		longtitude = new Label(shell, SWT.BEGINNING);
		latitude = new Label(shell, SWT.BEGINNING);
		quality = new Label(shell, SWT.BEGINNING);
		satalites = new Label(shell, SWT.BEGINNING);
		height = new Label(shell, SWT.BEGINNING);
		count = new Label(shell, SWT.BEGINNING);
		
		stop = new Button(shell, SWT.PUSH);
		stop.setText("Stop");
		stop.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent evt) {
				GpsTracking.stopTracking();
			/*	try {
					//GpsProtocolation.getInstance().writetodisk();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}*/
			}
		});
		
		time.setText("Time:                                        ");
		longtitude.setText("Longtitude:                            ");
		latitude.setText("Latitude:                                ");
		quality.setText("Quality:                                  ");
		satalites.setText("Satalites:                              ");
		height.setText("height:                                    ");
		count.setText("                                            ");
		
		GridLayout layout = new GridLayout();
	    layout.numColumns = 1;
	    shell.setLayout(layout);
		
		//shell.pack();
		shell.open();
		while (!shell.isDisposed()) {
			if (!shell.getDisplay().readAndDispatch()) {
				shell.getDisplay().sleep();
			}
		}
		display.dispose();
	}
	
	public void updategui(final String times, final String longtitudes, final String latitudes, final String qualitys, final String satalitess, final String heights, final int counts){
		Display.getDefault().asyncExec(new Runnable() {
			public void run() {
				System.out.println("Is Running");
				time.setText("Time: "+times);
				longtitude.setText("Longtitude: "+longtitudes);
				latitude.setText("Latitude: "+latitudes);
				quality.setText("Quality: "+qualitys);
				satalites.setText("Satalites: "+satalitess);
				height.setText("height: "+heights);
				count.setText(""+counts);
			}			
		}); 
	}
	
	public static TrackingDialog getInstance(){
		if(trackingdialog==null){
			trackingdialog = new TrackingDialog();
		}
		return trackingdialog;
	}
	

}
