package src;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.math.*;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;

public class Gui {
	
	static Gui gui;
	Display display;
	Shell shell;
	
	Label speed;
	Label kmh;
	Label speedAdd;
	Label way;
	Label time;
	Label acclivity;
	Label gpsData;
	Label gpsIcon;
	Button BtButton;
	Button ExitButton;

	
	public Shell getShell(){
		return shell;
	}
	
	public void setSpeedAsync(final float speedtmp){
		display.asyncExec(new Runnable() {
			public void run() {
				String finalSpeed = "";
				String finalMaxSpeed = "";
				
				String tmp = ""+speedtmp;
				for(int i=0; i<tmp.length();i++){
					finalSpeed = finalSpeed+tmp.charAt(i);
					if(tmp.charAt(i) == '.'){
						finalSpeed = finalSpeed+tmp.charAt(i+1);
						break;
					}
				}
				tmp = ""+SensorData.getInstance().getMaxSpeed();
				for(int i=0; i<tmp.length();i++){
					finalMaxSpeed = finalMaxSpeed+tmp.charAt(i);
					if(tmp.charAt(i) == '.'){
						finalMaxSpeed = finalMaxSpeed+tmp.charAt(i+1);
						break;
					}
				}
				speed.setText(finalSpeed);
			}			
		});
	}
	
	public void setSpeedAddAsync(final float maxSpeedTmp, final float aveSpeedTmp){
		display.asyncExec(new Runnable() {
			public void run() {
				String finalMaxSpeed = "";
				String finalAveSpeed = "";

				String tmp = "";
				tmp = ""+maxSpeedTmp;
				for(int i=0; i<tmp.length();i++){
					finalMaxSpeed = finalMaxSpeed+tmp.charAt(i);
					if(tmp.charAt(i) == '.'){
						finalMaxSpeed = finalMaxSpeed+tmp.charAt(i+1);
						break;
					}
				}
				
				tmp = "";
				tmp = ""+aveSpeedTmp;
				for(int i=0; i<tmp.length();i++){
					finalAveSpeed = finalAveSpeed+tmp.charAt(i);
					if(tmp.charAt(i) == '.'){
						finalAveSpeed = finalAveSpeed+tmp.charAt(i+1);
						break;
					}
				}
				speedAdd.setText("Max: "+finalMaxSpeed+" Ave: "+finalAveSpeed);
			}			
		});
	}
	

	
	public void setTime(final String timetmp){
		display.asyncExec(new Runnable() {
			public void run() {
				
				time.setText(timetmp);
			}			
		});
	}
	
	public void setWay(final float waytmp){
		display.asyncExec(new Runnable() {
			public void run() {
				String tmp = ""+waytmp;
				String way2 = "";
				for(int i=0; i<tmp.length();i++){
					way2 = way2+tmp.charAt(i);
					if(tmp.charAt(i) == '.'){
						way2 = way2+tmp.charAt(i+1);
						if(tmp.length() > 3)
							way2 = way2+tmp.charAt(i+2);
						break;
					}
				}
				way.setText(way2+" km");
			}			
		});
	}
	
	public void setAcclivity(final float acclivitytmp, final Image imagetmp){
		display.asyncExec(new Runnable() {
			public void run() {
				acclivity.setImage(imagetmp);
				//acclivity.setText("Acclivity:\n"+acclivitytmp);
			}			
		});
	}
	
	public void setGpsData(final String gpsDatatmp){
		display.asyncExec(new Runnable() {
			public void run() {
				gpsData.setText(gpsDatatmp);
			}			
		});
	}
	
	public void setGpsIcon(final boolean status){
		display.asyncExec(new Runnable() {
			public void run() {
				if(status)
					gpsIcon.setImage(getIcon("gps_on.png"));
				if(!status)
					gpsIcon.setImage(getIcon("gps_off.png"));
			}			
		});
	}
	
	public void setBtIcon(final boolean status){
		display.asyncExec(new Runnable() {
			public void run() {
				if(status)
					BtButton.setImage(getIcon("bluetooth_on.png"));
				if(!status)
					BtButton.setImage(getIcon("bluetooth_off.png"));
			}			
		});
	}
	
	public Image getIcon(String name){
		ImageData iconData = new ImageData(name);
		Image iconImage = new Image(display, iconData);
		return iconImage;
	}
	
	public void runGUI(){
	    shell.open();
	    while (!shell.isDisposed()) {
	      if (!display.readAndDispatch())
	        display.sleep();			
	    }
	    //newFont.dispose();
	    display.dispose();
	}
	
	public void initGUI(){
		display = new Display();
	    shell = new Shell(display);
	    shell.setBounds(10, 10, 770, 430);
	    shell.setText("Geek my bike");

	    GridLayout layout = new GridLayout(2, false);
	    shell.setLayout(layout);
	    
	    GridData gridDataSpeedComp = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 4);
	    Composite speedComp = new Composite(shell, SWT.BORDER);
	    speedComp.setLayoutData(gridDataSpeedComp);
	    GridLayout speedComplayout = new GridLayout(1, false);
	    speedComplayout.verticalSpacing = 8;
	    speedComp.setLayout(speedComplayout);
	    
	    GridData gridDataSpeed = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 4);
	    speed = new Label(speedComp, SWT.NONE);
	    speed.setLayoutData(gridDataSpeed);
	    speed.setText("78.9");
	    speed.setAlignment(SWT.CENTER);
	    
	    Font speedFont = speed.getFont();
	    FontData[] speedFontData = speedFont.getFontData();
	    for (int i = 0; i < speedFontData.length; i++) {
	          speedFontData[i].setHeight(100);
	    }
	    Font newSpeedFont = new Font(display, speedFontData);
	    speed.setFont(newSpeedFont);
	    
	    GridData gridDatakmh = new GridData(SWT.FILL, SWT.BOTTOM, false, false, 1, 1);
	    kmh = new Label(speedComp, SWT.NONE);
	    kmh.setLayoutData(gridDatakmh);
	    kmh.setText("km/h");
	    kmh.setAlignment(SWT.CENTER);
	    
	    Font kmhFont = kmh.getFont();
	    FontData[] kmhFontData = kmhFont.getFontData();
	    for (int i = 0; i < kmhFontData.length; i++) {
	          kmhFontData[i].setHeight(24);
	    }
	    Font newkmhFont = new Font(display, kmhFontData);
	    kmh.setFont(newkmhFont);
	    
	    GridData gridDataSpeedAdd = new GridData(SWT.FILL, SWT.FILL, false, false, 1, 2);
	    speedAdd = new Label(speedComp, SWT.NONE);
	    speedAdd.setLayoutData(gridDataSpeedAdd);
	    speedAdd.setText("Max: 24");
	    speedAdd.setAlignment(SWT.CENTER);
	    
	    Font speedAddFont = speedAdd.getFont();
	    FontData[] speedAddFontData = speedAddFont.getFontData();
	    for (int i = 0; i < speedAddFontData.length; i++) {
	          speedAddFontData[i].setHeight(28);
	    }
	    Font newSpeedAddFont = new Font(display, speedAddFontData);
	    speedAdd.setFont(newSpeedAddFont);
	    
	    
	    
	    
	    Composite addInfoComp = new Composite(shell, SWT.NONE);
	    GridData gridDataAddInfo = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 5);
	    addInfoComp.setLayoutData(gridDataAddInfo);
	    GridLayout gridLayoutAddInfo = new GridLayout(2, false);
	    addInfoComp.setLayout(gridLayoutAddInfo);
	    
	    
	    GridData gridDataWay = new GridData(SWT.FILL, SWT.FILL, true,  true, 1, 1);
	    way = new Label(addInfoComp, SWT.BORDER);
	    way.setLayoutData(gridDataWay);
	    way.setText("128.5 km");
	    way.setAlignment(SWT.CENTER);
	    
	    GridData gridDataTime = new GridData(SWT.FILL, SWT.FILL, true,  true, 1, 1);
	    time = new Label(addInfoComp, SWT.BORDER);
	    time.setLayoutData(gridDataTime);
	    time.setText("02:34:18\ndhdh");
	    time.setAlignment(SWT.CENTER);
	    
	    
	    GridData gridDataAcclivity = new GridData(SWT.FILL, SWT.FILL, true,  true, 2, 2);
	    acclivity = new Label(addInfoComp, SWT.BORDER);
	    acclivity.setLayoutData(gridDataAcclivity);
	    acclivity.setText("acclivity");
	    acclivity.setAlignment(SWT.CENTER);
	    
	    ImageData accImageData = new ImageData("blank_acc.png");
	    Image accImage = new Image(display, accImageData);
	    
	    setAcclivity(0.0f, accImage);
	    
	    
	    
	    
	    Composite controlComp = new Composite(shell, SWT.NONE);
	    GridData controlCompGridData = new GridData(SWT.FILL, SWT.FILL, false, true, 2, 4);
	    controlComp.setLayoutData(controlCompGridData);
	    GridLayout controlCompGridLayout = new GridLayout(5, false);
	    controlComp.setLayout(controlCompGridLayout);
	    

	    GridData gridDataGpsData = new GridData(SWT.FILL, SWT.FILL, true,  true, 2, 1);
	    gpsData = new Label(controlComp, SWT.BORDER);
	    gpsData.setLayoutData(gridDataGpsData);
	    gpsData.setText("GPS-Data");
	    
	    GridData gridDataGpsIcon = new GridData(SWT.FILL, SWT.FILL, true,  true, 1, 1);
	    gpsIcon = new Label(controlComp, SWT.NONE);
	    gpsIcon.setLayoutData(gridDataGpsIcon);
	    gpsIcon.setAlignment(SWT.CENTER);
	    //gpsIcon.setText("GPS\nIcon");
	    gpsIcon.setImage(getIcon("gps_off.png"));
	    
	    GridData gridDataBtButton = new GridData(SWT.FILL, SWT.FILL, true,  true, 1, 1);
	    BtButton = new Button(controlComp, SWT.NONE);
	    BtButton.setLayoutData(gridDataBtButton);
	    //BtButton.setText("Bt\nButton");
	    BtButton.setImage(getIcon("bluetooth_off.png"));
	    
	    GridData gridDataExitButton = new GridData(SWT.FILL, SWT.FILL, true,  true, 1, 1);
	    ExitButton = new Button(controlComp, SWT.NONE);
	    ExitButton.setLayoutData(gridDataExitButton);
	    //ExitButton.setText("Exit\nButton");
	    ExitButton.setImage(getIcon("exit.png"));
	    
	    
	    Menu bar = new Menu (shell, SWT.BAR);
		shell.setMenuBar (bar);
		
		MenuItem fileItem = new MenuItem (bar, SWT.CASCADE);
		fileItem.setText ("&File");
		
		Menu filesubmenu = new Menu (shell, SWT.DROP_DOWN);
		fileItem.setMenu (filesubmenu);
		MenuItem newfile = new MenuItem(filesubmenu, SWT.PUSH);
		MenuItem save = new MenuItem(filesubmenu, SWT.PUSH);
		MenuItem exit = new MenuItem (filesubmenu, SWT.PUSH);
		
		
		MenuItem optionItem = new MenuItem (bar, SWT.CASCADE);
		optionItem.setText ("&Option");
		
		Menu optionsubmenu = new Menu (shell, SWT.DROP_DOWN);
		optionItem.setMenu (optionsubmenu);
		MenuItem bike = new MenuItem (optionsubmenu, SWT.PUSH);
		
		
		MenuItem helpItem = new MenuItem (bar, SWT.CASCADE);
		helpItem.setText ("&?");
		
		Menu helpsubmenu = new Menu (shell, SWT.DROP_DOWN);
		helpItem.setMenu (helpsubmenu);
		MenuItem info = new MenuItem (helpsubmenu, SWT.PUSH);
		MenuItem license = new MenuItem (helpsubmenu, SWT.PUSH);

		ExitButton.addListener (SWT.Selection, new Listener () {
			public void handleEvent (Event e) {
				System.out.println ("Exit");
				exit();
				
			}
		});
		
		exit.addListener (SWT.Selection, new Listener () {
			public void handleEvent (Event e) {
				System.out.println ("Exit");
				exit();
				
			}
		});
		
		info.addListener (SWT.Selection, new Listener () {
			public void handleEvent (Event e) {
				System.out.println ("info");
			}
		});
		
		save.addListener (SWT.Selection, new Listener () {
			public void handleEvent (Event e) {
				System.out.println ("save");
				save();    
			}
		});
		
		newfile.addListener (SWT.Selection, new Listener () {
			public void handleEvent (Event e) {
				System.out.println ("new file");
				GpsProtocolation.getInstance().emptyGpsLog();
			}
		});
		
		bike.addListener (SWT.Selection, new Listener () {
			public void handleEvent (Event e) {
				System.out.println ("bike properties");
				BikeProperties.getInstance().bikePropertiesDialog();
			}
		});	
		exit.setText ("Exit");
		save.setText("Save");
		newfile.setText("New");
		bike.setText("Bike properties");
		info.setText("Info");
		license.setText("License");
		

		Display.getCurrent().addFilter(SWT.KeyDown, new Listener() {

			/**
			 * @see org.eclipse.swt.widgets.Listener#handleEvent(org.eclipse.swt.widgets.Event)
			 */
			public void handleEvent(Event event) {
				if (event.keyCode == SWT.F6)
					shell.setFullScreen(!shell.getFullScreen());
					
			}

		});

	  /*  Font speedFont = speed.getFont();
	    FontData[] speedFontData = speedFont.getFontData();
	    for (int i = 0; i < speedFontData.length; i++) {
	          speedFontData[i].setHeight(78);
	    }
	    Font newSpeedFont = new Font(display, speedFontData);
	    speed.setFont(newSpeedFont); */
	    
	    Font usualFont = way.getFont();
	    FontData[] usualFontData = usualFont.getFontData();
	    for (int i = 0; i < usualFontData.length; i++) {
	          usualFontData[i].setHeight(24);
	    }
	    Font newUsualFont = new Font(display, usualFontData);
	    way.setFont(newUsualFont); 
	    time.setFont(newUsualFont); 
	    
	    BluetoothConnectionDialog bluetoothConnectionDialog = new BluetoothConnectionDialog(shell);

	}
	
	public void save(){
		SimpleDateFormat formatter = new SimpleDateFormat ("HH:mm:ss'_'dd-MM-yy");
		Date currentTime = new Date();
	    FileDialog dialog = new FileDialog(shell, SWT.SAVE);
	    dialog.setFilterNames(new String[] { "txt Files" });
	    dialog.setFilterExtensions(new String[] { "*.txt" });                                                   
	    dialog.setFilterPath("/home/user"); 
	    dialog.setFileName("gps_data-"+formatter.format(currentTime)+".txt");
	    String path;
	    if((path = dialog.open()) != null){
	    	try {
	    		GpsProtocolation.getInstance().writetodisk(path);
	    	} catch (IOException e1) {
	    		// TODO Auto-generated catch block
	    		e1.printStackTrace();
	    	}
	    }
	}
	
	public void exit(){
		MessageBox messageBox = new MessageBox(shell, SWT.ICON_WARNING | SWT.ABORT | SWT.RETRY | SWT.IGNORE);
        messageBox.setText("Warning");
        messageBox.setMessage("Write log to disk before leaving?");
        int buttonID = messageBox.open();
        switch(buttonID) {
          case 512:
            save();
          case 1024:
            System.exit(1);
            break;
          case 2048:
            break;
        }
	}
	
	public static Gui getInstance(){
		if(gui == null){
			gui = new Gui();
		}
		return gui;
	}
}
