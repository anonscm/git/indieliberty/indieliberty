package src;

import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;

import icommand.nxt.NXT;

public class SensorData extends Thread {
	public static SensorData sensorData;
	
	public float way = 0;
	public int time = 0;
	
	public int count;
	public float speed;
	float maxSpeed = 0.0f;
	public String address;
	
	int averageCount = 0;
	float averageSumm = 0.0f;

	float multiplier;
	long beginTime;
	long MovingTime = 0;
	long lastMovingTime = 0;
	long standingTime = 0;
	
	int oldHeight;
	float oldWay;

	public void run() {

		setProperties();

		int lastcount = 0;
		beginTime = (long)(System.currentTimeMillis()/1000);

		while (true) {
			try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				count = Integer.parseInt((""
						+ (char) NXT.getBrickName().charAt(0)
						+ (char) NXT.getBrickName().charAt(1) + (char) NXT
						.getBrickName().charAt(2)));
				//System.out.println(count);
			} catch (NumberFormatException e) {
				count = 0;
				System.out.println("falsche daten vom NXT");
			}
			speed = (count + lastcount) / 2f
					* multiplier;
			speed = speed * 4; // count is alle 250ms geupdated
			
			if(speed > maxSpeed)
				maxSpeed = speed;
			if(speed != 0){
				calcAverageSpeed(speed);
				calcAcclivity();
			}
			Gui.getInstance().setSpeedAsync(speed);
			way += (getSpeed()/3.6f)/1000f; 
			Gui.getInstance().setWay(way);
			lastcount = count;
			Gui.getInstance().setSpeedAddAsync(getMaxSpeed(), getAveSpeed());
			
			Gui.getInstance().setTime(getBeginTime(beginTime)+"\n"
					+getMovingTime(beginTime));
			
			

		}

	}
	
	public float getSpeed(){
		return speed;
	}
	
	public float getMaxSpeed(){
		return maxSpeed;
	}
	
	public void calcAverageSpeed(float speedtmp){
		averageCount++;
		averageSumm = (averageSumm + speedtmp);
	}
	
	public float getAveSpeed(){
		return averageSumm/averageCount;
	}
	
	public void calcAcclivity(){
		if(oldHeight != 0){
			int verticalDistance = GpsTracking.getInstance().getHeight()-oldHeight;
			float horizontalDistance = getSpeed()/3.6f;
			float acclivity = verticalDistance/horizontalDistance;
			System.out.println(acclivity);
			//acclivity = -0.2f;
			
			Image acclivityDiagram = new Image(Gui.getInstance().display, 300, 150);
			GC gc = new GC(acclivityDiagram);
			
			gc.drawText(""+(acclivity*100)+"%",140,10,true);
			gc.setLineWidth(3);
			
			if(acclivity >= 0)
				gc.drawLine(1, 130, 300, 130-(int)(300*acclivity));
			else
				gc.drawLine(1, 50, 300, 50-(int)(300*acclivity));
			
			gc.dispose();
			//System.out.println(130-(int)(300*acclivity));
			
			Gui.getInstance().setAcclivity(acclivity, acclivityDiagram);
		}
		oldHeight = GpsTracking.getInstance().getHeight();
	}

	public void setProperties() {
		float ipt = (float) BikeProperties.getInstance().impulsNumber;
		float wheelsize = ((float) BikeProperties.getInstance().diameter) / 100f;
		multiplier = (1 / ipt) * wheelsize * 3.14f; // m/s
		multiplier *= 3.6f; // kmh
	}
	
	public String getMovingTime(long beginMillis){
		long currentMillis = System.currentTimeMillis()/1000-beginMillis;
		
		if(getSpeed() == 0){
			//standingTime = standingTime + (currentMillis-lastMovingTime); 
		}else{
			standingTime = currentMillis-lastMovingTime;
			lastMovingTime = currentMillis;
			MovingTime = MovingTime + standingTime;
			currentMillis = MovingTime;
		}
		//System.out.println(standingTime);
		
		
		int hours = (int)(currentMillis/3600);
		int minutes = (int)((currentMillis%3600)/60);
		int seconds = (int)((currentMillis%3600)%60);
		
		String secondsString = "";
		String minutesString = "";
		String hoursString = "";
		
		if(hours < 10){
			hoursString = "0"+hours;
		}else{
			hoursString = ""+hours;
		}
		
		if(minutes < 10){
			minutesString = "0"+minutes;
		}else{
			minutesString = ""+minutes;
		}
		
		if(seconds < 10){
			secondsString = "0"+seconds;
		}else{
			secondsString = ""+seconds;
		}
		
		String finalString = (hoursString+":"+minutesString+":"+secondsString);
		
		return finalString;
	}
	
	public String getBeginTime(long beginMillis){
		long currentMillis = System.currentTimeMillis()/1000-beginMillis;
		
		int hours = (int)(currentMillis/3600);
		int minutes = (int)((currentMillis%3600)/60);
		int seconds = (int)((currentMillis%3600)%60);
		
		String secondsString = "";
		String minutesString = "";
		String hoursString = "";
		
		if(hours < 10){
			hoursString = "0"+hours;
		}else{
			hoursString = ""+hours;
		}
		
		if(minutes < 10){
			minutesString = "0"+minutes;
		}else{
			minutesString = ""+minutes;
		}
		
		if(seconds < 10){
			secondsString = "0"+seconds;
		}else{
			secondsString = ""+seconds;
		}
		
		String finalString = (hoursString+":"+minutesString+":"+secondsString);
		
		return finalString;
	}
	
	
	public static SensorData getInstance() {
		if (sensorData == null) {
			sensorData = new SensorData();
		}
		return sensorData;
	}
	
	
}
