package src;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import java.lang.Math;

public class PhysicsDialog {
	static PhysicsDialog physicsDialog;

	public boolean activated;

	public Shell shell;

	int masse = 100;
	
	Label heightDiagram;

	Label speedDiagram;
	
	Label accDiagram;
	
	Label powerDiagram;

	public int shellWidth;

	public int shellHeight;

	Image heightMapImage;

	Image speedMapImage;
	
	Image accMapImage;
	
	Image powerMapImage;

	Text lbPhysicsData;
	
	public void initPhysicsDialog(){
		shell = new Shell(Gui.getDisplay());
		shell.setSize(1000, 600);
		shell.setText("Geek my bike - Physics view");
		shell.setLocation(Gui.getInstance().shell.getSize().x, 0);
		GridLayout shellGridLayout = new GridLayout(2, false);
		shell.setLayout(shellGridLayout);
		
		GridData dataCompGridData = new GridData(SWT.FILL, SWT.FILL, true, true, 2, 1);
		Composite dataComp = new Composite(shell, SWT.BORDER);
		dataComp.setLayoutData(dataCompGridData);
		
		GridData diagramCompGridData = new GridData(SWT.FILL, SWT.FILL, true, true, 2, 1);
		Composite diagramComp = new Composite(shell, SWT.BORDER);
		diagramComp.setLayoutData(diagramCompGridData);
		GridLayout diagramCompGridLayout = new GridLayout(2, false);
		diagramComp.setLayout(diagramCompGridLayout);
		
		
		GridData diagramGridData = new GridData(SWT.FILL, SWT.FILL,
				true, true, 1, 1);
		
		heightDiagram = new Label(diagramComp, SWT.BORDER);
		heightDiagram.setLayoutData(diagramGridData);
		heightDiagram.setSize(heightDiagram.getSize().x, 175);
		
		speedDiagram = new Label(diagramComp, SWT.BORDER);
		speedDiagram.setLayoutData(diagramGridData);
		speedDiagram.setSize(speedDiagram.getSize().x, 175);
		
		powerDiagram = new Label(diagramComp, SWT.BORDER);
		powerDiagram.setLayoutData(diagramGridData);
		powerDiagram.setSize(powerDiagram.getSize().x, 175);
		
		accDiagram = new Label(diagramComp, SWT.BORDER);
		accDiagram.setLayoutData(diagramGridData);
		accDiagram.setSize(accDiagram.getSize().x, 175);
		

		lbPhysicsData = new Text(dataComp, SWT.RIGHT | SWT.MULTI);
		lbPhysicsData.setSize(490, 150);
		update(0);
	}

	public void show() {
		shell.open();
	}

	public void update(int position) {
		String text;
		float speedkmh = Map.getInstance().speed.get(position);
		text = "Geschwindigkeit: " + speedkmh + " km/h";
		float speedmps = speedkmh / 3.6f;
		text += "\n" + speedmps + " m/s";
		float acceleration = 0;
		if (position != 0)
			acceleration = speedmps
					- (Map.getInstance().speed.get(position - 1) / 3.6f);
		else
			acceleration = speedmps;
		text += "\nBeschleunigung: " + acceleration + " m/s/s";
		text += "\nMasse: " + masse + " kg";
		
		float power = masse * acceleration * speedmps;
		if(power >= 0)
			text += "\nLeistung: " + power + "W";
		else
			text += "\nBremsleistung: " + power + "W";
			
		text += "\nArbeit bis hier: " + Map.getInstance().work.get(position) + "J";
		
		float maxspeed = 0;
		for (int i = 0; i < Map.getInstance().speed.size(); i++) {
			if (Map.getInstance().speed.get(i) > maxspeed)
				maxspeed = Map.getInstance().speed.get(i);
		}
		text += "\nMaximale Geschw.: " + maxspeed + " km/h";
		text += "\nMaximale Arbeit: " + Map.getInstance().work.get(Map.getInstance().work.size() -1) + "J";
		text += "\nMaximale Leistung: " + Map.getInstance().maxpower + "W";
		text += "\nMaximale Beschleunigung: " + Map.getInstance().maxacc + "m*s^-2";
		int height_diff = 0;
		if(position > 0)
			height_diff = Map.getInstance().height.get(position) - Map.getInstance().height.get(position-1);
		float acclivity = (float)height_diff / speedmps;
		text += "\n\nHöhenunterschied d. letz. Sek.: " + height_diff + "m";
		text += "\nSteigung: " + (int)(acclivity*100f) + "%";

		float acclivityAngle =(float) ( (Math.atan((double)acclivity)) / Math.PI * 180);
		text += "\nSteigungswinkel: " + acclivityAngle + "°";
		text += "\nMinimale Höhe: " + Map.getInstance().minheight + "m ü. NN";
		text += "\nMaximale Höhe: " + Map.getInstance().maxheight + "m ü. NN";
		
		lbPhysicsData.setText(text);
	}
	
	public void attachHeightDiagram(Image image) {
		heightDiagram.setImage(image);
	}

	public void attachSpeedDiagram(Image image) {
		speedDiagram.setImage(image);
	}

	public void attachAccDiagram(Image image) {
		accDiagram.setImage(image);
	}
	
	public void attachPowerDiagram(Image image) {
		powerDiagram.setImage(image);
	}

	
	public static PhysicsDialog getInstance() {
		if (physicsDialog == null) {
			physicsDialog = new PhysicsDialog();
		}
		return physicsDialog;
	}
}
