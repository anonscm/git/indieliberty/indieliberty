
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

//file download
import java.io.*;
import java.net.*;

//load png
import org.eclipse.swt.*;
import org.eclipse.swt.graphics.*;
import org.eclipse.swt.widgets.*;

// draw
import org.eclipse.swt.graphics.GC;

public class Main {

	static float maxlong = 0;

	static float maxlat = 0;

	static float minlat = 0;

	static float minlong = 0;

	static Display display;

	static Image imageMap;

	static String filePath;

	public static void main(String[] args) throws IOException {

		// System.setProperty( "proxySet", "true" );
		// System.setProperty( "proxyHost", "172.16.0.1" );
		// System.setProperty( "proxyPort", "8080" );
		OpeningDialog openingDialog = new OpeningDialog();
		Map.getInstance().downloadmapfile("map.png", Map.getInstance().scale,
				Map.getInstance().minlong, Map.getInstance().minlat,
				Map.getInstance().maxlong, Map.getInstance().maxlat, 0.001f);
		Map.getInstance().loadImage("map.png");
		// downloaddetailview(latitude, longtitude);
		System.out.println("drawing track...");
		// Map.getInstance().drawtrack(Map.getInstance().latitude,
		// Map.getInstance().longtitude);
		Gui.getInstance().setUpGui();
		imageMap = Map.getInstance().drawtrack(Map.getInstance().imaged,
				Map.getInstance().latitude, Map.getInstance().longtitude,
				Map.getInstance().minlong, Map.getInstance().minlat,
				Map.getInstance().maxlong, Map.getInstance().maxlat, 0.002f,
				"./map2.png");
		Gui.getInstance().attachRawMap(imageMap);
		Gui.getInstance().showGui();

	}

}
