

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.graphics.ImageLoader;
import org.eclipse.swt.widgets.Display;

public class Map {

	public float maxlong = 0;

	public float maxlat = 0;

	public float minlat = 0;

	public float minlong = 0;

	public int minheight = 0;

	public int maxheight = 0;

	public float maxspeed = 0;

	public float maxpower = 0;

	public float maxacc = 0;
	
	public float minacc = 0;

	public ArrayList<String> longtitude = new ArrayList<String>();

	public ArrayList<String> latitude = new ArrayList<String>();

	public ArrayList<Integer> height = new ArrayList<Integer>();

	public ArrayList<Float> speed = new ArrayList<Float>();
	
	public ArrayList<Float> acc = new ArrayList<Float>();

	public ArrayList<Float> power = new ArrayList<Float>();
	
	public ArrayList<Float> work = new ArrayList<Float>();

	public static Map map;

	public int scale = 0;

	public ImageData imaged;

	public ImageData backup;

	public ImageData backupHeight;

	public ImageData backupSpeed;
	
	public ImageData backupAcc;
	
	public ImageData backupPower;

	public ImageData bikedata;

	public Image bikeimage;

	public Image image;

	public Image imageHeight;
	
	public Image imageAcc;

	public Image imageSpeed;

	public Image imagePower;
	
	public void readData(String path) throws IOException {

		BufferedReader inputReader = null;

		try {
			inputReader = new BufferedReader(new FileReader(path));
		} catch (FileNotFoundException e) {
			System.out.println("Shit, file not found ...");
		}
		String tmp;
		while ((tmp = inputReader.readLine()) != null) {
			String[] parts = tmp.split(" ");
			if (parts[2].length() > 2) {
				latitude.add(recalculatelat(parts[2]));
				longtitude.add(recalculatelong(parts[3]));
				height.add(recalculateheight(parts[4]));
				speed.add(Float.parseFloat(parts[5]));
			}

		}
		for(int i = 0; i < speed.size(); i++) {
			if(speed.get(i) > maxspeed)
				maxspeed = speed.get(i);
		}
		acc.add(0f);
		for(int i = 1; i < speed.size(); i++) {
			acc.add( (speed.get(i)-speed.get(i-1)) / 3.6f);
			if(acc.get(i) > maxacc)
				maxacc = acc.get(i);
			if(acc.get(i) < minacc)
				minacc = acc.get(i);
		}
		if( (minacc*-1) > maxacc)
			maxacc = minacc*-1;
		else
			minacc = maxacc*-1;
		
		int masse = 100;
		for(int i = 0; i < acc.size(); i++) {
			
			if(acc.get(i) > 0)
				power.add( (float)masse * acc.get(i) * (speed.get(i)/3.6f) );
			else
				power.add(0f);
			if(power.get(i) > maxpower)
				maxpower = power.get(i);
		}
		
		work.add(0f);
		for(int i = 1; i < power.size(); i++) {
			work.add(work.get(i-1)+power.get(i));
		}

		float difflat = maxlat - minlat;
		float difflong = maxlong - minlong;
		int px = 500;
		float latpropx = difflat / (float) px;
		float longpropx = difflong / (float) px;
		int scale_lat = (int) (latpropx / 1.5E-9);
		int scale_long = (int) (longpropx / 2.5E-9);
		if (scale_lat > scale_long)
			scale = scale_lat;
		else
			scale = scale_long;

	}

	public String recalculatelong(String input) {
		int raw = Integer.parseInt("" + input.charAt(2));
		float fine = Integer.parseInt("" + input.charAt(3) + input.charAt(4)
				+ input.charAt(6) + input.charAt(7) + input.charAt(8)
				+ input.charAt(9));
		float coordinate = raw + (fine / 600000);
		if (coordinate > maxlong || maxlong == 0) {
			maxlong = coordinate;
		}
		if (coordinate < minlong || minlong == 0) {
			minlong = coordinate;
		}
		return Float.toString(coordinate);
	}

	public String recalculatelat(String input) {
		int raw = Integer.parseInt("" + input.charAt(0) + input.charAt(1));
		float fine = Integer.parseInt("" + input.charAt(2) + input.charAt(3)
				+ input.charAt(5) + input.charAt(6) + input.charAt(7)
				+ input.charAt(8));
		float coordinate = raw + (fine / 600000);
		if (coordinate > maxlat || maxlat == 0) {
			maxlat = coordinate;
		}
		if (coordinate < minlat || minlat == 0) {
			minlat = coordinate;
		}

		return Float.toString(coordinate);
	}

	public int recalculateheight(String input) {
		int tmp;
		tmp = (int) Float.parseFloat(input);
		if (tmp < minheight || minheight == 0) {
			minheight = tmp;
		}
		if (tmp > maxheight) {
			maxheight = tmp;
		}
		return tmp;
	}

	public ImageData loadImage(String path) {
		imaged = new ImageData(path);
		return imaged;
	}

	public Image drawtrack(ImageData imaged, final ArrayList<String> data_lat,
			final ArrayList<String> data_long, float min_long, float min_lat,
			float max_long, float max_lat, float border, String filename) {

		Display display = Gui.getDisplay();
		int iheight = imaged.height;
		int iwidth = imaged.width;
		Image image = new Image(display, imaged);
		try {

			float difflat = max_lat - min_lat;
			float difflong = max_long - min_long;

			difflat += border; // rahmen einrechnen...
			difflong += border; // rahmen einrechnen...

			int lastx = 0, lasty = 0;
			GC gc = new GC(image);
			gc.setLineWidth(2);
			Color color = display.getSystemColor(SWT.COLOR_BLUE);
			gc.setForeground(color);

			for (int i = 0; i < data_long.size(); i++) {
				float fx = Float.parseFloat(data_long.get(i));
				float fy = Float.parseFloat(data_lat.get(i));
				fx -= min_long; // lokale long
				fx += 0.001f;
				fy = max_lat - fy; // lokale lat
				fy += 0.001f; // rahmen einrechnen
				float rx = fx / difflong;
				float ry = fy / difflat;

				int px = (int) ((float) iwidth * rx);
				int py = (int) ((float) iheight * ry);

				if (i != 0)
					gc.drawLine(lastx, lasty, px, py);
				// kreuz alle 60 sekunden...
				/*
				 * if (i%60 == 0 ) { gc.drawLine(px-6, py-6, px+6,py+6);
				 * gc.drawLine(px-6, py+6, px+6,py-6); }
				 */
				lastx = px;
				lasty = py;
			}

			ImageLoader imageLoader = new ImageLoader();
			imageLoader.data = new ImageData[] { image.getImageData() };
			imageLoader.save(filename, SWT.IMAGE_PNG);

			// Gui.getInstance().attachRawMap(image);
			// display.dispose();
			gc.dispose();

		} catch (Exception e) {
			System.out
					.println("map file is corrupt - download probably failed due to an too large area");
		}
		return image;
	}

	public Image drawDeatilTrack(ImageData imaged,
			final ArrayList<String> data_lat,
			final ArrayList<String> data_long, float min_long, float min_lat,
			float max_long, float max_lat, float border, String filename) {

		Display display = Gui.getDisplay();
		int iheight = imaged.height;
		int iwidth = imaged.width;
		Image image = new Image(display, imaged);
		try {

			float difflat = max_lat - min_lat;
			float difflong = max_long - min_long;

			difflat += border; // rahmen einrechnen...
			difflong += border; // rahmen einrechnen...

			int lastx = 0, lasty = 0;
			GC gc = new GC(image);
			gc.setLineWidth(2);
			Color color = display.getSystemColor(SWT.COLOR_BLUE);
			gc.setForeground(color);

			for (int i = 0; i < data_long.size(); i++) {
				float fx = Float.parseFloat(data_long.get(i));
				float fy = Float.parseFloat(data_lat.get(i));
				fx -= min_long; // lokale long
				fx += 0.001f;
				fy = max_lat - fy; // lokale lat
				fy += 0.001f; // rahmen einrechnen
				float rx = fx / difflong;
				float ry = fy / difflat;

				int px = (int) ((float) iwidth * rx);
				int py = (int) ((float) iheight * ry);

				if (i != 0)
					gc.drawLine(lastx, lasty, px, py);
				// kreuz alle 60 sekunden...
				/*
				 * if (i%60 == 0 ) { gc.drawLine(px-6, py-6, px+6,py+6);
				 * gc.drawLine(px-6, py+6, px+6,py-6); }
				 */
				lastx = px;
				lasty = py;
			}

			float fx = Float.parseFloat(data_long.get(Gui.getInstance().scale
					.getSelection()
					* Map.getInstance().latitude.size() / 100));
			float fy = Float.parseFloat(data_lat.get(Gui.getInstance().scale
					.getSelection()
					* Map.getInstance().latitude.size() / 100));
			fx -= min_long; // lokale long
			fx += 0.001f;
			fy = max_lat - fy; // lokale lat
			fy += 0.001f; // rahmen einrechnen
			float rx = fx / difflong;
			float ry = fy / difflat;

			int px = (int) ((float) iwidth * rx);
			int py = (int) ((float) iheight * ry);

			gc.setBackground(display.getSystemColor(SWT.COLOR_GREEN));
			gc.fillOval(px - 6, py - 6, 12, 12);

			gc.setBackground(display.getSystemColor(SWT.COLOR_RED));
			gc.fillOval(px - 3, py - 3, 6, 6);
			// gc.drawOval(px-3, py-3, 6, 6);

			ImageLoader imageLoader = new ImageLoader();
			imageLoader.data = new ImageData[] { image.getImageData() };
			imageLoader.save(filename, SWT.IMAGE_PNG);

			// Gui.getInstance().attachRawMap(image);
			// display.dispose();
			gc.dispose();

		} catch (Exception e) {
			System.out
					.println("map file is corrupt - download probably failed due to an too large area");
		}
		return image;
	}

	public void downloaddetailview(int number) {

		float fx = Float.parseFloat(longtitude.get(number));
		float fy = Float.parseFloat(latitude.get(number));

		// double lat_diff = 0.0077;
		// double long_diff = 0.005;
		downloadmapfile("detailed_view.png", 10000, fx
				- Float.parseFloat("0.003"), fy - Float.parseFloat("0.002"), fx
				+ Float.parseFloat("0.003"), fy + Float.parseFloat("0.002"),
				0.001f);

		Image image = new Image(Gui.getDisplay(),loadImage("detailed_view.png"));

		// public Image drawtrack(ImageData imaged, final ArrayList<String>
		// data_lat, final ArrayList<String> data_long, float min_long, float
		// min_lat, float max_long, float max_lat, float border, String
		// filename) {

		Gui.getInstance().attachDetailMap(
				drawDeatilTrack(image.getImageData(), latitude, longtitude,
						fx - 0.003f, fy - 0.002f, fx + 0.003f, fy + 0.002f,
						0.002f, "detail2.png"));

	}

	public Image updateposition(int number, float border) {
		if (backup == null) {
			backup = loadImage("map2.png");
		}
		if (bikeimage == null) {
			bikedata = new ImageData("bike_trans.png");
			bikeimage = new Image(Gui.getDisplay(), bikedata);
		}
		if (image != null) {
			image.dispose();
		}
		Display display = Gui.getDisplay();
		int iheight = backup.height;
		int iwidth = backup.width;
		image = new Image(display, backup);
		try {

			float difflat = maxlat - minlat;
			float difflong = maxlong - minlong;

			difflat += border; // rahmen einrechnen...
			difflong += border; // rahmen einrechnen...

			GC gc = new GC(image);
			gc.setLineWidth(4);
			Color color = display.getSystemColor(SWT.COLOR_RED);
			gc.setForeground(color);

			float fx = Float.parseFloat(longtitude.get(number));
			float fy = Float.parseFloat(latitude.get(number));
			fx -= minlong; // lokale long
			fx += 0.001f;
			fy = maxlat - fy; // lokale lat
			fy += 0.001f; // rahmen einrechnen
			float rx = fx / difflong;
			float ry = fy / difflat;

			int px = (int) ((float) iwidth * rx);
			int py = (int) ((float) iheight * ry);

			if (number != 0)
				gc.drawImage(bikeimage, px - 12, py - 12);
			gc.dispose();
		} catch (Exception e) {
			System.out
					.println("map file is corrupt - download probably failed due to an too large area");
		}
		return image;
	}

	public Image updateHeightPosition(int number) {
		if (bikeimage == null) {
			bikedata = new ImageData("bike_trans.png");
			bikeimage = new Image(Gui.getDisplay(), bikedata);
		}
		if (imageHeight != null) {
			imageHeight.dispose();
		}
		Display display = Gui.getDisplay();
		imageHeight = new Image(display, backupHeight);
		int width = Gui.getInstance().rawMap.getImage().getImageData().width + 350;

		GC gc = new GC(imageHeight);
		gc.setForeground(Gui.getDisplay().getSystemColor(SWT.COLOR_BLACK));
		float xmulti = ((float) width / (float) height.size());
		float ymulti = (float) 150 / (maxheight - minheight);

		gc.setLineWidth((int) Math.ceil(xmulti));

		int px = (int) (xmulti * number);
		int py = (int) (175 - ((height.get(number) * ymulti) - (float) minheight
				* (float) ymulti));
		if (number != 1) {
			gc.drawImage(bikeimage, px - 12, py - 24);
		}

		gc.dispose();
		PhysicsDialog.getInstance().attachHeightDiagram(imageHeight);

		return imageHeight;
	}
	public Image updateSpeedPosition(int number) {
		if (bikeimage == null) {
			bikedata = new ImageData("bike_trans.png");
			bikeimage = new Image(Gui.getDisplay(), bikedata);
		}
		if (imageSpeed != null) {
			imageSpeed.dispose();
		}
		Display display = Gui.getDisplay();
		imageSpeed = new Image(display, backupSpeed);
		int width = Gui.getInstance().rawMap.getImage().getImageData().width + 350;

		GC gc = new GC(imageSpeed);
		gc.setForeground(Gui.getDisplay().getSystemColor(SWT.COLOR_BLACK));
		float xmulti = ((float) width / (float) speed.size());
		float ymulti = (float) 150 / (maxspeed);

		gc.setLineWidth((int) Math.ceil(xmulti));

		int px = (int) (xmulti * number);
		int py = (int) (175 - ((speed.get(number) * ymulti) ));
		if (number != 1) {
			gc.drawImage(bikeimage, px - 12, py - 24);
		}

		gc.dispose();
		PhysicsDialog.getInstance().attachSpeedDiagram(imageSpeed);

		return imageSpeed;
	}

	public Image updateAccPosition(int number) {
		if (bikeimage == null) {
			bikedata = new ImageData("bike_trans.png");
			bikeimage = new Image(Gui.getDisplay(), bikedata);
		}
		if (imageAcc != null) {
			imageAcc.dispose();
		}
		Display display = Gui.getDisplay();
		imageAcc = new Image(display, backupAcc);
		int width = Gui.getInstance().rawMap.getImage().getImageData().width + 350;

		GC gc = new GC(imageAcc);
		gc.setForeground(Gui.getDisplay().getSystemColor(SWT.COLOR_BLACK));
		float xmulti = ((float) width / (float) acc.size());
		float ymulti = (float) 150 / (maxacc - minacc);

		gc.setLineWidth((int) Math.ceil(xmulti));

		int px = (int) (xmulti * number);
		int py = (int) (175 - ((acc.get(number) * ymulti) - (float) minacc
				* (float) ymulti));
		if (number != 1) {
			gc.drawImage(bikeimage, px - 12, py - 24);
		}

		gc.dispose();
		PhysicsDialog.getInstance().attachAccDiagram(imageAcc);

		return imageAcc;
	}

	public Image updatePowerPosition(int number) {
		if (bikeimage == null) {
			bikedata = new ImageData("bike_trans.png");
			bikeimage = new Image(Gui.getDisplay(), bikedata);
		}
		if (imagePower != null) {
			imagePower.dispose();
		}
		Display display = Gui.getDisplay();
		imagePower = new Image(display, backupPower);
		int width = Gui.getInstance().rawMap.getImage().getImageData().width + 350;

		GC gc = new GC(imagePower);
		gc.setForeground(Gui.getDisplay().getSystemColor(SWT.COLOR_BLACK));
		float xmulti = ((float) width / (float) power.size());
		float ymulti = (float) 150 / (maxpower);

		gc.setLineWidth((int) Math.ceil(xmulti));

		int px = (int) (xmulti * number);
		int py = (int) (175 - ((power.get(number) * ymulti) ));
		if (number != 1) {
			gc.drawImage(bikeimage, px - 12, py - 24);
		}

		gc.dispose();
		PhysicsDialog.getInstance().attachPowerDiagram(imagePower);

		return imagePower;
	}
	
	public void downloadmapfile(String filename, int scale, float min_long,
			float min_lat, float max_long, float max_lat, float border) {
		System.out.println("downloading file...");
		// final static int size=1024;
		String fileAddress;
		fileAddress = ("http://tile.openstreetmap.org/cgi-bin/export?bbox="
				+ (min_long - border) + "," // minlong
				+ (min_lat - border) + "," // minlat
				+ (max_long + border) + "," // maxlong
				+ (max_lat + border) // maxlat
				+ "&scale=" + scale + "&format=png");
		String localFileName = filename;
		String destinationDir = ".";

		OutputStream os = null;
		URLConnection URLConn = null;
		// URLConnection class represents a communication link between the
		// application and a URL.

		InputStream is = null;
		try {
			URL fileUrl;
			byte[] buf;
			int ByteRead, ByteWritten = 0;
			fileUrl = new URL(fileAddress);
			os = new BufferedOutputStream(new FileOutputStream(destinationDir
					+ '/' + localFileName));

			// The URLConnection object is created by invoking the
			// openConnection method on a URL.

			URLConn = fileUrl.openConnection();
			is = URLConn.getInputStream();
			buf = new byte[1024];
			while ((ByteRead = is.read(buf)) != -1) {
				os.write(buf, 0, ByteRead);
				ByteWritten += ByteRead;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				is.close();
				os.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public void heightDiagram() {
		int width = Gui.getInstance().rawMap.getImage().getImageData().width + 350;
		imageHeight = new Image(Gui.getDisplay(), width, 175);

		GC gc = new GC(imageHeight);
		gc.setForeground(Gui.getDisplay().getSystemColor(SWT.COLOR_BLACK));
		float xmulti = ((float) width / (float) height.size());
		float ymulti = (float) 150 / (maxheight - minheight);

		System.out.println(ymulti);
		gc.setLineWidth((int) Math.ceil(xmulti));
		for (int i = 1; i < height.size(); i++) {
			int px = (int) (xmulti * i);
			int py = (int) (175 - ((height.get(i) - (float) minheight) * (float) ymulti));
			if (i != 1) {
				gc.drawLine(px, 175, px, py);
			}
		}
		gc.drawText(Integer.toString(maxheight) + "m ü. NN", 0, 10);
		gc.drawLine(0,25,20,25);
		gc.drawText(Integer.toString(minheight) + "m ü. NN", 0, 150);

		gc.dispose();
		backupHeight = imageHeight.getImageData();
		PhysicsDialog.getInstance().attachHeightDiagram(imageHeight);
	}

	public void speedDiagram() {
		int width = Gui.getInstance().rawMap.getImage().getImageData().width + 350;
		imageSpeed = new Image(Gui.getDisplay(), width, 175);

		GC gc = new GC(imageSpeed);
		gc.setForeground(Gui.getDisplay().getSystemColor(SWT.COLOR_BLACK));
		float xmulti = ((float) width / (float) speed.size());
		float ymulti = (float) 150 / (maxspeed);

		System.out.println(ymulti);
		gc.setLineWidth((int) Math.ceil(xmulti));
		for (int i = 1; i < speed.size(); i++) {
			int px = (int) (xmulti * i);
			int py = (int) (175 - ((speed.get(i)) * (float) ymulti));
			if (i != 1) {
				gc.drawLine(px, 175, px, py);
			}
		}
		gc.drawText(Float.toString(maxspeed) + " km/h", 0, 10);
		gc.drawLine(0,25,20,25);
		gc.drawText("0 km/h", 0, 150);

		gc.dispose();
		backupSpeed = imageSpeed.getImageData();
		PhysicsDialog.getInstance().attachSpeedDiagram(imageSpeed);
	}
	public void accDiagram() {
		int width = Gui.getInstance().rawMap.getImage().getImageData().width + 350;
		imageAcc = new Image(Gui.getDisplay(), width, 175);

		GC gc = new GC(imageAcc);
		gc.setForeground(Gui.getDisplay().getSystemColor(SWT.COLOR_BLACK));
		float xmulti = ((float) width / (float) acc.size());
		float ymulti = (float) 175 / (maxacc - minacc);

		System.out.println(ymulti);
		gc.setLineWidth((int) Math.ceil(xmulti));
		
		int pynull = (int) (175 - ((0 - (float) minacc) * (float) ymulti));
		gc.drawLine(0, pynull, width, pynull);
		for (int i = 1; i < acc.size(); i++) {
			int px = (int) (xmulti * i);
			int py = (int) (175 - ((acc.get(i) - (float) minacc) * (float) ymulti));
			if(py < pynull)
				gc.setForeground(Gui.getDisplay().getSystemColor(SWT.COLOR_GREEN));
			else
				gc.setForeground(Gui.getDisplay().getSystemColor(SWT.COLOR_RED));
			if (i != 1) {
				gc.drawLine(px, pynull, px, py);
			}
		}
		gc.setForeground(Gui.getDisplay().getSystemColor(SWT.COLOR_BLACK));
		
		gc.drawText(Float.toString(maxacc) + "m/s/s", 0, 10);
		//gc.drawLine(0,25,20,25);
		gc.drawText(Float.toString(minacc) + "m/s/s", 0, 150);

		gc.dispose();
		backupAcc = imageAcc.getImageData();
		PhysicsDialog.getInstance().attachAccDiagram(imageAcc);
	}
	public void powerDiagram() {
		int width = Gui.getInstance().rawMap.getImage().getImageData().width + 350;
		imagePower = new Image(Gui.getDisplay(), width, 175);

		GC gc = new GC(imagePower);
		gc.setForeground(Gui.getDisplay().getSystemColor(SWT.COLOR_BLACK));
		float xmulti = ((float) width / (float) power.size());
		float ymulti = (float) 150 / (maxpower);

		System.out.println(ymulti);
		gc.setLineWidth((int) Math.ceil(xmulti));
		for (int i = 1; i < power.size(); i++) {
			int px = (int) (xmulti * i);
			int py = (int) (175 - ((power.get(i)) * (float) ymulti));
			if (i != 1) {
				gc.drawLine(px, 175, px, py);
			}
		}
		gc.drawText(Float.toString(maxpower) + "W", 0, 10);
		gc.drawLine(0,25,20,25);
		gc.drawText("0W", 0, 150);

		gc.drawText("for mass=100kg", 100, 10);

		gc.dispose();
		backupPower = imagePower.getImageData();
		PhysicsDialog.getInstance().attachPowerDiagram(imagePower);
	}


	public static Map getInstance() {
		if (map == null) {
			map = new Map();
		}
		return map;
	}
}