import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.graphics.ImageLoader;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.*;

public class Gui {
	static Gui gui;

	static Display display;

	Shell shell;

	Label rawMap;

	Scale scale;

	Label detailMap;

/*	Label heightDiagram;

	Label speedDiagram;
	
	Label accDiagram;
	
	Label powerDiagram; */

	public int shellWidth;

	public int shellHeight;

	Image rawMapImage;

		Image heightMapImage;

	Image speedMapImage;
	
	Image accMapImage;
	
	Image powerMapImage; 

	Image globusImage;

	int scaleOld;
	

	public void setUpGui() {
		shell = new Shell(getDisplay());

		GridLayout layout = new GridLayout(14, true);
		layout.makeColumnsEqualWidth = false;
		//layout.horizontalSpacing = 10;
		layout.verticalSpacing = 4;
		shell.setLayout(layout);
		shell.setText("Geek my Bike - map view");
		
		PhysicsDialog.getInstance().initPhysicsDialog();

		GridData gridData = new GridData(SWT.BEGINNING, SWT.FILL, false, false,
				6, 2);

		rawMap = new Label(shell, SWT.BORDER);
		rawMap.setLayoutData(gridData);

		GridData scaleGridData = new GridData(SWT.CENTER, SWT.FILL, false,
				false, 2, 2);
		scale = new Scale(shell, SWT.VERTICAL);
		scale.setLayoutData(scaleGridData);
		scale.setMaximum(99);

		GridData detailMapGridData = new GridData(SWT.BEGINNING, SWT.FILL, true,
				false, 6, 2);
		detailMap = new Label(shell, SWT.BORDER);
		detailMap.setLayoutData(detailMapGridData);
		//detailMap.setSize(380, 380);

		/*		GridData diagramGridData = new GridData(SWT.FILL, SWT.FILL,
				true, true, 7, 2);
		
		heightDiagram = new Label(shell, SWT.BORDER);
		heightDiagram.setLayoutData(diagramGridData);
		heightDiagram.setSize(heightDiagram.getSize().x, 175);
		
		speedDiagram = new Label(shell, SWT.BORDER);
		speedDiagram.setLayoutData(diagramGridData);
		speedDiagram.setSize(speedDiagram.getSize().x, 175);
		
		powerDiagram = new Label(shell, SWT.BORDER);
		powerDiagram.setLayoutData(diagramGridData);
		powerDiagram.setSize(powerDiagram.getSize().x, 175);
		
		accDiagram = new Label(shell, SWT.BORDER);
		accDiagram.setLayoutData(diagramGridData);
		accDiagram.setSize(accDiagram.getSize().x, 175); */
		

		scale.addSelectionListener(new SelectionListener() {
			public void widgetDefaultSelected(SelectionEvent arg0) {
			}

			public void widgetSelected(SelectionEvent arg0) {
				if (rawMapImage != null) {
					rawMapImage.dispose();
				}
				rawMapImage = Map.getInstance().updateposition(
						scale.getSelection()
								* Map.getInstance().latitude.size() / 100,
						0.002f);
				attachRawMap(rawMapImage);

				if (heightMapImage != null) {
					heightMapImage.dispose();
				}
				heightMapImage = Map.getInstance().updateHeightPosition(
						scale.getSelection()
								* Map.getInstance().latitude.size() / 100 + 1);
				PhysicsDialog.getInstance().attachHeightDiagram(heightMapImage);
				
				if (speedMapImage != null) {
					speedMapImage.dispose();
				}
				speedMapImage = Map.getInstance().updateSpeedPosition(
						scale.getSelection()
								* Map.getInstance().latitude.size() / 100 + 1);
				PhysicsDialog.getInstance().attachSpeedDiagram(speedMapImage);
				
				if (accMapImage != null) {
					accMapImage.dispose();
				}
				accMapImage = Map.getInstance().updateAccPosition(
						scale.getSelection()
								* Map.getInstance().latitude.size() / 100 + 1);
				PhysicsDialog.getInstance().attachAccDiagram(accMapImage);
				
				if (powerMapImage != null) {
					powerMapImage.dispose();
				}
				powerMapImage = Map.getInstance().updatePowerPosition(
						scale.getSelection()
								* Map.getInstance().latitude.size() / 100 + 1);
				PhysicsDialog.getInstance().attachPowerDiagram(powerMapImage);

				System.out.println(scale.getSelection()
						* Map.getInstance().latitude.size() / 100);

				if (PhysicsDialog.getInstance().activated) {
					PhysicsDialog.getInstance().update(
							scale.getSelection()
									* Map.getInstance().latitude.size() / 100);
				}
			}

		});
		scale.addMouseListener(new MouseListener() {
			public void mouseDown(MouseEvent e) {
			}

			public void mouseUp(MouseEvent e) {
				System.out.println("Mouse Button up at:" + e.x + " " + e.y);
				if (scale.getSelection() != scaleOld) {
					attachDetailMap(detailLoadingScreen());
					detailMap.update();
					Map.getInstance().downloaddetailview(
							scale.getSelection()
									* Map.getInstance().latitude.size() / 100
									+ 1);
					scaleOld = scale.getSelection();
				}
			}

			public void mouseDoubleClick(MouseEvent e) {
			}
		});
	

	}

	public void showGui() {
		Map.getInstance().heightDiagram();
		Map.getInstance().speedDiagram();	
		Map.getInstance().accDiagram();
		Map.getInstance().powerDiagram();
		
		Map.getInstance().downloaddetailview(1);
		
		if (PhysicsDialog.getInstance().activated) {	
		   	PhysicsDialog.getInstance().show();
		}
		
		shell.open();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch())
				display.sleep();
		}
		display.dispose();
	}

	public void attachRawMap(Image image) {
		rawMap.setImage(image);
		rawMap.setSize(image.getImageData().width, image.getImageData().height);
		shellWidth = image.getImageData().width + 400;
	//	shellHeight = image.getImageData().height + 425;
		shellHeight = image.getImageData().height+200;
		shell.setSize(shellWidth, shellHeight);
		PhysicsDialog.getInstance().heightDiagram.setSize(image.getImageData().width + 350, 175);
		PhysicsDialog.getInstance().speedDiagram.setSize(image.getImageData().width + 350, 175);
		PhysicsDialog.getInstance().accDiagram.setSize(image.getImageData().width + 350, 175);
		PhysicsDialog.getInstance().powerDiagram.setSize(image.getImageData().width + 350, 175);
	}

	public void attachDetailMap(Image image) {
		detailMap.setImage(image);
		detailMap.setSize(image.getImageData().width, image.getImageData().height);
	}

/*	public void attachHeightDiagram(Image image) {
		heightDiagram.setImage(image);
	}

	public void attachSpeedDiagram(Image image) {
		speedDiagram.setImage(image);
	}

	public void attachAccDiagram(Image image) {
		accDiagram.setImage(image);
	}
	
	public void attachPowerDiagram(Image image) {
		powerDiagram.setImage(image);
	} */

	public Image detailLoadingScreen() {
		Image image = detailMap.getImage();

		GC gc = new GC(image);
		if (globusImage == null) {
			ImageData imageData = new ImageData("loading.png");
			globusImage = new Image(Gui.getDisplay(), imageData);
		}
		gc.drawImage(globusImage, image.getImageData().width / 2 - 40, image
				.getImageData().height / 2 - 32);
		gc.drawText("Loading...", image.getImageData().width / 2 - 40, image
				.getImageData().height / 2 + 40);
		gc.dispose();
		System.out.println("test");

		return image;
	}

	public static Display getDisplay() {
		if (display == null) {
			display = new Display();
		}
		return display;
	}

	public static Gui getInstance() {
		if (gui == null) {
			gui = new Gui();
		}
		return gui;
	}
}