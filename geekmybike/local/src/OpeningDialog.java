
import java.io.IOException;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.*;


public class OpeningDialog {
	
	public String path;
	final Shell dialog;
	
	final Button physicModeButton;	
	
	
	public OpeningDialog(){
		dialog = new Shell(Gui.getDisplay());
		
		GridLayout gridLayout = new GridLayout(12, false);
		gridLayout.makeColumnsEqualWidth = false;
		dialog.setLayout(gridLayout);
		
		
		GridData fileLabelGridData = new GridData(SWT.BEGINNING, SWT.BEGINNING, false, true, 1, 1);
		Label fileLabel = new Label(dialog, SWT.NONE);
		fileLabel.setLayoutData(fileLabelGridData);
		fileLabel.setText("File:");
		
		GridData fileTextGridData = new GridData(SWT.FILL, SWT.BEGINNING, true, false, 5, 1);
		final Text fileText = new Text(dialog, SWT.SINGLE);
		fileText.setLayoutData(fileTextGridData);
		
		GridData browseButtonGridData = new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 3, 1);
		Button browseButton = new Button(dialog, SWT.PUSH);
		browseButton.setLayoutData(browseButtonGridData);
		browseButton.setText("Browse");
		
		GridData analyseButtonGridData = new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 3, 1);
		Button analyseButton = new Button(dialog, SWT.PUSH);
		analyseButton.setLayoutData(analyseButtonGridData);
		analyseButton.setText("Analyse");
		
		GridData connectionPropertiesButtonGridData = new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 6, 1);
		Button connectionPropertiesButton = new Button(dialog, SWT.PUSH);
		connectionPropertiesButton.setLayoutData(connectionPropertiesButtonGridData);
		connectionPropertiesButton.setText("Connectionproperties");
		
		GridData physicModeButtonGridData = new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 6, 1);
		physicModeButton = new Button(dialog, SWT.CHECK);
		physicModeButton.setLayoutData(physicModeButtonGridData);
		physicModeButton.setText("Physic mode");
		physicModeButton.setCapture(true);
		
		browseButton.addSelectionListener(new SelectionListener() {
				public void widgetDefaultSelected(SelectionEvent arg0) {
				}

				public void widgetSelected(SelectionEvent arg0) {
					
					FileDialog fileDialog = new FileDialog(dialog, SWT.OPEN);
					path = fileDialog.open();
					fileText.setText(path);
					
				}
			}
		);
		
		analyseButton.addSelectionListener(new SelectionListener() {
			public void widgetDefaultSelected(SelectionEvent arg0) {
			}

			public void widgetSelected(SelectionEvent arg0) {
			
				PhysicsDialog.getInstance().activated = physicModeButton.getSelection();
				
				dialog.dispose();
				try {
					Map.getInstance().readData(path);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		);
		
		connectionPropertiesButton.addSelectionListener(new SelectionListener() {
			public void widgetDefaultSelected(SelectionEvent arg0) {
			}

			public void widgetSelected(SelectionEvent arg0) {
				
				connectionPropertiesDialog();
				
			}
		}
		);
		
		
		
		dialog.setSize(360, 120);
		dialog.setLocation(Gui.getDisplay().getBounds().width/2-dialog.getBounds().width/2,Gui.getDisplay().getBounds().height/2-dialog.getBounds().height/2);
		dialog.open();

		
		while (!dialog.isDisposed()) {
			if (!Gui.getDisplay().readAndDispatch())
				Gui.getDisplay().sleep();
		}
	}
	
	public void connectionPropertiesDialog(){
		final Shell connectionPropertiesDialog = new Shell(dialog);
		connectionPropertiesDialog.setText("Connection properties");
		
		GridLayout gridLayout = new GridLayout(6, false);
		gridLayout.makeColumnsEqualWidth = false;
		connectionPropertiesDialog.setLayout(gridLayout);
		
		
		GridData headlineLabelGridData = new GridData(SWT.CENTER, SWT.BEGINNING, true, false, 6, 1);
		Label headlineLabel = new Label(connectionPropertiesDialog, SWT.BORDER);
		headlineLabel.setLayoutData(headlineLabelGridData);
		headlineLabel.setText("Proxy settings");
		
		GridData serverLabelGridData = new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 2, 1);
		Label serverLabel = new Label(connectionPropertiesDialog, SWT.None);
		serverLabel.setLayoutData(serverLabelGridData);
		serverLabel.setText("Server:");
		
		GridData serverTextGridData = new GridData(SWT.FILL, SWT.BEGINNING, false, false, 4, 1);
		final Text serverText = new Text(connectionPropertiesDialog, SWT.BORDER);
		serverText.setLayoutData(serverTextGridData);
		
		GridData portLabelGridData = new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 2, 1);
		Label portLabel = new Label(connectionPropertiesDialog, SWT.None);
		portLabel.setLayoutData(portLabelGridData);
		portLabel.setText("Port:");
		
		GridData portTextGridData = new GridData(SWT.FILL, SWT.BEGINNING, false, false, 4, 1);
		final Text portText = new Text(connectionPropertiesDialog, SWT.BORDER);
		portText.setLayoutData(portTextGridData);
		
		
		GridData okButtonGridData = new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 3, 1);
		Button okButton = new Button(connectionPropertiesDialog, SWT.PUSH);
		okButton.setLayoutData(okButtonGridData);
		okButton.setText("Apply");
		
		GridData cancelButtonGridData = new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 3, 1);
		Button cancelButton = new Button(connectionPropertiesDialog, SWT.PUSH);
		cancelButton.setLayoutData(cancelButtonGridData);
		cancelButton.setText("Cancel");
		
		
		okButton.addSelectionListener(new SelectionListener() {
			public void widgetDefaultSelected(SelectionEvent arg0) {
			}

			public void widgetSelected(SelectionEvent arg0) {
				
				if((serverText.getText().isEmpty()) || (portText.getText().isEmpty())){
					MessageBox mb = new MessageBox(connectionPropertiesDialog, SWT.ICON_WARNING);
					mb.setText("No Arguments");
					mb.setMessage("Please fill in the right data");
					mb.open();
				}else{
					System.setProperty( "proxySet", "true" ); 
					System.setProperty( "proxyHost", serverText.getText() ); 
					System.setProperty( "proxyPort", portText.getText() );
				}
				
			}
		}
	);
		
		cancelButton.addSelectionListener(new SelectionListener() {
			public void widgetDefaultSelected(SelectionEvent arg0) {
			}

			public void widgetSelected(SelectionEvent arg0) {
				
				connectionPropertiesDialog.dispose();
				
			}
		}
	);
		
		
		
		connectionPropertiesDialog.setSize(250, 150);
		connectionPropertiesDialog.setLocation(Gui.getDisplay().getBounds().width/2-connectionPropertiesDialog.getBounds().width/2,Gui.getDisplay().getBounds().height/2-connectionPropertiesDialog.getBounds().height/2);
		connectionPropertiesDialog.open();
		while (!connectionPropertiesDialog.isDisposed()) {
			if (!Gui.getDisplay().readAndDispatch())
				Gui.getDisplay().sleep();
		}
		
	}
	
	
}
