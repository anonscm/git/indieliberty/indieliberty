#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>

#include "followline.h" // geht das??
#include "redzone.h"

// vars

int lastStep2[2];
int lastStep[2];

//## util.c

void setVars(int g, int s, int r, int c);

void rgb2hsv(int *r, int *g, int *b, int hsv[3]);
void map(char* pic, int mapmap[MAP_WIDTH][MAP_HEIGHT]);

void printline(int map[MAP_WIDTH][MAP_HEIGHT], int x);
void printall(int map[MAP_WIDTH][MAP_HEIGHT]);

void linielocation(int x, int map2[MAP_WIDTH][MAP_HEIGHT], int *a1, int *a2);

void drivetopixel(int x, int y);
void drivetopoint(float x, float y);

void setPrintall();

void victimfound(int color);