#include "util.h"


#include "../connectivity/communication.h"




/* ##################################################################
 * 
 * UTILITIES
 * 
 * ######################################################################## */




int minSpeed = 25; // wird zum auf der stelle drehen benutzt.

bool doprintall;

void setVars(int g, int s, int r, int c) {
	GREENCOUNT = g;
	SPEEDORG = s;
	RIGHTANGLE = r;
	CHECKPOINT = c;	
	
	oben = false;
	onramp = false;
}

void victimfound(int color) {
	setMotorLeft(FWD, 0);
	setMotorRight(FWD, 0);
	if(onramp) { // nicht rollen
		setMotorLeft(FWD, 15);
		setMotorRight(FWD, 15);	
	}
	if(color == SILVER)
		printf("SILVER :)\n");
	else if(color == GREEN)
		printf("GREEEEN :)\n");
	int i;
	for(i=0; i<3;i++)
		setLED(i, true);
	usleep(5000*1000);
	for(i=0; i<3;i++)
		setLED(i, false);
	if(onramp) { // vermeiden, das opfer doppelt zu finde..
		setMotorLeft(FWD, 255);
		setMotorRight(FWD, 255);
		usleep(3000 * 1000);
	}
}

/* Berechnung zu ungenau angenähert... 
 * ^		f(x) = ax + m 
 * |     /
 * |    /*P1    // P1 ist der pixel
 * |   /
 * |  /*P2		// P2: da wollen wir hin
 * | / 
 * |/					
 * |*------------------>			<- da sind wir...
 * a = (Py - m) / Px
 * 
 */
/*void drivetopixel(int x, int y) {
	
	// bild anpassen :-!
	int tmp = y;
	y = x;
	x = tmp - 30; // nullpunkt in die mitte packen :)

	//x = x - 4; 
	//y = y ; // anderer nullpunkt verschiebung bild <-> raeder 

	
	// wenn x null ist geradeaus fahren....
	if(x == 0) {
		drivetopoint(0, 1);	
	} else {
	
		int m = 37;// Abstand bildanfang - raeder = 5,5 cm = 37 px
	
		float a = (float)(y+m) /  (float)x;
	
		// unterschiede zwischen P2 und P1
		float diffx = sqrt( m*m / (a*a +1) ); 
		if(x < 0)
			diffx = -diffx; // da sqrt negativ sein kann... ;)
		
		float diffy =  abs(diffx * a);
	
		printf("diffy: %f; diffx: %f, a: %f\n", diffy, diffx, a); 
		drivetopoint(x - diffx, y - diffy);
	}
} */


/* #############
 * Alte Funktion, sorgt dafuer das man auf der Linie steht, sie
 * aber nicht unbedingt sieht... 
 * */
void drivetopixel(int x, int y) {
	x = x - 4;
	drivetopoint(y - 30, x); 
} 

// bild anfang 41px
// Abstand bildanfang - raeder = 5,5 cm = 37 px

//	float pixelprocm = 20 / 3; 
//	float RADABSTAND = 15,5cm; 
//  d = 15,5 * 20 /3 /2 = 52 // halber Radabstand

void drivetopoint(float x, float y) { // y muss positiv sein (0/0) ist aktuelle position
	printf(" (%f/%f) ", x, y);
	
	int maxSpeed = SPEED;
	int d = 52; //RADABSTAND / 2;	
	

	if(x == 0) { // geradeaus...
		setMotorLeft(FWD, maxSpeed);
		setMotorRight(FWD, maxSpeed);
	} else {
		float c = (x * x + y * y) / (2 * x); // radius des mittleren kreises..
		
		float r1 = (c + d);
		float r2 = (c - d);
	
		if(r2 == 0) { // rel wuerde DIV BY 0 sein...
			if(x > 0) {
				setMotorLeft(FWD, maxSpeed);
				setMotorRight(FWD, 0);
			} else {
				setMotorLeft(FWD, 0);
				setMotorRight(FWD, maxSpeed);
			}
		} else {
			float rel = r1 / r2;
			
			/* > 1 (Links schneller)
			 * = 1 (wird vorher ausgeschlossen / geradeaus..)
			 * 0 < 1 (Rechts schneller) 
			 * -1 < 0 (Links rückwärts / Rechts vorwärts)
			 * < -1 (Rechts rückwärts / Links vorwärts) */
			 
			if(rel > 1) {
				setMotorLeft(FWD, maxSpeed);
				setMotorRight(FWD, (int)(maxSpeed / rel) );	
			} else if(rel > 0) { 
				setMotorLeft(FWD, (int)(maxSpeed * rel) );
				setMotorRight(FWD, maxSpeed);
			} else if(rel > -1) {
				if( (int)(maxSpeed * rel) > -minSpeed )  // falls rueckwaerts aber zu langsam...
					setMotorLeft(REV, minSpeed);
				else 
					setMotorLeft(FWD, (int)(maxSpeed * rel) ); // Altes Programm... (if-else entfernen)
				setMotorRight(FWD, maxSpeed);
			} else {
				setMotorLeft(FWD, maxSpeed);
				if( (int)(maxSpeed / rel) > -minSpeed )  // falls rueckwaerts aber zu langsam...
					setMotorRight(REV, minSpeed);
				else 
					setMotorRight(FWD, (int)(maxSpeed / rel) ); //Altes Programm.. (if-else entfernen)
			}
		}
	}
	/* ##################################
	 * LAST STEP IST SCHEISSE PROGRAMMIERT :)
	 * ####################################### */
	 int i;
	 for(i = 0; i < 2; i++)
	 	lastStep2[i] = lastStep[i];
	 lastStep[0] = getLeftSpeed();
	 lastStep[1] = getRightSpeed();
}




void map(char* pic, int mapmap[MAP_WIDTH][MAP_HEIGHT]) {
		int x,y;
		int green = 0;
		int silver = 0;
		for (y = 0; y < MAP_HEIGHT; y++) {
			for (x = 0; x < MAP_WIDTH; x++) {

				int hsv[3];
				int pos = (y*4 * IMG_HEIGHT + x*4) * 3;

				//int r = pic[pos] - 200; //rotstich wegmachen
				//if(r < 0) {
				//	r = 0;
				//}
				int r = pic[pos];
				int g = pic[pos + 1];
				int b = pic[pos + 2];
				
				
				rgb2hsv(&r, &g, &b, hsv);
				
				
				if( (hsv[1] > 15)
						 && (hsv[0] > 80)
						 && (hsv[0] < 180) ){
					mapmap[x][y] = GREEN;
				}
				else {
					if(hsv[2] > 75)
						mapmap[x][y] = WHITE;
					else
						mapmap[x][y] = BLACK;
				}				
			}
		}
		lastVictim++;
		detectvictim(green, silver);
		
		if(doprintall) {
			printall(mapmap);
			doprintall = false;
		}
//		printf("Green: %i ", green);
}



void printline(int map[MAP_WIDTH][MAP_HEIGHT], int x) {
	int i = 0;
	for(i = 0; i < MAP_HEIGHT; i++) {
		if(map[x][i] == BLACK)
			printf("X");
		else if(map[x][i] == WHITE)
			printf("0");
		else if(map[x][i] == SILVER)
			printf("S");
		else
			printf("G");	
	}	
	printf("(%i) ", x);
}

void printall(int map[MAP_WIDTH][MAP_HEIGHT]) {
	printf("###############################################\n");	
	int i;
	for(i = 0; i < MAP_WIDTH; i++) {
		printline(map, i);	
		printf(" %i\n", i);	
	}
	printf("###############################################\n");	
}

void rgb2hsv(int *r, int *g, int *b, int hsv[3]) {
		int* min; // Min. value of RGB
		int* max; // Max. value of RGB
		int delMax; // Delta RGB value

		
		if (*r > *g) {	
			min = g;
			max = r;
		} else {
			min = r;
			max = g;
		}
		if (*b > *max)
			max = b;
		if (*b < *min)
			min = b;
		
		delMax = *max - *min;

		float H = 0;
		float S = 0;
		float V = ((float)*max * 100) / 255;
		if (delMax != 0) {
			S = 1 - ((float) *min / (float) *max);
			if (r == max)
				H = ((*g - *b) / (float) delMax) * 60;
			else if (g == max)
				H = (2 + (*b - *r) / (float) delMax) * 60;
			else if (b == max)
				H = (4 + (*r - *g) / (float) delMax) * 60;
		}
		if(H < 0)
			H += 360;
		
		hsv[0] = (int) (H);
		hsv[1] = (int) (S * 100);
		hsv[2] = (int) (V);
		
}
	
	
void linielocation(int x, int map2[MAP_WIDTH][MAP_HEIGHT], int *a1, int *a2) {
		int count = 0;
		int start = -1;
		int luecke = 0;
		int i = 0;
		for (i = 0; i < 60; i++) {
			
			if (map2[x][i] == BLACK) {
				if (luecke > 5) {
					if (count > 5) {
						break;
					} else {
						start = i;
						count = 0;
					}
				}
				if (count == 0)
					start = i;
				count++;
				luecke = 0;
			} else { // WEISS
				luecke++;
			}
		}
		if( (start != -1) && (count > 5) ){
			*a1 = start;
			*a2 = start+count; 
		}
}	

void setPrintall() {
	doprintall = true;	
}