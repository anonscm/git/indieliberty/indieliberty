#include "util.h"
#include "followline.h"


#include "../connectivity/communication.h"

int oben_hindernis = 0;

bool activate_victims;
bool activate_black;

bool isobstacle;

int lost_time = 0;

int linie_count;

int speed_ausrichten = 170;

/* #########################################
 * ey, voll krass nix ende erkennen wenn gegen die wand stossen tuen
 *#############################################*/

void redzone(int map[MAP_WIDTH][MAP_HEIGHT]) { // vor 04-21 6:24: US-Sensoren
 	if(activate_black) {
	 	printf("REDZONE...");
 	
 		int anfang = -1, ende = -1;
		int i;
 		for(i = 70; i > CHECKPOINT; i = i - 2) {
			linielocation(i, map, &anfang, &ende);
			if( (anfang == 0) || (ende == 60) ) {
				break;
			}
		}
		if(anfang == -1) {
			linie_count = 0;	
		} else {
			linie_count++;	
		}
		printf("    line: %i ,  anfang: %i\n",linie_count , anfang);
		if(linie_count > 7) {
			isobstacle = false;
			usleep(1000 * 1000);
			if(!isobstacle) {
				printf("ENDE!!!!\n");
				setRemoteOnly(true);
				setMotorLeft2(FWD, 0);
				setMotorRight2(FWD, 0);
				for(i=0; i < 4; i++)
					setLED(i, true);
				usleep(10000 * 1000);
				for(i=0; i < 4; i++)
					setLED(i, false);
				setRemoteOnly(false);
				linie_count = 0;
			}
		}
 	}
}


void redzone_detectvictim(int green, int silver) {
	int i;
	if(activate_victims) {
		if( (green > 100) && (lastVictim > 35) ) {
			printf("[victim].");
			isobstacle = false;
			usleep(100 * 1000);
			if(!isobstacle) {
				lastVictim = 0; 
				printf("REDZONE: green victim found.");
				setMotorLeft(FWD, 0);
				setMotorRight(FWD, 0);
				for(i=0; i < 4; i++)
					setLED(i, true);
				usleep(3000 * 1000);
				lost_time = lost_time + 3000;
				for(i=0; i < 4; i++)
					setLED(i, false);
				setMotorLeft(FWD, 255);
				setMotorRight(FWD, 255);
			}
		} else if( (silver > 45) && (lastVictim > 35) ) {
			printf("[victim].");
			isobstacle = false;
			usleep(100 * 1000);
			if(!isobstacle) {
				lastVictim = 0; 
				printf("REDZONE: silver victim found.");
				setMotorLeft(FWD, 0);
				setMotorRight(FWD, 0);
				for(i=0; i < 4; i++)
					setLED(i, true);
				usleep(3000 * 1000);
				lost_time = lost_time + 3000;
				for(i=0; i < 4; i++)
					setLED(i, false);
				setMotorLeft(FWD, 255);
				setMotorRight(FWD, 255);
			}
		}
	}
}


long timer1;
void redzone_obstacle() {
	isobstacle = true;
	activate_victims = false;	
	oben_hindernis++;
	printf("Hindernis Nr. %i \n", oben_hindernis);
	if(oben_hindernis == 1) {
		activate_black = false;
		activate_victims = false;
		setMotorLeft(REV, 255);
		setMotorRight(REV, 255);
		usleep(500 * 1000);
		setMotorLeft(FWD, 255);
		setMotorRight(REV, 255);
		usleep(900 * 1000);
		ausrichten();
		usleep(3000 * 1000);	
		setMotorLeft(FWD, 0);
		setMotorRight(FWD, 0);
		usleep(1000 * 1000);
		setMotorLeft(FWD, 255);
		setMotorRight(REV, 255);
		usleep(900 * 1000);
		ausrichten();
	} else {
		activate_black = false;
		activate_victims = false;
		setMotorLeft(REV, 255);
		setMotorRight(REV, 255);
		usleep(600 * 1000);
		setMotorLeft(REV, 255);
		setMotorRight(FWD, 255);
		usleep(1900 * 1000);
		ausrichten();
	}
	activate_victims = true;
 	if(oben_hindernis > 10)
 		activate_black = true;
}
//altes programm: 04-21,6:35pm
void redzone_obstacle_left() {
	isobstacle = true;
	//redzone_obstacle();
	setMotorLeft(REV, 0);
	setMotorRight(REV, 255);
	usleep(400 * 1000);
	setMotorLeft(FWD, 255);
	setMotorRight(FWD, 255);
}

void redzone_obstacle_right() {
	isobstacle = true;
	//redzone_obstacle();
	setMotorLeft(REV, 255);
	setMotorRight(REV, 0);
	usleep(400 * 1000);
	setMotorLeft(FWD, 255);
	setMotorRight(FWD, 255);
}

void setRedzone(bool b) {
	oben = b;	
}

bool isRedzone() {
	return oben;	
}

void ausrichten() {
	setMotorLeft(REV, speed_ausrichten);
	setMotorRight(REV, speed_ausrichten);
	usleep(800 * 1000);
	int i;
	for(i = 0; i < 4; i++) {
		setMotorLeft(REV, 70);
		setMotorRight(FWD, 20);
		usleep(200 * 1000);
		setMotorLeft(FWD, 20);
		setMotorRight(REV, 70);
		usleep(200 * 1000);	
	}
	setMotorLeft(REV, speed_ausrichten);
	setMotorRight(REV, speed_ausrichten);
	usleep(400 * 1000);
	
	setMotorLeft(REV, 0);
	setMotorRight(REV, 0);
	usleep(1000 * 1000);
	setMotorLeft(FWD, 255);
	setMotorRight(FWD, 255);
}