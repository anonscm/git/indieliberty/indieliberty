#include "followline.h"
#include "util.h"
#include "redzone.h"

#include "../connectivity/communication.h"
#include "../ifmcam/cam.h"

//#include "../Main.h"

#define CURVEADD 100
#define RADABSTAND 70
#define KAMERAABSTAND 80




int lueckecount = 0;

bool program_started = false;
bool strategy_running = true;
bool obstacleThreadRunning = false;

bool avoiding_obstacle = false;



bool firstimage = true;

bool ramp_found = false;
int rampe_zeit = 0;

int last_ramp;


int rechterwinkel = 0;

bool luecke;


int lastVictim = 0;

void processImage(char* buffer_data) {
//	setMotorLeft(FWD, 0);
//	setMotorRight(FWD, 0);
//	usleep(4000 * 1000);
	
	/*if( (program_started) && (strategy_running) ){
		int tmp_map[MAP_WIDTH][MAP_HEIGHT];
		map(buffer_data, tmp_map);
		if(oben) {
			redzone(tmp_map);		
		} else {
			if(!onramp) {
				linie(tmp_map);
			} else {
				setMotorLeft(FWD, 210);
				setMotorRight(FWD, 255);	
			} 
		}
		last_ramp++;
		rampe_zeit++;
	}*/
	
	int tmp_map[MAP_WIDTH][MAP_HEIGHT];
	map(buffer_data, tmp_map);
	printall(tmp_map);

// sonst verschachteln sich die funktionen, da sie sich immer gegenseitig aufrufen:
//	int t1;
//	pthread_t cam_get_image;
//	pthread_create(&cam_get_image, NULL, cam_receive, (void *)t1);

	setReadyForNextFrame();

}


void linie(int map[MAP_WIDTH][MAP_HEIGHT]) {
		if(firstimage) {
			printall(map);
			firstimage = false;
		}
	//	checkgap(map);
		int anfang = -1, ende = -1;
		
		int a1, a2;
		
		int check = CHECKPOINT;
	
		int i;
		for(i = 70; i > CHECKPOINT; i = i - 2) {
			linielocation(i, map, &anfang, &ende);
			check = i;
			if( (anfang == 0) || (ende == 60) ) {
				break;
			}
		}
			
		
		if( (anfang == -1) || (ende == -1) ) { // CHECKPOINT zeigt luecke...
			lueckecount++;
			luecke = true;
		//	printf("\n## Lücke ##")
			for(i = CHECKPOINT; i < 70; i = i+1) {
				linielocation(i, map, &anfang, &ende);
				
				if(anfang != -1) {
					break;
				} 
			} 
		}
		if(anfang != -1) { // keine luecke
			printline(map, check);
			lueckecount = 0;
			luecke = false;
			int c;	
			int kurvenstaerke = ende - anfang;
	
			
			c = (int) ((anfang+ende) / 2);  // 0 bis 60
	
					
			//int p = 60 /  (60 - kurvenstaerke) * c - 30 * kurvenstaerke / ( 60 - kurvenstaerke) ; 
			
			//int p = (30 * kurvenstaerke / (60 - kurvenstaerke) + c) * (60 - kurvenstaerke) / 60;
			
			//printf("C: %i, P: %i  ", c, p);			
			printf("Kurve: %i    ", kurvenstaerke);
			
			if( ((anfang < 3) || (ende > 57)) && (kurvenstaerke < 10) ) { // linie geht aussem bild raus und so
				drivetopixel(CHECKPOINT - 15, c); 
			} else if(kurvenstaerke > 30) { // rechter winkel
				if(anfang < 3) {
					drivetopixel(CHECKPOINT - 15, 3);
					rechterwinkel = 3;
				} else if(ende > 57) {
					drivetopixel(CHECKPOINT - 15, 57);
					rechterwinkel = 3;
				} else
					drivetopixel(CHECKPOINT, c); 				
			} else if(rechterwinkel != 0) { 
				rechterwinkel--;
				if(anfang < 30)
					drivetopixel(CHECKPOINT - 15, 3);
				else
					drivetopixel(CHECKPOINT - 15, 57);
			
			} else {
				drivetopixel(CHECKPOINT , c);		
			}		
				
		} else { // luecke
			printf("Lücke...\n");
		 	printf("[Left: %i, Right: %i]", lastStep2[0], lastStep2[1]);
			if( (lastStep2[0] > (20) ) && (lastStep2[1] > (20) ) ) {
				setMotorLeft(FWD, SPEED);
				setMotorRight(FWD, SPEED);	
			} else if(lastStep2[0] > lastStep2[1]) {
				setMotorLeft(FWD, SPEED);
				setMotorRight(REV, SPEED/2);	
			} else {
				setMotorLeft(REV, SPEED/2);
				setMotorRight(FWD, SPEED);	
			}
	//		setMotorLeft(FWD, SPEED);
	//		setMotorRight(FWD, SPEED);	
		}
}

void detectvictim(int green, int silver) {
	if(oben) {
		redzone_detectvictim(green, silver);
	} else {
		if ( ((green+silver) > GREENCOUNT) && (lastVictim > 18)) {
			//if(onramp) {  //&& ramp_found) {
			//	printf("ignoriere opfer.... \n");	
			//}  else {
				setMotorLeft(FWD, 255);
				setMotorRight(FWD, 255);
				usleep(100*1000);	
				if(!obstacleThreadRunning) {	
					if(green < GREENCOUNT)
						victimfound(SILVER);
					else
						victimfound(GREEN);
				}	
				setMotorLeft(REV, 255);
				setMotorRight(REV, 255);
				usleep(300*1000);
				setMotorLeft(FWD, 255);
				setMotorRight(FWD, 255);				
				lastVictim = 0;
			//}
		}
	}
}

void obstacle() {
	if(!program_started) {
//		setMotorLeft(FWD, 255);
//		setMotorRight(FWD, 255);
		usleep(2000 * 1000); // warten...
		setMotorLeft(FWD, 0 );
		setMotorRight( FWD, 0);
		program_started = true;
		SPEED = SPEEDORG;
	} else if(!obstacleThreadRunning) {
		obstacleThreadRunning = true;
		printf("BUMMM :)\n");
		if(oben) {		
		//	strategy_running = true;
			redzone_obstacle();
		} else {	
			strategy_running = false;
	
			setMotorLeft(FWD, 0);
			setMotorRight(FWD, 0);
			setAvrObstacle();
			
			usleep(2000 * 1000);
			setMotorLeft(FWD, 255);
			setMotorRight(FWD, 255);
			usleep(800 * 1000);
			
			lastStep[0] = 255;
			lastStep[1] = 255;
			lastStep2[0] = 255;
			lastStep2[1] = 255;
			
			
		/*	if(luecke) {
				setMotorLeft(REV, 255);
				setMotorRight(REV, 255);
				usleep(500 * 1000);
			} else { */
//				avoiding_obstacle = true;
		/*		setMotorLeft(FWD, 0 );
				setMotorRight(FWD, 0);
				usleep(100 * 1000);
				
				setMotorLeft(REV, 255);
				setMotorRight(REV, 255);
				usleep(850 * 1000);
				

				setMotorLeft(REV, 255);
				setMotorRight(FWD, 255);				
				usleep(800 * 1000);	
				
				setMotorLeft(FWD, 255);
				setMotorRight(FWD, 255);
				usleep(1500 * 1000);
				
				setMotorLeft(FWD, 255);
				setMotorRight(REV, 255);
				usleep(700 * 1000);
				
				setMotorLeft(FWD, 255);
				setMotorRight(FWD, 255);
				usleep(2500 * 1000); 

				setMotorLeft(FWD, 255);
				setMotorRight(REV, 255);
				usleep(750 * 1000);

				setMotorLeft(FWD, 255);
				setMotorRight(FWD, 255);
				usleep(1000 * 1000); */
				
				
		//	}	
		}
		obstacleThreadRunning = false;	
		strategy_running = true;
	}
}

void obstacle_links() {
	if(!obstacleThreadRunning) {
		obstacleThreadRunning = true;
		if(!oben)
			strategy_running = false;
		
		if(onramp) {
			setMotorLeft(REV, 100);
			setMotorRight(REV, 100);
			usleep(150 * 1000); 
			setMotorLeft(FWD, 255);	
			setMotorRight(FWD, 0);
			usleep(300 * 1000);
			setMotorLeft(FWD, 255);
			setMotorRight(FWD, 255);
		} else if (oben) {
			redzone_obstacle_left();
		} else {
			setMotorLeft(REV, 255);
			setMotorRight(REV, 255);
			usleep (1200 * 1000);	
			setMotorLeft(FWD, 255);
			setMotorRight(FWD, 255);		
		}
		strategy_running = true;
		obstacleThreadRunning = false;
	}
}

void obstacle_rechts() {
	if(!obstacleThreadRunning) {
		obstacleThreadRunning = true;
		if(!oben) 
			strategy_running = false;
		
		if(onramp) {
			setMotorLeft(REV, 100);
			setMotorRight(REV, 100);
			usleep(150 * 1000);
			setMotorLeft(FWD, 0);	
			setMotorRight(FWD, 255);
			usleep(300 * 1000);
			setMotorLeft(FWD, 255);
			setMotorRight(FWD, 255);
		} else if (oben) {
			redzone_obstacle_right();
		} else {
			setMotorLeft(REV, 255);
			setMotorRight(REV, 255);
			usleep (1200 * 1000);	
			setMotorLeft(FWD, 255);
			setMotorRight(FWD, 255);		
		}
		strategy_running = true;
		obstacleThreadRunning = false;
	}
}

void rampe() {
	if(!oben) {
		printf("RAMPE...\n");
		SPEED = 255;
		onramp = true;	
		ramp_found = false;
		rampe_zeit = 0;
	}
}

void nixrampe() {
	if(!oben) {
		printf("nix RAMPE :)\n");
		SPEED = SPEEDORG;
		onramp = false;
		last_ramp = 0;
		printf("rampe_zeit: %i\n", rampe_zeit);
		if(rampe_zeit > 40) {
			printf("OOOOOOOOOOOOOOOOOOOBBBBEEEEEEN =)=)=)=)\n");
			oben = true;	
		}
		rampe_zeit = 0;
	}
}

bool getObstacleThreadRunning() {
	return obstacleThreadRunning;
}