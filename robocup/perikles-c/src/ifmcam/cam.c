#include "cam.h"

#include "../strategy/followline.h"
#include "./color.h"

char* camip = "192.168.2.16";



//int ctrl_frame_size = 36; // ohne rgb color map
int ctrl_frame_size = 804; // mit rgb color map 

int max_chunk_size = 38420; 

int sockid;
struct sockaddr_in address;
int len;
	
	
int ctrlFrameNr = 0;

bool ready_to_recv_next = false;

void init_cam() {
	
	
	sockid = socket(AF_INET, SOCK_DGRAM, 0); // inet / UDP
//	sockid = socket(AF_INET, SOCK_STREAM, 0); // inet / tcp
	
	address.sin_family = AF_INET;
	address.sin_addr.s_addr = inet_addr(camip);
	address.sin_port = htons(3100); // UDP data port
	//address.sin_port = htons(113); // TCP Ident port
	len = sizeof(address);
	
//	result = bind(sockid, (struct sockaddr *)&address, len);
//	printf("Bind: %i\n", result);
	
	//result = connect(sockid, (struct sockaddr *)&address, len);
	//printf("Connect: %i\n", result);
		


	sendControlFrame();
	
	setReadyForNextFrame();
	while(true) {
		cam_receive();
		while(!ready_to_recv_next);
		ready_to_recv_next = false;
	}
}

void sendControlFrame() {
	int result;
	char frame[ctrl_frame_size];
	controlFrame(&frame);
	result = sendto(sockid, frame, ctrl_frame_size, 0, (struct sockaddr_in *)&address, len);
//	result = send(sockid, frame, 36, 0);
	printf("Sending Control-Frame: %i\n", result);
	ctrlFrameNr++;
}


void setReadyForNextFrame() {
	ready_to_recv_next = true;
}

void cam_receive() {
	printf("Receiving next image..\n");
	
/* RECEIVE DATA */

//  result = recvfrom(sockid, buffer, sizeof(buffer), 0, (struct sockaddr_in *)&address, len);
	
	unsigned char image[230400]; // 320*240*3
	unsigned int chunks_per_frame = 10;
	unsigned int frame_nr;
	unsigned int chunk_nr = 0;
	unsigned int checkChunkNr;
	unsigned int last_ctrl_frame_nr; // FEHLER -> siehe specs
	int offset = 0;
	
	bool validImage = false;
	
	while(!validImage) {
		
		validImage = true;
		checkChunkNr = 0;
		chunk_nr = 0;
		offset = 0;
		
		while(chunk_nr < (chunks_per_frame-1)) {
			unsigned char buffer[max_chunk_size];
			int size;
			
			size = recv(sockid, buffer, sizeof(buffer), 0);	
			printf("size: %i,  ", size);
		
		
			if(size < 100) { // letzter frame
				printf("Last Frame... ctrl frame!?\n");
				validImage = false;	
			}
		
				
			last_ctrl_frame_nr = buffer[1]/2; // FEHLER -> siehe specs
			frame_nr = buffer[2];
			chunks_per_frame = (unsigned int)(buffer[3]*256) + (unsigned int)(buffer[4]); 		
			chunk_nr = (unsigned int)(buffer[5]*256) + (unsigned int)(buffer[6]);
			if(chunk_nr != checkChunkNr) {
				printf("chunkNr wrong...");
			//	validImage = false;	
			//	chunk_nr = chunks_per_frame; // schleife beenden
			} else {
				checkChunkNr++;
			
				color3320toRGB(buffer, image, offset, size);
			
				offset = offset +  ((size-19)*3);
				printf("ChunkNr: %i, perFrame: %i\n", chunk_nr, chunks_per_frame);
				printf("Offset: %i, FrameNr: %i, LastCtrl: %i\n", offset, frame_nr, last_ctrl_frame_nr);
			}
		}
		if(!validImage) {
			sleep(1);
			sendControlFrame();	
		}
		
	}	
	//sleep(10);
	
	
	/*if(!isImage) {
		printf("No Image - Sending Control Frame... \n");
		sendControlFrame();	
	}*/
	
	
	//char smallimage[57600];
	//shrink_image(image, smallimage);
	
	
	printf("Sending image [Debug]\n");
	sendImage(image);

	//usleep(200 * 1000);

	printf("Processing image [strategy]\n");	
	processImage(image);
	
}

void controlFrame(char frame[ctrl_frame_size]) { // V3
	
	//bits 0..63
	frame[0] = 0x80; // protocol ident
	frame[1] = 0x00; // settings / no change
	frame[2] = 0x00; // interlace flag / heater flag	// ???
	frame[3] = 0x00; // rate (0) 						// funkt ioniert mit 0
	frame[4] = 0xF0; // Maximum data chunk size (16 bit) // 1024 bytes works
	frame[5] = 0x00; // Maximum data chunk size 		//  using 128KB = 0x=8000
	frame[6] = 0xFF; // brightness						// ...
	frame[7] = 0x00; // rotation (16bit)		 
	
	// bits 64 .. 127
	frame[8]  = 0x00; // rotation 
	frame[9]  = 0x00; // mirror vertical axis			// ...
	frame[10] = 0x00; // src reg x (16bit)				
	frame[11] = 0x00; // src reg x
	frame[12] = 0x00; // src reg y (16bit)
	frame[13] = 0x00; // src reg y
	frame[14] = 0x01; // source width = 320;
	frame[15] = 0x40;
	
	// bits 128 .. 191asd
	frame[16] = 0x00;	// source height = 240;
	frame[17] = 0xF0;
	frame[18] = 0x00; 	// destination x position of region
	frame[19] = 0x00;
	frame[20] = 0x00; 	// destination y position of region
	frame[21] = 0x00;
	frame[22] = 0x01; 	// destination width of region = 320?
	frame[23] = 0x40;
	
	// bits 192.. 255
	frame[24] = 0x00; // destination height of region = 240?
	frame[25] = 0xF0;
	frame[26] = 0x06 + (ctrlFrameNr*2); // incrementing 7bit frame number, bit7 reserved	 // umgedreht?
	frame[27] = 0x00; // reserved
	frame[28] = 0x00; // reserved
	frame[29] = 0x00; // reserved
	frame[30] = 0x00; // reserved
	frame[31] = 0x00; // reserved
	
	// bits 256 .. 287
	frame[32] = 0x00; // reserved;
	frame[33] = 0x00; // reserved;
	frame[34] = 0xaa; // Number of colors... 
	frame[35] = 0x00; // 0xaa00 == 8bit!?
	
	// RGB color map with 8bit x 3 x 256 bits
	int offset = 36;
	colorRGBto3320(frame, offset);
	
}


void shrink_image(unsigned char img[230400], unsigned char tmp[57600]) {
	
	//char tmp[160 * 120 *3];
	int x;
	int a,b,c;
	for(b = 0; b < 120; b++) {			// hoehe
		for(a = 0; a < 160; a++) {		// breite
			for(c = 0; c < 3; c++) {	// farbe
				x = img[ (a*2 + b*320*2)*3 + c];
				x = x + img[ (a*2+1 + b*320*2)*3 + c];
				x = x + img[ (a*2 + (b*2+1)*320)*3 + c];
				x = x + img[ (a*2+1 +(b*2+1)*320)*3 + c];
				x = x / 4;
				tmp[(b*160 + a)*3 + c] = x;
			}
		}
	} 
	return tmp;
}
