#include <stdlib.h>

void colorM1toRGB(unsigned char* in, unsigned char* out, int offset, int size);
void color3320toRGB(unsigned char* in, unsigned char* out, int offset, int size);
void colorFixedtoRGB(unsigned char* in, unsigned char* out, int offset, int size);
	
	
void colorRGBto3320(unsigned char* frame, int offset);
void colorRGBtoFixed(unsigned char* frame, int offset);
void colorRGBtoM1(unsigned char* frame, int offset);
