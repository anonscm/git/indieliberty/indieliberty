#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>


void init_cam();
void cam_receive();
void controlFrame(char* frame);
void setReadyForNextFrame();

void sendControlFrame();

void shrink_image(unsigned char img[230400], unsigned char tmp[57600]);