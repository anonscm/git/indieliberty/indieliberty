#include "color.h"



//	Modell 1	
void colorM1toRGB(unsigned char* in, unsigned char* out, int offset, int size) {
	int i;	
	for(i = 0; i < (size-19); i++) {  
		unsigned int index = (unsigned int)in[19+i];
	 	if(index < 216) {				
			out[offset+i*3 + 0] = (index/36) *51;
			out[offset+i*3 + 1] = ((index/6)%6) * 51;//((index%36) / 6) * 51;
			out[offset+i*3 + 2] = (index%6) * 51;	
				
		} else {
			out[offset+i*3 + 0] = 255;
			out[offset+i*3 + 1] = 255;
			out[offset+i*3 + 2] = 255;
		}
	}
}

//	3320	
void color3320toRGB(unsigned char* in, unsigned char* out, int offset, int size) {
	int i;	
	for(i = 0; i < (size-19); i++) {  
		unsigned int index = (unsigned int)in[19+i];
		out[offset+i*3 + 0] = (index/32) * 36;
		out[offset+i*3 + 1] = ((index%32) / 4) * 36;//((index%36) / 6) * 51;
		out[offset+i*3 + 2] = (index%4) * 85;	
	}				
}  

// ein paar feste farben			
void colorFixedtoRGB(unsigned char* in, unsigned char* out, int offset, int size) {
	int i;	
	for(i = 0; i < (size-19); i++) {  
 		if(in[19+i] == 0) {
			out[offset+i*3 + 0] = 255;
			out[offset+i*3 + 1] = 255;
			out[offset+i*3 + 2] = 255;	
		} else if(in[19+i] == 1) {
			out[offset+i*3 + 0] = 255;
			out[offset+i*3 + 1] = 0;
			out[offset+i*3 + 2] = 0;	
		} else if(in[19+i] == 2) {
			out[offset+i*3 + 0] = 0;
			out[offset+i*3 + 1] = 255;
			out[offset+i*3 + 2] = 0;	
		} else if(in[19+i] == 3) {
			out[offset+i*3 + 0] = 0;
			out[offset+i*3 + 1] = 0;
			out[offset+i*3 + 2] = 255;	
		} else {
			out[offset+i*3 + 0] = 0;
			out[offset+i*3 + 1] = 0;
			out[offset+i*3 + 2] = 0;
		} 		
	}
}

void colorRGBtoFixed(unsigned char* frame, int offset) {

//EIN paar feste farben		
	// 0
	frame[36] = 128;
	frame[37] = 128;
	frame[38] = 128;
	
	// 1
	frame[39] = 128;
	frame[40] = 0;
	frame[41] = 0;
	
	// 2
	frame[42] = 0;
	frame[43] = 128;
	frame[44] = 0;
	
	// 3
	frame[45] = 0;
	frame[46] = 0;
	frame[47] = 128;
	
	offset += 4*3;
	
	//auffuellen
	int i;
	for(i = offset; i < offset+252; i++) {
		frame[offset + i + 0] = 0;
		frame[offset + i + 1] = 0;
		frame[offset + i + 2] = 0;	
	}
}

void colorRGBtoM1(unsigned char* frame, int offset) {
	//216 farben..: [Modell 1]
	int i;
	int j;
	int k;
	
	for(i = 0; i < 6; i++) {
		for(j = 0; j < 6; j++) {
			for(k = 0; k < 6; k++) {
				frame[offset + 0] = i*51;
				frame[offset + 1] = j*51;
				frame[offset + 2] = k*51;
				offset = offset + 3;	
			}
		}
	} 
	//auffuellen
	for(i = offset; i < offset+40; i++) {
		frame[offset + i + 0] = 255;
		frame[offset + i + 1] = 255;
		frame[offset + i + 2] = 255;	
	} 
}
void colorRGBto3320(unsigned char* frame, int offset) {
	// Modell 2:  - 3320
	int i;
	int j;
	int k;
	
	for(i = 0; i < 8; i++) {
		for(j = 0; j < 8; j++) {
			for(k = 0; k < 4; k++) {
				frame[offset + 0] = i*36;
				frame[offset + 1] = j*36;
				frame[offset + 2] = k*85;
				offset = offset + 3;	
			}
		}
	} 
}