#include <termios.h>
#include <stdio.h>
#include <stdbool.h>
#include <math.h>
#include <stdlib.h>

#define FWD true
#define REV false

#define LEFT true
#define RIGHT false

void communication();
void setup_term();
//void writetoavr();
void readsensordata();
void setMotorLeft(bool Dir, int Speed);
void setMotorRight(bool Dir, int Speed);
void setMotorLeft2(bool Dir, int Speed);
void setMotorRight2(bool Dir, int Speed);
void setLED(int Nr, bool state);
float getAvrSpannung();
void setCommSpeed(int time);

void sendMotorCommand(bool Motor, bool Dir, int Speed);
void sendLEDCommand(int Nr, bool state);
void sendCommands();
void avrSendThread(); 

bool getLED(int Nr);

void checkCommunication();

int getAnalog(int Nr);
bool getDigitalB(int Nr);
bool getDigitalC(int Nr);
bool getDigitalD(int Nr);

int getRightSpeed();
int getLeftSpeed();


void setRemoteOnly(bool b);
bool remoteOnly();

void setAvrObstacle();