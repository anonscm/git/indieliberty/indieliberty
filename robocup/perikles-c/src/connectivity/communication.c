#include "communication.h"
#include "../strategy/followline.h"

#include "pthread.h"

FILE *device;

bool inverseLeft = false; 
bool inverseRight = true;
bool inverseMotors = false; // wird VOR inverse right/left getestet - vertauscht diese also nicht.

bool lastTiltState = false;

int commSpeed = 0;

int analog_values[8];
bool portB[8];
bool portC[8];
bool portD[8];

int US1;
int US2;

float avr_spannung = 0;

int touchsensors = 0;

int LeftSpeed = 0;
int RightSpeed = 0;
bool LeftDir;
bool RightDir;
bool LED[4];

bool connected = false;

bool connection_active;

bool mode_remoteonly = false;

bool pause_sending = false;

//bool avr_obstacle = false;

void setCommSpeed(int time) {
	commSpeed = time;
}

void communication() {
	setup_term();
	connected = true;
	
	while(true) {
		connection_active = true;
		readsensordata();
	}
}

void readsensordata() {
	//printf("readsensordata()\n");
	usleep(1* 1000); // sensordaten nicht zu oft updaten ;)
	
	if(fgetc(device) != 10) { // übertragung falsch...
		int c1=0;
		int c2=0;
		while(!( (c1 == 10) && (c2 == 10) )) {
			c1 = c2;
			while( (c2 = fgetc(device)) == EOF);
			//printf("%i ", c2); 
			//if(c2 == 10) 
			//	printf("\n");
			//usleep(500 * 1000); //vorgang beobahcten..	
		}
	}
	char data[8];
	int vcount = 0;
	int c;
	for(vcount = 0; vcount < 8; vcount = vcount+1) {
		
		while( (c = fgetc(device)) == EOF); // auf daten warten
		int i = 0;
		for(i = 0; i<8; i++) {
			data[i] = ' ';
		}
		int count = 0;
		while( c != 10) {   // LF
    			data[count] = c;
    			count = count + 1;
			while( (c = fgetc(device)) == EOF);
		}
		analog_values[vcount] = strtol(&data, NULL, 10);
				
	}
	int i = 0;

//	for(i = 0; i<8;i++){
//		printf("Analog[%i]: %i\r\n", i, analog_values[i]);
//	}

	int value;
	// PORTB
	for(value = 0; value < 8; value++) {
		while( (c = fgetc(device)) == EOF);
		portB[value] = (c==49);	// char 1
	}
	fgetc(device); //LF
	// PORTC
	for(value = 0; value < 8; value++) {
		while( (c = fgetc(device)) == EOF);
		portC[value] = (c==49); // char 1	
	}
	fgetc(device); //LF
	// PORTD
	for(value = 0; value < 8; value++) {
		while( (c = fgetc(device)) == EOF);
		portD[value] = (c==49); // char 1
	}
	fgetc(device); //LF
	
//	for(i = 0; i < 8; i++) {
//		if(portD[i])
//			printf("portD[%i]: true\n", i);
//		else
//			printf("portD[%i]: false\n", i);	
//	}
		
	//US Sensoren:
	// US1
	while( (c = fgetc(device)) == EOF); // auf daten warten
	for(i = 0; i<8; i++) {
		data[i] = ' ';
	}
	int count = 0;
	while( c != 10) {   // LF
    	data[count] = c;
    	count = count + 1;
		while( (c = fgetc(device)) == EOF);
	}
	US1 = strtol(&data, NULL, 10);
//	printf("US1: %i \n", US1);
	// US2
	for(i = 0; i<8; i++) {
		data[i] = ' ';
	}
	while( (c = fgetc(device)) == EOF); // auf daten warten
	count = 0;
	while( c != 10) {   // LF
    	data[count] = c;
    	count = count + 1;
		while( (c = fgetc(device)) == EOF);
	}
	US2 = strtol(&data, NULL, 10);
	//printf("US2: %s \n", data);
	
/* ########### Werte verarbeiten ############### */	
		
	//printf("neigung: %i\n", analog_values[4]);Analog
	// Neigungssensor
	if(analog_values[4] < 500) { //nixrampe
		if(lastTiltState == true) {
			lastTiltState = false; 	
			int t;
			pthread_t obstacleThread;
			pthread_create(&obstacleThread, NULL, nixrampe, (void *)t);	
		}	
	} else { // rampe
		if(lastTiltState == false) {
			lastTiltState = true; 	
			int t;
			pthread_t obstacleThread;
			pthread_create(&obstacleThread, NULL, rampe, (void *)t);	
			
		}	
	}
	// Touchsensoren
	if(!getObstacleThreadRunning()) {
		if(analog_values[5] < 500) { //(portD[2] || portD[3]) { // oben
			touchsensors++;
			if(touchsensors > 1) {
				printf("boom\n");
				int t;
				pthread_t obstacleThread;
				pthread_create(&obstacleThread, NULL, obstacle, (void *)t);	
			}
		} else if(analog_values[0] > 500) { // Links
			touchsensors++;
			if(touchsensors > 1) {
		  		printf("Korrektur Links...\n");
				int t;
				pthread_t obstacleThread;
				pthread_create(&obstacleThread, NULL, obstacle_links, (void *)t);
			}
		} else if(analog_values[2] > 500) { // Rechts
		    touchsensors++;
			if(touchsensors > 1) {
			  	printf("Korrektur Rechts...\n");
				int t;
				pthread_t obstacleThread;
				pthread_create(&obstacleThread, NULL, obstacle_rechts, (void *)t);	
			} 
		} else {
			touchsensors = 0;	
		}
	}
	
	avr_spannung =  5.313725488 * analog_values[6] * 5 / 1023; 
//	printf("%i\n\r", analog_values[6]);
//	printf("%fV\n\r", avr_spannung);

//	printf(data);
//	printf("\r\n");
}





void setup_term() {
	while(!(device = fopen("/dev/ttyUSB0", "r+"))) { //wait for device
		usleep(10);
	}
	printf("AVR: Connected!\n\r");
	struct termios settings;
	tcgetattr(fileno(device), &settings);
	settings.c_lflag &= ~ECHO;
	cfsetispeed(&settings, B38400);
	cfsetospeed(&settings, B38400);
	tcsetattr(fileno(device),TCSANOW, &settings);
}

void avrSendThread() {
	while(true) {
		int tcom;
		pthread_t avrcom;
		pthread_create(&avrcom, NULL, sendCommands, (void *)tcom);	
		pthread_join(avrcom, NULL);
		usleep(2 * 1000);
		printf("sendCommandThread terminated. - restarting..\n");
	}
}


void sendCommands() {
	while(!connected); // warten bis verbindung herstellt ist.
	while(connected) {
		if(!pause_sending) {
			if(!inverseMotors) {
				sendMotorCommand(LEFT, LeftDir, LeftSpeed);
				usleep(1000);
				sendMotorCommand(RIGHT, RightDir, RightSpeed);
			} else {
				sendMotorCommand(RIGHT, LeftDir, LeftSpeed);
				usleep(1000);
				sendMotorCommand(LEFT, RightDir, RightSpeed);
			} 
			
			int i;
			for(i = 0; i < 4; i++) {
				usleep(10);
				sendLEDCommand(i+2, LED[i]); // LEDs 0-3 sind 2-5 auf avr, da 0,1 für I2C benutzt werden und 6,7 für die Motoren 
			}
			usleep(1000);
		}
	}	
	printf("[sendCommandThread] Connection lost.\n");
}

void sendMotorCommand(bool Motor, bool Dir, int Speed) {
	char command[8];
	
	// anfang
	command[0] = 'a';
	
	//Welcher Motor?
	if(Motor == LEFT)
		command[1] = 'A';
	else
		command[1] = 'B';
	// Richtung
	if(Dir)
		command[2] = '1';
	else
		command[2] = '0';
	
	// Speed
	char tmp[3];
	sprintf(tmp,"%d",Speed);
	if(Speed >= 100) {
		command[3] = tmp[0];
		command[4] = tmp[1];
		command[5] = tmp[2];
	} else if(Speed >= 10) {
		command[3] = '0';
		command[4] = tmp[0];
		command[5] = tmp[1];
	} else {
		command[3] = '0';
		command[4] = '0';
		command[5] = tmp[0];
	}
	
	// Abschluss
	command[6] = 'q';
	
	// String vervollständigen
	command[7] = '\0';
	
	// Senden...
//	printf("Motorbefehl senden:  ");
	fprintf(device, &command);
	
	//ausgeben:
//	printf("%s \n", command);
}

// direkter Aufruf nur durch Remote
void setMotorLeft2(bool Dir, int Speed) {
	if(inverseLeft)
		Dir = !Dir;
	LeftSpeed = Speed;
	LeftDir = Dir;
}
// direkter Aufruf nur durch Remote
void setMotorRight2(bool Dir, int Speed) {
	if(inverseRight)
		Dir = !Dir;
	RightSpeed = Speed;
	RightDir = Dir;
}

// zum Aufruf durchs normale Programm
void setMotorLeft(bool Dir, int Speed) {
	if(Speed < 0) {
		Dir = !Dir;
		Speed = Speed * -1;	
	}
	printf("Left: %i, %i    ", Speed, Dir);
	if(!mode_remoteonly) {
		setMotorLeft2(Dir, Speed);
	}	

}

// zum Aufruf durchs normale Programm
void setMotorRight(bool Dir, int Speed) {
	if(Speed < 0) {
		Dir = !Dir;
		Speed = Speed * -1;	
	}	
	printf("Right: %i, %i\n", Speed, Dir);
	
	if(!mode_remoteonly) {
		setMotorRight2(Dir, Speed);
	}	
}



void setLED(int Nr, bool state) {
	LED[Nr] = state;
}

bool getLED(int Nr) {
	return LED[Nr];	
}

void sendLEDCommand(int Nr, bool state) {
	char command[6];
	
	command[0] = 'a'; // anfang
	command[1] = 'L'; // LED
	command[2] = Nr + 48; // int to char ; nummer
	if(state)
		command[3] = '0';
	else
		command[3] = '1';
	command[4] = 'q'; //ende..
	
	// String vervollständigen
	command[5] = '\0';
	
	fprintf(device, &command); // senden
//	printf("LED: %s \n", command);
}

void checkCommunication() {
	connection_active = true;
	while(!connected); // warten bis verbindung steht
	while(true) {
		connection_active = false;
		usleep(500 * 1000);
		if(!connection_active) {
			printf("Housten, wir haben ein Problem =(\n");	
			setup_term();
		}	
	}
}


float getAvrSpannung(){
	return avr_spannung;
}

int getUS(bool side) {
	if(side == LEFT) 
		return US2;
	return US1;
}

int getAnalog(int Nr) {
	return analog_values[Nr];	
}

bool getDigitalB(int Nr) {
	return portB[Nr];
}
bool getDigitalC(int Nr) {
	return portC[Nr];
}
bool getDigitalD(int Nr) {
	return portD[Nr];
}
bool remoteOnly() {
	return mode_remoteonly;	
}

void setRemoteOnly(bool b) {
	mode_remoteonly = b;	
}

int getLeftSpeed() {
	if (!LeftDir) {
		return (LeftSpeed * -1);	
	}	
	return LeftSpeed;
}

/*#######################################
 * #####################################
 * #################################
 * ############################## ACHTUNG */

int getRightSpeed() {
	if(RightDir) { //// GAAAAAAAAAAAANZ unsauber (weil motor inversed....)
		return (RightSpeed * -1);	
	}	
	return RightSpeed;
}

void setAvrObstacle() { // Zeit: 6100
	pause_sending = true;
	
	usleep(1000 * 1000);
	
	char command[8];
	
	command[0] = 'a'; // anfang
	command[1] = 'C'; // LED
	command[2] = 'H'; // int to char ; nummer
	
	command[3] = '6';
	command[4] = '0';
	command[5] = '0';
	
	command[6] = 'q';
	
	// String vervollständigen
	command[7] = '\0';
	
	fprintf(device, &command); // senden
	printf("AVR-OBSTACLE: %s \n", command);
	
	usleep(50 * 1000);
	
	//nochmal senden:
	fprintf(device, &command); // senden
	
	
	usleep(6100 * 1000);
	pause_sending = false;
}