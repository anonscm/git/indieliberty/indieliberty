#include "console.h"

#include "./communication.h"
#include "../strategy/followline.h"
#include "../strategy/redzone.h"
#include "../strategy/util.h"
#include "pthread.h"


FILE *stdin;

bool LED[4];

struct termios defaultsettings;

bool enabled;

void waitForCommands() {
	initConsole();
	char c;	
	while(enabled) {
		c = fgetc(stdin);
		int i;
	//	printf("[Console]: %c", c);
		switch(c) {
			//Sensoren:
			case 'u':
				printf("Links: %icm, Rechts: %icm\n",getUS(LEFT), getUS(RIGHT));
				break;
			case 'i':
				printf("Analog:\n");
				for(i=0;i<8;i++)
					printf("[%i] %i \n", i, getAnalog(i));
				break;
			case 'o':
				printf("Digital D:\n");
				for(i=0;i<8;i++) {
					printf("[%i] %i\n", i, getDigitalD(i));
				}
				break;
			// LEDs
			case 'z':
				setLED(0, !getLED(0));
				break;
			case 'x':
				setLED(1, !getLED(1));
				break;
			case 'c':
				setLED(2, !getLED(2));
				break;
			case 'v':
				setLED(3, !getLED(3));
				break;
			//Kamera
			case 'p':
				setPrintall();
				break;
			//Motoren
			case 'w':
				setMotorLeft2(FWD, 255);
				setMotorRight2(FWD, 255);
				break;
			case 's':
				setMotorLeft2(REV, 255);
				setMotorRight2(REV, 255);
				break;	
			case 'a':
				setMotorLeft2(REV, 255);
				setMotorRight2(FWD, 255);
				break;
			case 'd':
				setMotorLeft2(FWD, 255);
				setMotorRight2(REV, 255);
				break;
			case 'e':
				setMotorLeft2(FWD, 255);
				setMotorRight2(FWD, 0);
				break;
			case 'q':
				setMotorLeft2(FWD, 0);
				setMotorRight2(FWD, 255);
				break;
			case 'r': // Redzone programm Manuell starten / stoppen
				if(isRedzone()) {
					setRedzone(false);
					printf("Redzone deactivated.\n");
				} else {
					setRedzone(true);
					printf("Redzone activated.\n");
				}
				break;
			case 'g': // touchsensoren ausloesen:)
				printf("boom\n");
				int t;
				pthread_t obstacleThread;
				pthread_create(&obstacleThread, NULL, obstacle, (void *)t);	
				break;
			case ' ':
				setMotorLeft2(FWD, 0);
				setMotorRight2(FWD, 0);
				break;
			case 'n': // debug...
				startDebug();
				break;
			case 'm':
				setMotorLeft2(FWD, 0);
				setMotorRight2(FWD, 0);
				setRemoteOnly(!remoteOnly());
				if(remoteOnly())
					printf("Remote Only\n");
				else
					printf("Remote-Only disabled.\n");
				break;
			default:
				if(remoteOnly()) {
					setMotorLeft2(FWD, 0);
					setMotorRight2(FWD, 0);	
				}
				break;
		}
		usleep(500); // CPU-Auslastung unten halten...
		/*if(remoteOnly()) {
			setMotorLeft2(FWD, 0);
			setMotorRight2(FWD, 0);	
		}*/
	}
}

void initConsole() {
	stdin = fopen("/dev/tty", "r"); // Standart-Terminal öffnen	
	struct termios settings;
	tcgetattr(fileno(stdin), &settings);
	defaultsettings = settings;
//	settings.c_lflag &= ~ICANON;
//	settings.c_lflag &= ~ECHO;
	tcsetattr(fileno(stdin),TCSANOW, &settings);
	printf("Console-Remote enabled.\n");
	enabled = true;
}

void resetConsole() {
	printf("Console-Remote disabled.\n");
	enabled = false;
	tcsetattr(fileno(stdin), TCSANOW, &defaultsettings);

/*	struct termios settings;	
	tcgetattr(fileno(stdin), &settings);
//	defaultsettings = settings;
	settings.c_lflag &= ICANON;
	settings.c_lflag &= ECHO;
	tcsetattr(fileno(stdin),TCSANOW, &settings);
*/		
}