/*
 * connection.c
 *
 *  Created on: Nov 22, 2008
 *      Author: toti
 */

#include "../strategy/followline.h"
#include "../connectivity/connection.h"
#include "../utilities/remote.h"
#include "../utilities/systeminformation.h"
#include "../Main.h"
#include <string.h>
#include <stdbool.h>
#include <signal.h>



int socketIndexData = 0;
int socketIndexInformation = 0;
int sockfdData[99];
int sockfdInformation[99];
int socket_descData;
int socket_descInfo;
int count = 0;
int connectedData = 0;
int connectedInformation = 0;




void sighandlerData(int sig_num){
/*	if(connectedData == 1)
		printf("Connection reseted by peer [Data sigHandler]\n");*/
    connectedData = 0;
}
/*
void sendImage(){
	sighandler_t handlerData;
	handlerData = signal(SIGPIPE, sighandlerData);

	while(true){
		if(getGstreamerConnectionStatus() && (connectedData == 1)){
			char tmp[57601];
			getImage(tmp);
			tmp[57600] == ';';
			int len = strlen (tmp);
			int offsetData = 0;
			int numRead = 0;
			while((offsetData < len) && ((numRead=send(sockfdData[socketIndexData - 1], &tmp[offsetData], strlen(&tmp[offsetData]), 0)) >= 0)){
				offsetData += numRead;
			}
			if ((offsetData == -1) || (numRead < 1)) {
				printf("Connection reseted by peer [Data]\n");
				connectedData = 0;
			}
		}else
			sleep(1);
	}
}*/
int i = 0;

void sendImage(char* buf){
	sighandler_t handlerData;
	handlerData = signal(SIGPIPE, sighandlerData);
	printf("SENDING IMAGE =)[1]\n");
	if(connectedData == 1){
		printf("SENDING IMAGE =)[2]\n");
		//char tmp[230400];
		//strcpy(tmp, buf);
		int len = 230400;//= 57600; //// strlen(tmp);
		int offsetData = 0;
		int numRead = 0;
		while((offsetData < len) && ((numRead=send(sockfdData[socketIndexData - 1], &buf[offsetData], len, 0)) >= 0)){
			offsetData += numRead;
		}
		printf("offsetData: %i, len: %i, numRead: %i\n", offsetData, len , numRead);
	//	printf("%d\n", ++i);
		if ((offsetData == -1) || (numRead < 1)) {
			printf("Connection reseted by peer [Data]\n");
			connectedData = 0;
		}
	}

}

void sendImageThreaded(char* buf){
	int t;
	pthread_t sendImagesThread;
	pthread_create(&sendImagesThread, &buf, sendImage, (void *)t);
}

int getDataSocket(){
	return sockfdData[socketIndexData - 1];
}

int getInformationSocket(){
	return sockfdInformation[socketIndexInformation - 1];
}

void receiveInformation(){
	while(1 == 1){
		if(connectedInformation == 1){
			int nbytes;
			char buf[5000] = "";
			int status = recv(sockfdInformation[socketIndexInformation -1], buf, sizeof(buf), 0);
			if(status > 0){
				printf("%s\n", buf);
				parseCommand(buf);
			}else{
				sleep(1);
			}
		}
	}
}

void sighandlerInfo(int sig_num){
/*	if(connectedInformation == 1)
		printf("Connection reseted by peer [Information sigHandler]\n");*/
	connectedInformation = 0;
}

void sendInformation(int id, char* input){
	sighandler_t handlerInfo;
	handlerInfo = signal(SIGPIPE, sighandlerInfo);
	if(connectedInformation == 1){
		int len = 512;
		char prefix[4];
		char argument[len];
		sprintf(prefix,"<%d> ",id);
		sprintf(argument,"%s%s;", prefix, input);
		int temp = send(sockfdInformation[socketIndexInformation - 1], argument, len, 0);
		if (temp == -1) {
			printf("Connection reseted by peer [Information]\n");
			connectedInformation = 0;
		}
	}
}

void openInformationServer(){

	struct sockaddr_in addressInfo;
	int addrlen;

	printf("Start server [information]\n");
	if ((socket_descInfo=socket(AF_INET,SOCK_STREAM,0))==0){
		perror("Create socket");
	    exit(EXIT_FAILURE);
	}

	addressInfo.sin_family = AF_INET;
	addressInfo.sin_addr.s_addr = INADDR_ANY;
	addressInfo.sin_port = htons(667);
	//printf("Bind\n");
	if (bind(socket_descInfo,(struct sockaddr *)&addressInfo,sizeof(addressInfo))<0){
	    perror("bind");
	    exit(EXIT_FAILURE);
	}
	//printf("Listen\n");
	if (listen(socket_descInfo,3)<0){
	    perror("listen");
	    exit(EXIT_FAILURE);
	}


	while(1 == 1){
	  printf("Waiting for new connection [information]...\n");
	  addrlen=sizeof(addressInfo);


	  if ((sockfdInformation[++socketIndexInformation]=accept(socket_descInfo,(struct sockaddr *)&addressInfo,&addrlen))<0){
	    perror("accept [information]");
	    exit(EXIT_FAILURE);
	  }
	  printf("Current Socket %d [information]\n",sockfdInformation[socketIndexInformation]);
	  if(socketIndexInformation != 1){
		  close(sockfdInformation[socketIndexInformation-1]);
		 // connectedInformation = 0;
	  }

	  printf("Verbindung aufgebaut [information]\n");
	  connectedInformation = 1;
	}
	return 0;
}


void openDataServer(){

	struct sockaddr_in addressData;
	int addrlen;

	printf("Start Server [Data]\n");
	if ((socket_descData=socket(AF_INET,SOCK_STREAM,0))==0){
		perror("Create socket");
	    exit(EXIT_FAILURE);
	}

	addressData.sin_family = AF_INET;
	addressData.sin_addr.s_addr = INADDR_ANY;
	addressData.sin_port = htons(666);
	//printf("Bind\n");
	if (bind(socket_descData,(struct sockaddr *)&addressData,sizeof(addressData))<0){
	    perror("bind");
	    exit(EXIT_FAILURE);
	}
	//printf("Listen\n");
	if (listen(socket_descData,3)<0){
	    perror("listen");
	    exit(EXIT_FAILURE);
	}


	while(1 == 1){
	  printf("Waiting for new connection [Data]...\n");
	  addrlen=sizeof(addressData);


	  if ((sockfdData[++socketIndexData]=accept(socket_descData,(struct sockaddr *)&addressData,&addrlen))<0){
	    perror("accept");
	    exit(EXIT_FAILURE);
	  }
	  printf("Current Socket %d [Data]\n",sockfdData[socketIndexData]);
	  if(socketIndexData != 1){
		  close(sockfdData[socketIndexData-1]);
		//  connectedData = 0;
	  }

	  printf("Verbindung aufgebaut [Data]\n");
	  connectedData = 1;
	}
	return 0;
}

void parseCommand(char* command){
	/*
	 * <1> Systeminformations
	 * <2> Motorcommands
	 */
	int serviceId = strtol(&command[1], NULL, 10);
	char argument[strlen(command)-4];
	int i;
	for(i=4;i<strlen(command);i++){
		argument[i-4] = command[i];
	}

	switch(serviceId){
		case 1:
			handleRequest(strtol(&argument, NULL, 10));
			break;
		case 2:
			handleRemoteCommand(strtol(&argument, NULL, 10));
			break;

	}

}

void closeDataSocket() {	
	close(sockfdData[socketIndexData]);
}
