/*
 * connection.h
 *
 *  Created on: Nov 22, 2008
 *      Author: toti
 */

#ifndef CONNECTION_H_
#define CONNECTION_H_


#endif /* CONNECTION_H_ */
#include <stdlib.h>
#include <stdio.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <unistd.h>


//void sendImage();
void sendImage(char* buf);
void sendImageThreaded(char* buf);
void openDataServer();
void openInformationServer();
void receiveInformation();
int getSocket();
void parseCommand(char* command);
void sendInformation(int id, char* input);

void closeDataSocket();
