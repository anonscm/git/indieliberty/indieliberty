/*
 * Main.h
 *
 *  Created on: Jan 14, 2009
 *      Author: toti
 */

#ifndef MAIN_H_
#define MAIN_H_

#endif /* MAIN_H_ */


void startServerDataThread();

void startServerInformationThread();

void startSendImagesThread();

void startAvrcommThread();

void startCheckCommunicationThread();

void startThreads();

void startDebug();


