#include "../connectivity/connection.h"
#include "../connectivity/communication.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


void getN810BatteryStatus(){
	system("battery-status > batteryN810.txt");
	FILE* batteryN810;
	batteryN810=fopen("./batteryN810.txt", "r");
	char* str;
	while(!feof(batteryN810)) {
	    fgets(str, 200, batteryN810);
	}
	fclose(batteryN810);
	system("rm ./batteryN810.txt");
	char argument[256];
	sprintf(argument,"[BATTERY_N810] %s", str);
	sendInformation(1, argument);
}

void getN810Temp(){
	system("internal-temp > tempN810.txt");
	FILE* tempN810;
	tempN810=fopen("./tempN810.txt", "r");
	char* str;
	while(!feof(tempN810)) {
	    fgets(str, 200, tempN810);
	}
	fclose(tempN810);
	system("rm ./tempN810.txt");
	char argument[256];
	sprintf(argument,"[TEMP_N810] %s", str);
	sendInformation(1, argument);
}

void getAVRBatteryStatus(){
	char* argument;
	float tmp = getAvrSpannung();
	printf(argument,"%f", tmp);
	printf("%s\n", argument);
	sendInformation(1, argument);
}

int frames = 0;
time_t start_time;
time_t stop_time;

int getFrameRate(){
	  if(start_time == 0){
		  start_time = time( NULL );
	  }
	  frames++;
	  stop_time = time( NULL ) ;
	  int elapsed = stop_time - start_time ;
	  if(elapsed > 0){
		  //printf("%d\n", frames);
		  start_time = time( NULL );
		  char argument[256];
		  sprintf(argument,"[FRAMERATE] %d", frames);
		  sendInformation(1, argument);
		  frames = 0;
	  }

}

void handleRequest(int i){
	switch(i){
	case 1:
		getN810BatteryStatus();
		break;
	case 2:
		getAVRBatteryStatus();
	case 3:
		getN810Temp();
		break;
	}
}


