/*
 * systeminformation.h
 *
 *  Created on: Jan 3, 2009
 *      Author: toti
 */

#ifndef SYSTEMINFORMATION_H_
#define SYSTEMINFORMATION_H_


#endif /* SYSTEMINFORMATION_H_ */


void handleRequest(int i);
void getN810BatteryStatus();
void getN810Temp();
int getFrameRate();
