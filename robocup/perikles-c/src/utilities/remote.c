#include "../connectivity/communication.h"

void handleRemoteCommand(int i){
	switch(i){
		case 1:
			setMotorLeft(true, 255);
			setMotorRight(true, 255);
			break;
		case 2:
			setMotorLeft(false, 255);
			setMotorRight(false, 255);
			break;
		case 3:
			setMotorLeft(false, 255);
			setMotorRight(true, 255);
			break;
		case 4:
			setMotorLeft(true, 255);
			setMotorRight(false, 255);
			break;
		case 5:
			setMotorLeft(true, 0);
			setMotorRight(false, 0);
			break;
	}
}
