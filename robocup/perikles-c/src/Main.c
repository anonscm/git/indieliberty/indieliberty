#include "connectivity/connection.h"
#include "pthread.h"
#include "connectivity/communication.h"
#include "connectivity/console.h"
#include "utilities/remote.h"
#include "strategy/followline.h"
#include "strategy/util.h"
#include "Main.h"

#include <signal.h>

bool debug = true;

/**********/

void startServerDataThread(){
	int t;
	pthread_t serverDataThread;
	pthread_create(&serverDataThread, NULL, openDataServer, (void *)t);
}

void startServerInformationThread(){
	int t1;
	pthread_t serverInformationThread;
	pthread_create(&serverInformationThread, NULL, openInformationServer, (void *)t1);
}

void startReceiveInformationThread(){
	int t2;
	pthread_t receiveInformationThread;
	pthread_create(&receiveInformationThread, NULL, receiveInformation, (void *)t2);
}

void startAvrGetThread(){
	int t3;
	pthread_t avrget;
	pthread_create(&avrget, NULL, communication, (void *)t3);
}

void startAvrPushThread(){
	int tpush;
	pthread_t avrpush;
	pthread_create(&avrpush, NULL, avrSendThread, (void *)tpush);
}

void startSendImagesThread(){
	int t4;
	pthread_t sendImagesThread;
	pthread_create(&sendImagesThread, NULL, sendImage, (void *)t4);
}

void startConsoleThread(){
	int tconsole;
	pthread_t console;
	pthread_create(&console, NULL, waitForCommands, (void *)tconsole);
}


void startCheckCommunicationThread(){
	int t5;
	pthread_t checkcomm;
	pthread_create(&checkcomm, NULL, checkCommunication, (void *)t5);
}

void startThreads(){
	startConsoleThread();
	startAvrGetThread();
	startAvrPushThread();
	startCheckCommunicationThread();
}

void startDebug() {
		startServerDataThread();
	//	startServerInformationThread();
		//startSendImagesThread();
	//	startReceiveInformationThread();	
}

void sigint() { //Strg+C
	closeDataSocket();
	resetConsole();
	setRemoteOnly(true);
	setMotorLeft2(FWD, 0);
	setMotorRight2(FWD, 0);
	sleep(1);
	exit(0);	
}

int main(int argc, char **argv){
	
	
	(void) signal(SIGINT, sigint);
	
	//PRESETS
	int GREENCOUNT2 = 500;
	int CHECKPOINT2 = 55;
	int RIGHTANGLE2 = 20;
	int SPEEDORG2 = 70;
	
	printf("Arguments:\n");
	*argv++; // ./perikles
	
	while(argc--) {
    	char* tmp = *argv++;
    	printf("%s\n", tmp);
    	argc--;
    	switch(tmp[0]) {
    		case 's': 
    			tmp = *argv++;
    			SPEEDORG2 = atoi(tmp);
    			break;
    		case 'c':
    			tmp = *argv++;
    			CHECKPOINT2 = atoi(tmp);
     			break;
    		case 'g':
    			tmp = *argv++;
    			GREENCOUNT2 = atoi(tmp);
    			break;
    		case 'r':
    			tmp = *argv++;
    			RIGHTANGLE2 = atoi(tmp);
    			break;
    		case 't':
    			tmp = *argv++;
    			setCommSpeed(atoi(tmp));
    			break;
    		case 'd':
    	   		debug = true;
    	   		break;	
    	}
	}
	usleep(1000*1000);
	setLED(0, false);
	setLED(1, false);
	setLED(2, false);
	setLED(3, false);
	setVars(GREENCOUNT2, SPEEDORG2, RIGHTANGLE2, CHECKPOINT2);
	startThreads();

	if(debug)
	{
		startDebug();
	}
	/*setMotorLeft(FWD, SPEEDORG2);
	setMotorRight(FWD, SPEEDORG2);
	usleep(3000 * 1000);
	setMotorLeft( FWD, 255);
	setMotorRight(FWD, 255);
	usleep(3000 * 1000); 
	setMotorLeft(FWD, 999);
	setMotorRight(FWD, 999);
	usleep(3000 * 1000); 
	setMotorLeft(FWD, 99);
	setMotorRight(FWD, 99);
	usleep(3000 * 1000);
	*/
	//drivetopixel(30, 40);
	//drivetopixel(-30, 40);
  
  	//init_gstreamer();
	
	init_cam();
	
	
	while(1 == 1){
		sleep(1);
	}

	printf("End Reached\n");
	return;
}