package robocup.camera.color;

import robocup.tools.UnsignedTypes;

public class HSV {
	public static int maxSaturation;
	public static int maxValue;
	
	
	static public short[] rgb2hsv(short r, short g, short b) {

		short[] hsv = new short[3];

		short min; // Min. value of RGB
		short max; // Max. value of RGB
		short delMax; // Delta RGB value

		if (r > g) {
			min = g;
			max = r;
		} else {
			min = r;
			max = g;
		}
		if (b > max)
			max = b;
		if (b < min)
			min = b;

		delMax = (short) (max - min);

		float H = 0;
		float S = 0;
		float V = ((float) max * 100f) / 255f;

		if (delMax == 0) {
			H = 0;
			S = 0;
		} else {
			S = 1f - ((float) min / (float) max);
			if (r == max)
				H = ((g - b) / (float) delMax) * 60;
			else if (g == max)
				H = (2 + (b - r) / (float) delMax) * 60;
			else if (b == max)
				H = (4 + (r - g) / (float) delMax) * 60;
		}
		if (H < 0f)
			H += 360f;

		hsv[0] = (short) (H);
		hsv[1] = (short) (S * 100f);
		hsv[2] = (short) (V);

		return hsv;
	}

	byte[] getHSV(byte[] buf, int v) {
		byte[] out = new byte[160 * 120 * 3];
		for (int i = 0; i < 160; i++) {
			for (int j = 0; j < 120; j++) {
				int[] hsv = new int[3];
				int pos = (j * 160 + i) * 3;

				int r = UnsignedTypes.unsignedByteToInt(buf[pos]);
				int g = UnsignedTypes.unsignedByteToInt(buf[pos + 1]);
				int b = UnsignedTypes.unsignedByteToInt(buf[pos + 2]);

				rgb2hsv(r, g, b, hsv);
				int h;
				if (v == 0) {
					h = hsv[0] * 255 / 360;
				} else if (v == 1) {
					// TODO: wie kann maxSaturation == 0 sein??
					try {
						h = hsv[1] * 255 / maxSaturation; // wird normiert...
					} catch (ArithmeticException e) {
						h = 0;
					}
				} else {
					h = hsv[2] * 255 / maxValue; // normieren..
				}

				out[pos] = (byte) h; // intToUnsignedByte(h);
				out[pos + 1] = (byte) h; // intToUnsignedByte(h);
				out[pos + 2] = (byte) h; // intToUnsignedByte(h);
			}
		}
		return out;
	}

	byte[] highlightColors(byte[] buf) {
		byte[] out = new byte[160 * 120 * 3];
		for (int i = 0; i < 160; i++) {
			for (int j = 0; j < 120; j++) {
				int[] hsv = new int[3];
				int pos = (j * 160 + i) * 3;

				int r = UnsignedTypes.unsignedByteToInt(buf[pos]);
				int g = UnsignedTypes.unsignedByteToInt(buf[pos + 1]);
				int b = UnsignedTypes.unsignedByteToInt(buf[pos + 2]);

				rgb2hsv(r, g, b, hsv);

				if ((hsv[1] > 14) && (hsv[0] > 10) && (hsv[0] < 150)) {
					// H to RGB:
					int hi = hsv[0] / 60;
					float f = hsv[0] / 60f - (float) hi; 
					int p = 0;
					int q = (int) (255f * (1f - f));
					int t = (int) (255f * (f));

					switch (hi) {
					case 0:
						r = 255;
						g = t;
						b = p;
						break;
					case 1:
						r = q;
						g = 255;
						b = p;
						break;
					case 2:
						r = p;
						g = 255;
						b = t;
						break;
					case 3:
						r = p;
						g = q;
						b = 255;
						break;
					case 4:
						r = t;
						g = p;
						b = 255;
						break;
					case 5:
						r = 255;
						g = p;
						b = q;
						break;

					}

					out[pos] = (byte) r; // intToUnsignedByte(r);
					out[pos + 1] = (byte) g; // intToUnsignedByte(g);
					out[pos + 2] = (byte) b; // intToUnsignedByte(b);

				} else {
					if (hsv[2] > 50) {
						out[pos] = (byte) 255; // intToUnsignedByte(r);
						out[pos + 1] = (byte) 255; // intToUnsignedByte(g);
						out[pos + 2] = (byte) 255; // intToUnsignedByte(b);
					} else {
						out[pos] = (byte) 0; // intToUnsignedByte(r);
						out[pos + 1] = (byte) 0; // intToUnsignedByte(g);
						out[pos + 2] = (byte) 0; // intToUnsignedByte(b);
					}
				}
			}
		}

		return out;
	}

	byte[] getHcolored(byte[] buf) {
		byte[] out = new byte[160 * 120 * 3];
		for (int i = 0; i < 160; i++) {
			for (int j = 0; j < 120; j++) {
				int[] hsv = new int[3];
				int pos = (j * 160 + i) * 3;

				int r = UnsignedTypes.unsignedByteToInt(buf[pos]);
				int g = UnsignedTypes.unsignedByteToInt(buf[pos + 1]);
				int b = UnsignedTypes.unsignedByteToInt(buf[pos + 2]);

				rgb2hsv(r, g, b, hsv);
				// H to RGB:
				int hi = hsv[0] / 60;
				float f = hsv[0] / 60f - (float) hi; // ((float)hsv[0] /
				// 60f) - (float)
				// (hsv[0] / 60);
				int p = 0;
				int q = (int) (255f * (1f - f));
				int t = (int) (255f * (f));

				switch (hi) {
				case 0:
					r = 255;
					g = t;
					b = p;
					break;
				case 1:
					r = q;
					g = 255;
					b = p;
					break;
				case 2:
					r = p;
					g = 255;
					b = t;
					break;
				case 3:
					r = p;
					g = q;
					b = 255;
					break;
				case 4:
					r = t;
					g = p;
					b = 255;
					break;
				case 5:
					r = 255;
					g = p;
					b = q;
					break;

				}

				out[pos] = (byte) r; // intToUnsignedByte(r);
				out[pos + 1] = (byte) g; // intToUnsignedByte(g);
				out[pos + 2] = (byte) b; // intToUnsignedByte(b);

			}
		}

		return out;
	}
	
	public static void rgb2hsv(int r, int g, int b, int hsv[]) {

		int min; // Min. value of RGB
		int max; // Max. value of RGB
		int delMax; // Delta RGB value

		if (r > g) {
			min = g;
			max = r;
		} else {
			min = r;
			max = g;
		}
		if (b > max)
			max = b;
		if (b < min)
			min = b;

		delMax = max - min;

		float H = 0;
		float S = 0;
		float V = ((float) max * 100f) / 255f;

		if (delMax == 0) {
			H = 0;
			S = 0;
		} else {
			S = 1f - ((float) min / (float) max);
			if (r == max)
				H = ((g - b) / (float) delMax) * 60;
			else if (g == max)
				H = (2 + (b - r) / (float) delMax) * 60;
			else if (b == max)
				H = (4 + (r - g) / (float) delMax) * 60;
		}
		if (H < 0f)
			H += 360f;

		hsv[0] = (int) (H);
		hsv[1] = (int) (S * 100f);
		hsv[2] = (int) (V);
	}

	static int[] rgb2hsv(int[] rgb) {
		int[] hsv = new int[3];
		rgb2hsv(rgb[0], rgb[1], rgb[2], hsv);
		return hsv;
	}

	static void resetMaxSaturation() {
		maxSaturation = 0;
	}

}
