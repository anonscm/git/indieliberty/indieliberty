package robocup.camera.color;

import java.util.ArrayList;

import robocup.tools.UnsignedTypes;


public class ColorMap {
	
	public static boolean log = true;
	
	ArrayList<Color> colors = new ArrayList<Color>();
	
	public void addColor(Color c) {
		colors.add(c);
	}

	public Color getColor(int id) { 
		if(id < 0)
			return null;
		return colors.get(id);
	}
	
	public int numberOfColors() {
		return colors.size();
	}
	
	public void createByteMap(byte[] frame, int offset) {

		for(int i = 0; i < numberOfColors(); i++) {
			frame[offset + 0] = UnsignedTypes.intToUnsignedByte(colors.get(i).r);
			frame[offset + 1] = UnsignedTypes.intToUnsignedByte(colors.get(i).g);
			frame[offset + 2] = UnsignedTypes.intToUnsignedByte(colors.get(i).b);
			offset = offset + 3;
		}

	}
	
	public int getIndexWithColor(int c) {
		for(int i = 0; i < numberOfColors(); i++)
			if(colors.get(i).color == c)
				return i;
		
		return -1;
	}
	
	
	public static ColorMap create3320map() {
		ColorMap cMap = new ColorMap();
		
		for (int i = 0; i < 8; i++) {
			for (int j = 0; j < 8; j++) {
				for (int k = 0; k < 4; k++) {
					cMap.addColor(new Color(Color.NOCOLOR, i * 36, j * 36, k * 85));
				}
			}
		}
		cMap.automaticallyCalcColors();
		
		return cMap;
	}

	private void automaticallyCalcColors() {
		Approximation approx = new Approximation();
		
		for(int i = 0; i < numberOfColors(); i++) {
			colors.get(i).color = approx.getClosestColor(colors.get(i).r, colors.get(i).g, colors.get(i).b); 
		}
	}
	
	public static ColorMap createFixedMap(boolean fillup) {
		
		int green = 15;
		int bwGrenze = 128;

		
		ColorMap cMap = new ColorMap();
		
		/*	Eigene Gruenwerte
	 	cMap.addColor(new Color(Color.GREEN, 0, 255, 0));
		cMap.addColor(new Color(Color.GREEN, 0 , 0 , 255));
		cMap.addColor(new Color(Color.GREEN, 0, 128, 255));
		*/
		
		// RN Wissen gruen 
		cMap.addColor(new Color(Color.GREEN, 42, 255, 0));
		cMap.addColor(new Color(Color.GREEN, 45, 175, 20));
		cMap.addColor(new Color(Color.GREEN, 24, 143, 0));
		cMap.addColor(new Color(Color.GREEN, 134, 218, 118));
		cMap.addColor(new Color(Color.GREEN, 39, 81, 31));
	
		
		
		for(int i = 100; i < 200; i += 2) {
			cMap.addColor(new Color(Color.GREEN, i, i + green , i));
			
		}
		
		
		for(int i = 255; i > bwGrenze; i -= 2) {
			cMap.addColor(new Color(Color.WHITE, i, i, i));
		}
		
	
		for(int i = 0; i < bwGrenze; i += 2) {
			cMap.addColor(new Color(Color.BLACK, i, i, i));
		}
		
		cMap.addColor(Color.blue);
		cMap.addColor(Color.red);
		
		
		// gemessene Werte:
		cMap.addColor(new Color(Color.GREEN, 81, 109, 87));
		
		
		if(log) {
			System.out.println("Size of ColorMap: " + cMap.numberOfColors() );
		}
		
		// needed for IFMCam
		if(fillup) {
			cMap.fillUp(256);
		}
		
		
		return cMap;
	}


	private void fillUp(int size) {
		while(numberOfColors() < size) {
			addColor(new Color(Color.BLACK, 0, 0, 0));
		}
	}
}

