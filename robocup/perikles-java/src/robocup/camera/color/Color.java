package robocup.camera.color;

public class Color {

	
	public static final int NOCOLOR = -1;
	public static final int GREEN = 0;
	public static final int WHITE = 1;
	public static final int BLACK = 2;
	public static final int SILVER = 3;
	
	// Sonstige Farben
	public static final int RED = 100;
	public static final int BLUE = 101;
	
	
	public static Color red = new Color(RED, 255,0,0);
	public static Color blue = new Color(BLUE, 0,0, 255);
	public static Color green = new Color(GREEN, 0, 255, 0);
	
	public int color;
	public int r; 
	public int g;
	public int b;
	
	public Color(int c, int r, int g, int b) {
		this.color = c;
		this.r = r;
		this.g = g;
		this.b = b;
	}
	
	static public String getColorString(int c) { 
		if(c == Color.BLACK) {
			return "black";
		}
		if(c == Color.WHITE) {
			return "white";
		}
		if(c == Color.GREEN) {
			return "green";
		}
		if(c == Color.SILVER) {
			return "silver";
		}
		if(c == Color.RED) {
			return "red";
		}
		if(c == Color.BLUE) {
			return "blue";
		}
		return "UNDEFINED COLOR";
	}
}
