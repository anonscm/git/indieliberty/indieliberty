package robocup.camera.color;

public class Approximation {
	
	final int numberOfValues;
	
	
	ColorMap colors = new ColorMap();
	
	public Approximation() {	
		
		colors.addColor(new Color(Color.BLACK, 0, 0, 0));
		colors.addColor(new Color(Color.WHITE, 255,255,255));
		colors.addColor(new Color(Color.GREEN, 0, 255, 0));
		colors.addColor(new Color(Color.GREEN, 100, 255, 100));
		
		colors.addColor(new Color(Color.WHITE, 220, 220, 220));
		colors.addColor(new Color(Color.WHITE, 130, 130, 130));
		
		
		colors.addColor(Color.blue);
		colors.addColor(Color.red);
	
//		colors = ColorMap.createFixedMap(false);
		
		numberOfValues = colors.numberOfColors();
	}


	
	
	public int getClosestColor(int r, int g, int b) {
		
		int lowestID = -1;
		int lowestValue = 10000;
		
		int currentID;
		int currentValue;
		
		for(currentID = 0; currentID < numberOfValues; currentID++) {
		
			currentValue = Math.abs(r - colors.getColor(currentID).r) + Math.abs(g - colors.getColor(currentID).g) + Math.abs(b - colors.getColor(currentID).b);
			
			if(currentValue < lowestValue) { 
				lowestValue = currentValue;
				lowestID = currentID;
			}
			
		}
		
		return colors.getColor(lowestID).color;
	}
	
	public String getPixelInfo(int r, int g, int b) {
		return getPixelInfo(r, g, b, colors);
	}
	
	public String getPixelInfo(int r, int g, int b, ColorMap cMap) {
		String info;
		
		info = "R: " + r + " G: " + g + " B: " + b + "\n";
		
		int lowestID = -1;
		int lowestValue = 10000;
		
		int currentID;
		int currentValue;
		
		for(currentID = 0; currentID < numberOfValues; currentID++) {
		
			currentValue = Math.abs(r - cMap.getColor(currentID).r) + Math.abs(g - cMap.getColor(currentID).g) + Math.abs(b - cMap.getColor(currentID).b);
			
			info += Color.getColorString(cMap.getColor(currentID).color) + ": " + currentValue + "\n";
			
			if(currentValue < lowestValue) { 
				lowestValue = currentValue;
				lowestID = currentID;
			}
			
		}
		
		info += "Closest: " + Color.getColorString(colors.getColor(lowestID).color) + "; R:" + colors.getColor(lowestID).r + "; G:" + colors.getColor(lowestID).g + "; B:" + colors.getColor(lowestID).b + "\n";
		info += "---------------------------------------------------\n;";
		return info;
	}
	
	// TODO: this is a performance issue
	public static int getClosestColorInColorMap(ColorMap cMap, int r, int g, int b) {
		
		int lowestID = -1;
		int lowestValue = 10000;
		
		int currentID;
		int currentValue;
		
		for(currentID = 0; currentID < cMap.numberOfColors(); currentID++) {
		
			currentValue = Math.abs(r - cMap.getColor(currentID).r) + Math.abs(g - cMap.getColor(currentID).g) + Math.abs(b - cMap.getColor(currentID).b);
			
			if(currentValue < lowestValue) { 
				lowestValue = currentValue;
				lowestID = currentID;
			}
			
		}
		
		return cMap.getColor(lowestID).color;
	}
	
	

	
}
