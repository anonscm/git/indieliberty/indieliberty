package robocup.camera.interfaces;

public interface IVideoSource extends Runnable {
	
	public int getImageHeight();
	public int getImageWidth();
	
	public void close();
	
	public void setListener(IImageListener l);
		
}
