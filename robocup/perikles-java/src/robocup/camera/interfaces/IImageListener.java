package robocup.camera.interfaces;

import robocup.camera.AbstractRoboCupImage;

public interface IImageListener {
		
	public void newImage(AbstractRoboCupImage image);
	
	public void open();
	public void close();

	
}
