package robocup.camera;

import java.util.ArrayList;

import robocup.camera.color.Approximation;
import robocup.camera.color.Color;
import robocup.camera.color.ColorMap;
import robocup.camera.tools.UnsignedTypes;
import robocup.camera.tools.images.RGBImage;

public class RoboCupColorImage extends AbstractRoboCupImage{

	public static ColorMap defaultcMap;
	private final ColorMap cMap;

	private Integer[] colorImage = null;
	
	
	private void calculateRgbFromColorImage() {
		int imageSize = width * height;

		
		byte[] rgbImage = new byte[imageSize * 3];

		for (int i = 0; i < imageSize; i++) {
			// System.out.println("createImage " + i + " Index: " +
			// colorImage[i]);
			if ((colorImage[i] >= 0) && (colorImage[i] < cMap.numberOfColors())) {
				rgbImage[i * 3 + 0] = (UnsignedTypes.intToUnsignedByte(cMap
						.getColor(colorImage[i]).r));
				rgbImage[i * 3 + 1] = (UnsignedTypes.intToUnsignedByte(cMap
						.getColor(colorImage[i]).g));
				rgbImage[i * 3 + 2] = (UnsignedTypes.intToUnsignedByte(cMap
						.getColor(colorImage[i]).b));
			}

		}
		this.rgbImage = new RGBImage(rgbImage, width, height);

	}

	private void calculateColorImageFromRGB() {
		byte[] rgbImage = this.rgbImage.getBytes();
		
		Approximation approx = new Approximation();
		colorImage = new Integer[width * height];
		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; j++) {
				int pos = (j * width + i) * 3;

				colorImage[j * width + i] = 
					new Integer(
						cMap.getIndexWithColor(
								approx.getClosestColor(
										UnsignedTypes.unsignedByteToInt(rgbImage[pos]),
										UnsignedTypes.unsignedByteToInt(rgbImage[pos + 1]),
										UnsignedTypes.unsignedByteToInt(rgbImage[pos + 2])
								)
						)
					);

			}
		}
	}

	private void calculateBlackDotsFromColorImage() {
		if (colorImage == null) {
			calculateColorImageFromRGB();
		}
		blackDots = new ArrayList<Integer[]>();
		for (int i = 0; i < colorImage.length; i++) {
			try {
				if (cMap.getColor(colorImage[i]).color == Color.BLACK) {
					blackDots.add(new Integer[] { i % width, i / width });
				}
			} catch (NullPointerException e) {
				//falls im colorImage[i] eine negative zahl steht - TODO: warum ist die negativ?
			} catch (ArrayIndexOutOfBoundsException e2) {
				System.err.println("What the Fuck????");
				// TODO warum passiert das bitte????
			}
		}
	}
	
	private void calculateBlackDotsBorderFromColorImage() {
		if (colorImage == null) {
			calculateColorImageFromRGB();
		}
		blackDotsBorder = new ArrayList<Integer[]>();
		// oberer Rand:
		for (int i = 0; i < width; i++) {
			if (cMap.getColor(colorImage[i]).color == Color.BLACK) {
				blackDotsBorder.add(new Integer[] { i , 0 });
			}
		}
		// links und rechts:
		for (int i = 0; i < height; i++) {
			// links:
			if (cMap.getColor(colorImage[i*width]).color == Color.BLACK) {
				blackDotsBorder.add(new Integer[] { 0, i });
			}
			// rechts:
			if (cMap.getColor(colorImage[i*width + width - 1]).color == Color.BLACK) {
				blackDotsBorder.add(new Integer[] { width -1, i });
			}
		}
		
		
		// clean single dots
		final int THRESHOLD = 3; // hoechstens 3 pixel auseinander
		Integer[] thisone;
		Integer[] last = {-100, -100 };
		boolean hasPrev = true; // auf true damit "-1" nicht geloescht wird
		for(int i = 0; i < blackDotsBorder.size(); i++) {
			thisone = blackDotsBorder.get(i);
			
			int diff = THRESHOLD + 1; // falls nicht in der gleichen Spalte/ Zeile diff > Threshold
		
			// diff berechnen
			if(thisone[0] == last[0]) {
				diff = last[1] - thisone[1];
			} else if(thisone[1] == last[1]) {
				diff = last[0] - thisone[0];
			}
		
			// handeln:
			if(Math.abs(diff) > THRESHOLD) {
				// vorheriges checken:
				if(!hasPrev) {
					blackDotsBorder.remove(i-1);
				}
				hasPrev = false;
				
			} else {
				hasPrev = true;
			}
			
			last = thisone;
		}
		// letzten loeschen, falls er keinen vorgaenger hat:
		if(!hasPrev) {
			blackDotsBorder.remove(blackDotsBorder.size() -1);
		}
	}
	
	
	public void setPixelColor(int pos, int colorIndex) {
		
		colorImage[pos] = colorIndex;
		
		if ((colorImage[pos] >= 0) && (colorImage[pos] < cMap.numberOfColors())) {
			rgbImage.getBytes()[pos * 3 + 0] = (UnsignedTypes.intToUnsignedByte(cMap
					.getColor(colorImage[pos]).r));
			rgbImage.getBytes()[pos * 3 + 1] = (UnsignedTypes.intToUnsignedByte(cMap
					.getColor(colorImage[pos]).g));
			rgbImage.getBytes()[pos * 3 + 2] = (UnsignedTypes.intToUnsignedByte(cMap
					.getColor(colorImage[pos]).b));
		}
		blackDots = null;
		blackDotsBorder = null;
		grayScaleImage = null;
		bwImage = null;
	}

	public void changedColorImage() {
		blackDotsBorder = null;
		blackDots = null;
		rgbImage = null;
	}


	public Integer[] getColorImage() {
		if (colorImage == null) {
			calculateColorImageFromRGB();
		}
		return colorImage;
	}

	public ColorMap getcMap() {
		return cMap;
	}


	public RoboCupColorImage(int width, int height, ColorMap cMap, Integer[] colorImage) {
		super(width, height, null);
		this.cMap = cMap;
		this.colorImage = colorImage;
	}
	


	@Override
	protected void calculateBlackDots() {
		calculateBlackDotsFromColorImage();		
	}

	@Override
	protected void calculateBlackDotsBorder() {
		calculateBlackDotsBorderFromColorImage();
	}
	
	@Override
	protected void calculateRGB() {
		calculateRgbFromColorImage();		
	}

	
}
