package robocup.camera.processing;

import java.util.ArrayList;

import org.eclipse.swt.graphics.Image;

import robocup.camera.AbstractRoboCupImage;
import robocup.camera.color.Color;
import robocup.camera.processing.utils.CircleHelper;
import robocup.camera.processing.utils.DistanceHelper;
import robocup.camera.processing.utils.KreiseStrategie;
import robocup.camera.tools.ColorDifference;
import robocup.camera.tools.Histogramm;
import robocup.camera.tools.Thresholding;
import robocup.camera.tools.images.BWImage;
import robocup.camera.tools.images.GreyScale;
import robocup.camera.tools.images.RGBImage;
import robocup.config.RobotConfig;
import robocup.debug.ui.Gui;
import robocup.misc.RelocatedFunction;
import robocup.misc.interfaces.Function;

public class GUIUpdater extends Thread {
	private Image[] swtImage = new Image[7];


	private ProcessingTools tools = new ProcessingTools();
	private ColorDifference colorDifference = new ColorDifference();

	private long allTime = 0;
	private int processCount = 0;
	
	
	private CircleHelper[] circles;
	private int circleCount;

	private DistanceHelper dh;
	private DistanceHelper dhObstacles;
	
	private ImageProcessing processing;
	
	public GUIUpdater(ImageProcessing ip) {
		processing = ip;
	}
	

	private void distanceHelper(int width, int height) {
		if (dh == null) {
			dh = new DistanceHelper(width, height);
			dh.calculateAll();
		}
		if (dhObstacles == null) {
			dhObstacles = new DistanceHelper(width, height,
					RobotConfig.obstacleDetectionHeight);
			dhObstacles.calculateAll();
		}

	}
	private void circleHelper(int corrWidth, int corrHeight) {
		if (circles == null) {
			circles = KreiseStrategie.calcCircles(corrWidth, corrHeight);
			circleCount = circles.length;
		}
	}


	public void updateGUI(AbstractRoboCupImage image) {

		long start = System.currentTimeMillis();
		long end, time;

		try {
			int w = image.getWidth();
			int h = image.getHeight();

			System.out.println("------------------------------------------");
			System.out.println("Processing Image! (" + w + "x" + h + ")");

			// Fadenkreuz
//			image.getRGBImage().addVerticalLine(w / 2, 2);
//			image.getRGBImage().addHorizontalLine(h / 2, 2);

			swtImage[0] = tools.showOriginalImage(image);

			distanceHelper(w, h);

			
			
			 RGBImage correctGreyAsRGB = dh.correctImage(image.getRGBImage()).getAsRGB();
			 
			 int distanceOnImage = correctGreyAsRGB.getHeight() + dh.getYOffset() - processing.lastDistance;
			 
			 if( (distanceOnImage >= 0) && (distanceOnImage < correctGreyAsRGB.getHeight())) {
				 correctGreyAsRGB.addHorizontalLine((distanceOnImage), 0);
			 }
			 
			 swtImage[1] = correctGreyAsRGB.asSWTImage();
			// maxRGB(image, w, h);

			/* ##### Hindernis erkennung ######## */

//			redDiff(image.getRGBImage());

			/* ##### Hindernis erkennung ende ######## */

			GreyScale greyScale = tools.greyScale(image, swtImage[1]);

			
			

			long start2 = System.currentTimeMillis();
			GreyScale correctedImage = dh.correctImage(greyScale);

			long end2 = System.currentTimeMillis();
			long time2 = end2 - start2;
			System.out.println("[time] correcting dauerte: " + time2 + " ms");

			RGBImage correctRGB = correctedImage.getAsRGB();

			circleHelper(correctRGB.getWidth(), correctRGB.getHeight());

			for (int i = 0; i < circleCount; i++) {
				correctRGB.addFunction(circles[i].getFunction(), Color.red);
			}

			BWImage correctedBW = correctedImage
					.convertToBW(RobotConfig.maximumBlack);

			swtImage[2] = correctedBW.getAsRGB().asSWTImage();

			// Integer[] driveTo = driveByJJStrategie(correctedBW, false);
			//
			// correctRGB.addVerticalLine(driveTo[0], 2);
			// correctRGB.addHorizontalLine(driveTo[1], 2);

			int cw = correctedBW.getWidth();
			int ch = correctedBW.getHeight();

			KreiseStrategie strategy = new KreiseStrategie();

			ArrayList<Integer[]> dots = strategy.getRegressDots(circles,
					correctedBW, processing.lastDistance - dh.getYOffset());

			for (Integer[] dot : dots) {
				// correctRGB.addVerticalLine(dot[0], 1);
				// correctRGB.addHorizontalLine(dot[1], 1);
				correctRGB.addDot(dot[0], dot[1], 1);
			}

			try {
				Function fx = strategy.drive(dots, dh.getYOffset(), cw, ch,
					null, 0, false);

				Function correctedFunction = new RelocatedFunction(fx,
						correctedBW.getWidth() / 2, dh.getYOffset()
								+ correctedBW.getHeight(), 1, -1);

				correctRGB.addFunction(correctedFunction, Color.blue);

			} catch (ArithmeticException e) {
				// luecke..

			}

			swtImage[3] = correctRGB.asSWTImage();

			// swtImage[3] = histogramm(w, h, greyScale, thres[0], thres[1]);

			// redDiff(image.getRGBImage());

			// kantenErkennungUndVisualisierung(image);

			// hough(w, h, blackWhiteBorders);

			Gui.getInstance().attachImageAsync(swtImage);

			end = System.currentTimeMillis();
			time = end - start;
			allTime += time;
			processCount++;
			long avg = allTime / (long) processCount;
			System.out.println("Processing took " + time
					+ " ms, Durchschnitt: " + avg + " ms");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	void redDiff(RGBImage image) {
		int thres = 50;

		long start2;
		long end2;
		long time2;
		/* redDifference Analyse */
		start2 = System.currentTimeMillis();

		GreyScale redDiff = colorDifference.getColorDiffImage(image, 0, false);

		swtImage[4] = redDiff.asSWTImage();

		swtImage[5] = redDiff.convertToBWWithFixedThreshold(thres).getAsRGB()
				.asSWTImage();

		int[] hist = Thresholding.getHistogramm(redDiff);

		hist[0] = 0; // improve histogramm -drawing

		GreyScale histogramm = Histogramm.getHistogrammAsGreyScaleImage(hist,
				image.getWidth(), image.getHeight());

		swtImage[6] = histogramm.asSWTImage();

		end2 = System.currentTimeMillis();
		time2 = end2 - start2;
		System.out.println("[time] redDiff  dauerte: " + time2 + " ms");
	}

}
