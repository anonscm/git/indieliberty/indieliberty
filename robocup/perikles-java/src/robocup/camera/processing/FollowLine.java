package robocup.camera.processing;

import java.util.ArrayList;

import robocup.camera.AbstractRoboCupImage;
import robocup.camera.processing.ImageProcessing.states;
import robocup.camera.processing.utils.CircleHelper;
import robocup.camera.processing.utils.DistanceHelper;
import robocup.camera.processing.utils.KreiseStrategie;
import robocup.camera.tools.images.BWImage;
import robocup.camera.tools.images.GreyScale;
import robocup.config.RobotConfig;
import robocup.robot.PriorityWrapper;
import robocup.robot.interfaces.IMotorControl;

public class FollowLine extends Thread {
	/**
	 * maximale geschwindigkeit in degrees / sec]
	 * 
	 */
	private static final int maxSpeed = 140;
	
	
	private static final boolean doLog = false;
	private static final boolean doDetailTimeLog = false;


	private static final int verstaerkung = 20;
	
	private CircleHelper[] circles;
//	private int circleCount;

	private DistanceHelper dh;

	private IMotorControl robot = new PriorityWrapper(PriorityWrapper.PRIORITY_FOLLOWING_LINE);
	private IMotorControl gapRobot = new PriorityWrapper(PriorityWrapper.PRIORITY_GAP);
	
	public boolean run = true;
	private ImageProcessing processing;
	
	private long allTime = 0;
	private int processCount = 0;

	
	public FollowLine(ImageProcessing ip) {
		processing = ip;
	}
	
	public void run() {
		try {
			while (run) {
				process(processing.lastImage);
				Thread.sleep(1);// damit beendet werden kann
			}
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void process(AbstractRoboCupImage image) {
		if ((processing.state == states.FOLLOWING_LINE || processing.state == states.GAP)) {

			try {

				if (image != null) {
					long start = System.currentTimeMillis();
					drive(image);

					long end = System.currentTimeMillis();
					long time = end - start;
					allTime += time;
					processCount++;
					long avg = allTime / (long) processCount;
					log("Processing took " + time
							+ " ms, Durchschnitt: " + avg + " ms");
				} else {
					log("No image, check video source!");
					Thread.sleep(300); // zeit lassen..
				}

			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

	}
	
	public void drive(AbstractRoboCupImage image) throws InterruptedException {

		distanceHelper(image.getWidth(), image.getHeight());

		long start2 = System.currentTimeMillis();

		GreyScale correctedImage = dh.correctImage(image.getRGBImage());

		// GreyScale correctedImage = dh.correctImage(greyScale);

		long end2 = System.currentTimeMillis();
		long time2 = end2 - start2;
		timeLog("correcting dauerte: " + time2 + " ms");

		// RGBImage correctRGB = correctedImage.getAsRGB();

		// for(int i = 0; i < circleCount; i++) {
		// correctRGB.addFunction(circles[i].getFunction(), Color.red);
		// }

		distanceHelper(image.getWidth(), image.getHeight());

		start2 = System.currentTimeMillis();
		BWImage correctedBW = correctedImage
				.convertToBW(RobotConfig.maximumBlack);

		int cw = correctedBW.getWidth();
		int ch = correctedBW.getHeight();

		end2 = System.currentTimeMillis();
		time2 = end2 - start2;
		timeLog("convertToBW dauerte: " + time2 + " ms");

		circleHelper(cw, ch);

		KreiseStrategie strategy = new KreiseStrategie();

		start2 = System.currentTimeMillis();

		 int distanceOnImage =  processing.lastDistance - dh.getYOffset();
		 
		 // wenn zu viel abgeschnitten werden wuerde, ignorieren
		 if(distanceOnImage < (ch /2)) {
			 distanceOnImage = ch;
		 }
		
		ArrayList<Integer[]> dots = strategy.getRegressDots(circles,
				correctedBW, distanceOnImage);

		end2 = System.currentTimeMillis();
		time2 = end2 - start2;
		timeLog("regressdots dauerte: " + time2 + " ms");

		start2 = System.currentTimeMillis();

		try {
			if (processing.state == states.GAP) {
				PriorityWrapper.setMinimumPriority(PriorityWrapper.PRIORITY_FOLLOWING_LINE);
				processing.state = states.FOLLOWING_LINE;
			}
			strategy
					.drive(dots, dh.getYOffset() - verstaerkung, cw, ch, robot, maxSpeed, true);
		} catch (ArithmeticException e) {

			if ((processing.state != states.GAP) && (processing.state == states.FOLLOWING_LINE)) {
				robot.setMotors(true, maxSpeed, true, maxSpeed);
//				new Gap().start();
			}
		}
		end2 = System.currentTimeMillis();
		time2 = end2 - start2;
		timeLog("fahren dauerte: " + time2 + " ms");

		// motoren nicht ueberlasten
		// try {
		// Thread.sleep(35);
		// } catch (InterruptedException e) {
		// e.printStackTrace();
		// }
		// ImageTools.printImage(image.getBwImage());

		// Strategies.driveToFarestPoint(image.getBwImage(), robot);
		// robot.setMotors(false, 900, true, 900);
		// Thread.sleep(1000);
		// robot.setMotors(false, 0, false, 10);
	}
	
	private void distanceHelper(int width, int height) {
		if (dh == null) {
			dh = new DistanceHelper(width, height);
			dh.calculateAll();
		}
	}

	private void circleHelper(int corrWidth, int corrHeight) {
		if (circles == null) {
			circles = KreiseStrategie.calcCircles(corrWidth, corrHeight);
//			circleCount = circles.length;
		}
	}

	
	private void log(String string) {
		if (doLog) {
			System.out.println("[FollowLine] " + string);
		}
	}
	
	private void timeLog(String s) {
		if (doDetailTimeLog) {
			System.out.println("[time] " + s);
		}
	}
	
	class Gap extends Thread {
		@Override
		public void run() {

			try {
				processing.state = states.GAP;
				final int timeRightAngle = 1600;
				PriorityWrapper
						.setPriority(PriorityWrapper.PRIORITY_GAP);
				gapRobot.setMotors(false, maxSpeed, true, maxSpeed);
				Thread.sleep(timeRightAngle);
				gapRobot.setMotors(true, maxSpeed, false, maxSpeed);
				Thread.sleep(timeRightAngle * 2);
				gapRobot.setMotors(false, maxSpeed, true, maxSpeed);
				Thread.sleep(timeRightAngle);
				gapRobot.setMotors(true, maxSpeed, true, maxSpeed);
			} catch (InterruptedException e) {
				// ignore
			}
		}

	}
}
