package robocup.camera.processing;

import org.eclipse.swt.graphics.Image;

import robocup.camera.AbstractRoboCupImage;
import robocup.camera.tools.Histogramm;
import robocup.camera.tools.HoughTransformation;
import robocup.camera.tools.ImageTools;
import robocup.camera.tools.Thresholding;
import robocup.camera.tools.filter.Filter;
import robocup.camera.tools.images.BWImage;
import robocup.camera.tools.images.GreyScale;
import robocup.camera.tools.images.RGBImage;
import robocup.config.RobotConfig;

public class ProcessingTools {
	
	public int[][] hough(int w, int h, boolean[][] blackWhiteBorders) {
		long start2;
		long end2;
		long time2;
		/* Hough-Transformation */
		start2 = System.currentTimeMillis();

		int[][] hough = HoughTransformation.apply(blackWhiteBorders, w, h);

		end2 = System.currentTimeMillis();
		time2 = end2 - start2;
		System.out.println("[time] Hough dauerte: " + time2 + " ms");
		return hough;
	}
	
	
	public Image histogramm(int w, int h, int[][] originalGreyScale,
			int thres1, int thres2) {
		long start2;
		long end2;
		long time2;
		/* GreenDifference Analyse */
		start2 = System.currentTimeMillis();

		/* TEST */

		GreyScale histogramm = Histogramm.getHistogrammAsGreyScaleImage(
				Thresholding.getHistogramm(originalGreyScale, w, h), w, h);
		// int[][] histogramm =
		// Histogramm.getHistogrammAsGreyScaleImage(Thresholding.getPartialHistogramm(originalGreyScale,
		// thres1, w, h), w, h);

		Histogramm.addThreshold(histogramm, thres1);
		Histogramm.addThreshold(histogramm, thres2);
		//		
		end2 = System.currentTimeMillis();
		time2 = end2 - start2;
		System.out.println("[time] histogramm erstellen dauerte: (unused) "
				+ time2 + " ms");

		return histogramm.asSWTImage();
	}
	
	
	public Image maxRGB(AbstractRoboCupImage image, int w, int h) {
		long start2;
		long end2;
		long time2;
		// MAXRGB
		start2 = System.currentTimeMillis();
		RGBImage maxRGB = new RGBImage(ImageTools.getRGBMaxImage(image), w, h);
		Image dest = maxRGB.asSWTImage();

		end2 = System.currentTimeMillis();
		time2 = end2 - start2;
		System.out.println("[time] maxRGB erstellen dauerte: " + time2 + " ms");
		
		
		return dest;
	}
	
	public GreyScale greyScale(AbstractRoboCupImage image, Image destSWTImage) {
		long start2;
		long end2;
		long time2;
		// GreyScale

		start2 = System.currentTimeMillis();

		GreyScale greyScale = image.getGreyScaleImage();

		destSWTImage = greyScale.asSWTImage();

		end2 = System.currentTimeMillis();
		time2 = end2 - start2;
		System.out.println("[time] greyScale erstellen dauerte: " + time2
				+ " ms");
		return greyScale;
	}
	
	public Image showOriginalImage(AbstractRoboCupImage image) {
		long start2 = System.currentTimeMillis();

		Image dest = image.getRGBImage().asSWTImage();

		long end2 = System.currentTimeMillis();
		long time2 = end2 - start2;
		System.out.println("[time] constructing Image took: " + time2 + " ms");
		
		return dest;
	}


	public int[] thresholding(GreyScale greyScale, Image destSWTImage) {
		long start2;
		long end2;
		long time2;

		// graubild doppelt trennen:
		start2 = System.currentTimeMillis();

		int thres[] = new int[2];

		thres[0] = Thresholding.otsu(greyScale);
		thres[1] = Thresholding.otsuPartialImage(greyScale, thres[0]);
		System.out.println("Thresholds: " + thres[0] + " " + thres[1]);

		GreyScale splitImage = ImageTools.splitImage(greyScale, thres[0],
				thres[1]);

		destSWTImage = splitImage.asSWTImage();

		end2 = System.currentTimeMillis();
		time2 = end2 - start2;
		System.out.println("[time] thresholding dauerte: " + time2 + " ms");
		return thres;
	}
	
	
	/** 
	 * 
	 * @param image
	 * @return for Images: ... TODO
	 */
	public Image[] kantenErkennungUndVisualisierung(AbstractRoboCupImage image) {

		int w = image.getWidth();
		int h = image.getHeight();
		GreyScale originalGreyScale = image.getGreyScaleImage();

		long start2;
		long end2;
		long time2;
		/* Kantenerkennung */
		start2 = System.currentTimeMillis();

		Filter filter = new Filter.SobelX();
		int[][] bordersX = Filter.apply(filter, originalGreyScale, false);

		filter = new Filter.SobelY();
		int[][] bordersY = Filter.apply(filter, originalGreyScale, false);

		GreyScale bordersCombined = new GreyScale(ImageTools.combineXandY(
				bordersX, bordersY, w, h), w, h);

		end2 = System.currentTimeMillis();
		time2 = end2 - start2;
		System.out.println("[time] kantenbild erstellen dauerte: " + time2
				+ " ms");

		/*
		 * Schwarz-Weiss-Kantenbild erstellen und nach RGB konvertieren (fuer
		 * darstellung)
		 */
		start2 = System.currentTimeMillis();

		/*
		 * boolean[][] blackWhiteBordersX = ImageTools.convertGreyToBW(bordersX,
		 * w, h); boolean[][] blackWhiteBordersY =
		 * ImageTools.convertGreyToBW(bordersY, w, h);
		 * 
		 * // BW bilder werden spaeter zerstoert byte[] blackWhiteRGBX =
		 * ImageTools.getBWasRGB(blackWhiteBordersX, w, h); byte[]
		 * blackWhiteRGBY = ImageTools.getBWasRGB(blackWhiteBordersY, w, h);
		 */

		BWImage blackWhiteBorders = bordersCombined.convertToBW(RobotConfig.maximumBlack);

		RGBImage blackWhiteRGB = blackWhiteBorders.getAsRGB();

		GreyScale bordersAngle = ImageTools.getAnglesRound(bordersX, bordersY,
				blackWhiteBorders.getBWImage(), w, h);

		end2 = System.currentTimeMillis();
		time2 = end2 - start2;
		System.out.println("[time] BW-Bild + Konvertierung nach RGB  dauerte: "
				+ time2 + " ms");

		/* Kantenbild analysieren */
		start2 = System.currentTimeMillis();

		RGBImage pathImage = new RGBImage(null, w, h);

		ImageTools.addPathAnalysisGreyScale(pathImage, bordersAngle,
				blackWhiteBorders);

		end2 = System.currentTimeMillis();
		time2 = end2 - start2;
		System.out.println("[time] kantenbild analysieren dauerte: " + time2
				+ " ms");

		/* Kantenbild darstellen: */
		start2 = System.currentTimeMillis();

		
		/*
		 * rgbImage = ImageTools.getGreyScaleAsRGB(bordersX, w, h); tmp[1] =
		 * ImageTools.constructImage(rgbImage, w, h, 24); rgbImage =
		 * ImageTools.getGreyScaleAsRGB(bordersY, w, h); tmp[2] =
		 * ImageTools.constructImage(rgbImage, w, h, 24);
		 */

		Image[] dest = new Image[4];
		
		dest[1] = bordersCombined.asSWTImage();

		dest[2] = blackWhiteRGB.asSWTImage();

		dest[3] = bordersAngle.asSWTImage();

		// byte[] rgbBW = ImageTools.getBWasRGB(image);
		// tmp[1] = ImageTools.constructImage(rgbBW, w, h, 24);

		// tmp[5] = ImageTools.constructImage(blackWhiteRGBX, w, h, 24);
		// tmp[6] = ImageTools.constructImage(blackWhiteRGBY, w, h, 24);

		// ergebnis der analyse als bild:
		dest[0] = pathImage.asSWTImage();

		end2 = System.currentTimeMillis();
		time2 = end2 - start2;
		System.out.println("[time] kantenbild visualisieren (1) dauerte: "
				+ time2 + " ms");
		
		return dest;
	}

	
}
