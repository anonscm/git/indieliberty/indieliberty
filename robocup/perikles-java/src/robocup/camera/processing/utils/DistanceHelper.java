package robocup.camera.processing.utils;

import robocup.camera.tools.UnsignedTypes;
import robocup.camera.tools.images.GreyScale;
import robocup.camera.tools.images.RGBImage;

public class DistanceHelper {


	/** Winkel zwischen Lot von der Linse und Richtung der Kamera 
	 * ( in Grad )
	 * 
	 * mit 37 grad funktioniert das Linie folgen recht gut, mehr sollten es nicht sein, sonst sieht der Roboter zu wenig direkt vor sich (haengt nat. von h ab)
	 *
	 *Tip zum berechnen:     tan(winkelKamera) = horizontaler abstand (kamera, bildmitte) / h   [ = 90 /132 -> 11.4.10] 
	 */
	private final int winkelKamera = 34;
	
	
	
	private final double cutTopFactor = 2d/4d;
	
	/**
	 * _halber_ Horizontaler Kamera Oeffnungswinkel 
	 * (in Grad) 
	 */
	private final int gamma = 28; // sind ausgemessen// 71grad stehen in der Spec von Creative Live! Optia


	
	/**
	 * Senkrechter KameraOeffnungswinkel (wird aus horizontalem berechnet)
	 * (in Grad)
	 */
	private final double beta =   Math.toDegrees(Math.asin(3d/4d * Math.sin(Math.toRadians(2*gamma))));

	
	/**
	 * Winkel zwischen Lot von Linse und gedachter Linie zwischen unterstem Punkt 
	 * (in Grad)
	 */
	private final double alpha = winkelKamera - beta/2;
	
	
	/**
	 * Hoehe der KameraLinse ueberm boden.. (in mm)
	 */
	private int h = 132;

	/**
	 *  breite des ausgangsbildes
	 */
	private final int width;
	
	/** 
	 * hoehe des ausgangsbildes
	 */
	private final int height;
	
	

	private final double alphaRad = Math.toRadians(alpha);
	private final double betaRad = Math.toRadians(beta);
	private final double gammaRad = Math.toRadians(gamma);
	
	
	// enthalten die abstande... TODO: double / float 
	/**
	 * enthaelt die abstande in mm in y-Richtung fuer jeden pixel, 0 ist der naechste am roboter
	 */
	private double[] dy;
	
	private double[][][] distancesByPixel;
	
	private int[][][] pixelByDistance;
	
	
	private int imageWidth;
	private int imageHeight;
	
	
	private int dyMin;
//	private double mmPerPixel;
	
	public DistanceHelper(int width, int height, int heightOverFloor) {
		this(width, height);
		h = h - heightOverFloor;
	}
	
	public DistanceHelper(int width, int height) {
		this.width = width;
		this.height = height;
			
		dy = new double[height];
		
		distancesByPixel = new double[width][height][2];
		
	}
	
	

	public void calculateAll() {
	
		calculateDy();
		
		for(int x = 0; x < width; x++) {
			for(int y = 0; y < height; y++) {
				
				distancesByPixel[x][y][0] = dx(x,y);
				distancesByPixel[x][y][1] = dy[y];
			}
		}
		
		// groesster x abstand:
		double dxMax = dx(width, dy[height-1]) * 2;
		double dxMin = dx(width, dy[0]) * 2;
		
		int dyMin = (int) Math.ceil(dy[0]);
		int dyMax = (int) Math.floor(dy[height-1]);
		
		int dySpan = dyMax - dyMin;
		
//		double k = (double)dxMin/(double)dxMax;
		
//		int dxMinPixel = (int) (k * width/2);
		
		
		
		System.out.println("dy: " + dyMin + " - " + dyMax + "   L-R abstand: " + dxMin + " - " + dxMax);
		
		
		imageWidth = (int) dxMin;
		imageHeight = (int) (dySpan * cutTopFactor) ;
		
		pixelByDistance = new int[imageWidth][imageHeight][2];
		
		this.dyMin = dyMin;
		
		for(int dx = 0; dx < imageWidth/2; dx++ ) {
			for(int dy = 0; dy < imageHeight; dy++) {

				int pixelx =  px(dx, dy + dyMin);
				int pixely = height - py(dy + dyMin);
				
				if(pixely == height) {
					pixely = height -1;
				}
				
				// links von der mitte
				pixelByDistance[imageWidth/2 - dx][dy][0] = width/2  - pixelx;
				pixelByDistance[imageWidth/2  - dx][dy][1] = pixely ;
				
				// rechts von der mitte
				pixelByDistance[imageWidth/2  + dx][dy][0] = width/2 + pixelx;
				pixelByDistance[imageWidth/2  + dx][dy][1] = pixely;
			}
		}
	
	}
	
	
	/** 
	 * returns corrected image y is in mm, x is NOT!
	 * @param original
	 * @return
	 */
	public GreyScale correctImage(GreyScale original) {
		
		int[][] greyScale = new int[imageWidth][imageHeight];
		
		for(int dx = 0; dx < imageWidth; dx++ ) {
			for(int dy = 0; dy < imageHeight; dy++) {
				int x = pixelByDistance[dx][dy][0];
				if(x >= width) {
					System.err.println("x >= width: " + x );
					x = width-1;
					
				}
				
				int y = pixelByDistance[dx][dy][1];
				
				
//				System.out.println("dx: " + dx + " dy: " + dy + " x:" + x + " y: " + y);

				//TODO: try-catch is bad for performance
				try { 
					greyScale[dx][imageHeight -1 - dy] = original.getData()[x][y];
				} catch(Exception e) {
					e.printStackTrace();
				}
			}
		}
		
		
		return new GreyScale(greyScale, imageWidth, imageHeight);
	}
	
	public GreyScale correctImage(RGBImage rgbImage) {
		byte[] rgb = rgbImage.getBytes();
		
		int[][] greyScale = new int[imageWidth][imageHeight];
		
		for(int dx = 0; dx < imageWidth; dx++ ) {
			for(int dy = 0; dy < imageHeight; dy++) {
				int x = pixelByDistance[dx][dy][0];
				if(x >= width) {
					System.err.println("x >= width: " + x );
					x = width-1;
					
				}
				
				int y = pixelByDistance[dx][dy][1];
				
				
//				System.out.println("dx: " + dx + " dy: " + dy + " x:" + x + " y: " + y);
				//TODO: try-catch is bad for performance
				try { 
					int pos = (y * rgbImage.getWidth() + x)*3;
					greyScale[dx][imageHeight -1 - dy] = (
						3 *UnsignedTypes.unsignedByteToInt(rgb[pos]) +
						6* UnsignedTypes.unsignedByteToInt(rgb[pos]) +
						UnsignedTypes.unsignedByteToInt(rgb[pos])) /10;
				} catch(Exception e) {
					e.printStackTrace();
				}
			}
		}

		return new GreyScale(greyScale, imageWidth, imageHeight);
	}
	
	
	
	/**
	 * Gibt den Abstand in y-Richtung in Pixeln von der Bildunterseite an
	 * @param dy Abstand in y-Richtung von der Kamera in mm
	 * @return
	 */
	int py(double dy) {
		return (int) ((Math.atan(dy/h) - alphaRad) / betaRad * height);
	}
	
	/**
	 * Pixelabstand von der mitte
	 */
	int px(double dx, double dy) {
		return (int) ((Math.atan(dx/pythagoras(dy, h)) / gammaRad * (width/2d)) );
	}
	
	/** 
	 * 
	 * @param x x-Wert (NICHT von der Mitte!!!)
	 * @param dy Abstand von der Kamera in y-Richtung in mm
	 * @return Abstand in mm von der Bildmitte 
	 */
	double dx(int x, double dy) {
		//norming x:
		x = x - (width/2);
		if(x < 0) {
			x = -x;
		}
		
		if(x >= width) {
			throw new IllegalArgumentException("x >= width");
		}
		
		return pythagoras(dy, h) * Math.tan(gammaRad * (double) x /  ((double)width / 2d) );
	}
	
	
	
	double dx_wrong(int x, double dy) {
		//norming x:
		x = x - (width/2);
		if(x < 0) {
			x = -x;
		}
		
		return dy * Math.tan(gammaRad * (double) x /  ((double)width / 2d) );
	}
	
	private void calculateDy() {
		
		for(int y = 0; y < height; y++) {
			dy[y] = h * Math.tan((double)y/ (double)height * betaRad + alphaRad);
		}
	}
	
	private double pythagoras(double x, double y) {
		return Math.sqrt(x*x + y*y);
	}
	
	public double getHorizontalDistance(int x, int y) {
		return distancesByPixel[x][y][0];
	}
	
	public double getVerticalDistance(int x, int y) {
		return distancesByPixel[x][y][1];
	}
	
	public int getYOffset() {
		return dyMin;
	}

	
	
	
	
}
