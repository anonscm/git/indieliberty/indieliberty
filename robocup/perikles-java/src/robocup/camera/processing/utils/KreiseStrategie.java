package robocup.camera.processing.utils;

import java.util.ArrayList;

import robocup.camera.tools.images.BWImage;
import robocup.camera.tools.regression.RegressionsKreisV2;
import robocup.config.RobotConfig;
import robocup.misc.interfaces.Function;
import robocup.robot.interfaces.IMotorControlBasic;

public class KreiseStrategie {

	/**
	 * Mindestanzahl der aufeinanderfolgenden pixel damit eine Linie erkannt
	 * wird
	 */
	private static final int threshold = 10;

	private static final boolean log = false;

	public static CircleHelper[] calcCircles(int w, int h) {

		int circleCount = 4;

		CircleHelper[] circles = new CircleHelper[circleCount];

		//
		// circles[0] = new CircleHelper(w / 2, w, h);
		// circles[1] = new CircleHelper(w * 3 / 4, w, h);
		// circles[2] = new CircleHelper(w, w, h);

		circles[0] = new CircleHelper(w * 2 / 8, w, h);
		circles[1] = new CircleHelper(w * 3 / 8, w, h);
		circles[2] = new CircleHelper(w * 4 / 8, w, h);
		circles[3] = new CircleHelper(w * 5 / 8, w, h);

		// circles[3] = new CircleHelper(w*5/4, w, h);

		return circles;

	}

	public ArrayList<Integer[]> getRegressDots(CircleHelper[] circles,
			BWImage img, int maxY) {

		boolean[][] bw = img.getBWImage();

		ArrayList<Integer[]> regressDots = new ArrayList<Integer[]>();

		for (int i = circles.length - 1; i >= 0; i--) {

			// for(int i = 0; i < circles.length; i++) {

			ArrayList<Integer[]> dots = circles[i].dotsOnCircle;

			int countL = 0;
			int countR = 0;

			for (int x = 0; x < dots.size() / 2 - 1; x++) {

				Integer[] dotR = dots.get(dots.size() / 2 + x);

				// wenn da ein hinderniss ist ignorieren
				if (!(dotR[1] > maxY)) {

					if (bw[dotR[0]][dotR[1]]) {
						// der punkt ist weiss
						countR = 0;

					} else {
						// der punkt ist schwarz

						countR++;

						if (countR > threshold) {
							log("Linie im " + i
									+ ". Kreis rechts gefunden; x: " + dotR[0]
									+ ", y: " + dotR[1]);

							regressDots.add(dotR);
							break;
						}
					}

					Integer[] dotL = dots.get(dots.size() / 2 - x);

					if (bw[dotL[0]][dotL[1]]) {
						// der punkt ist weiss
						countL = 0;

					} else {
						// der punkt ist schwarz

						countL++;

						if (countL > threshold) {
							log("Linie im " + i
									+ ". Kreis rechts gefunden; x: " + dotL[0]
									+ ", y: " + dotL[1]);

							regressDots.add(dotL);
							break;

						}
					}
				}
			}

		}

		return regressDots; // luecke

	}

	/**
	 * 
	 * @param regressDots
	 *            1 px ^= 1 mm
	 * @param yOffset
	 *            abstand roboter - bildunterkante in mm
	 * @param w
	 * @param h
	 * @param robot
	 * @param maxSpeed
	 * @param doDrive
	 * @return
	 */
	public Function drive(ArrayList<Integer[]> regressDots, int yOffset, int w,
			int h, IMotorControlBasic robot, int maxSpeed, boolean doDrive) {

		// normieren
		ArrayList<Integer[]> mmDots = new ArrayList<Integer[]>();
		for (Integer[] dot : regressDots) {

			int xmm = (int) ((dot[0] - w / 2));
			int ymm = (h - dot[1]) + yOffset;

			mmDots.add(new Integer[] { xmm, ymm });

		}

		RegressionsKreisV2 reg = new RegressionsKreisV2();

		int radius = reg.getRadius(mmDots);

		log("fahre mit radius = " + radius + "mm");

		if (doDrive) {
			StrategyHelper.driveRadius(radius, RobotConfig.radAbstand / 2,
					maxSpeed, robot);
		}
		return reg;
	}

	private void log(String s) {
		if (log) {
			System.out.println("[KreiseStrategie] " + s);
		}
	}

}
