
package robocup.camera.processing.utils;

import java.util.ArrayList;

import robocup.misc.interfaces.Function;

public class CircleHelper {

//	private static DistanceHelper distanceHelper = RoboCup.distanceHelper;
	
	private final int radius;
	private final int imageWidth;
	private final int imageHeight;
	
	private KreisFunktion kreis;

	public ArrayList<Integer[]> dotsOnCircle = new ArrayList<Integer[]>();

	private int mittelpunktY;
	
	/**
	 * 
	 * @param radius
	 * @param mittelpunktY
	 * @param imageWidth
	 * @param imageHeight
	 */
	public CircleHelper(int radius, int mittelpunktY, int imageWidth, int imageHeight) {
		this.radius = radius;
		this.mittelpunktY = mittelpunktY;
		this.imageWidth = imageWidth;
		this.imageHeight = imageHeight;
		
		calc();
	}
	

	/**
	 * nimmt als Y-Wert des Mittelpunktes einfach 4/3 * imageHeight
	 * @param radius
	 * @param imageWidth
	 * @param imageHeight
	 */
	public CircleHelper(int radius, int imageWidth, int imageHeight) {
		this.radius = radius;
		this.mittelpunktY = imageHeight * 4 / 3;
		this.imageWidth = imageWidth;
		this.imageHeight = imageHeight;
		
		calc();
	}

	
	private void calc() {
		kreis = new KreisFunktion(imageWidth /2 , mittelpunktY, radius);
		
		for(int x = 0; x < imageWidth; x++) {
			
			int y = kreis.f(x);
			if( y >= 0  && y < imageHeight) {

				dotsOnCircle.add(new Integer[] { x,  y });
			}
		}
		
	}
	
	public Function getFunction() {
		return kreis;
	}
	
	public class KreisFunktion implements Function {
		
		
		private final int r;
		private final int mY;
		private final int mX;


		KreisFunktion(int mX, int mY, int r) {
			this.mX = mX;
			this.mY = mY;
			this.r = r;
		}


		@Override
		public int f(int x) {
			
			return (int) (-Math.sqrt(r*r - (x - mX) * (x - mX)) + mY);
		}
		
		public double f(double x) {
			return (-Math.sqrt(r*r - (x - mX) * (x - mX)) + mY);
		}
		
	}
	
}