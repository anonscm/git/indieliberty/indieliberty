package robocup.camera.processing.utils;

import robocup.camera.tools.images.BWImage;
import robocup.robot.interfaces.IMotorControlBasic;

public class Strategies {

	public static int[] driveToFarestPoint(BWImage image, IMotorControlBasic robot) {
		return driveToFarestPoint(image.getBWImage(), image.getWidth(), image.getHeight(), robot);
	}
	
	public static int[] driveToFarestPoint(boolean[][] bwImage, int w, int h, IMotorControlBasic robot) {
		//TODO: double might cause performance problems
		
		int[] farestPoint = new int[2];
		double farestDistance = 0;
		for (int x = 0; x < w; x++) {
			for (int y = (h-1)/2; y >= 0; y--) {
				
				int x2 = x - w/2;
				int y2 = h - y;
				
				if(bwImage[x][y]) {
					double distance = Math.sqrt(x2*x2 + y2*y2);
					
					if(distance > farestDistance) {
						farestDistance = distance;
						farestPoint[0] = x;
						farestPoint[1] = y;
					}
					
					break;
				}
			}
		}
		
		System.out.println("ich fahr jetzt nach: " + farestPoint[0] + " / " + farestPoint[1]+ " weil das voll weit weg ist. =) ");
		
//TODO:		StrategyHelper.drivetopoint(farestPoint[0] - w/2, h - farestPoint[1], robot);
		
		return farestPoint;
	}
	
	
	// bild anfang 41px
	// Abstand bildanfang - raeder = 5,5 cm = 37 px

//		float pixelprocm = 20 / 3; 
//		float RADABSTAND = 15,5cm; 
	//  d = 15,5 * 20 /3 /2 = 52 // halber Radabstand
	
	

}
