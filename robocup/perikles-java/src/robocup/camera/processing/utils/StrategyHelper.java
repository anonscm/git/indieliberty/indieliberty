package robocup.camera.processing.utils;

import robocup.robot.interfaces.IMotorControlBasic;

public class StrategyHelper {
	
	
	
	/**
	 * // y muss positiv sein (0/0) ist aktuelle position
	 * @param x verschiebung nach rechts in gleicher einheit wie:
	 * @param y verschiebung nach vorne..
	 * @param robot
	 * @param maxSpeed maximale geschwindigkeit
	 * @param d radAbstand in mm
	 */
	public static void drivetopoint(int x, int y, IMotorControlBasic robot, int maxSpeed, int d) { 
		
		d = d / 2;

		if(x == 0) { // geradeaus...
			robot.setMotors(true, maxSpeed, true, maxSpeed);
	
		} else {
			float c = (x * x + y * y) / (2 * x); // radius des mittleren kreises..
			
			driveRadius(c, d, maxSpeed, robot);
		}
	}

	public static void driveRadius(float radius,int halberRadAbstand,
			int maxSpeed, IMotorControlBasic robot) {
		float rR = (radius + halberRadAbstand);
		float rL = (radius - halberRadAbstand);

		if( (rL == 0) || (rR == 0) ) { 
			if(radius > 0) {
				
				
				robot.setMotors(true, maxSpeed, true, 0);
			} else {
				robot.setMotors(true, 0, true, maxSpeed);
			}
		} else {
			float rel = rL / rR;
			
			/* > 1 (Links schneller)
			 * = 1 (wird vorher ausgeschlossen / geradeaus..)
			 * 0 < 1 (Rechts schneller) 
			 * -1 < 0 (Links rückwärts / Rechts vorwärts)
			 * < -1 (Rechts rückwärts / Links vorwärts) */
			 
			if(rel > 1) {
				robot.setMotors(true, maxSpeed, true, (int)(maxSpeed / rel) );	
		
			} else if(rel > -1) {
				robot.setMotors(true, (int)(maxSpeed * rel), true, maxSpeed);
		
			} else {
				robot.setMotors(true, maxSpeed, true, (int)(maxSpeed / rel) ); 
			}
		}
	}

	
}
