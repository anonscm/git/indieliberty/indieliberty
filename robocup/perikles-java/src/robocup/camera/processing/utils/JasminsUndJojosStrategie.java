package robocup.camera.processing.utils;

import java.util.ArrayList;

import robocup.camera.tools.images.BWImage;

/**
 * currently unused
 * @author Mattis Pasch, Torsten Wylegala, Jasmin Mueller, Johannes Held
 *
 */
public class JasminsUndJojosStrategie {
	
	
	/**
	 * Mindestanzahl der aufeinanderfolgenden pixel damit eine Linie erkannt wird
	 */
	private static final int threshold = 4;


	private static final boolean log = true;
	
	
	/** radAbstand in mm
	 * 
	 */
	private final int radAbstand = 160;
	
	
	
	
	public Integer[] drive(CircleHelper[] circles, BWImage img) {
		
		boolean[][] bw = img.getBWImage();
		
		
		for(int i = circles.length - 1; i >= 0; i--) {
	
//		for(int i = 0; i < circles.length; i++) {
			
		
			ArrayList<Integer[]> dots = circles[i].dotsOnCircle;
			
			int countL = 0;
			int countR = 0;
			
			for(int x = 0; x < dots.size()/2-1; x++) {
				
				Integer[] dotR = dots.get(dots.size()/2 + x);
				
				if(bw[dotR[0]][dotR[1]]) {
					// der punkt ist weiss
					countR = 0;
					
				} else {
					// der punkt ist schwarz
					
					countR++;
					
					if(countR > threshold) {
						log("Linie im " + i + ". Kreis rechts gefunden; x: " + dotR[0] + ", y: " + dotR[1]);
						
						return dotR;
					}
				}
				
				Integer[] dotL = dots.get(dots.size()/2 - x);
				
				if(bw[dotL[0]][dotL[1]]) {
					// der punkt ist weiss
					countL = 0;
					
				} else {
					// der punkt ist schwarz
					
					countL++;
					
					if(countL > threshold) {
						log("Linie im " + i + ". Kreis rechts gefunden; x: " + dotL[0] + ", y: " + dotL[1]);
						
						return dotL;
					}
				}
			}
		}
	
		return new Integer[]{0, 0}; // luecke	
	}
		
	
	
	
	

	/** erwartet normierte x, y werte
	 * 
	 * @param x
	 * @param y
	 * @return
	 */
	public double getVerhaeltnis(int x, int y) {
		
		double alpha = Math.tan(x/y);
		
		double radius = Math.sqrt(x*x + y*y);
		
		double aRechts =  cosinusSatz(radAbstand/2, radius, alpha);
		double aLinks = cosinusSatz(radAbstand/2, radius, 180 - alpha);
		
		double vLinks = pythagoras(aLinks, radAbstand/2);
		double vRechts = pythagoras(aRechts, radAbstand/2);
		
		
		double k = vLinks / vRechts;
		return k;
	}
	
	private double cosinusSatz(double b, double c, double alpha) {
		return b * b + c * c - c * b * Math.cos(alpha);
	}

	private double pythagoras(double c, double b) {
		return Math.sqrt(c*c - b*b);
	}
	
	
	private void log(String s) {
		if(log) {
			System.out.println(s);
		}
	}
}

