package robocup.camera.processing;

import robocup.camera.processing.ImageProcessing.states;
import robocup.robot.PriorityWrapper;
import robocup.robot.interfaces.IMotorControl;

public class ObstacleAvoider extends Thread {
	/**
	 * maximale geschwindigkeit in degrees / sec]
	 * 
	 */
	private static final int maxSpeed = 140;

	private IMotorControl obstacleRobot = new PriorityWrapper(
			PriorityWrapper.PRIORITY_OBSTACLE);
	
	private IMotorControl avoidingRobot = new PriorityWrapper(PriorityWrapper.PRIORITY_AVOIDINGWALL);

	private ImageProcessing processing;

	public ObstacleAvoider(ImageProcessing ip) {
		this.processing = ip;
	}

	public void run() {
		try {
			System.out.println("Avoiding Obstacle.");
			processing.state = states.OBSTACLE;
			PriorityWrapper.setPriority(PriorityWrapper.PRIORITY_OBSTACLE);

			final int rightAngle = 1400;
			final int zurSeite = 2200;

			obstacleRobot.setMotors(false, maxSpeed, false, maxSpeed);
			Thread.sleep(1000);
			obstacleRobot.setMotors(true, maxSpeed, false, maxSpeed);
			Thread.sleep(rightAngle);
			obstacleRobot.setMotors(true, maxSpeed, true, maxSpeed);
			Thread.sleep(zurSeite);
			obstacleRobot.setMotors(false, maxSpeed, true, maxSpeed);
			Thread.sleep(rightAngle);
			obstacleRobot.setMotors(true, maxSpeed, true, maxSpeed);
			Thread.sleep(5000);
			obstacleRobot.setMotors(false, maxSpeed, true, maxSpeed);
			Thread.sleep(rightAngle);
			obstacleRobot.setMotors(true, maxSpeed, true, maxSpeed);
			Thread.sleep(zurSeite);
			obstacleRobot.setMotors(true, maxSpeed, false, maxSpeed);
			Thread.sleep(rightAngle);
			obstacleRobot.setMotors(false, maxSpeed, false, maxSpeed);
			Thread.sleep(900);
			obstacleRobot.setMotors(true, 0, true, 0);

			PriorityWrapper
					.setPriority(PriorityWrapper.PRIORITY_FOLLOWING_LINE);
			processing.state = states.FOLLOWING_LINE;
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("Finished avoiding Obstacle, following line..");
	}

	public void hitLeft() {
		new Thread(new Runnable() {
			public void run() {

				try {
					System.out.println("Avoiding wall on LEFT side.");
					processing.state = states.AVOIDINGWALL;
					PriorityWrapper.setPriority(PriorityWrapper.PRIORITY_AVOIDINGWALL);
					
					avoidingRobot.setMotors(true, 0, true, 0);
					Thread.sleep(200);
					avoidingRobot.setMotors(false, maxSpeed, false, maxSpeed/2);
					Thread.sleep(1200);
					avoidingRobot.setMotors(true, maxSpeed, true, maxSpeed);
					Thread.sleep(700);
					
					
					if(processing.state == states.AVOIDINGWALL) {
						PriorityWrapper
							.setPriority(PriorityWrapper.PRIORITY_FOLLOWING_LINE);
						processing.state = states.FOLLOWING_LINE;
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
				System.out
						.println("Finished avoiding wall on the left, following line..");
			}
		}).start();
	}

	public void hitRight() {
		new Thread(new Runnable() {
			public void run() {

				try {
					System.out.println("Avoiding wall on RIGHT side.");
					processing.state = states.AVOIDINGWALL;
					PriorityWrapper.setPriority(PriorityWrapper.PRIORITY_AVOIDINGWALL);
					
					
					avoidingRobot.setMotors(true, 0, true, 0);
					Thread.sleep(200);
					avoidingRobot.setMotors(false, maxSpeed/2, false, maxSpeed);
					Thread.sleep(1200);
					avoidingRobot.setMotors(true, maxSpeed, true, maxSpeed);
					Thread.sleep(700);
					

					if(processing.state == states.AVOIDINGWALL) {
						PriorityWrapper
							.setPriority(PriorityWrapper.PRIORITY_FOLLOWING_LINE);
						processing.state = states.FOLLOWING_LINE;
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
				System.out
						.println("Finished avoiding wall on the right, following line..");
			}
		}).start();

	}

}
