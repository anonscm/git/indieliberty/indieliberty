package robocup.camera.processing;

import robocup.robot.PriorityWrapper;
import robocup.robot.interfaces.IMotorControl;

public class RedZone extends Thread {

	private ImageProcessing processing;

	public RedZone(ImageProcessing ip) {
		processing = ip;
	}

	private static final int speed = 200;

	public void run() {
		try {
			
			System.out.println("Redzone!!!");
			PriorityWrapper.setPriority(PriorityWrapper.PRIORITY_REDZONE);

			IMotorControl robot = new PriorityWrapper(
					PriorityWrapper.PRIORITY_REDZONE);

			// // an die Wand stellen...
			// robot.setMotors(true, speed, true, speed);
			// Thread.sleep(2500);
			//		
			// // an die andere Wand.. (nach links hinten drehen)	
			// robot.setMotors(false, speed, false, 0);

			// erstmal stehenbleiben
			robot.setMotors(true, 0, true, 0);
			Thread.sleep(1000);

			int max = 20;

			int[] distances = new int[max];

			int maxDistance = 0;
			
			for (int i = 0; i < max; i++) {
				robot.setMotors(false, speed, true, speed);
				Thread.sleep(100);
				robot.setMotors(true, 0, true, 0);
				Thread.sleep(1000);
				distances[i] = processing.lastDistance;
				
				if(distances[i] > distances[maxDistance]) {
					maxDistance = i;
				}
				System.out.println(i + ": " + distances[i]);
			}
			
			robot.setMotors(true, speed, false, speed);
			Thread.sleep(100 * (max - maxDistance));
			
			robot.setMotors(true, 0, true, 0);
			Thread.sleep(3000);
			
			robot.setMotors(true, 900, true, 900);
			Thread.sleep(3000);
			
			robot.setMotors(true, 0, true, 0);
			

		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
