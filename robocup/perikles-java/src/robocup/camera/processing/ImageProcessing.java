package robocup.camera.processing;

import robocup.RoboCup;
import robocup.camera.AbstractRoboCupImage;
import robocup.camera.interfaces.IImageListener;

import robocup.camera.tools.ImageTools;

import robocup.debug.ui.Gui;
import robocup.debug.ui.IGUIButtonListener;
import robocup.robot.PriorityWrapper;

import robocup.robot.interfaces.ITouchListener;
import robocup.robot.interfaces.IUSListener;

public class ImageProcessing implements IImageListener, ITouchListener,
		IUSListener, IGUIButtonListener {

	// zeit damit die rampe als wirkliche rampe erkannt wird: (in ms)

	private static final long minRampTime = 4500;

	private boolean doLog = true;

	public enum states {
		NOTSTARTED, FOLLOWING_LINE, RAMP, REDZONE, GAP, OBSTACLE, AVOIDINGWALL
	}

	states state = states.NOTSTARTED;

	AbstractRoboCupImage lastImage = null;

	public int lastDistance = 255;

	public static boolean run = true;

	private boolean isLeftTouch = false;
	private boolean isRightTouch = false;
	private boolean isOnRamp = false;

	private FollowLine followLine = new FollowLine(this);
	private GUIUpdater guiUpdater = new GUIUpdater(this);
	private Ramp ramp = new Ramp(this);
	private ObstacleAvoider obstableAvoider = new ObstacleAvoider(this);

	private long timeOnRamp;

	public void newImage(AbstractRoboCupImage image) {
		lastImage = image;
	}

	public void guiButtonPressed() {

		// TEST:
		// robot.setMotorLeft(false, 1000);
		// robot.setMotors(true, 255, true, 255);
		// robot.setMotorRight(true, 1000);
		// robot.driveCircle(IMotorControl.LEFT, 10);

		if (lastImage != null) {
			guiUpdater.updateGUI(lastImage);
		} else {
			System.out.println("Can't update GUI: NO IMAGE");
		}
	}

	public void close() {
		run = false;
	}

	public void open() {
		if (RoboCup.startGUI) {
			Gui.getInstance().setButtonListener(this);
			new Thread(Gui.getInstance()).start();
		}

		followLine.start();

	}

	private void avoidObstacle() {
		if (!obstableAvoider.isAlive()) {

			obstableAvoider = new ObstacleAvoider(this); // neu instanziieren
			// sonst gibts
			// exceptions

			obstableAvoider.start();
		}
	}

	public void printImage() {
		ImageTools.printImage(lastImage.getBwImage());
	}

	@Override
	public void onUSChange(int location, int distance) {
		distance *= 10; // von cm auf mm

		distance -= 50; // 50 mm hinter der kamera ist der US

		log("US changed, loc: " + location + " , distance: " + distance + "mm");

		lastDistance = (distance == 0) ? 0 : distance;

	}

	@Override
	public void onTouch(int location) {
		try {
			if (state == states.NOTSTARTED) {

				log("Release to start program.");

			} else {
				if (location == ITouchListener.LEFT) {
					isLeftTouch = true;
					Thread.sleep(100);
					if (isRightTouch) {
						avoidObstacle();
					} else {
						if (state == states.FOLLOWING_LINE
								|| state == states.GAP) {
							obstableAvoider.hitLeft();
						}
					}
				} else if (location == ITouchListener.RIGHT) {
					isRightTouch = true;
					Thread.sleep(100);
					if (isLeftTouch) {
						avoidObstacle();
					} else {
						if (state == states.FOLLOWING_LINE
								|| state == states.GAP) {
							obstableAvoider.hitRight();
						}
					}
				} else if (location == ITouchListener.RAMP) {
					log("entered ramp.");

					isOnRamp = true;
					timeOnRamp = System.currentTimeMillis();
					if (state != states.RAMP) {
						// TODO: check if really on ramp

						if (!ramp.isAlive()) {
							// zur sicherheit neu istanziieren
							ramp = new Ramp(this);
							ramp.start();
						}
					}
				}
				// bei allen sensoren
				if (state == states.RAMP) {
					ramp.touchOnRamp = true;

				}

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onRelease(int location) {
		if (state == states.NOTSTARTED) {

			log("Starting program...");
			state = states.FOLLOWING_LINE;

			// test1`
			// state = states.REDZONE;
			// new RedZone(this).start();
		} else {
			if (location == ITouchListener.LEFT) {
				isLeftTouch = false;
			} else if (location == ITouchListener.RIGHT) {
				isRightTouch = false;
			}

			if (location == ITouchListener.RAMP) {
				log("exited ramp.");
				isOnRamp = false;

				long rampTime = System.currentTimeMillis() - timeOnRamp;
				if (rampTime > minRampTime) {
					state = states.REDZONE;
					new RedZone(this).start();
				} else {
					state = states.FOLLOWING_LINE;
					PriorityWrapper
							.setPriority(PriorityWrapper.PRIORITY_FOLLOWING_LINE);
				}
			}
		}
	}

	private void log(String string) {
		if (doLog) {
			System.out.println("[ImageProcessing] " + string);
		}

	}

}
