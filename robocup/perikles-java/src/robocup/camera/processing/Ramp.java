package robocup.camera.processing;

import robocup.camera.processing.ImageProcessing.states;
import robocup.robot.PriorityWrapper;
import robocup.robot.interfaces.IMotorControl;

public class Ramp extends Thread{
	boolean touchOnRamp;
	
	private static final int rampSpeed = 90;
	private IMotorControl rampRobot = null;
	private ImageProcessing processing;
	
	
	public Ramp(ImageProcessing ip) {
		processing = ip;
	}
	
	
	@Override
	public void run() {
		try {
			processing.state = states.RAMP;
			PriorityWrapper
					.setPriority(PriorityWrapper.PRIORITY_RAMP);

			rampRobot = new PriorityWrapper(
					PriorityWrapper.PRIORITY_RAMP);

			rampRobot.setMotors(true, rampSpeed, true,
					rampSpeed);

			while (processing.state == states.RAMP) {

				if (touchOnRamp) {
					touchOnRamp = false;
					rampRobot.setMotors(false, rampSpeed,
							true, 0);

					Thread.sleep(1300);

					rampRobot.setMotors(true, rampSpeed,
							true, rampSpeed);
				}
				Thread.sleep(5);
			}
		} catch (InterruptedException e) {

		}
	}
}
