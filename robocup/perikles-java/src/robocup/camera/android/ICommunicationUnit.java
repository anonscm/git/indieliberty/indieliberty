package robocup.camera.android;

public interface ICommunicationUnit {
	
	public void setImage(byte[] image);
	
	public byte[] getImage();
	
	public int getImageWidth();
	
	public int getImageHeight();
	
	public void setImageWidth(int width);
	
	public void setImageHeight(int height);
	
}
