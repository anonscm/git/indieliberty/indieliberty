package robocup.camera.android;

import robocup.camera.color.ColorMap;
import robocup.camera.interfaces.IImageListener;
import robocup.camera.interfaces.IVideoSource;


public class AndroidCam implements IVideoSource {

	ColorMap colorMap = ColorMap.createFixedMap(false);
	
	IImageListener listener = null;
	byte[] image;
	
	public AndroidCam(){
	}
	
	public void close() {	
	}

	public ColorMap getColorMap() {
		return colorMap;
	}

	public int getImageHeight() {
		return 240;
	}

	public int getImageWidth() {
		return 320;
	}

	public void setListener(IImageListener l) {
		listener = l;
	}

	public void run() {
//	TODO	listener.newImage(this.image);
	}
	
	public void setImageByte(byte[] image){
		this.image = image;
	}

	

}
