package robocup.camera.android;

import java.util.ArrayList;

import robocup.camera.AbstractRoboCupImage;
import robocup.camera.RoboCupColorImage;
import robocup.camera.interfaces.IImageListener;
import robocup.camera.interfaces.IVideoSource;
import robocup.camera.tools.regression.RegressionsGerade;

import robocup.robot.interfaces.IMotorControl;
import robocup.robot.interfaces.ITouchListener;

public class ImageProcessingAndroid implements IImageListener, ITouchListener {

	@SuppressWarnings("unused")
	private IVideoSource videoSource;
	@SuppressWarnings("unused")
	private IMotorControl robot;

	// TODO IMG_HEIGHT und WIDTH aus VideoSource holen


	final int IMG_HEIGHT = 320;

	final int IMG_WIDTH = 480;
	
	private RegressionsGerade regress = new RegressionsGerade(IMG_HEIGHT+30, IMG_WIDTH);
	
	private int imgCount = 0;
	
	boolean running = false;
	
//	int[] lastImgInGui = new int[57600];

//	byte[] lastImg;
	
//	ICommunicationUnit cu = null;
	
//	Approximation approx = new Approximation();

	public void setVideoSource(IVideoSource vs) {
		videoSource = vs;
	}

	public void newImage(byte[] buf) {
        //10-25 14:14:59.494: ERROR/Camera(5077): _getParameters: antibanding=auto;antibanding-values=off,50hz,60hz,auto;effect-values=mono,negative,solarize,pastel,mosaic,resize,sepia,posterize,whiteboard,blackboard,aqua;jpeg-quality=100;jpeg-thumbnail-height=384;jpeg-thumbnail-quality=90;jpeg-thumbnail-width=512;luma-adaptation=0;nightshot-mode=0;picture-format=jpeg;picture-size=2048x1536;preview-format=yuv420sp;preview-frame-rate=15;preview-size=480x320;rotation=0;whitebalance=auto;whitebalance-values=auto,custom,incandescent,fluorescent,daylight,cloudy,twilight,shade

		
		
		imgCount++;
		if(!running) {
			running = true;
			System.out.println("[ImageProcessing]: Image: " + imgCount + " , size: " + buf.length);
			
//			boolean[] bwImage = new boolean[IMG_HEIGHT * IMG_WIDTH];
			ArrayList<Integer[]> blackDots = new ArrayList<Integer[]>();
			for (int i = 0; i < IMG_WIDTH; i++) {
				for (int j = 0; j < IMG_HEIGHT; j++) {
//					int pos = (j * IMG_WIDTH + i) * 3;
					int pos = (j*IMG_WIDTH + i); // Y-Werte
//					pos += ( (j/2)  * IMG_WIDTH); // farbwerte Zeile
//					if(j%2 == 0) {
//						pos += i; // aktuelle Zeile
//					}
//					bwImage[pos] = (buf[pos] > 50);
					try {
						if(buf[pos] < 50)
							blackDots.add(new Integer[] {i,j});
					} catch(Exception e) {
						System.out.println("OutOfBounds:" + pos);
					}
				}
			} 
			System.out.println("Anzahl der schw. Punkte:" + blackDots.size());
			float m = regress.getSteigung(blackDots);
			System.out.println("Regressionsgerade-Steigung:" + m); 
			running = false;
		}
	}

		
	private static ImageProcessingAndroid imageProcessingAndroid;

	public static ImageProcessingAndroid getInstance() {
		if (imageProcessingAndroid == null)
			imageProcessingAndroid = new ImageProcessingAndroid();
		return imageProcessingAndroid;
	}

	public void newImage(int[] image) {
		// not used
	}

	public void close() {
		// TODO Auto-generated method stub

	}

	public void open() {
		// TODO Auto-generated method stub

	}

	public void setMotorControl(IMotorControl imca) {
		robot = imca;
	}

	public void onTouch(int location) {
		// TODO Auto-generated method stub

	}

	public void newImage(RoboCupColorImage image) {
		// TODO Auto-generated method stub
		
	}

	public void newImage(AbstractRoboCupImage image) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onRelease(int location) {
		// TODO Auto-generated method stub
		
	}

}
