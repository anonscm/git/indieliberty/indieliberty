package robocup.camera.tools;

import robocup.camera.AbstractRoboCupImage;

/** 2D-Kantenerkennung, Rest ist auch mit der Klasse robocup.camera.tools.filter.Filter moeglich
 * 
 * @author Mattis Pasch, Torsten Wylegala, Jasmin Mueller, Johannes Held
 *
 */
public class Kantenerkennung {

		// for "calculate2D" (2D)
	private static final int THRESHOLD2D = 25;
	private static final int MULTIPLIER2D = 255 / THRESHOLD2D;
	private static int[] op2D = { -1, 2, -1 };

	
	/* returns greyImage */
	public static int[][] calculate2D(AbstractRoboCupImage image, boolean vertical) {
		int w = image.getWidth();
		int h = image.getHeight();
		int[][] kantenbild = new int[w][h];

		int[][] greyImage = image.getGreyScaleImage().getData();

		int tmp;

		for (int y = 0; y < h; y++) {
			for (int x = 0; x < w; x++) {

				try {
					tmp = 0;

					if (vertical) {
						tmp += op2D[0] * greyImage[x][y - 1];
						tmp += op2D[1] * greyImage[x][y];
						tmp += op2D[2] * greyImage[x][y + 1];
					} else {
						tmp += op2D[0] * greyImage[x - 1][y];
						tmp += op2D[1] * greyImage[x][y];
						tmp += op2D[2] * greyImage[x + 1][y];
					}
					tmp /= 2;

					if (tmp < 0)
						tmp *= -1;
					// if(tmp > THRESHOLD2D) {
					// tmp = 255;
					// } else {
					// tmp = 0;
					// }
					tmp *= MULTIPLIER2D;
					if (tmp > 255) {
						tmp = 255;
					}
					kantenbild[x][y] = tmp;
				} catch (ArrayIndexOutOfBoundsException e) {
					kantenbild[x][y] = 0; // TODO: raender anders machen...
				}
			}
		}
		return kantenbild;
	}


}
