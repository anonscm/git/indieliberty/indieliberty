package robocup.camera.tools;

import robocup.camera.tools.images.GreyScale;


public class Thresholding {
	
	
	public static int otsu(GreyScale greyScale) {
		return otsu(greyScale.getData(), greyScale.getWidth(), greyScale.getHeight());
	}

	/**
	 * Returns Threshold for given data based on Otsu's Method
	 * 
	 * @param data
	 *            grayScaleImage as int[w][h] with positive values only
	 * @param w
	 *            width
	 * @param h
	 *            height
	 * @return threshold
	 */
	public static int otsu(int[][] data, int w, int h) {

		int[] hist = getHistogramm(data, w, h);

		int threshold = otsu(w, h, hist, 255);
		return threshold;
	}

	public static int otsuPartialImage(GreyScale greyScale, int threshold1) {
		return otsuPartialImage(greyScale.getData(), threshold1, greyScale.getWidth(), greyScale.getHeight());
	}
	
	/**
	 * Returns Threshold for given data based on Otsu's Method only for values under the specified threshold 
	 * 
	 * @param data
	 *            grayScaleImage as int[w][h] with positive values only
	 * @param threshold1 
	 * 				the threshold
	 * @param w
	 *            width
	 * @param h
	 *            height
	 * @return threshold
	 */
	public static int otsuPartialImage(int[][] data, int threshold1, int w, int h) {
	
		int[] hist = getPartialHistogramm(data, threshold1, w, h);

		
		int threshold = otsu(w, h, hist, threshold1-1);
		return threshold;
	}

	
	/** errechnet threshold auf Basis eines Histogramm hist der laenge maxValue
	 * @param w
	 * @param h
	 * @param hist Histogramm muss int[maxValue] sein..
	 * @param maxValue
	 * @return
	 */
	private static int otsu(int w, int h, int[] hist, int maxValue) {
			
		int size = 0; // menge der Pixel (wird dynamisch berechnet, damit auch Teilbilder genommen werden koennen)
		
		float sum = 0;
		for (int t = 0; t < maxValue; t++) {
			size += hist[t];
			sum += t * hist[t];
		}

		float sumB = 0;
		int backgroundWeight = 0;
		int foregroundWeight = 0;

		float maxVariance = 0;
		int threshold = 0;

		for (int t=0 ; t<256 ; t++) {
		   backgroundWeight += hist[t];               
		   if (backgroundWeight == 0) continue;

		   foregroundWeight = size - backgroundWeight;
		   if (foregroundWeight == 0) break;

		   sumB += (float) (t * hist[t]);

		   float meanBackground = sumB / backgroundWeight;
		   float meanForeground = (sum - sumB) / foregroundWeight;

		   float betweenClassVariance = (float) (backgroundWeight * foregroundWeight) * (meanBackground - meanForeground) * (meanBackground - meanForeground);

		   if (betweenClassVariance > maxVariance) {
		      maxVariance = betweenClassVariance;
		      threshold = t;
		   }
		}
		return threshold;

	}
	
	public static int[] getPartialHistogramm(int[][] data, int threshold1,
			int w, int h) {
		int[] hist = new int[threshold1]; // histogram of image data
		for (int y = 0; y < h; y++) {
			for (int x = 0; x < w; x++) {
				int tmp = data[x][y];
				//TODO: geht davon aus das die naechsten berge links vom 1. threshold sind
				if(tmp < threshold1) {
					hist[tmp]++;
				}
			}
		}
		return hist;
	}

	public static int[] getHistogramm(GreyScale greyScale) {
		return getHistogramm(greyScale.getData(), greyScale.getWidth(), greyScale.getHeight());
	}
	
	public static int[] getHistogramm(int[][] data, int w, int h) {
		
		int[] hist = new int[256]; // histogram of image data
		for (int y = 0; y < h; y++) {
			for (int x = 0; x < w; x++) {
				int tmp = data[x][y];
				hist[tmp]++;
			}
		}
		return hist;
	}

}
