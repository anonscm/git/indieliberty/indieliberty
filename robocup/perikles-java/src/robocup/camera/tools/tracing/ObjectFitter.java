package robocup.camera.tools.tracing;

import java.util.ArrayList;

import robocup.misc.interfaces.Function;

public class ObjectFitter implements Function{
	
	private AbstractTraceObject traceObject;
	
	float lineB;
	float lineM;
	float lineError;

	public ObjectFitter(AbstractTraceObject traceObject) {
		this.traceObject = traceObject;
	}
	
	// TODO: vllt sollte r mit gedrehtem bild ausgerechnet werden, da sonst senkrechte Geraden
	// einen sehr niedrigen r ausweisen 
	public void calcLine() {
		ArrayList<Integer[]> dots = traceObject.getDots();
		
		final int n = dots.size();
		
		int mittelx = 0;
		int mittely = 0;
		
		for(int i = 0; i < n; i++) {
			mittelx += dots.get(i)[0];
			mittely += dots.get(i)[1];
		}
		mittelx /= n;
		mittely /= n;
		
		// (!) Achtung werden im folgenden nicht durch n geteilt
		long sxy = 0;
		long sx2 = 0;
		
		// wird fuer korrelationskoeffizienten rxy gebraucht:
		long sy2 = 0;
		
		int x, y;
		int tmpX, tmpY;
		
		for(int i = 0; i < n; i++) {
			x = dots.get(i)[0];
			y = dots.get(i)[1];
			
			// werden mehrmals gebraucht:
			tmpX = (x - mittelx); 
			tmpY = (y - mittely);
			
			sxy += tmpX * tmpY;
			sx2 += tmpX * tmpX;
			
			sy2 += tmpY * tmpY;
		}		
		
		lineM = (float)sxy / (float)sx2;
		lineB = mittely - lineM*mittelx;
		// TODO: Square root rechnet mit doubles -> Problem?
	
		lineError = (float)sxy / (float)Math.sqrt((double)(sx2*sy2));
		
	}

	public void print() {
		// Achtung: entsprichen nicht der wirklchen Funktion, sondern sind angepasst
		System.out.println("y = " + -lineM + "x + " + (240 - lineB) + " r = " + lineError);
		System.out.println("x = " + (1/lineM) + "y + " + (-lineB/lineM));		
	}
	
	/** Format: x = InvM * y + InvB
	 * 
	 * @return
	 */
	public float getInvM() {
		return (1/lineM);
	}
	
	/** Format: x = InvM * y + InvB
	 * 
	 * @return
	 */
	public float getInvB() {
		return (-lineB/lineM);
	}

	public int f(int x) {
		
		return (int)(lineM*x + lineB);
	}
	
	
	
}
