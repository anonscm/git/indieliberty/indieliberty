package robocup.camera.tools.tracing;

import java.util.ArrayList;

public abstract class AbstractTraceObject {
	protected ArrayList<Integer[]> dots = new ArrayList<Integer[]>();

	
	int size() {
		return dots.size();
	}

	public ArrayList<Integer[]> getDots() {
		return dots;
	}

	/**
	 * 
	 * @return [0] minx, [1] maxx, [2] miny, [3] maxy
	 */
	public int[] getBounds() {
		// TODO: if needed, could be calculated on adding...

		int[] bounds = new int[4];
		bounds[0] = 100000;
		bounds[1] = 0;
		bounds[2] = 100000;
		bounds[3] = 0;

		Integer[] tmp;
		for (int i = 0; i < size(); i++) {
			tmp = dots.get(i);
			if (tmp[0] < bounds[0]) {
				bounds[0] = tmp[0];
			}
			if (tmp[0] > bounds[1]) {
				bounds[1] = tmp[0];
			}
			if (tmp[1] < bounds[2]) {
				bounds[2] = tmp[1];
			}
			if (tmp[1] > bounds[3]) {
				bounds[3] = tmp[1];
			}
		}
		return bounds;
	}
	
	
	
	abstract protected void add(int x, int y);
	
	@SuppressWarnings("unchecked")
	public static ArrayList<ObjectFitter> getObjectFitterForLines(ArrayList objects) {
		ArrayList<ObjectFitter> of = new ArrayList<ObjectFitter>();
		
		ObjectFitter tmp;
		for(int n = 0; n < objects.size(); n++) {
			tmp = new ObjectFitter((AbstractTraceObject)objects.get(n));
			tmp.calcLine();
			of.add(tmp);
		}
		return of;
	}
}
