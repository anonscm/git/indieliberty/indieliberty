package robocup.camera.tools.tracing;

import java.util.ArrayList;

/**  Liefert momentan noch ziemlichen Schwachsinn
 * 
 * @author Mattis Pasch, Torsten Wylegala, Jasmin Mueller, Johannes Held
 *
 */
public class CompareLines {

	private static final float THRESHOLD_M = 2f;
	private static final float THRESHOLD_B = 30f;
	ArrayList<ObjectFitter> objects;
	
	
	public CompareLines(ArrayList<ObjectFitter> objects) {
		this.objects = objects;
	}
	
	public void compare() {
		ObjectFitter o1, o2;
		for(int i = 0; i < objects.size(); i++) {
			for(int j = i+1; j < objects.size(); j++) {
				o1 = objects.get(i);
				o2 = objects.get(j);

				// normal:
//				if(almost(o1.lineM, o2.lineM, THRESHOLD_M)) {
//					if(almost(o1.lineB, o2.lineB, THRESHOLD_B)) {
//						System.out.println("Objekte " + i + " und " + j + " gehoeren zusammen");
//					} else {
//						System.out.println("Objekte " + i + " und " + j + " sind parallel");
//					}
//				}
				// inverse
				float m1 = o1.getInvM();
				float b1 = o1.getInvB();
				float m2 = o2.getInvM();
				float b2 = o2.getInvB();
				
				if(almostRel(m1, m2, THRESHOLD_M)) {
					
					// abstand:   scheint d = - (1/2m) + wurzel(1/(4 * m^2) + (b2 - b1)^2 )
					float deltaB = b2 - b1;
					float m = (m1 + m2) / 2f;
					//TODO: uses double -> problem?
					float d = - (1f / (2f* m) ) + (float)Math.sqrt( 1d/ (4d * m*m) + deltaB*deltaB );
					
					if(almostAbs(b1, b2, THRESHOLD_B)) {
						System.out.println("Objekte " + i + " und " + j + " gehoeren zusammen, abstand: " + d);
					} else {
						System.out.println("Objekte " + i + " und " + j + " sind parallel, abstand: " + d);
						
					}	
				}
			}
		}
	}

	private boolean almostRel(float f1, float f2, float threshold) {
		float relation = f1 / f2;
			
		return ( ( (1/threshold) < relation)  && (relation < (1*threshold)));		
	}
	
	private boolean almostAbs(float f1, float f2, float threshold) {
		return ( f1-threshold < f2)  && (f2 < (f1+threshold));
	}
	
		
}
