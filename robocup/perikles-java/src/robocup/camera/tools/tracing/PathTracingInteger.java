package robocup.camera.tools.tracing;

import java.util.ArrayList;
import java.util.Stack;


/** Does only work if int-values are excactly the same
 * 
 * @author Mattis Pasch
 *
 */
public class PathTracingInteger {
	
	public static final int minSize = 100;
	public static final int radius = 3;
	
	//public static final int tolerance = 15;
	
	int[][] bild;
	final int width;
	final int height;


	public ArrayList<TraceObject> objects = new ArrayList<TraceObject>();
	
	final private boolean[][] booleanBild;

	public void analyse() {
		int n = -1;
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {

				if (booleanBild[x][y]) {
					n++;
					objects.add(new TraceObject());
					traceStep(objects.get(n), x, y, bild[x][y]);
					if (objects.get(n).size() < minSize) {
						objects.remove(n);
						n--;
					}
				} 
			}
		}
	}

	private void traceStep(TraceObject to, int startx, int starty, int value) {

		Stack<int[]> stack = new Stack<int[]>();

		
		int[] current;

		int tmpx, tmpy;

		to.add(startx, starty);
		stack.push(new int[] { startx, starty});

		while (stack.size() > 0) {
			
			if(stack.size() > 1000000) {
				System.err.println("Image too complex for Tracing");
				return;
			}

			current = stack.pop();

			for (int sx = -radius; sx <= radius; sx++) {
				for (int sy = -radius; sy <= radius; sy++) {
					tmpx = current[0] + sx;
					tmpy = current[1] + sy;
					if ((tmpx > 0) && (tmpx < width) && (tmpy > 0)
							&& (tmpy < height)) {
						if( (booleanBild[tmpx][tmpy]) && (bild[tmpx][tmpy] == value) ) {
							stack.add(new int[] { tmpx, tmpy });
							to.add(tmpx, tmpy);
						}
					}
				}
			}
		}
	}


	public PathTracingInteger(int[][] bild, boolean[][] booleanBild, int width, int height) {
		this.bild = bild;
		this.width = width;
		this.height = height;
		this.booleanBild = booleanBild;
	}

	public class TraceObject extends AbstractTraceObject{
		
		protected void add(int x, int y) {
			dots.add(new Integer[] { x, y });
			// dieser punkt soll nicht mehr verwendet werden:
			booleanBild[x][y] = false;
		}
	}
}
