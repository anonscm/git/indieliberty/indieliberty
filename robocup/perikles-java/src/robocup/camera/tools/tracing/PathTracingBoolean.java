package robocup.camera.tools.tracing;

import java.util.ArrayList;
import java.util.Stack;

public class PathTracingBoolean {
	
	public static final int minSize = 80;
	public static final int radius = 2;
	boolean[][] bild;
	final int width;
	final int height;


	public ArrayList<TraceObject> objects = new ArrayList<TraceObject>();

	
	public void analyse() {
		int n = -1;
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {

				if (bild[x][y]) {
					n++;
					objects.add(new TraceObject());
					traceStep(objects.get(n), x, y);
					if (objects.get(n).size() < minSize) {
						objects.remove(n);
						n--;
					}
				}
			}
		}
	}

	private void traceStep(TraceObject to, int startx, int starty) {

		Stack<int[]> stack = new Stack<int[]>();

		int[] current;

		int tmpx, tmpy;

		to.add(startx, starty);
		stack.push(new int[] { startx, starty });

		while (stack.size() > 0) {

			if(stack.size() > 2000) {
				System.err.println("Image to complex for Tracing");
				return;
			}
			
			current = stack.pop();

			for (int sx = -radius; sx <= radius; sx++) {
				for (int sy = -radius; sy <= radius; sy++) {
//				for (int sy = radius; sy >= -radius; sy--) {
	
					tmpx = current[0] + sx;
					tmpy = current[1] + sy;
					if ((tmpx > 0) && (tmpx < width) && (tmpy > 0)
							&& (tmpy < height)) {
						if (bild[tmpx][tmpy]) {
							to.add(tmpx, tmpy);
							stack.add(new int[] { tmpx, tmpy });
						}
					}
				}
			}
		}
	}

	public PathTracingBoolean(boolean[][] bild, int width, int height) {
		this.bild = bild;
		this.width = width;
		this.height = height;
	}

	public class TraceObject extends AbstractTraceObject{
	
		protected void add(int x, int y) {
			dots.add(new Integer[] { x, y });
			// dieser punkt soll nicht mehr verwendet werden:
			bild[x][y] = false; 
		}
	}
}
