package robocup.camera.tools;

import robocup.camera.tools.images.GreyScale;
import robocup.camera.tools.images.RGBImage;

public class ColorDifference {

	public static boolean doLog = false;
	
	
	// TODO: remove static Thresholds
	private static final int minPixels = 2000;
	private static final int THRESHOLD = 100;


//	private IVictimListener victimListener;
	
	
	public boolean checkObstacle(RGBImage image, int color) {
		final int width = image.getWidth();
		final int height = image.getHeight();
		
		byte[] rgbImage = image.getBytes();
		
		int count = 0;

		int pos = 0;
		
		int diff;
		for(int y = 0; y < height; y++) {
			for(int x = 0; x < width; x++) {
				
				diff = 0;
				if(color == 0) {
					diff = UnsignedTypes.unsignedByteToInt(rgbImage[pos+1]) +	UnsignedTypes.unsignedByteToInt(rgbImage[pos + 2]);
				} else if(color == 1) {
					diff = UnsignedTypes.unsignedByteToInt(rgbImage[pos]) +	UnsignedTypes.unsignedByteToInt(rgbImage[pos + 2]);
				} else if(color == 2) {
					diff = UnsignedTypes.unsignedByteToInt(rgbImage[pos]) +	UnsignedTypes.unsignedByteToInt(rgbImage[pos + 1]);
				}
				diff /= 2;
				diff = UnsignedTypes.unsignedByteToInt(rgbImage[pos + color]) - diff;
				
				if(diff > THRESHOLD) {
					count++;
				}
				
				if(count > minPixels) {
					return true;
				}
				
				pos += 3;
			}
		}
		return false;
	}
	
	public GreyScale getColorDiffImage(RGBImage image, int color, boolean doImprove) {
		final int width = image.getWidth();
		final int height = image.getHeight();
		
		byte[] rgbImage = image.getBytes();
		
//		int count = 0;
		
		int[][] colorDiff = new int[width][height];
		
		int max = 0;
		
		int pos = 0;
		
//		int trennlinie = 0;
		
		int diff;
		for(int y = 0; y < height; y++) {
			for(int x = 0; x < width; x++) {
				
				diff = 0;
				if(color == 0) {
					diff = UnsignedTypes.unsignedByteToInt(rgbImage[pos+1]) +	UnsignedTypes.unsignedByteToInt(rgbImage[pos + 2]);
				} else if(color == 1) {
					diff = UnsignedTypes.unsignedByteToInt(rgbImage[pos]) +	UnsignedTypes.unsignedByteToInt(rgbImage[pos + 2]);
				} else if(color == 2) {
					diff = UnsignedTypes.unsignedByteToInt(rgbImage[pos]) +	UnsignedTypes.unsignedByteToInt(rgbImage[pos + 1]);
				}
				diff /= 2;
				diff = UnsignedTypes.unsignedByteToInt(rgbImage[pos + color]) - diff;
				if(diff < 0)
					diff = 0;
				
				colorDiff[x][y] = diff;
				
//				if(diff > THRESHOLD) {
//					count++;
//				}
				if(diff > max) {
					max = diff;
				}
//				
//				if(count < 1000) {
//					trennlinie = y;
//				}
				
				pos += 3;
			}
		}

//		log("Gruenes Opfer liegt unterhalb von " + trennlinie);

		
//		if(count > minGreen) {
//			victimListener.onVictim(IVictimListener.VICTIM_GREEN);
//			log("#### Green Victim: found " + count + " green pixels #####");
//		}
		
		if((doImprove) && (max < 200)) { // sonst unnoetiger rechenaufwand...
			float factor = 255 / max;

			log("Improving GreenDiff-Image with max = " + max +  ", factor: " + factor);
			for(int y = 0; y < height; y++) {
				for(int x = 0; x < width; x++) {
					colorDiff[x][y] = (int) (colorDiff[x][y] * factor);
				}
			}
		}
		
		return new GreyScale(colorDiff, width, height);
	}
	
//	public void setListener(IVictimListener victimListener) {
//		this.victimListener = victimListener;
//	}
	
	private static void log(String s) {
		if(doLog) {
			System.out.println(s);
		}
	}

}
