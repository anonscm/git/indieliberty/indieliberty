package robocup.camera.tools.images;

import java.util.ArrayList;

import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.graphics.PaletteData;
import org.eclipse.swt.widgets.Display;

import robocup.camera.color.Color;
import robocup.camera.tools.UnsignedTypes;
import robocup.camera.tools.tracing.AbstractTraceObject;
import robocup.camera.tools.tracing.ObjectFitter;
import robocup.misc.interfaces.Function;

/** 
 * Behandelt RGB-Bilder in Form eines 1D- byte-arrays mit der Größe w*h*3
 * 
 * @author Mattis Pasch, Torsten Wylegala, Jasmin Mueller, Johannes Held
 *
 */
public class RGBImage {
	private byte[] rgbImage;
	private final int width;
	private final int height;
	
	
	public RGBImage(byte[] rgbImage, int width, int height) {
		if(rgbImage == null) {
			this.rgbImage = new byte[width*height*3];
		} else {
			this.rgbImage = rgbImage;
		}
		this.width = width;
		this.height = height;
	}
	
	public void addFunction(Function function,
			Color color) {
		addFunction(function, color, 0, width, 0, height);
	}

	public void addFunction(Function function, Color color, final int minx,
			final int maxx, final int miny, final int maxy) {

		byte r = UnsignedTypes.intToUnsignedByte(color.r);
		byte g = UnsignedTypes.intToUnsignedByte(color.g);
		byte b = UnsignedTypes.intToUnsignedByte(color.b);

		for (int x = minx; x < maxx; x++) {
			int y = function.f(x);
			if ((miny <= y) && (y < maxy)) {

				int pos = (y * width + x) * 3;
				rgbImage[pos] = r;
				rgbImage[pos + 1] = g;
				rgbImage[pos + 2] = b;
			}
		}
	}
	
	/**
	 * Returns "greyScaleImage" of one Color of the supplied RGB-Image
	 * 
	 * @param rgbImage
	 * @param width
	 * @param height
	 * @param color
	 *            0: Red; 1: Green; 2: Blue
	 * @return "greyScaleImage" of one Color
	 */
	public int[][] getScaleImageOfColor(int color) {
		int[][] scaleImage = new int[width][height];
		int pos = color;
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {
				scaleImage[x][y] = rgbImage[pos];
				pos += 3;
			}
		}
		return scaleImage;
	}

	
	/** Bild runterrechnen durchschnitt aus 4px
	 * 
	 * @param buf 320x240
	 * @return 160x120
	 */
	public static byte[] rezise320to160(byte[] buf) {
		byte[] buf2 = new byte[160 * 120 * 3];

		int x;
		int a, b, c;
		for (b = 0; b < 120; b++) { // hoehe
			for (a = 0; a < 160; a++) { // breite
				for (c = 0; c < 3; c++) { // farbe
					x = UnsignedTypes
							.unsignedByteToInt(buf[(a * 2 + b * 320 * 2) * 3
									+ c]);
					x = x
							+ UnsignedTypes
									.unsignedByteToInt(buf[(a * 2 + 1 + b * 320 * 2)
											* 3 + c]);
					x = x
							+ UnsignedTypes
									.unsignedByteToInt(buf[(a * 2 + (b * 2 + 1) * 320)
											* 3 + c]);
					x = x
							+ UnsignedTypes
									.unsignedByteToInt(buf[(a * 2 + 1 + (b * 2 + 1) * 320)
											* 3 + c]);
					x = x / 4;
					buf2[(b * 160 + a) * 3 + c] = (byte) x;
				}
			}
		}
		return buf2;

	}

	/** Bild runterrechnen, nimmt einen von 4 pixeln (schneller als durchschnitt)
	 * 
	 * @param buf 320x240
	 * @return 160x120
	 */
	public static byte[] rezise320to160v2(byte[] buf) {
		byte[] buf2 = new byte[160 * 120 * 3];
		int a, b, c;
		for (b = 0; b < 120; b++) { // hoehe
			for (a = 0; a < 160; a++) { // breite
				for (c = 0; c < 3; c++) { // farbe
					buf2[(b * 160 + a) * 3 + c] = (byte) UnsignedTypes
							.unsignedByteToInt(buf[(a * 2 + b * 320 * 2) * 3
									+ c]);
				}
			}
		}
		return buf2;

	}
	
	@SuppressWarnings("unchecked")
	public void addTracingObjects(ArrayList objects) {
		Color c;
		Color cFunction = new Color(Color.WHITE, 255, 255, 255);

		AbstractTraceObject current;
		for (int i = 0; i < objects.size(); i++) {
			if (i % 3 == 0) {
				c = Color.blue;
			} else if (i % 3 == 1) {
				c = Color.green;
			} else {
				c = Color.red;
			}
			current = (AbstractTraceObject) objects.get(i);
			addDots(current.getDots(), c);
			System.out.println("Lineare Funktion des " + i + ". Objekts ("
					+ Color.getColorString(c.color) + "):");
			ObjectFitter fitter = new ObjectFitter(current);
			fitter.calcLine();
			fitter.print();
			int[] bounds = current.getBounds();
			addFunction(fitter, cFunction, bounds[0], bounds[1], bounds[2], bounds[3]);
		}
	}

	public void addDots(ArrayList<Integer[]> dots, Color color) {
		int pos;
		byte r = UnsignedTypes.intToUnsignedByte(color.r);
		byte g = UnsignedTypes.intToUnsignedByte(color.g);
		byte b = UnsignedTypes.intToUnsignedByte(color.b);
		for (int n = 0; n < dots.size(); n++) {
			pos = dots.get(n)[0] + dots.get(n)[1] * width;
			pos *= 3;
			rgbImage[pos] = r;
			rgbImage[pos + 1] = g;
			rgbImage[pos + 2] = b;
		}
	}
	
	
	public Image asSWTImage() {
		return asSWTImage(24);
	}
	
	
	public Image asSWTImage(int bpp) {
		PaletteData palette = new PaletteData(0xff0000, 0x00ff00, 0x0000ff);
		ImageData data = new ImageData(width, height, bpp, palette, width * 3,
				rgbImage);
		return new Image(Display.getDefault(), data);
	}

	/** may be used by implementations of this class
	 *  uses Y = 0.3*R + 0.59*G + 0.11*B
	 */
	public int[][] calculateGreyScale() {
		
		int[][] grayScaleImage = new int[width][height];
		int pos = 0;
		for(int y=0; y < height; y++) {
			for(int x = 0; x < width; x++) {

				grayScaleImage[x][y] = (int) (
										0.3f * (float)UnsignedTypes.unsignedByteToInt(rgbImage[pos])
									  +0.59f * (float)UnsignedTypes.unsignedByteToInt(rgbImage[pos + 1])
									  +0.11f * (float)UnsignedTypes.unsignedByteToInt(rgbImage[pos + 2]));
				
				pos += 3;
			}
		}
		return grayScaleImage;
	}
	
	/** may be used by implementations of this class
	 *  uses Y = 0.3*R + 0.59*G + 0.11*B in approximation
	 */
	public GreyScale calculateGreyScaleInteger() {
		
		int[][] grayScaleImage = new int[width][height];
		int pos = 0;
		for(int y=0; y < height; y++) {
			for(int x = 0; x < width; x++) {

				grayScaleImage[x][y] = (3 *UnsignedTypes.unsignedByteToInt(rgbImage[pos]) 
									 +  6 * UnsignedTypes.unsignedByteToInt(rgbImage[pos + 1])
									 +  UnsignedTypes.unsignedByteToInt(rgbImage[pos + 2]));
				grayScaleImage[x][y] /= 10;
				pos += 3;
			}
		}
		return new GreyScale(grayScaleImage, width, height);
	}
	
	/** Gibt nur eine Farbe des RGB Bildes zurueck...
	 * 
	 * @param rgbImage
	 * @param color 0: Rot, 1: Grün, 2: Blau
	 * @param width
	 * @param height
	 * @return
	 */
	public int[][] getColorAsGreyScale(int color) {
		int[][] grayScaleImage = new int[width][height];
		int pos = 0;
		for(int y=0; y < height; y++) {
			for(int x = 0; x < width; x++) {
				grayScaleImage[x][y] = (UnsignedTypes.unsignedByteToInt(rgbImage[pos + color]));
				pos += 3;
			}
		}
		return grayScaleImage;
	}
	
	public int getWidth() {
		return width;
	}
	
	public int getHeight() {
		return height;
	}
	
	public byte[] getBytes() {
		return rgbImage;
	}

	
	final static private byte valueof255 = UnsignedTypes.intToUnsignedByte(255);

	
	/**
	 * color may be Red: 0, Green: 1, Blue: 2
	 * @param y
	 * @param color
	 */
	public void addHorizontalLine(int y, int color) {
		int pos = 3*y*width;
		
		for(int x = 0; x < width; x++) {
			rgbImage[pos + 0] = (color == 0 ? valueof255 : 0);
			rgbImage[pos + 1] = (color == 1 ? valueof255 : 0);
			rgbImage[pos + 2] = (color == 2 ? valueof255 : 0);
			pos += 3;
		}
		
	}
	
	/**
	 * color may be Red: 0, Green: 1, Blue: 2
	 * @param y
	 * @param color
	 */
	public void addVerticalLine(int x, int color) {
		int pos = x * 3;
		
		for(int y = 0; y < height; y++) {
			rgbImage[pos + 0] = (color == 0 ? valueof255 : 0);
			rgbImage[pos + 1] = (color == 1 ? valueof255 : 0);
			rgbImage[pos + 2] = (color == 2 ? valueof255 : 0);
			pos += 3*width;
		}
		
	}
	
	
	/** colors 9 dots :)
	 * 
	 */
	public void addDot(int x, int y, int color) {
		
		
		
		for(int a = -1; a <= 1; a++) {
			for(int b = -1; b <= 1; b++) {
		
				try {
				int pos = ( (y+b) * width + (x+a)  )  * 3;
				
				rgbImage[pos + 0] = (color == 0 ? valueof255 : 0);
				rgbImage[pos + 1] = (color == 1 ? valueof255 : 0);
				rgbImage[pos + 2] = (color == 2 ? valueof255 : 0);
				} catch (ArrayIndexOutOfBoundsException e) {
					// ignore dots on border.. TODO: exception is unefficent
				}
			}
		}
			
	}
}
