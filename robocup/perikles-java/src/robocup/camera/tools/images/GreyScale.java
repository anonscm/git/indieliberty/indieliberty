package robocup.camera.tools.images;

import org.eclipse.swt.graphics.Image;

import robocup.camera.tools.Thresholding;
import robocup.tools.UnsignedTypes;

public class GreyScale {
	
	private int[][] greyScale;
	private final int width;
	private final int height;
	
	public GreyScale(int[][] greyScale, int width, int height) {
		this.greyScale = greyScale;
		this.width = width;
		this.height = height;
	}
	public BWImage convertToBWWithFixedThreshold(int thres) {
		boolean[][] bw = new boolean[width][height];
		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				if (greyScale[x][y] > thres)
					bw[x][y] = true;
				else
					bw[x][y] = false;
			}
		}
		return new BWImage(bw, width, height);
	}
	
	public BWImage convertToBW(int maximumBlack) {
		
		int THRESHOLD = Thresholding.otsu(greyScale, width, height);
		
		if(THRESHOLD > maximumBlack) {
			THRESHOLD = maximumBlack;
		}
		
		boolean[][] bw = new boolean[width][height];
		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				if (greyScale[x][y] > THRESHOLD)
					bw[x][y] = true;
				else
					bw[x][y] = false;
			}
		}
		return new BWImage(bw, width, height);
	}
	
	public RGBImage getAsRGB() {
		byte[] byteImage = new byte[width * height * 3];

		byte tmp;
		int pos = 0;
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {
				if(greyScale[x][y] >= 0) {
					tmp = UnsignedTypes.intToUnsignedByte(greyScale[x][y]);
				} else {
					tmp = UnsignedTypes.intToUnsignedByte(-greyScale[x][y]);
				}
				byteImage[pos + 0] = tmp;
				byteImage[pos + 1] = tmp;
				byteImage[pos + 2] = tmp;
				pos += 3;
			}
		}
		return new RGBImage(byteImage, width, height);
	}

	
	// TODO: unefficent (conversion to RGB and back)
	public Image asSWTImage() {
		return getAsRGB().asSWTImage();
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}

	public int[][] getData() {
		return greyScale;
	}
}
