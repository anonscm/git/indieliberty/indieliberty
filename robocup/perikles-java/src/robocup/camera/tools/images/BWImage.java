package robocup.camera.tools.images;

import robocup.camera.tools.UnsignedTypes;

public class BWImage {
	
	private boolean[][] bwImage;
	private final int width;
	private final int height;
	
	public BWImage(boolean[][] image, int width, int height) {
		bwImage = image;
		this.width = width;
		this.height = height;
	}
	
	public boolean[][] getBWImage() {
		return bwImage;
	}
	
	public RGBImage getAsRGB() {

		byte[] rgbBW = new byte[width * height * 3];

		byte white = UnsignedTypes.intToUnsignedByte(255);

		int pos = 0;
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {
				if (bwImage[x][y]) {
					rgbBW[pos] = white;
					rgbBW[pos + 1] = white;
					rgbBW[pos + 2] = white;
				} else {
					rgbBW[pos] = 0;
					rgbBW[pos + 1] = 0;
					rgbBW[pos + 2] = 0;
				}
				pos += 3;
			}
		}
		return new RGBImage(rgbBW, width, height);
	}
	
	/**
	 * May not do what you expect -> see algorithm
	 * 
	 * @param bw1
	 * @param bw2
	 * @param w
	 * @param h
	 */
	public static void subtract(boolean[][] bw1, boolean[][] bw2, int w, int h) {
		for (int y = 0; y < h; y++) {
			for (int x = 0; x < w; x++) {
				if (bw2[x][y])
					bw1[x][y] = false;
			}
		}
	}
	
	public boolean[][] getInverse() {
		boolean[][] newImage = new boolean[width][height];
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {
				newImage[x][y] = !(bwImage[x][y]);
			}
		}
		return newImage;
	}

	public int getHeight() {
		return height;
	}
	
	public int getWidth() {
		return width;
	}
}
