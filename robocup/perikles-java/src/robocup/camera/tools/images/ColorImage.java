package robocup.camera.tools.images;

import java.util.ArrayList;

import robocup.camera.RoboCupColorImage;
import robocup.camera.color.Color;
import robocup.camera.color.ColorMap;
import robocup.camera.tools.UnsignedTypes;
import robocup.camera.tools.regression.RegressionsGerade;
import robocup.camera.tools.regression.RegressionsKreis;
import robocup.misc.interfaces.Function;

public class ColorImage {
	private static final int abstandObenAchse = 0;
	public static byte[] createImage(ColorMap cMap, int[] buf) {
		int imageSize = 320 * 240;

		byte[] image = new byte[imageSize * 3];

		for (int i = 0; i < imageSize; i++) {
			// System.out.println("createImage " + i + " Index: " + buf[i]);
			if ((buf[i] >= 0) && (buf[i] < cMap.numberOfColors())) {
				image[i * 3 + 0] = UnsignedTypes.intToUnsignedByte(cMap
						.getColor(buf[i]).r);
				image[i * 3 + 1] = UnsignedTypes.intToUnsignedByte(cMap
						.getColor(buf[i]).g);
				image[i * 3 + 2] = UnsignedTypes.intToUnsignedByte(cMap
						.getColor(buf[i]).b);

			}
		}
		return image;
	}
	public static void addLinearRegression(RoboCupColorImage image) {

		final int abstandObenAchse = image.getHeight();
		RegressionsGerade regG = new RegressionsGerade(abstandObenAchse, image
				.getWidth());

		float m = regG.getSteigung(image.getBlackDots());

		System.out.println("Steigung der Gerade: " + m);

		drawFunction(image, regG, image.getcMap()
				.getIndexWithColor(Color.GREEN));

	}

	public static void addCirclularRegression(RoboCupColorImage image) {
		// byte[] image;
		int imgWidth = image.getWidth();

		RegressionsKreis regK = new RegressionsKreis(abstandObenAchse,
				imgWidth);

		ArrayList<Integer[]> blackDots = image.getBlackDots();

		int r;
		try {
			r = regK.getRadius(blackDots);
		} catch (ArithmeticException ae) {
			System.err.println("Arithmetic Exception during calc. (chernov)");
			r = 0;
		}

		System.out.println("radius Chernov (rot): " + r);

		drawFunction(image, regK, image.getcMap().getIndexWithColor(Color.RED));

		try {
			r = regK.getRadiusKasa(image.getBlackDotsBorder());
		} catch (ArithmeticException ae) {
			System.err
					.println("Arithmetic Exception during calc. (kasa - borderBlackDots)");
			r = 0;
		}

		System.out.println("radius kasa-nur Rand (Gruen): " + r);

		drawFunction(image, regK, image.getcMap()
				.getIndexWithColor(Color.GREEN));

		try {
			r = regK.getRadiusKasa(blackDots);
		} catch (ArithmeticException e) {
			System.err.println("Error during calc of Kasa-Method.");
			r = 0;
		}
		System.out.println("radius KASA (blau): " + r);

		drawFunction(image, regK, image.getcMap().getIndexWithColor(Color.BLUE));
		// regK.printDistanceSums(blackDots);

		image.changedColorImage();

	}

	private static void drawFunction(RoboCupColorImage image,
			Function function, int color) {
		int w = image.getWidth();
		int h = image.getHeight();
		// Integer[] colorImage = image.getColorImage();
		for (int x = 0; x < w; x++) {
			int y = function.f(x);
			if ((0 <= y) && (y < h)) {
				int pos = (y * w + x);
				image.setPixelColor(pos, color);
			}
		}
	}
	
	public static void removeSingleDots(RoboCupColorImage image) {
		Integer[] colorImage = image.getColorImage();
		ColorMap cMap = image.getcMap();

		int w = image.getWidth();
		int h = image.getHeight();

		final int numberOfUsedColors = 4;

		for (int x = 1; x < w - 1; x++) {
			for (int y = 1; y < h - 1; y++) {
				int pos = x + y * w;

				int[] colors = new int[numberOfUsedColors];
				for (int i = 0; i < numberOfUsedColors; i++)
					colors[i] = 0;

				int[] dots = new int[9];

				dots[0] = cMap.getColor(colorImage[pos - w - 1]).color;
				dots[1] = cMap.getColor(colorImage[pos - w - 0]).color;
				dots[2] = cMap.getColor(colorImage[pos - w + 1]).color;

				dots[3] = cMap.getColor(colorImage[pos - 1]).color;
				dots[4] = cMap.getColor(colorImage[pos + 0]).color;
				dots[5] = cMap.getColor(colorImage[pos + 1]).color;

				dots[6] = cMap.getColor(colorImage[pos + w - 1]).color;
				dots[7] = cMap.getColor(colorImage[pos + w - 0]).color;
				dots[8] = cMap.getColor(colorImage[pos + w + 1]).color;

				for (int i = 0; i < 9; i++) {
					if (dots[i] > numberOfUsedColors)
						dots[i] = Color.WHITE;
					colors[dots[i]]++;
				}

				for (int i = 0; i < numberOfUsedColors; i++) {
					if (colors[i] >= 6)
						colorImage[pos] = cMap.getIndexWithColor(i);
				}

			}
		}
		image.changedColorImage();
	}
	static byte[] getImageFromColorPic(RoboCupColorImage image) {
		Integer[] colorImage = image.getColorImage();
		int w = image.getWidth();
		int h = image.getHeight();
		ColorMap cMap = image.getcMap();

		byte[] out = new byte[w * h * 3];
		for (int i = 0; i < w; i++) {
			for (int j = 0; j < h; j++) {
				int pos = (j * w + i) * 3;

				int tmp = cMap.getColor(colorImage[(j * w + i)]).color;
				if (tmp == Color.BLACK) {
					out[pos] = (byte) 0;
					out[pos + 1] = (byte) 0;
					out[pos + 2] = (byte) 0;
				} else if (tmp == Color.WHITE) {
					out[pos] = (byte) 255;
					out[pos + 1] = (byte) 255;
					out[pos + 2] = (byte) 255;
				} else if (tmp == Color.GREEN) {
					out[pos] = (byte) 0;
					out[pos + 1] = (byte) 255;
					out[pos + 2] = (byte) 0;
				} else if (tmp == Color.SILVER) {
					out[pos] = (byte) 0;
					out[pos + 1] = (byte) 0;
					out[pos + 2] = (byte) 255;
				}
			}
		}
		return out;
	}
}
