package robocup.camera.tools;

import robocup.camera.AbstractRoboCupImage;
import robocup.camera.tools.images.BWImage;
import robocup.camera.tools.images.GreyScale;
import robocup.camera.tools.images.RGBImage;
import robocup.camera.tools.tracing.AbstractTraceObject;
import robocup.camera.tools.tracing.CompareLines;
import robocup.camera.tools.tracing.PathTracingBoolean;
import robocup.camera.tools.tracing.PathTracingInteger;

public class ImageTools {

	final static byte valueOf255 = UnsignedTypes.intToUnsignedByte(255);

	
	public static void addPathAnalysis(RGBImage pathImage,
			boolean[][] blackWhite, int w, int h) {
		PathTracingBoolean pathTracing = new PathTracingBoolean(blackWhite, w,
				h);
		pathTracing.analyse();
		pathImage.addTracingObjects(pathTracing.objects);
	}
	
	public static void addPathAnalysisGreyScale(RGBImage pathImage, GreyScale greyScale, BWImage bwImage) {
		PathTracingInteger pathTracing = new PathTracingInteger(greyScale.getData(), bwImage.getBWImage(), greyScale.getWidth(), greyScale.getHeight());
		pathTracing.analyse();
		pathImage.addTracingObjects(pathTracing.objects);
		CompareLines cl = new CompareLines(AbstractTraceObject.getObjectFitterForLines(pathTracing.objects));
		cl.compare();
	}


	public static int[][] combineXandY(int[][] sobelX, int[][] sobelY, int w,
			int h) {
		int[][] combined = new int[w][h];
		for (int y = 0; y < h; y++) {
			for (int x = 0; x < w; x++) {
				combined[x][y] = (int) Math.sqrt(sobelX[x][y] * sobelX[x][y]
						+ sobelY[x][y] * sobelY[x][y]);
			}
		}
		return combined;
	}

	public static int[][] getAngles(int[][] iX, int[][] iY, int w, int h) {
		int[][] angles = new int[w][h];
		for (int y = 0; y < h; y++) {
			for (int x = 0; x < w; x++) {
				if (iX[x][y] == 0) {
					if (iY[x][y] == 0) {
						angles[x][y] = 0;
					} else {
						angles[x][y] = 90;
					}
				} else {
					angles[x][y] = (int) Math.toDegrees(Math.tan(iY[x][y]
							/ iX[x][y]));
				}
			}
		}
		return angles;
	}

	public static int[][] getAngles(int[][] iX, int[][] iY,
			boolean[][] isBorder, int w, int h) {
		int[][] angles = new int[w][h];
		for (int y = 0; y < h; y++) {
			for (int x = 0; x < w; x++) {
				if (isBorder[x][y]) {
					if (iX[x][y] == 0) {
						if (iY[x][y] == 0) {
							angles[x][y] = 0;
						} else {
							angles[x][y] = 90;
						}
					} else {
						angles[x][y] = (int) Math.toDegrees(Math.atan(iY[x][y]
								/ iX[x][y]));
					}
				} else {
					angles[x][y] = 255;
				}
			}
		}
		return angles;
	}

	/**
	 * rundet, was der wikipediaseite ueber den canny-algorithmus zufolge ligitim
	 * ist, da es eh nur 4 richtungen gibt ;)
	 * iX und iY MUESSEN auch NEGATIVE werte enthalten!!!
	 * 
	 * @param iX 
	 * @param iY
	 * @param isBorder
	 * @param w
	 * @param h
	 * @return int[][] angles welcher die werte 0, 45, 90 und 135 enthalten kann
	 */
	public static GreyScale getAnglesRound(int[][] iX, int[][] iY,
			boolean[][] isBorder, int w, int h) {
		int[][] angles = new int[w][h];
		for (int y = 0; y < h; y++) {
			for (int x = 0; x < w; x++) {
				if (isBorder[x][y]) {
					if (iX[x][y] == 0) {
						if (iY[x][y] == 0) {
							angles[x][y] = 0;
						} else {
							angles[x][y] = 90;
						}
					} else if(iY[x][y] == 0) { // schneller machen ;)
						angles[x][y] = 0;
					} else {

						float rel = (float) iY[x][y] / (float) iX[x][y];
						if (rel > 0) {
							if (rel < 0.414f) {
								angles[x][y] = 0;
							} else if (rel < 2.414f) {
								angles[x][y] = 45;
							} else {
								angles[x][y] = 90;
							}
						} else {
							if (rel > -0.414f) {
								angles[x][y] = 0;
							} else if (rel > -2.414f) {
								angles[x][y] = 135;
							} else {
								angles[x][y] = 90;
							}
						}
					}
				} else {
					angles[x][y] = 255;
				}
			}
		}
		return new GreyScale(angles, w, h);
	}
	
	/**
	 * TODO: bei gleichen werten wird eins von beidem genommen -> nicht korrekt aber egal!?
	 * @param image
	 * @return
	 */
	public static byte[] getRGBMaxImage(AbstractRoboCupImage image) {
		int w = image.getWidth();
		int h = image.getHeight();
		int size = w*h*3;
		
		byte[] maxRGB = new byte[size];
		
		byte[] original = image.getRGBImage().getBytes();
		
		int v1, v2, v3, max;
		for(int i = 0; i < size; i+=3) {
			v1 = UnsignedTypes.unsignedByteToInt(original[i]);
			v2 = UnsignedTypes.unsignedByteToInt(original[i+1]);
			v3 = UnsignedTypes.unsignedByteToInt(original[i+2]);
			if(v1 > v2) {
				if(v1 > v3) {
					max = 0;
				} else {
					max = 2;
				}
			} else {
				if(v2 > v3) {
					max = 1;
				} else {
					max = 2;
				}
			}
			//TODO: geht von default wert 0 aus
			maxRGB[i+max] = original[i+max];
		
		}
		
		return maxRGB;
	}
	
	//TODO: unabhaenig von der anzahl der thresholds machen
	public static GreyScale splitImage(GreyScale greyScale, int thres1, int thres2) {
			
		int[][] greyData = greyScale.getData(); 
		final int h = greyScale.getHeight();
		final int w = greyScale.getWidth();
		
		int[][] splitImage = new int[w][h];
		
		int lowerThres;
		int higherThres;
		if(thres1 > thres2) {
			lowerThres = thres2;
			higherThres = thres1;
		} else {
			lowerThres = thres1;
			higherThres = thres2;
		}
		
		int current;
		for (int y = 0; y < h; y++) {
			for (int x = 0; x < w; x++) {
				current = greyData[x][y];
				if(current > higherThres) {
					splitImage[x][y] = 255;
				} else if(current < lowerThres) {
					splitImage[x][y] = 0;
				} else {
					splitImage[x][y] = 128;
				}
			}
		}
		return new GreyScale(splitImage, w, h);
	}
	

	public static void printImage(BWImage image) {
		boolean[][] data = image.getBWImage();
		
		for(int y = 0; y < image.getHeight(); y++) {
			String line = "";
			for(int x = 0; x < image.getWidth(); x++) {
				line += (data[x][y] ? 'X' : 'O'); 
			}
			System.out.println(line);
		}
	}

}