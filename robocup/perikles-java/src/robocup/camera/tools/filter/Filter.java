package robocup.camera.tools.filter;

import robocup.camera.tools.images.GreyScale;

/** Applies one of the filters included to a greyScale Image 
 * 
 * @author Mattis Pasch, Torsten Wylegala, Jasmin Mueller, Johannes Held
 *
 */
public abstract class Filter {

	/** applies Filter
	 * 
	 * @param filter
	 * @param greyImage greyScale-Image which is NOT changed
	 * @param w
	 * @param h
	 * @return new int[][] result
	 */
	public static int[][] apply(Filter filter, GreyScale greyImage) {
		return apply(filter, greyImage.getData(), greyImage.getWidth(), greyImage.getHeight(), true);
	}
	
	/** applies Filter
	 * 
	 * @param filter
	 * @param greyImage greyScale-Image which is NOT changed
	 * @param w
	 * @param h
	 * @param absoluteValues true: values < 0 are *(-1), false: result contains negative values
	 * @return new int[][] result
	 */
	public static int[][] apply(Filter filter, GreyScale greyImage, boolean absoluteValues) {
		return apply(filter, greyImage.getData(), greyImage.getWidth(), greyImage.getHeight(), absoluteValues);
	}
	
	
	
	/** applies Filter
	 * 
	 * @param filter
	 * @param greyImage greyScale-Image which is NOT changed
	 * @param w
	 * @param h
	 * @param absoluteValues true: values < 0 are *(-1), false: result contains negative values
	 * @return new int[][] result
	 */
	public static int[][] apply(Filter filter, int[][] greyImage, int w, int h, boolean absoluteValues) {
			int[][] mask = filter.getFilter();
			
			int[][] result = new int[w][h];
			int tmp;

			for (int y = 1; y < h - 1; y++) {
				for (int x = 1; x < w - 1; x++) {

					tmp = 0;

					// 1-3
					tmp += mask[0][0] * greyImage[x - 1][y - 1];
					tmp += mask[0][1] * greyImage[x][y - 1];
					tmp += mask[0][2] * greyImage[x + 1][y - 1];
					// 4-6
					tmp += mask[1][0] * greyImage[x - 1][y];
					tmp += mask[1][1] * greyImage[x][y];
					tmp += mask[1][2] * greyImage[x + 1][y];
					// 7-9
					tmp += mask[2][0] * greyImage[x - 1][y + 1];
					tmp += mask[2][1] * greyImage[x][y + 1];
					tmp += mask[2][2] * greyImage[x + 1][y + 1];

					tmp /= filter.divider();

					if( (absoluteValues) && (tmp < 0) ){
						tmp *= -1;
					}
					if (tmp > 255) {
						tmp = 255;
					}
					result[x][y] = tmp;

				}
			}
			return result;
	}
	
	public abstract int[][] getFilter();
	public abstract int size();
	public abstract int divider();
	
	/** Rauschfilter von: wikipedia / Canny-Algorithmus
	 * 
	 * @author Mattis Pasch, Torsten Wylegala, Jasmin Mueller, Johannes Held
	 *
	 */
	public static class Rauschfilter extends Filter {
		
		private static final int[][] cannyMask = { {1,2,1} , {2,4,2}, {1,2,1} };
		private static final int cannyDivider = 16;
			
		@Override
		public int divider() {
			return cannyDivider;
		}

		@Override
		public int[][] getFilter() {
			return cannyMask;
		}

		@Override
		public int size() {
			return 3;
		}
		
	}
	/** Sobeloperator in X-Richtung 
	 * 
	 * @author Mattis Pasch, Torsten Wylegala, Jasmin Mueller, Johannes Held
	 *
	 */
	public static class SobelX extends Filter {

		private static final int[][] mask = { {1,0,-1} , {2,0,-2}, {1,0,-1} };
		private static final int divider = 4;
		
		@Override
		public int divider() {
			return divider;
		}

		@Override
		public int[][] getFilter() {
			return mask;
		}

		@Override
		public int size() {
			return 3;
		}
		
	}
	
	/** Sobeloperator in Y-Richtung 
	 * 
	 * @author Mattis Pasch, Torsten Wylegala, Jasmin Mueller, Johannes Held
	 *
	 */
	public static class SobelY extends Filter {

		private static final int[][] mask = { {1,2,1} , {0,0,0}, { -1, -2, -1} };
		private static final int divider = 4;
		
		@Override
		public int divider() {
			return divider;
		}

		@Override
		public int[][] getFilter() {
			return mask;
		}

		@Override
		public int size() {
			return 3;
		}
		
	}
	
	public static class Kantenerkennung extends Filter {

		private static final int[][] mask = { { -1, -1, -1 }, { -1, 8, -1 },
				{ -1, -1, -1 } };
		private static final int divider = 2;
		
		
		@Override
		public int divider() {
			return divider;
		}

		@Override
		public int[][] getFilter() {
			return mask;
		}

		@Override
		public int size() {
			return 3;
		}
		
	}


}
