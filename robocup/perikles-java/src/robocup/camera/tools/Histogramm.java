package robocup.camera.tools;

import robocup.camera.tools.images.GreyScale;

public class Histogramm {
	//TODO: allgemein programmieren
	public static GreyScale getHistogrammAsGreyScaleImage(int[] hist, int width, int height) {
		
		
		int[][] image = new int[width][height];
		
		//maximum holen:
		int max = 0;
		for (int i = 0; i < hist.length; i++) {
			if(hist[i] > max) { 
				max = hist[i];
			}
		}
		
		double k = (double)height / (double)max;
		
		for(int i = 0; i < hist.length; i++) {
			int value = (int) Math.floor(hist[i] * k);
			
			for(int j=1; j <= value; j++) {
				image[i][height - j] = 255;
			}
		}
		
		
		
		return new GreyScale(image, width, height);
	}

	
	public static void addThreshold(GreyScale histogramm, int thres) {
		for(int y = 0; y < histogramm.getHeight(); y++) {
			histogramm.getData()[thres][y] = 100;
		}
	}
}
