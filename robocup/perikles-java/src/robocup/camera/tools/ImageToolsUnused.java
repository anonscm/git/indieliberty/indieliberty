package robocup.camera.tools;

import java.util.ArrayList;

import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.graphics.PaletteData;
import org.eclipse.swt.widgets.Display;

import robocup.camera.color.Color;
import robocup.camera.color.HSV;


public class ImageToolsUnused {
	

	ImageData map2imgData(int[][] map) {
		PaletteData palette = new PaletteData(0xFF, 0xFF00, 0xFF0000);
		ImageData imageData = new ImageData(160, 120, 24, palette);
		for (int x = 0; x < 160; x++) {
			for (int y = 0; y < 120; y++) {
				switch (map[x][y]) {
				case Color.GREEN:
					imageData.setPixel(x, y, 0x00FF00);
					break;
				case Color.BLACK:
					imageData.setPixel(x, y, 0x00);
					break;
				case Color.WHITE:
					imageData.setPixel(x, y, 0xFFFFFF);
					break;
				case Color.SILVER:
					imageData.setPixel(x, y, 0xFF);
					break;
				}
			}
		}
		return imageData;
	}

	Image map2img(int[][] map) {
		return new Image(Display.getDefault(), map2imgData(map));
	}

	public int[][] map(int[] pic) {
		int[][] map = new int[160][120];
		for (int y = 0; y < 120; y++) {
			for (int x = 0; x < 160; x++) {

				int[] hsv = new int[3];
				int pos = (y * 160 + x) * 3;

				int r = pic[pos];
				int g = pic[pos + 1];
				int b = pic[pos + 2];

				HSV.rgb2hsv(r, g, b, hsv);

				if (hsv[1] > HSV.maxSaturation)
					HSV.maxSaturation = hsv[1];

				if (hsv[2] > HSV.maxValue)
					HSV.maxValue = hsv[2];

				/*
				 * if(hsv[2] > 50) map[x][y] = WHITE; else if(hsv[1] < 25)
				 * map[x][y] = BLACK; else map[x][y] = GREEN;
				 */
				if ((hsv[1] > 50) && (hsv[0] > 14) && (hsv[0] < 90)) {
					map[x][y] = Color.GREEN;
				} else {
					if (hsv[2] > 50)
						map[x][y] = Color.WHITE;
					else
						map[x][y] = Color.BLACK;
				}
			}
		}
		return map;
	}
	
	
	public static void printBlackDots(ArrayList<Integer[]> blackDots) {
		System.out.println("=======BLACKDOTS:=========");
		for (int i = 0; i < blackDots.size(); i++) {
			System.out.println(i + ") x: " + blackDots.get(i)[0] + ", y: "
					+ blackDots.get(i)[1]);
		}
		System.out.println("==========================");
	}

	
}
