package robocup.camera.tools;

/** Interessantes Konzept, fuer unsere Anwendung aber viel zu rechenintensiv 
 * 
 * @author Mattis Pasch, Torsten Wylegala, Jasmin Mueller, Johannes Held
 *
 */
public class HoughTransformation {
	
	/** HoughTransformation ausfuehren: Achtung Ausgabe ist gut ausgebbar, verarbeitung schwierig!
	 * @param bwImage
	 * @param w
	 * @param h
	 * @returns Hough-Akkumulation, damit ausgebbar auf w und h genormt
	 */
	public static int[][] apply(boolean[][] bwImage, int w, int h) {
		int max_d = (int) Math.ceil(Math.sqrt( (h/2) * (h/2) + (w/2) * (w/2)));
	//	int min_d = -max_d;
		
		final int resultWidth = w;
		final int resultHeight = h;
		
		float alphaFactor = (float)resultWidth / (float)Math.PI;
		float dFactor = (float)resultHeight / (float)(max_d * 2);
		
		
		int[][] akkumulation = new int[resultWidth][resultHeight];

		int d, alpha;
		for(int y = 0; y < h; y++) {
			for(int x = 0; x < w; x++) {
				if(bwImage[x][y]) {
					int x0 = x - w / 2;
					int y0 = y - h / 2;
					for(alpha = 0; alpha < resultWidth; alpha ++) {
						d = (int) (x0 * Math.cos((float)alpha / alphaFactor) + y0 * Math.sin((float)alpha / alphaFactor));
						d += max_d;
						d = (int) ((float)d * dFactor);
						// raender sind durch rundungen nicht abgedeckt:
						if(d == 240) { 
							d = 239;
						}
						try {
						akkumulation[alpha][(int)(d*dFactor)]+= 1;
						} catch(ArrayIndexOutOfBoundsException e) {
							e.printStackTrace();
						}
					}
				}
			}
		}
		
		return akkumulation;
	}

}
