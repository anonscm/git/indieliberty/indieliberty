package robocup.camera.tools.regression;

import java.util.ArrayList;

import robocup.misc.interfaces.Function;

public class RegressionsKreis implements Function {
	private final int abstandObenAchse;
	private final int imgWidth;
	private final int achseSeite;
	
	
	private long radius;
	
	public RegressionsKreis(int abstandObenAchse, int imgWidth) {
		this.abstandObenAchse = abstandObenAchse;
		this.imgWidth = imgWidth;
		achseSeite = this.imgWidth / 2;
	}
	
		
	/** Kasa Method - funktioniert, aber probleme wg. Tendenz zu kleinen Kreisen
	 * siehe: http://www.math.uab.edu/~chernov/cl/book.pdf
	 * 
	 * @param blackDots
	 * @return
	 */
	public int getRadiusKasa(ArrayList<Integer[]> blackDots) {
	
		final int n = blackDots.size();
		
		long sumx2 = 0;
		long sumx3 = 0;
		long sumy2x = 0;
		
		int x;
		int y;
		
		for(int i = 0; i < n; i++) {
			x = blackDots.get(i)[0] - achseSeite;
			y = abstandObenAchse - blackDots.get(i)[1];
//			System.out.println("i: " + i + " X: " + x + " Y: " + y); 
			sumx2 += x * x;
			sumx3 += x*x*x;
			sumy2x += y*y*x;
		}
		if(sumx2 == 0) {
			radius = 0;
			throw new ArithmeticException("/ by zero");
		}
		radius = (sumx3+sumy2x) /  (2*sumx2);
		
		
		return (int)radius;
	}

	/** Chernov-Ososkov - TODO: Probleme... - warum? vllt weil vorfaktor nicht mehr stimmt wegen festgelegtem mittelpunkt?
	 * 
	 * @param blackDots
	 * @return
	 */
	public int getRadius(ArrayList<Integer[]> blackDots) {
		
		final int n = blackDots.size();
		
		//def: z := x^2 + y^2
		
		long sumz2 = 0;
		long sumxz = 0;
		
		int x;
		int y;
		
		
		
		for(int i = 0; i < n; i++) {
			x = blackDots.get(i)[0] - achseSeite;
			y = abstandObenAchse - blackDots.get(i)[1];
//			System.out.println("i: " + i + " X: " + x + " Y: " + y); 
			int z = x*x + y*y;
			sumz2 += z * z;
			sumxz += x * z;
		}
		
//		System.out.println("sumz2: " + sumz2 + "  2*sumxz: " + (2*sumxz) );
		
		if(sumxz == 0) {
			radius = 0;
			throw new ArithmeticException("/ by zero");
		}
		radius = (sumz2) / (2*sumxz) ;
		
		
		
		return (int)radius;
	}

	public int f(int x) throws ArithmeticException{
		int y = 0;
		
		//remap to 0/0
		x = x - achseSeite;
				
		// (x-r)² + y² - r² = 0
		// y = sqrt[  r² - (x-r)²  ]
		// TODO: wird durch double sqrt auf manchen systemen seeeehr langsam!
		
		y = Math.round((float) Math.sqrt( (radius*radius) - (x-radius)*(x-radius) ) );
		
		// remap to image
		y = abstandObenAchse - y;
		
		return y;
	}
}
