package robocup.camera.tools.regression;

import java.util.ArrayList;

import robocup.misc.interfaces.Function;

public class RegressionsKreisV2 implements Function {

	
	
	private long radius;
	
	/** Kasa Method - funktioniert, aber probleme wg. Tendenz zu kleinen Kreisen
	 * siehe: http://www.math.uab.edu/~chernov/cl/book.pdf
	 * 
	 * @param blackDots
	 * @return
	 */
	public int getRadiusKasa(ArrayList<Integer[]> blackDots) {
	
		final int n = blackDots.size();
		
		long sumx2 = 0;
		long sumx3 = 0;
		long sumy2x = 0;
		
		int x;
		int y;
		
		for(int i = 0; i < n; i++) {
			x = blackDots.get(i)[0];
			y = blackDots.get(i)[1];
//			System.out.println("i: " + i + " X: " + x + " Y: " + y); 
			sumx2 += x * x;
			sumx3 += x*x*x;
			sumy2x += y*y*x;
		}
		if(sumx2 == 0) {
			radius = 0;
			throw new ArithmeticException("/ by zero");
		}
		radius = (sumx3+sumy2x) /  (2*sumx2);
		
		
		return (int)radius;
	}

	/** Chernov-Ososkov - TODO: Probleme... - warum? vllt weil vorfaktor nicht mehr stimmt wegen festgelegtem mittelpunkt?
	 * 
	 * @param blackDots
	 * @return
	 */
	public int getRadius(ArrayList<Integer[]> blackDots) {
		
		final int n = blackDots.size();
		
		//def: z := x^2 + y^2
		
		long sumz2 = 0;
		long sumxz = 0;
		
		int x;
		int y;
		
		
		
		for(int i = 0; i < n; i++) {
			x = blackDots.get(i)[0] ;
			y = blackDots.get(i)[1];
//			System.out.println("i: " + i + " X: " + x + " Y: " + y); 
			int z = x*x + y*y;
			sumz2 += z * z;
			sumxz += x * z;
		}
		
//		System.out.println("sumz2: " + sumz2 + "  2*sumxz: " + (2*sumxz) );
		
		if(sumxz == 0) {
			radius = 0;
			throw new ArithmeticException("/ by zero");
		}
		radius = (sumz2) / (2*sumxz) ;
		
		
		
		return (int)radius;
	}

	public int f(int x) throws ArithmeticException{
		int y = 0;
		
				
		// (x-r)² + y² - r² = 0
		// y = sqrt[  r² - (x-r)²  ]
		// TODO: wird durch double sqrt auf manchen systemen seeeehr langsam!
		
		y = Math.round((float) Math.sqrt( (radius*radius) - (x-radius)*(x-radius) ) );
		
		return y;
	}
}
