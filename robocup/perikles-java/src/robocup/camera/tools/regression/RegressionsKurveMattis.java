/* 
 *    0/0
 *   	  X--------------------------|                -----------		-> +x-Achse
 * 		  |							 |					^
 * 		  |							 |					|
 * 		  |							X|					
 * 		  |					   X	 |				AbstandAchseBildrand
 * 		  |					X		 |
 * 		  |				  X			 |
 * 		  |				X			 |
 * 		  |__________________________|					|
 * 														v
 * 		  	X---------------------X    <- (Rad-)Achse	  -------------
 * 			 		  
 * 		  |
 * 		  V
 * 		-y-Achse
 * 
 * 
 * 		f(x) = sqrt [ -x² + xw - 1/4 * w² + r(2x - w) ] - AbstandAchseBildrand
 * 		
 * 		w = Bildbreite
 */


package robocup.camera.tools.regression;

import java.util.ArrayList;

public class RegressionsKurveMattis {
	
//	public static int LEFT = -1;
//	public static int RIGHT = 1;
	
	private static final float STEPS = 0.5f;
	
	private final int imgWidth;
	private final int abstandAchseBildrand;
	
	RegressionsKurveMattis(int imgWidth, int abstandAchseBildrand) {
		this.imgWidth = imgWidth;
		this.abstandAchseBildrand = abstandAchseBildrand;
	}
	
	
	private float radius;
//	private int direction;
	
	float f(float x) {
		float y = -x*x + x*imgWidth - 1/4*imgWidth*imgWidth + radius * (2*x - imgWidth);
		y = (float) Math.sqrt((double)y); // TODO float???
		y -= abstandAchseBildrand;
		return y;
	}
	
	private float getApproxDistance(int Px, int Py) { // abstand auf der gerade die vom kreismittelpunkt (r+0,5w / AbstandAchseBildrand) zum punkt geht
		float Mx = (float)radius + 0.5f * imgWidth;
		float My = -(float) abstandAchseBildrand;
		float m = (Px - Mx) / (Py - My);
		
		
		float b = Py - m * Px;

		// Schnittgerade: y = mx + b
		
		// i: Schnittstelle...
		float i = 0.0f ;
		while( !isAlmostEqual(f(i), (m * i + b)) ) {
			i += STEPS * 2;
		}
		
		
		// Pythagoras TODO: in floats rechnen...
		float distance = (float) Math.sqrt((double) ((Mx-Px)*(Mx-Px) + (My-Py) * (My-Py)) ); 
		
		return distance;
	}
	
	private boolean isAlmostEqual(float a, float b) {
		return  (  ( (a+STEPS) > b ) && ( b >= (a-STEPS) )  );
	}
	
	
	public void printDistances(ArrayList<Integer[]> blackDots) {
		for(int radius = 0; radius < 30; radius++) {
			setRadius((float)radius);
			float sumOfDistanceSquares = 0;
			for(int i = 0; i < blackDots.size(); i++) {
				sumOfDistanceSquares += Math.pow(getApproxDistance(blackDots.get(i)[0], -blackDots.get(i)[1]),2);
			}
			System.out.println(radius + "|" + sumOfDistanceSquares);
		}
	}


	public float getRadius() {
		return radius;
	}


	public void setRadius(float radius) {
		this.radius = radius;
	}
}
