/*Diese Klasse berechnet nur Geraden die durch einen Punkt gehen,
 *  hier wird der Achsenmittelpunkt benutzt */

package robocup.camera.tools.regression;

import java.util.ArrayList;

import robocup.misc.interfaces.Function;


public class RegressionsGerade implements Function {
	
	private final int abstandObenAchse;
	private final int imgWidth;
	
	private float steigung;
	private float b;
	
	public RegressionsGerade(int abstandObenAchse, int imgWidth) {
		this.abstandObenAchse = abstandObenAchse;
		this.imgWidth = imgWidth;
	}
	
	public float getSteigung(ArrayList<Integer[]> blackDots) {
	
		final int n = blackDots.size();
		final int achseSeite = imgWidth / 2;
		
		int sumxy = 0;
		int sumx2 = 0;
		
		int x;
		int y;
		
		for(int i = 0; i < n; i++) {
			x = blackDots.get(i)[0] - achseSeite;
			y = abstandObenAchse - blackDots.get(i)[1];
			sumxy += x*y;
			sumx2 += x*x;
		}
		
		float m = (float) sumxy / (float) sumx2;
			
		steigung = -m;
		b =  abstandObenAchse - m * (imgWidth/2);
		return m;
	}
	
	public int f(int x) {
		return  Math.round(steigung*x + b);
	}
}
