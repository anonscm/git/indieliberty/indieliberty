package robocup.camera.v4l;

import java.nio.ByteBuffer;

import robocup.camera.SimpleImage;

import robocup.camera.interfaces.IImageListener;
import robocup.camera.interfaces.IVideoSource;
import robocup.camera.tools.images.RGBImage;

import au.edu.jcu.v4l4j.FrameGrabber;
import au.edu.jcu.v4l4j.V4L4JConstants;
import au.edu.jcu.v4l4j.VideoDevice;
import au.edu.jcu.v4l4j.exceptions.V4L4JException;

public class V4LCam implements IVideoSource {
	
	private int width;
	private int height;
	
	private VideoDevice vd;
	
	private IImageListener imageListener;
	private long sleepTime = 5;
	private int maxWidth;
	private int maxHeight;

	private boolean stop = false;
	private String fileName = null;
	
	public void close() {
		System.out.println("[V4L] shutting down...");
		vd.release();
		stop = true;
	}

	public int getImageHeight() {
		return height;
	}

	public int getImageWidth() {
		return width;
	}

	public void setListener(IImageListener l) {
		imageListener = l;
	}

	public void run() {
	
		try {
		
			Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {

				public void run() {
					close();
				}

			}));
			go();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	private void go() throws Exception  {
		
		boolean successful = false;
		
		
		if(fileName != null) {
			vd = new VideoDevice(fileName); // throws V4L4JException if fails
		} else {
			// try /dev/video0, /dev/video1, etc
			String dev = "/dev/video";
			int n = 0;
			while(!successful) {
				try {
					vd = new VideoDevice(dev + n);
					successful = true;
				} catch(V4L4JException e) {
					n++;
					if(n > 256) {
						throw e; // stop trying
					}
				}
			}
		}
		
		
		
		
		
		if(!vd.supportRGBConversion()) {
			vd.release();
			throw new Exception("[V4L] The video device does not support RGB encoding");
		}
		
		FrameGrabber fg;
		
		
		int input = 0;
		int standard = V4L4JConstants.STANDARD_WEBCAM;
		
		fg = vd.getRGBFrameGrabber(maxWidth, maxHeight, input, standard);
		
		this.width = fg.getWidth();
		this.height = fg.getHeight();
		
		System.out.println("Image size is: " + fg.getWidth() + "x" + fg.getHeight());
		
		
		
		fg.startCapture();
		ByteBuffer b;
		byte[] tmp;
		
		while(!stop) {

			Thread.sleep(sleepTime); //hohe CPU-Auslastung verhindern... TODO: wie macht man das sauber?
			
			b = fg.getFrame();
			tmp = new byte[b.limit()];
			b.get(tmp);
			
			SimpleImage image = new SimpleImage(new RGBImage(tmp, width, height));
			imageListener.newImage(image);
		}
		
		System.out.println("[V4L] shutdown complete");
	}
	
	public V4LCam(int maxWidth, int maxHeight) {
		this.maxWidth = maxWidth;
		this.maxHeight = maxHeight;
	}
	
	
	public void setSleepTime(long sleepTime) {
		this.sleepTime = sleepTime;
	}
	
	
	public void setMaxSize(int maxWidth,int maxHeight) {
		this.maxWidth = maxWidth;
		this.maxHeight = maxHeight;
	}


	public V4LCam(String dev) {
		fileName  = dev;
	}
}
