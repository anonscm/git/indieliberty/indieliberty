package robocup.camera;

import java.util.ArrayList;

import robocup.camera.tools.images.BWImage;
import robocup.camera.tools.images.GreyScale;
import robocup.camera.tools.images.RGBImage;

public abstract class AbstractRoboCupImage {
	protected final int width;
	protected final int height;

	protected RGBImage rgbImage = null;
	protected GreyScale grayScaleImage = null;
	
	protected BWImage bwImage = null;
	
	protected ArrayList<Integer[]> blackDots = null;
	protected ArrayList<Integer[]> blackDotsBorder = null;
		
	
	protected int bwThres;
	
	public RGBImage getRGBImage() {
		if (rgbImage == null) {
			calculateRGB();
		}
		return rgbImage;

	}
	public BWImage getBwImage() {
		if(bwImage == null) {
			calculateBW();
		}
		return bwImage;
	}

	
	public ArrayList<Integer[]> getBlackDots() {
		if (blackDots == null) {
			calculateBlackDots();
		}
		return blackDots;
	}

	public ArrayList<Integer[]> getBlackDotsBorder() {
		if (blackDotsBorder == null) {
			calculateBlackDotsBorder();
		}
		return blackDotsBorder;
	}
	
	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}
	
	public GreyScale getGreyScaleImage() {
		if (grayScaleImage == null) {
			calculateGreyScale();
		}
		return grayScaleImage;
	}
	
	public int getBWTreshold() {
		if(bwThres == 0) {
			calculateBW();
		}
		return bwThres;
	}


	
	/** Converts GreyScale to BW using the otsu- Threshold method
	 * may be used by implementations 
	 */
	protected void calculateBwFromGrayScale() {
		if(grayScaleImage == null) {
			getGreyScaleImage();
		}
		bwImage = grayScaleImage.convertToBW(255);
	}
	
	/**
	 * gets BlackDots from B&W Image, may be used by implementations
	 */
	protected void calculateBlackDotsFromBW() {
		getBwImage(); // calculates BWImage if needed
		
		blackDots = new ArrayList<Integer[]>();
		for(int y = 0; y < height; y++) {
			for(int x= 0; x < width; x++) {
				if(bwImage.getBWImage()[x][y]) {
					blackDots.add(new Integer[]{x, y});
				}
			}
		}
	}
	
	
	// may be overridden if needed:
	
	/**
	 * stub: must be overriden if RGB image is not supplied
	 */
	protected void calculateRGB() {}
	
	/**
	 * gets B&W-Image from GreyScale, may be overridden by implementation
	 */
	protected void calculateBW() {
		calculateBwFromGrayScale();
	}
	
	/**
	 * gets GreyScale-Image from RGB, may be overridden by implementation
	 */
	protected void calculateGreyScale() {
		grayScaleImage = getRGBImage().calculateGreyScaleInteger();
	}
	
	/**
	 * gets BlackDots from B&W-Image, may be overridden by implementation
	 */
	protected void calculateBlackDots() {
		calculateBlackDotsFromBW();
	}
	
	protected abstract void calculateBlackDotsBorder();

	
	/**
	 * standard constructor, expects RGBImage, if null calculateRGB() is called
	 * @param width width of the image
	 * @param height heightof the image
	 * @param RGBImage byte[width * height * 3] with RGB values
	 */
	public AbstractRoboCupImage(int width, int height, byte[] rgbImage) {
		this.width = width;
		this.height = height;
		if(rgbImage == null) {
			calculateRGB();
		} else {
			this.rgbImage = new RGBImage(rgbImage, width, height);
		}
	}
	
	public AbstractRoboCupImage(RGBImage rgbImage) {
		this.width = rgbImage.getWidth();
		this.height = rgbImage.getHeight();
		if(rgbImage == null) {
			calculateRGB();
		} else {
			this.rgbImage = rgbImage;
		}
	}

}
