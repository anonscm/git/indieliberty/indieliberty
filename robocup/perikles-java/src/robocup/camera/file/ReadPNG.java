package robocup.camera.file;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import org.eclipse.swt.graphics.ImageData;

import robocup.camera.SimpleImage;
import robocup.camera.interfaces.IImageListener;
import robocup.camera.interfaces.IVideoSource;
import robocup.camera.tools.images.RGBImage;

public class ReadPNG implements IVideoSource {

	IImageListener imageListener;
	File file;
	
	public void close() {
		// TODO Auto-generated method stub

	}

	public int getImageHeight() {
		// TODO Auto-generated method stub
		return 0;
	}

	public int getImageWidth() {
		// TODO Auto-generated method stub
		return 0;
	}

	public void setListener(IImageListener l) {
		imageListener = l;
	}

	public void run() {
//		try {
//			BufferedImage image = ImageIO.read(file);
//			System.out.println("Isimage == null: " + (image == null));
//			System.out.println("Type: " +  image.getType());
//			System.out.println("px200/200" + image.);
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		while(true) {
		try {
			ImageData imgData = new ImageData(new FileInputStream(file));
			
			RGBImage rgbImage = new RGBImage(imgData.data, imgData.width, imgData.height);
			//			System.out.println("img == null? " + (img==null));
			SimpleImage img = new SimpleImage(rgbImage);
			imageListener.newImage(img);
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
		
		try {
			Thread.sleep(5000);
			} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}
	}
	
	public ReadPNG(File file) {
		this.file = file;
	}

	

}
