package robocup.camera.stereocam;

import robocup.camera.tools.UnsignedTypes;
import robocup.camera.tools.images.GreyScale;
import robocup.camera.tools.images.RGBImage;

public class Tools {
	
	public GreyScale calcDiffs(RGBImage i1, RGBImage i2) {
		int w = i1.getWidth();
		int h = i1.getHeight();
		int[][] greyScale = new int[w][h];
		
		byte[] rgb1 = i1.getBytes();
		byte[] rgb2 = i2.getBytes();
		
		int pos = 0;
		for(int y = 0; y < h; y++) {
			for(int x = 0; x < w; x++) {
				
				int diff = 0;
				for(int i=0; i<3;i++) {
					diff += Math.abs(UnsignedTypes.unsignedByteToInt(rgb1[pos+i]) - UnsignedTypes.unsignedByteToInt(rgb2[pos+i]));
				}
				diff /= 3;
				
				greyScale[x][y] = diff;
					
				pos += 3;
			}
		}
		
		
		return new GreyScale(greyScale, w, h);
	}
	
	public RGBImage overlayImages(GreyScale g1, GreyScale g2) {
		int w = g1.getWidth();
		int h = g1.getHeight();
		
		byte[] rgb = new byte[w*h*3];
		
		int[][] grey1 = g1.getData();
		int[][] grey2 = g2.getData();
		
		int pos = 0;
		for(int y = 0; y < h; y++) {
			for(int x = 0; x < w; x++) {
				
				rgb[pos + 1] = UnsignedTypes.intToUnsignedByte(grey1[x][y]);
				rgb[pos + 2] = UnsignedTypes.intToUnsignedByte(grey2[x][y]);
				
				
				pos += 3;
			}
		}
		
		return new RGBImage(rgb, w, h);
	}
	
}
