package robocup.camera.stereocam;

import org.eclipse.swt.graphics.Image;

import robocup.camera.AbstractRoboCupImage;
import robocup.camera.interfaces.IImageListener;
import robocup.camera.interfaces.IVideoSource;
import robocup.camera.tools.images.GreyScale;
import robocup.debug.ui.Gui;
import robocup.debug.ui.IGUIButtonListener;

public class Processing implements IGUIButtonListener {
	private final IVideoSource leftCam;
	private final IVideoSource rightCam;
	private AbstractRoboCupImage lastImageLeft;
	private AbstractRoboCupImage lastImageRight;
	
	private Image[] swtImage = new Image[7];

	private Tools tools = new Tools();
	
	public Processing(IVideoSource leftCam, IVideoSource rightCam) {
		this.leftCam = leftCam;
		this.rightCam = rightCam;
		
		leftCam.setListener(new IImageListener() {
			
			@Override
			public void open() {}
			
			@Override
			public void newImage(AbstractRoboCupImage image) {
				lastImageLeft = image;
			}
			
			@Override
			public void close() {}
		});
		
		rightCam.setListener(new IImageListener() {
			
			@Override
			public void open() {}
			
			@Override
			public void newImage(AbstractRoboCupImage image) {
				lastImageRight = image;
			}
			
			@Override
			public void close() {}
		});
		
		new Thread(leftCam, "left Camera").start();
		new Thread(rightCam, "right Camera").start();
		
		// setup and start GUI
		Gui.getInstance().setButtonListener(this);
		new Thread(Gui.getInstance(), "GUI").start();
		
	}

	@Override
	public void guiButtonPressed() {
		if(lastImageLeft != null && lastImageRight != null) {
		
			process();
		} else {
			System.err.println("[Processing] Image missing");
		}
	}

	private void process() {
		//Processing
		swtImage[0] = lastImageLeft.getRGBImage().asSWTImage();
		swtImage[1] = lastImageRight.getRGBImage().asSWTImage();
		
		GreyScale diffs = tools.calcDiffs(lastImageLeft.getRGBImage(), lastImageRight.getRGBImage());
		
		swtImage[2] = diffs.asSWTImage();
		
		swtImage[3] = tools.overlayImages(lastImageLeft.getGreyScaleImage(), lastImageRight.getGreyScaleImage()).asSWTImage();
		
		
		Gui.getInstance().attachImageAsync(swtImage);
	}
	
	
	
}
