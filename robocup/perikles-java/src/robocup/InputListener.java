package robocup;

import java.io.IOException;

import robocup.camera.processing.ImageProcessing;

public class InputListener implements Runnable {

	private boolean run = true;
	private ImageProcessing imageProcessing;
	
	
	public void stop() {
		run = false;
	}

	public void run() {
		try {
			while (run) {
				int cmd = System.in.read();
				switch (cmd) {
				case 112:
					System.out.println("print :)");
					if(imageProcessing != null) 
						imageProcessing.printImage();
					break;					
				default:
					break;
				}

			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void setImageProcessing(ImageProcessing imageProcessing) {
		this.imageProcessing = imageProcessing;
	}


}
