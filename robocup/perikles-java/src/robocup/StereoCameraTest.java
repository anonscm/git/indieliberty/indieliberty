package robocup;

import java.io.IOException;

import robocup.camera.stereocam.Processing;
import robocup.camera.v4l.V4LCam;
import robocup.debug.ui.Gui;

/**
 *  this might not work with two USB Cameras connected to the same hub,
 *  try experimenting with different image sizes and usb ports..
 * @author Mattis Pasch
 *
 */
public class StereoCameraTest {
		
		public static boolean startGUI = true;
		
		public static int maxWidth = 320;
		public static int maxHeight = 240;
		
		public static void main(String[] args) throws IOException,
				InterruptedException {

		

			// Konsolen Inputs:
//			InputListener consoleIn = new InputListener();
//			new Thread(consoleIn).start();
			
			
			V4LCam leftCam = new V4LCam("/dev/video2");
			V4LCam rightCam = new V4LCam("/dev/video4");
			
			leftCam.setMaxSize(maxWidth, maxHeight);
			rightCam.setMaxSize(maxWidth, maxHeight);
					
			
			Processing processing = new Processing(leftCam, rightCam);
					
		
			if(startGUI) {
				// GUI erstellen und mit robot vernüpfen
				Gui.getInstance();
			}
			
			
						
			Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
				
				@Override
				public void run() {
					System.out.println("[RoboCup] closing...");
				}
			}));

		}
		

}
