package robocup;

import java.io.File;
import java.io.IOException;

import robocup.camera.file.ReadPNG;
import robocup.camera.interfaces.IImageListener;
import robocup.camera.interfaces.IVideoSource;
import robocup.camera.processing.ImageProcessing;
import robocup.camera.v4l.V4LCam;
import robocup.debug.ui.Gui;
import robocup.robot.DummyRobot;
import robocup.robot.NXTV2;
import robocup.robot.PriorityWrapper;
import robocup.robot.interfaces.IMotorControl;
import robocup.robot.interfaces.IRobot;
import robocup.robot.interfaces.ISensors;

/**
 * TODO:
 *  - wackelkontakt spannungsumwandler
 *  - kreiseStrategie nimmt erstbesten
 *  - redDiff aufloesung
 *  - redzone
 *  - hindernisse
 *  
 * 
 * @author Mattis Pasch, Torsten Wylegala, Jasmin Mueller, Johannes Held
 *
 */
public class RoboCup {


	
	private static int tryWidth = 320;
	private static int tryHeight = 240;
	
	public static boolean startGUI = true;
	
	private static IRobot robot = null;
	private static IVideoSource videoSource = null;
	private static ImageProcessing imageProcessing = null;
	public static void main(String[] args) throws IOException,
			InterruptedException {

	

		// Konsolen Inputs:
		InputListener consoleIn = new InputListener();
		new Thread(consoleIn).start();
		

		String videoSourceName = "";
		
		for(int i = 0; i < args.length; i++) {
			if((args[i].equals("-r")) || (args[i].equals("--robot"))) {
				robot = createRobot(args[i+1]);
				i++;
			} else if((args[i].equals("-v"))  || (args[i].equals("--video"))){
				videoSourceName = args[i+1];
				i++;
			} else if((args[i].equals("-s")) || (args[i].equals("--size"))) {
				setDimensions(args[i+1]);
				i++;
			} else if(args[i].equals("--nogui")) {
				startGUI = false;
			} else {
				System.err.println("Unknown Argument: " + args[i]);
				printHelp();
			}
		}
		// videoSource muss nachher initialisiert werden (wegen --size)
		videoSource = createVSFromString(videoSourceName);
		
		
		// wenn kein roboter angegeben:
		if(robot == null) {
			robot = NXTV2.getNXT();
		}
		
	
//		circleHelper = new CircleHelper(tryWidth, tryHeight, tryHeight/2);
		
		IMotorControl motorControl = robot;
		ISensors sensors = robot;

		motorControl.setup();

		PriorityWrapper.setRobot(motorControl);

		imageProcessing = new ImageProcessing();
		
		IImageListener imageListener = imageProcessing;
				
		consoleIn.setImageProcessing(imageProcessing);
		
		sensors.setUSSensorListener(imageProcessing);
		sensors.setTouchListener(imageProcessing);
		sensors.listen();

		if(startGUI) {
			// GUI erstellen und mit robot vernüpfen
			Gui.getInstance().setRobot(new PriorityWrapper(PriorityWrapper.PRIORITY_REMOTE));
		}
		
		
		if (videoSource != null) {
			
			imageListener.open();

			videoSource.setListener(imageProcessing);
			new Thread(videoSource).run();
		} else {
			System.out.println("No videoSource initialized");
		}
		
		Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
			
			@Override
			public void run() {
				System.out.println("[RoboCup] closing...");
				videoSource.close();
				robot.close();
				imageProcessing.close();
			}
		}));

		
		System.out.println("perikles started.");
	}

	private static void printHelp() {
		System.out.println("Help:");
		System.out.println("-r, --robot Roboter, z.B. NXT");
		System.out.println("-s, --size Bild-Format, z.B. 320x240");
		System.out.println("-v, --video Video-Quelle, z.B. V4L");
		System.out.println("--nogui : keine GUI starten");
	}

	private static void setDimensions(String string) {
		String tmp[] = string.split("x");		
		
		tryWidth = Integer.parseInt(tmp[0]);
		tryHeight = Integer.parseInt(tmp[1]);
	}

	private static IRobot createRobot(String name) {
		IRobot robot;
		try {
			if (name.equals("DummyRobot") || name.equalsIgnoreCase("Dummy")) {
				System.out.println("Using DummyRobot...");
				// DummyRobot
				robot = new DummyRobot();
			} else if(name.equalsIgnoreCase("NXT")) {
				System.out.println("Using NXT...");
				// NXT
				robot = NXTV2.getNXT();
			} else {
				System.out.println("Could not find " + name + ". Using NXT...");
				// NXT
				robot = NXTV2.getNXT();
			}
		} catch (ArrayIndexOutOfBoundsException e) {
			System.out.println("No Robot supplied, using NXT...");
			robot = NXTV2.getNXT();
		}
		return robot;
	}

	private static IVideoSource createVSFromString(String videoSourceName) {
		
		if (videoSourceName.equals("V4L")) {
			return new V4LCam(tryWidth, tryHeight);
		} else if (videoSourceName.equals("PNG")) {
			return new ReadPNG(new File(
					"/home/mattis/Desktop/testimg.png"));
		}
		
		try { // to create videoSource

			System.out.println("VideoSource:" + videoSourceName);

			return (IVideoSource) Class.forName(videoSourceName)
					.newInstance();
		} catch (Exception e) {
			System.out.println("Could not find the class specified:");
			e.printStackTrace();

			System.out.println("Using Dummy...:");
			return new V4LCam(tryWidth, tryHeight);

		}
	}
	
}
