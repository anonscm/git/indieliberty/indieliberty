package robocup.debug.connectivity;

import java.io.Serializable;

import robocup.camera.android.ICommunicationUnit;

public class CommunicationUnit implements ICommunicationUnit, Serializable{
	
	private static final long serialVersionUID = 1L;

	byte[] data = null;
	
	int imageHeight = 0;
	int imageWidth = 0;
	
	public byte[] getImage() {
		// TODO Auto-generated method stub
		return data;
	}

	public void setImage(byte[] image) {
		// TODO Auto-generated method stub
		data = image;
	}

	public int getImageHeight() {
		return imageHeight;
	}

	public int getImageWidth() {
		return imageWidth;
	}

	public void setImageHeight(int height) {
		imageHeight = height;
		
	}

	public void setImageWidth(int width) {
		imageWidth = width;
	}

}
