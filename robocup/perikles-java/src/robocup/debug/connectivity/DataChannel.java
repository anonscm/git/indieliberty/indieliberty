package robocup.debug.connectivity;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

import robocup.misc.Logger;

public class DataChannel implements Runnable {

	static DataChannel dataChannel = null;

	ServerSocket ssocket;

	ArrayList<Socket> csocket = new ArrayList<Socket>();
	ArrayList<DataOutputStream> dos = new ArrayList<DataOutputStream>();

	public void run() {
		try {
			init();

			while (true) {
				waitForConnections();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void waitForConnections() throws IOException {
		Logger.println("Waiting for Connections");
		Socket tmpS = ssocket.accept();
		DataOutputStream tmpDOS = new DataOutputStream(tmpS.getOutputStream());

		csocket.add(tmpS);
		dos.add(tmpDOS);
	}

	public void sendImage(byte[] image){
//		for(int i=0;i<csocket.size();i++){
//			Socket tmp = csocket.get(i);
//				if(tmp.isConnected()){
//					try {
//						ObjectOutputStream oos = this.oos.get(i);
//						oos.writeObject(cu);
//						oos.flush();
//						
//						Logger.println("Sended picture to: "+tmp.getInetAddress());
//					} catch (IOException e) {
//						e.printStackTrace();
//					}
//				}
//		}
		
		for(int i=0;i<csocket.size();i++){		
			Socket tmpSocket = csocket.get(i);
			DataOutputStream tmpStream = dos.get(i);
		
			if(tmpSocket.isConnected()){
				try {
				tmpStream.write(image);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	private void init() throws IOException {
		Logger.println("Init Connections - started");
		ssocket = new ServerSocket(1234);
		Logger.println("Init Connections - finished");
	}

	public void close() {
		for (int i = 0; i < csocket.size(); i++) {
			Socket tmp = csocket.get(i);
			if (tmp.isConnected()) {
				try {
					tmp.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public static DataChannel getInstance() {
		if (dataChannel == null)
			dataChannel = new DataChannel();
		return dataChannel;
	}

}
