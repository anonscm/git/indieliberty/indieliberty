package robocup.debug.connectivity;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;                            

import java.net.Socket;
import java.net.UnknownHostException;

public class InformationChannel {
	
	public static InformationChannel connection;
	

	public String address = "127.0.0.1";
	public Socket informationSocket = null;
	public BufferedInputStream inInformation = null;
	public BufferedOutputStream outInformation = null;
	
	public void setupInformationConnection() throws UnknownHostException, IOException{
		int informationPort = 667;
		informationSocket = new Socket(address, informationPort);
		System.out.println("Verbindung aufgebaut [Information]");
		inInformation = new BufferedInputStream(informationSocket.getInputStream());
		outInformation = new BufferedOutputStream(informationSocket.getOutputStream());
	}
	
	public void writeInfoChannel(int id, String arguments){
		try {
			this.getBfOutputStreamInformation().write(("<"+id+"> "+arguments).getBytes());
			this.getBfOutputStreamInformation().flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void setupAllConnections() throws UnknownHostException, IOException, InterruptedException{
		setupInformationConnection();
	}
	
	public BufferedInputStream getBfInputStreamInformation(){
		return inInformation;
	}
	
	
	public BufferedOutputStream getBfOutputStreamInformation(){
		return outInformation;
	}
	
	public void closeConnections() {
		try {
			informationSocket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static InformationChannel getInstance(){
		if(connection == null)
			connection = new InformationChannel();
		return connection;
	}
}
