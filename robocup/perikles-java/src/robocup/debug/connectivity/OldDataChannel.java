package robocup.debug.connectivity;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;

import robocup.camera.SimpleImage;
import robocup.camera.interfaces.IImageListener;
import robocup.camera.interfaces.IVideoSource;
import robocup.camera.tools.images.RGBImage;

public class OldDataChannel extends Thread implements IVideoSource {


	private static OldDataChannel dataChannel;

	private IImageListener listener;

	private int imgHeight = 240;

	private int imgWidth = 320;


	private ServerSocket serverSocket = null;
	private Socket dataSocket = null;
	private BufferedInputStream inData = null;
	private BufferedOutputStream outData = null;
	
	public String address = "192.168.1.102";
	public int dataPort = 6666;

	public OldDataChannel(){
		try {
		serverSocket = new ServerSocket(dataPort);
	} catch (UnknownHostException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	}
	
	public boolean sendPicture(byte[] data){
		
		if(dataSocket == null)
			return false;
		
		if(dataSocket.isClosed())
			return false;
		
		try {
			getBfOutputStreamData().write(data);
			getBfOutputStreamData().flush();
			return true;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return false;
	}
	
	public void run() {
		try {
			dataSocket = serverSocket.accept();
			System.out.println("Verbindung aufgebaut [Data]");
			inData = new BufferedInputStream(dataSocket.getInputStream());
			outData = new BufferedOutputStream(dataSocket.getOutputStream());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

//		try {
//				dataSocket = new Socket(address, dataPort);
//				System.out.println("Verbindung aufgebaut [Data]");
//				inData = new BufferedInputStream(dataSocket.getInputStream());
//				outData = new BufferedOutputStream(dataSocket.getOutputStream());
//
//			} catch (UnknownHostException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			} catch (IOException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//				
//		// Size: 230400
//
		byte[] buf = new byte[imgWidth * imgHeight * 3];
//
//		System.out.println("Start loop [Data]");
//
//		while (true) {
//
//			// System.out.println("RGB2HSV test\n");
//			// int[] hsv = new int[3];
//			// rgb2hsv(200, 100 , 50, hsv);
//			// System.out.println("200,100,50: " + hsv[0] + hsv[1] + hsv[2]);
//
//			int offset = 0;
//			int numRead = 0;
//			try {
//				while (offset < buf.length
//						&& (numRead = getBfInputStreamData().read(buf, offset,
//										buf.length - offset)) > 0) {
//					offset += numRead;
//					System.out.println("Offset: " + Integer.toString(offset));
//				}
//			} catch (IOException e1) {
//				e1.printStackTrace();
//			}
//			if (offset < buf.length) {
//				try {
//					throw new IOException("Could not completely read all data");
//				} catch (IOException e) {
//					e.printStackTrace();
//				}
//				break;
//			}
//			System.out.println("Got frame! =)");
//			
			listener.newImage(new SimpleImage(new RGBImage(buf, 1, 1)));
//		}
	}
		

	public void close() {
		try {
			dataSocket.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	public int getImageHeight() {
		return imgHeight;
	}

	public int getImageWidth() {
		return imgWidth;
	}

	public void setListener(IImageListener l) {
		listener = l;
	}


	
	public BufferedInputStream getBfInputStreamData(){
		return inData;
	}
	
	public BufferedOutputStream getBfOutputStreamData(){
		return outData;
	}
		

	public static OldDataChannel getInstance() {
		if (dataChannel == null)
			dataChannel = new OldDataChannel();
		return dataChannel;
	}

}
