// For HSV settings look at Revision <= 195

package robocup.debug.ui;

import org.eclipse.swt.SWT;
import org.eclipse.swt.SWTException;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

import robocup.camera.processing.ImageProcessing;
import robocup.debug.connectivity.InformationChannel;
import robocup.robot.PriorityWrapper;
import robocup.robot.interfaces.IMotorControl;

public class Gui extends Thread {

	Display display;

	Label[] imageView;

	public Label pixelInfo;

	public Shell shell;

	public static Gui gui;

	private IGUIButtonListener buttonListener;
	
	
	private IMotorControl robot;

	public void run() {

		display = new Display();
		shell = new Shell(display);
		shell.setLayout(new GridLayout(1, false));
		shell.setSize(1280, 700);
		shell.setLocation(1, 1);

		imageView = new Label[7];

		GridData compGridData = new GridData(SWT.FILL, SWT.FILL, true, true, 1,
				2);
		compGridData.heightHint = 240;
		compGridData.widthHint = 1280;

		Composite composite1 = new Composite(shell, SWT.NONE);
		composite1.setLayout(new GridLayout(4, true));
		composite1.setLayoutData(compGridData);

		GridData imageViewGridData = new GridData(SWT.FILL, SWT.FILL, true,
				true, 1, 1);
		imageViewGridData.heightHint = 240;
		imageViewGridData.widthHint = 320;

		for (int i = 0; i < 4; i++) {
			imageView[i] = new Label(composite1, SWT.BORDER);
			imageView[i].setLayoutData(imageViewGridData);
		}

		Composite compositeHSV = new Composite(shell, SWT.NONE);
		compositeHSV.setLayout(new GridLayout(4, true));
		compositeHSV.setLayoutData(compGridData);

		for (int i = 4; i < 7; i++) {
			imageView[i] = new Label(compositeHSV, SWT.BORDER);
			imageView[i].setLayoutData(imageViewGridData);
		}

		Composite compSettings = new Composite(compositeHSV, SWT.NONE);
		compSettings.setLayout(new GridLayout(2, true));
		GridData compSettingsGrid = new GridData(SWT.FILL, SWT.NONE, false,
				false, 1, 2);
		compSettingsGrid.heightHint = 240;
		compSettingsGrid.widthHint = 320;
		compSettings.setLayoutData(compSettingsGrid);

		MouseListener mouseListener = new MouseListener() {
			public void mouseDoubleClick(MouseEvent arg0) {
				// Event wird nicht verarbeitet.
			}

			public void mouseDown(MouseEvent arg0) {
				final MouseEvent arg = arg0;
				new Thread(new Runnable() {

					public void run() {
						int yMove = 24;
						// TODO das y= y-yMove wegmachen irgendwie: Unsauber!
						System.out.println("mouseDownEvent: x: " + arg.x
								+ " y:" + (arg.y - yMove));
						printPixelInfo(arg.x, (arg.y - yMove));
					}

				}).start();
			}

			public void mouseUp(MouseEvent arg0) {
				// Event wird nicht verarbeitet.
			}

		};
		imageView[0].addMouseListener(mouseListener);

		pixelInfo = new Label(compSettings, SWT.FILL);
		pixelInfo.setText("PixelInfo");

		GridData compGridData2 = new GridData(SWT.FILL, SWT.NONE, false, false,
				1, 2);
		compGridData2.heightHint = 60;
		compGridData2.widthHint = 1280;

		Composite composite3 = new Composite(shell, SWT.NONE);
		composite3.setLayout(new GridLayout(4, true));
		composite3.setLayoutData(compGridData2);

		GridData buttonGridData = new GridData(SWT.FILL, SWT.NONE, true, true,
				1, 1);
		imageViewGridData.heightHint = 20;
		imageViewGridData.widthHint = 160;

		Button getImageButton = new Button(composite3, SWT.FILL);
		getImageButton.setLayoutData(buttonGridData);
		getImageButton.setSize(100, 20);
		getImageButton.setText("Get Image!");

		getImageButton.addSelectionListener(new SelectionListener() {

			public void widgetSelected(SelectionEvent event) {
				buttonListener.guiButtonPressed();
			}

			public void widgetDefaultSelected(SelectionEvent event) {
				// stub
			}
		});

		Button driveButton = new Button(composite3, SWT.FILL);
		driveButton.setLayoutData(buttonGridData);
		driveButton.setSize(100, 20);
		driveButton.setText("Get Image!");

		driveButton.addSelectionListener(new SelectionListener() {

			public void widgetSelected(SelectionEvent event) {
//	TODO: dieser button macht nix			imageProcessing.guiDrive();
			}

			public void widgetDefaultSelected(SelectionEvent event) {
				// stub
			}
		});

		shell.addKeyListener(new KeyListener() {

			public void keyPressed(KeyEvent e) {
				System.out.println("Key pressed!" + e.keyCode);
				switch (e.keyCode) {
				case SWT.ARROW_UP:
					System.out.println("REMOTE: FWD");
					PriorityWrapper
							.setPriority(PriorityWrapper.PRIORITY_REMOTE);
					robot.setMotorLeft(true, 255);
					robot.setMotorRight(true, 255);
					break;
				case SWT.ARROW_DOWN:
					System.out.println("REMOTE: REV");
					PriorityWrapper
							.setPriority(PriorityWrapper.PRIORITY_REMOTE);
					robot.setMotorLeft(false, 255);
					robot.setMotorRight(false, 255);
					break;
				case SWT.ARROW_LEFT:
					System.out.println("REMOTE: LEFT");
					PriorityWrapper
							.setPriority(PriorityWrapper.PRIORITY_REMOTE);
					robot.setMotorLeft(false, 255);
					robot.setMotorRight(true, 255);
					break;
				case SWT.ARROW_RIGHT:
					System.out.println("REMOTE: RIGHT");
					PriorityWrapper
							.setPriority(PriorityWrapper.PRIORITY_REMOTE);
					robot.setMotorLeft(true, 255);
					robot.setMotorRight(false, 255);
					break;
				default:
					System.out.println("REMOTE: STOP");
					PriorityWrapper
							.setPriority(PriorityWrapper.PRIORITY_FOLLOWING_LINE);
					robot.setMotorLeft(true, 0);
					robot.setMotorRight(true, 0);
				}
			}

			public void keyReleased(KeyEvent e) {
				// Do nothing!?
			}
		});

		shell.open();
		while (!shell.isDisposed()) {
			try {
				if(!display.readAndDispatch()) {
					display.sleep();
				}
			} catch(SWTException e) {
				e.printStackTrace();
			}
		}
			
		display.dispose();

		try {
			InformationChannel.getInstance().closeConnections();
		} catch (Exception e) {
			// ignore
		}
		System.exit(0);
	}

	private String info;

	public void updatePixelInfo(String i) {
		info = i;
		display.asyncExec(new Runnable() {
			public void run() {
				System.out.println("Info: " + info);

				pixelInfo.setText(info);
				pixelInfo.pack();
			}
		});
	}

	long timeOld = System.currentTimeMillis();

	int imageCount = 0;

	private boolean attachImageAsync_WORKING = false;

	public void attachImageAsync(final Image[] image) {
		if (!attachImageAsync_WORKING) {
			display.asyncExec(new Runnable() {
				public void run() {
					attachImageAsync_WORKING = true;
					imageCount++;

					for (int i = 0; i < image.length; i++) {
						if (image[i] != null) {
							imageView[i].setImage(image[i]);
							image[i].dispose();
						}
					}

					if ((System.currentTimeMillis() - timeOld) > 1000) {
						imageCount = 0;
						timeOld = System.currentTimeMillis();
					}
					attachImageAsync_WORKING = false;

				}
			});
		}
	}

	public Image resize(Image srcData) {
		Image destImage = new Image(Display.getDefault(), 320, 240);
		GC gc = new GC(destImage);
		gc.drawImage(srcData, 0, 0, 80, 60, 0, 0, 320, 240);
		gc.dispose();
		srcData.dispose();
		return destImage;
	}

	public Image resize160(Image srcData) {
		Image destImage = new Image(Display.getDefault(), 320, 240);
		GC gc = new GC(destImage);
		gc.drawImage(srcData, 0, 0, 160, 120, 0, 0, 320, 240);
		gc.dispose();
		srcData.dispose();
		return destImage;
	}

	public static Gui getInstance() {
		if (gui == null)
			gui = new Gui();
		return gui;
	}

	public void setRobot(IMotorControl robot) {
		this.robot = robot;
	}

	
	private void printPixelInfo(int x, int y) {
		Gui.getInstance().updatePixelInfo("X: " + x + " Y: " + y);
	}

	public void setButtonListener(IGUIButtonListener buttonListener) {
		this.buttonListener = buttonListener;
	}
}
