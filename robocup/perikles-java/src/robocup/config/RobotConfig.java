package robocup.config;

public class RobotConfig {
	/* Settings.... */

	/**
	 * radAbstand in mm
	 */
	public static final int radAbstand = 160;
	
	// minimalwert fuer Thresholding
	public static final int maximumBlack = 130;

	// hoehe der neonroehre
	public static final int obstacleDetectionHeight = 23;

}
