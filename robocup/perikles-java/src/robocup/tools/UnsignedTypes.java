package robocup.tools;

public class UnsignedTypes {
	public static int unsignedByteToInt(byte b) {
		return (int) b & 0xFF;
	}

	public static byte intToUnsignedByte(int i) {
		return (byte) (i & 0xFF);
	}
	
	public static short unsignedByteToShort(byte b) {
		return (short) (b & 0xFF);
	}
}
