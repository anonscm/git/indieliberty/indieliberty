package robocup.misc;

import robocup.misc.interfaces.Function;

public class RelocatedFunction implements Function {

	private final Function function;
	
	private int moveY = 0;
	private int moveX = 0;

	private int factorY = 1;

	private int factorX = 1;
	
	public RelocatedFunction(Function function, int moveX, int moveY, int factorX, int factorY) {
		this.function = function;
		
		this.moveX = moveX;
		this.moveY = moveY;
		
		this.factorX = factorX;
		this.factorY = factorY;
		
	}
	
	@Override
	public int f(int x) {
		

		x *= factorX;
		
		x -= moveX;
		
		
		int fx = function.f(x);
		
		
		fx *= factorY;
		fx += moveY;
		
		return fx;
	}

}
