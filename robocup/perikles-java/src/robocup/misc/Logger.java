package robocup.misc;

import robocup.misc.interfaces.IOutput;

public class Logger {
	
	static IOutput output;
	
	public static void registerOutput(IOutput _output){
		output = _output;
	}
	
	public static void println(String msg){
		if(output != null)
			output.printLog(msg);
		else
			System.out.println(msg);
	}
}
