package robocup.misc.interfaces;

public interface Function {
	public int f(int x);
}
