package robocup.misc.interfaces;

public interface IOutput {
	public void printLog(String msg);
}
