package robocup.interfaces;

public interface IVictimListener {
	public static int VICTIM_ANY = 0;
	public static int VICTIM_GREEN = 1;
	public static int VICTIM_SILVER = 2;
	
	public void onVictim(int type);
}
