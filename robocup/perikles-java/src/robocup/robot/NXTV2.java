package robocup.robot;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import lejos.nxt.remote.NXTCommand;
import lejos.pc.comm.NXTComm;
import lejos.pc.comm.NXTCommException;
import lejos.pc.comm.NXTCommFactory;
import lejos.pc.comm.NXTInfo;
import robocup.robot.interfaces.IRampListener;
import robocup.robot.interfaces.IRobot;
import robocup.robot.interfaces.ITouchListener;
import robocup.robot.interfaces.IUSListener;
import robocup.tools.UnsignedTypes;

/**
 * setMotorLeft und setMotorRight sind implementiert -> reaktionszeit ca. 10ms,
 * d.h. fuer 2 Befehle zusammen ca. 20 ms driveCircle implementiert, faehrt
 * momentan 30Grad -> reaktionszeit 20ms - 250ms
 * 
 * @author Mattis Pasch, Torsten Wylegala, Jasmin Mueller, Johannes Held
 * 
 */
public class NXTV2 implements IRobot {

	private final static boolean doLog = false;

	private static final int CONNECTION_TYPE = NXTCommFactory.USB;



	private static NXTV2 nxt;
	private boolean connected = false;
	private boolean isConnecting = false;

	public String mac = "00:16:53:01:C4:93";

	private ITouchListener touchListener = null;
	private IUSListener usListener = null;
	

	private int defaultSpeed = 0;

	private NXTComm nxtComm;
	private NXTInfo nxtInfo;
	private DataInputStream in;
	private DataOutputStream out;

	private int id = 0;

	private final int timeBufferSize = 50;
	private long[] time = new long[timeBufferSize];
	private int time0Index = 0;

	private boolean doNotSendNextCommand = false;


	private long timeOfLastCommand = System.currentTimeMillis();

	public void setup() { // from IMotorControl
		connect();
	}

	public void listen() { // from ISensors
		connect();

		new Thread(new Runnable() {
			public void run() {
					try {
						while (connected) {
							try {
							parseCommand();
							}catch (ParserException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}  catch (Exception e) {
						e.printStackTrace();
					}
			}
		}).start();
	}

	// TODO auslagern, ist aber etwas kompliziert...
	private boolean parseCommand() throws IOException, ParserException {

		byte tmp = ' ';

		// auf Begin command warten...
		while (in.readByte() != NXTCommandV2.CMD_BEGIN)
			;

		// das naechste byte ist "CMD_TYPE"
		byte cmdType = in.readByte();

		// CMD_HAS_EXTRAS oder CMD_END
		tmp = in.readByte();

		if (tmp == NXTCommandV2.CMD_END) {
			if(cmdType == NXTCommandV2.CMD_TYPE_SENSOR_TOUCH) {
				touchListener.onTouch(ITouchListener.MIDDLE);
				return true;
			} else {
				return false;
			}
		} 
		
		// falls kein CMD_END:
		if (tmp != NXTCommandV2.CMD_HAS_EXTRAS) {
			throw new ParserException("HAS_EXTRAS missing: " + tmp);
		}

		int extrasSize = Integer.valueOf(in.readByte());

		byte[] extras = new byte[extrasSize];
		int i;
		for (i = 0; i < extrasSize; i++) {
			tmp = in.readByte();
			if (tmp == NXTCommandV2.CMD_END) {
				break;
			} else {
				extras[i] = tmp;
			}
		}
		if (cmdType == NXTCommandV2.CMD_TYPE_PING) {
			int pingID = UnsignedTypes.unsignedByteToInt(extras[0]) * 256
					+ UnsignedTypes.unsignedByteToInt(extras[1]);
			log("Ping:): " + pingID);

			if (pingID < this.id - 2) {

//				System.err.println("NXT lagging behind!!!!");
				// TODO: stopbefehl senden...
//				doNotSendNextCommand = true;
			} else {
				calcTime(pingID);
			}

		} else if(cmdType == NXTCommandV2.CMD_TYPE_SENSOR_US) {
			if(extrasSize != 3) {
				throw new ParserException("US: extrasSize != 3");
			}
			int location = IUSListener.MIDDLE;
			
			if(extras[0] == NXTCommandV2.LEFT) {
				location = IUSListener.LEFT;
			} else if(extras[0] == NXTCommandV2.RIGHT) {
				location = IUSListener.RIGHT;
			}
			int distance = UnsignedTypes.unsignedByteToInt(extras[1]) * 256
				+ UnsignedTypes.unsignedByteToInt(extras[2]);
			
			
			usListener.onUSChange(location, distance);
			
		} else if (cmdType == NXTCommandV2.CMD_TYPE_SENSOR_TOUCH) {
			
			int location = ITouchListener.MIDDLE;
			
			if(extras[0] == NXTCommandV2.LEFT) {
				location = ITouchListener.LEFT;
			} else if(extras[0] == NXTCommandV2.RIGHT) {
				location = ITouchListener.RIGHT;
			} else if(extras[0] == NXTCommandV2.RAMP) {
				location = ITouchListener.RAMP;
			}
			
			if(extras[1] == NXTCommandV2.RELEASED) {
				touchListener.onRelease(location);
			} else if(extras[1] == NXTCommandV2.PRESSED) {
				touchListener.onTouch(location);
			} else {
				throw new ParserException("[sensors] extras[1] is neither PRESSED nor RELEASED");
			}
		}

		return true;
	}

	public void setDefaultSpeed(int speed) {
		defaultSpeed = speed;
	}

	/** 
	 * radius > 0: nach rechts fahren
	 * radius < 0: nach links fahren
	 * @param radius
	 */
	public void driveCircle(int dir, int radius, int speed) {

		if(dir == LEFT) {
			radius = -radius;
		}
		
		int radAbstand = 153;
		int maxSpeed = 600;
		
		int radiusLinks = (radius + radAbstand/2);
		int radiusRechts = (radius - radAbstand/2);
		
		int verhaeltnisLR = radiusLinks / radiusRechts;
		
		int speedRight;
		int speedLeft;
		
		if(radius < 0) { // nach links:	
			speedRight = maxSpeed;
			speedLeft = maxSpeed * verhaeltnisLR;
		} else {
			speedLeft = maxSpeed;
			speedRight = maxSpeed / verhaeltnisLR;
		}
		
		setMotors(true, speedLeft, true, speedRight);
	}
	
	public void driveCircle(int dir, int rad) {
		driveCircle(dir, rad, defaultSpeed);
	}

//	public void driveCircle(final int dir, final int rad, final int speed) {
//		sendCommand(NXTCommandV2.createCircleCommand(rad, speed, dir, id));
//	}

	public void setMotorLeft(boolean dir, int speed) {
		sendCommand(NXTCommandV2.createMotorACommand(speed, dir, id));
	}

	public void setMotorRight(boolean dir, int speed) {
		sendCommand(NXTCommandV2.createMotorCCommand(speed, dir, id));
	}
	
	public void setMotors(boolean dirLeft, int speedLeft, boolean dirRight,
			int speedRight) {
		sendCommand(NXTCommandV2.createBothMotorsCommand(dirLeft, speedLeft, dirRight, speedRight, id));
		
	}

	private void sendCommand(byte[] b) {
		while(System.currentTimeMillis() - timeOfLastCommand < 40) {
		}
		
		if ((connected) && (!doNotSendNextCommand)) {
			try {
				id++;
				takeTime();
				out.write(b);
				out.flush();
				log("Wrote: " + b + " id: " + (UnsignedTypes.unsignedByteToInt(b[1]) * 256 + UnsignedTypes.unsignedByteToInt(b[2])));
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else if (doNotSendNextCommand) {
			doNotSendNextCommand = false;
		} else {
			System.err.println("[NXT] not connected");
		}
	}

	private void takeTime() {
		int diff = id - time0Index;
		if (diff >= timeBufferSize) {
			time0Index += timeBufferSize;
			diff -= timeBufferSize;
		}
		time[diff] = System.currentTimeMillis();
	}

	private void calcTime(int pingID) {
		long now = System.currentTimeMillis();
		int diff = pingID - time0Index;
		if (diff < 0) {
			diff += timeBufferSize;
		}
		if (diff < timeBufferSize) {
			long ping = now - time[diff];
			log("Command " + pingID + " took " + ping + " ms");
		} else {
			log("WTF?");
		}
	}

	public void setOnRampListener(IRampListener rl) {
		// hat keinen rampen Sensor;
	}

	public void setTouchListener(ITouchListener tl) {
		touchListener = tl;
	}
	
	public boolean hasRampSensor() {
		return false;
	}

	public boolean hasTouchSensor() {
		return true;
	}

	private void connect() {
		while(isConnecting) {
			try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		if (connected) {
			return;
		}


		try {
			
			isConnecting = true;
			
			// Bei fehler hier:
			// libjlibnxt.so muss in /usr/lib/jni/ sein... ist unter lib zu
			// finden,
			// muss aber noch kopiert werden (benoetigt root)
			// oder irgendwie anders im LD_LIBRARY_PATH sein ( nicht Classpath!)
			nxtComm = NXTCommFactory.createNXTComm(CONNECTION_TYPE);
			try {
				nxtInfo = nxtComm.search("NXT", CONNECTION_TYPE)[0];
				if (nxtInfo == null) {
					throw new NullPointerException();
				}
				if (nxtComm.open(nxtInfo)) {
					// programm starten:
					
					NXTCommand nxtCommand = NXTCommand.getSingleton();
					nxtCommand.setNXTComm(nxtComm);
					
//					log("[NXT] current Program: " + nxtCommand.getCurrentProgramName());
					
					nxtCommand.startProgram("rc.RoboCup.nxj");
					
					
					log("[NXT] Program started.");

//					nxtComm.
					nxtComm.close();

					Thread.sleep(100);
					
					if (nxtComm.open(nxtInfo)) {
						in = (new DataInputStream(nxtComm.getInputStream()));
						out = (new DataOutputStream(nxtComm.getOutputStream()));

						connected = true;
						log("Connected");
					} else {
						System.out
								.println("[NXT] Connection failed (program running)");
					}
				} else {
					System.out
							.println("[NXT] Connection failed (on starting program)");
					connected = false;
				}

			} catch (Exception e) {
				System.err.println("[NXT] Not found. Turned on?");
				connected = false;
			}
			// nxtInfo = new NXTInfo(CONNECTION_TYPE, "NXT", mac);

		} catch (NXTCommException e) {
			connected  = false;
			e.printStackTrace();
		}
		
		isConnecting = false;

		Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {

			public void run() {
				close();
			}

		}));
	}

	// private void intToByteArray(byte[] input, int offset, int value) {
	// input[offset] = (byte) (value >>> 24);
	// input[offset + 1] = (byte) (value >>> 16);
	// input[offset + 2] = (byte) (value >>> 8);
	// input[offset + 3] = (byte) value;
	// }

	public void close() {
		try {
			log("Closing NXT connection.");
			in.close();
			out.close();
			nxtComm.close();
			connected = false;
		} catch (Exception e) {
			e.printStackTrace();
		} 
	}

	private void log(String s) {
		if (doLog)
			System.out.println("[NXT] " + s);
	}

	private NXTV2() {
		// stub
	}

	public static NXTV2 getNXT() {
		if (nxt == null)
			nxt = new NXTV2();
		return nxt;
	}

	class ParserException extends Exception {

		private static final long serialVersionUID = 1L;

		public ParserException(String s) {
			super(s);
		}

	}

	@Override
	public boolean hasUSSensor() {
		return true;
	}

	@Override
	public void setUSSensorListener(IUSListener ul) {
		usListener = ul;
	}

	
}
