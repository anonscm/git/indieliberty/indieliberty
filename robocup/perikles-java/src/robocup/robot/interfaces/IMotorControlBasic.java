package robocup.robot.interfaces;

public interface IMotorControlBasic {
	public void setup();
	public void close();
	
	/**
	 * 
	 * @param dir true: Forward, false: Backwards
	 * @param speed
	 */
	public void setMotorLeft(boolean dir, int speed);
	
	/**
	 * 
	 * @param dir true: Forward, false: Backwards
	 * @param speed
	 */
	public void setMotorRight(boolean dir, int speed);
	
	/**
	 * 
	 * @param dirLeft true: Forward, false: Backwards
	 * @param speedLeft
	 * @param dirRight true: Forward, false: Backwards
	 * @param speedRight
	 */
	public void setMotors(boolean dirLeft, int speedLeft, boolean dirRight, int speedRight);
	
}
