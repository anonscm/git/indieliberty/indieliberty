package robocup.robot.interfaces;

public interface IMotorControl extends IMotorControlBasic {
	
	public static int LEFT = -1;
	public static int RIGHT = 1;
	
	public void setDefaultSpeed(int speed);
	
	public void driveCircle(int dir, int rad);
	public void driveCircle(int dir, int rad, int speed);
}
