package robocup.robot.interfaces;

public interface IUSListener {

	public static int MIDDLE = 0;
	public static int LEFT = 1;
	public static int RIGHT = 2;
	public static int BACK = 3;
	
	public void onUSChange(int location, int distance);
}
