package robocup.robot.interfaces;

public interface IRampListener {
	public void onRamp(boolean onRamp);
}
