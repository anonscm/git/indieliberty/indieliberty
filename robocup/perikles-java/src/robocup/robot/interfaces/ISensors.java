package robocup.robot.interfaces;


public interface ISensors {
	
	public void listen();
	
	public boolean hasRampSensor();
	public boolean hasTouchSensor();
	public boolean hasUSSensor();
	
	public void setTouchListener(ITouchListener tl);	
	public void setOnRampListener(IRampListener rl);
	
	public void setUSSensorListener(IUSListener ul);
}
