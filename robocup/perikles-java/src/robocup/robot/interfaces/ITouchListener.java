package robocup.robot.interfaces;

public interface ITouchListener {
	public static int MIDDLE = 0;
	public static int LEFT = 1;
	public static int RIGHT = 2;
	public static int BACK = 3;
	public static int RAMP = 4;
	
	public void onTouch(int location);
	
	public void onRelease(int location);
}
