package robocup.robot.interfaces;

/**
 * Stub interface which includes ISensors, IMotorControl, IMotorControlBasic,
 * so one Robot can be "packed"
 * @author mpasch
 *
 */
public interface IRobot extends ISensors, IMotorControl, IMotorControlBasic {
}
