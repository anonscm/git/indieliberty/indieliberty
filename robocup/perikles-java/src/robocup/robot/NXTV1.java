package robocup.robot;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import lejos.pc.comm.NXTComm;
import lejos.pc.comm.NXTCommException;
import lejos.pc.comm.NXTCommFactory;
import lejos.pc.comm.NXTInfo;
import robocup.robot.interfaces.IRampListener;
import robocup.robot.interfaces.IRobot;
import robocup.robot.interfaces.ITouchListener;
import robocup.robot.interfaces.IUSListener;

public class NXTV1 implements IRobot {

	private static final boolean doLog = false;
	private static final int CONNECTION_TYPE = NXTCommFactory.USB;
	
	private static NXTV1 nxt;
	private static boolean connected = false;
	

	public String mac = "00:16:53:01:C4:93";
	
	ITouchListener touchListener = null;
	private int defaultSpeed = 0;
	
	
	private NXTComm nxtComm;
	private NXTInfo nxtInfo;
	private DataInputStream in;
	private DataOutputStream out;

	
	public void setup() { // from IMotorControl
		connect();
	}
	
	public void listen() { // from ISensors
		connect();

		new Thread(new Runnable() {
			public void run() {
				while(true) {
					// TODO auf inputs warten/verarbeiten.....
				}
			}
		}).start();
	}
	
	public void setDefaultSpeed(int speed) {
		defaultSpeed = speed;
	}
	
	public void driveCircle(int dir, int rad) {
		driveCircle(dir, rad, defaultSpeed);
	}

	public void driveCircle(final int dir, final int rad, final int speed) {
		sendCommand(NXTCommandV1.createCircleCommand(rad, speed, dir));
	}

	public void setMotorLeft(boolean dir, int speed) {
		sendCommand(NXTCommandV1.createMotorACommand(speed, dir));
	}

	public void setMotorRight(boolean dir, int speed) {
		sendCommand(NXTCommandV1.createMotorCCommand(speed, dir));
	}
	
	private void sendCommand(final String s) {
		new Thread(new Runnable() {
			public void run() {
				if(connected) {
					try {
						out.write(s.getBytes());
						out.flush();
						log("[NXT] Wrote: " + s); 
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				} else {
					System.err.println("[NXT] not connected");
				}

			}
		}).start();
	}
	
	public void setOnRampListener(IRampListener rl) {
		// hat keinen rampen Sensor;
	}

	public void setTouchListener(ITouchListener tl) {
		touchListener = tl;
	}

	public boolean hasRampSensor() {
		return false;
	}

	public boolean hasTouchSensor() {
		return true;
	}
	
	private void connect() {
		if(connected) {
			return;
		}
	
		try {
			// Bei fehler hier:
			// libjlibnxt.so muss in /usr/lib/jni/ sein...  ist unter lib zu finden, 
			// muss aber noch kopiert werden (benoetigt root) 
			nxtComm = NXTCommFactory.createNXTComm(CONNECTION_TYPE);
			try {
				nxtInfo = nxtComm.search("NXT", CONNECTION_TYPE)[0];
				if(nxtInfo == null) {
					throw new NullPointerException();
				}
				if(nxtComm.open(nxtInfo)) {

					in =  (new DataInputStream(nxtComm.getInputStream()));
					out = (new DataOutputStream(nxtComm.getOutputStream()));
					
					connected = true;
				} else {
					System.out.println("[NXT] Connection failed"); 
				}
			} catch(Exception e) {
				System.err.println("[NXT] Not found. Turned on?");
			} 
//			nxtInfo = new NXTInfo(CONNECTION_TYPE, "NXT", mac);
			
			
		} catch (NXTCommException e) {
			e.printStackTrace();
		}
				
		Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {

			public void run() {
				close();
			}
			
		}));
	}
	
//	private void intToByteArray(byte[] input, int offset, int value) {
//		input[offset] = (byte) (value >>> 24);
//		input[offset + 1] = (byte) (value >>> 16);
//		input[offset + 2] = (byte) (value >>> 8);
//		input[offset + 3] = (byte) value;
//	}
	
	public void close() {
		try {
			in.close();
			out.close();
			nxtComm.close();
			connected = false;
		} catch (IOException e) {
			// ignore
		}
	}
	
	private void log(String s) {
		if(doLog) 
			System.out.println(s);
	}
	
	private NXTV1() {
		// stub
	}
	
	public static NXTV1 getNXT() { 
		if(nxt == null) 
			nxt = new NXTV1();
		return nxt;
	}

	@Override
	public void setMotors(boolean dirLeft, int speedLeft, boolean dirRight,
			int speedRight) {
		setMotorLeft(dirLeft, speedLeft);
		setMotorRight(dirRight, speedRight);
		
	}

	@Override
	public boolean hasUSSensor() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void setUSSensorListener(IUSListener ul) {
		// TODO Auto-generated method stub
		
	}
}
