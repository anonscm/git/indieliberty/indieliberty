/* Version 2 of RoboCup <-> NXT communication spec
 * 
 * 
 * 
 * Command Formatting PC -> NXT:
 * (1Byte) CMD_BEGIN 
 * (2Bytes) CMD_id ([0..255^2]
 * (1Byte) CMD_TYPE_X
 * (1Byte) SPEED [0..255]
 * (1Byte) CMD_DIR_X
 * 
 * (1Byte) CMD_HAS_EXTRAS  			||  (1Byte) CMD_END
 * (1Byte) size of extras (in Byte)
 * (x Bytes) EXTRAS (z.b. radius)
 * (1Byte) CMD_END
 * 
 * TODO: ausschreiben:
 * 
		cmd[0] = CMD_BEGIN;
		cmd[1] = UnsignedTypes.intToUnsignedByte(id / 255);
		cmd[2] = UnsignedTypes.intToUnsignedByte(id % 255);
		cmd[3] = CMD_TYPE_MOTOR_BOTH;
		cmd[4] = UnsignedTypes.intToUnsignedByte(speedLeft / 255);
		cmd[5] = UnsignedTypes.intToUnsignedByte(speedLeft % 255);
		cmd[6] = dirLeft ? FWD : REV;
		cmd[7] = UnsignedTypes.intToUnsignedByte(speedRight / 255);
		cmd[8] = UnsignedTypes.intToUnsignedByte(speedRight % 255);
		cmd[9] = dirRight ? FWD : REV;
		cmd[10] = CMD_END;
 * 
 * 
 * 
 * Command Formatting NXT -> PC:
 * (1Byte) CMD_BEGIN 
 * (1Byte) CMD_TYPE_X
 * 
 * (1Byte) CMD_HAS_EXTRAS  			||  (1Byte) CMD_END
 * (1Byte) size of extras (in Byte)
 * (x Bytes) EXTRAS (z.b. source-id)
 * (1Byte) CMD_END
 * 
 * 
 */

package robocup.robot;

import robocup.robot.interfaces.IMotorControl;
import robocup.tools.UnsignedTypes;

public class NXTCommandV2 {

	public static final byte CMD_BEGIN = 'a';
	public static final byte CMD_END = 'b';
	public static final byte CMD_HAS_EXTRAS = 'c';

	public static final byte CMD_TYPE_SENSOR_TOUCH = 'f';
	public static final byte CMD_TYPE_SENSOR_US = 'g';
	
	public static final byte CMD_TYPE_MOTOR_BOTH = 'j';
	public static final byte CMD_TYPE_MOTOR_A = 'k';
	public static final byte CMD_TYPE_MOTOR_C = 'l';
	public static final byte CMD_TYPE_CIRCLE = 'm';
	public static final byte CMD_TYPE_PING = 'n';
	
//	public static final byte CMD_TYPE_TOUCH = 'o';

	public static final byte FWD = 'u';
	public static final byte REV = 'v';

	public static final byte LEFT = 'x';
	public static final byte RIGHT = 'y';
	public static final byte RAMP = 'z';
	
	public static final byte RELEASED = 0;
	public static final byte PRESSED = 1;
	
	public static byte[] createCircleCommand(int radius, int speed, int dir,
			int id) {
		if (dir == IMotorControl.LEFT)
			return createCircleCommand(radius, speed, LEFT, id);
		else
			return createCircleCommand(radius, speed, RIGHT, id);
	}

	public static byte[] createCircleCommand(int radius, int speed, byte dir,
			int id) {
		if (speed < 0)
			speed *= -1;
		byte[] cmd = new byte[11];
		cmd[0] = CMD_BEGIN;
		cmd[1] = UnsignedTypes.intToUnsignedByte(id / 255);
		cmd[2] = UnsignedTypes.intToUnsignedByte(id % 255);
		cmd[3] = CMD_TYPE_CIRCLE;
		cmd[4] = UnsignedTypes.intToUnsignedByte(speed);
		cmd[5] = dir;
		cmd[6] = CMD_HAS_EXTRAS;
		cmd[7] = 2; // 255 * 255 fuer den radius
		cmd[8] = UnsignedTypes.intToUnsignedByte(radius / 256);
		cmd[9] = UnsignedTypes.intToUnsignedByte(radius % 256);
		cmd[10] = CMD_END;
		return cmd;
	}

	public static byte[] createMotorACommand(int speed, byte dir, int id) {
		if (speed < 0)
			speed *= -1;
		byte[] cmd = new byte[7];
		cmd[0] = CMD_BEGIN;
		cmd[1] = UnsignedTypes.intToUnsignedByte(id / 255);
		cmd[2] = UnsignedTypes.intToUnsignedByte(id % 255);
		cmd[3] = CMD_TYPE_MOTOR_A;
		cmd[4] = UnsignedTypes.intToUnsignedByte(speed);
		cmd[5] = dir;
		cmd[6] = CMD_END;
		return cmd;

	}

	public static byte[] createMotorCCommand(int speed, byte dir, int id) {
		if (speed < 0)
			speed *= -1;
		byte[] cmd = new byte[7];
		cmd[0] = CMD_BEGIN;
		cmd[1] = UnsignedTypes.intToUnsignedByte(id / 255);
		cmd[2] = UnsignedTypes.intToUnsignedByte(id % 255);
		cmd[3] = CMD_TYPE_MOTOR_C;
		cmd[4] = UnsignedTypes.intToUnsignedByte(speed);
		cmd[5] = dir;
		cmd[6] = CMD_END;
		return cmd;	
	}

	public static byte[] createMotorACommand(int speed, boolean dir, int id) {
		if (dir)
			return createMotorACommand(speed, FWD, id);
		else
			return createMotorACommand(speed, REV, id);
	}

	public static byte[] createMotorCCommand(int speed, boolean dir, int id) {
		if (dir)
			return createMotorCCommand(speed, FWD, id);
		else
			return createMotorCCommand(speed, REV, id);
	}

	public static byte[] createBothMotorsCommand(boolean dirLeft,
			int speedLeft, boolean dirRight, int speedRight, int id) {
		if(speedLeft < 0) {
			speedLeft = -speedLeft;
			dirLeft = !dirLeft;
		}
		if(speedRight < 0) {
			speedRight = -speedRight;
			dirRight = !dirRight;
		}
		
		
		byte[] cmd = new byte[11];
		cmd[0] = CMD_BEGIN;
		cmd[1] = UnsignedTypes.intToUnsignedByte(id / 255);
		cmd[2] = UnsignedTypes.intToUnsignedByte(id % 255);
		cmd[3] = CMD_TYPE_MOTOR_BOTH;
		cmd[4] = UnsignedTypes.intToUnsignedByte(speedLeft / 255);
		cmd[5] = UnsignedTypes.intToUnsignedByte(speedLeft % 255);
		cmd[6] = dirLeft ? FWD : REV;
		cmd[7] = UnsignedTypes.intToUnsignedByte(speedRight / 255);
		cmd[8] = UnsignedTypes.intToUnsignedByte(speedRight % 255);
		cmd[9] = dirRight ? FWD : REV;
		cmd[10] = CMD_END;
		
		
		return cmd;
	
	}
	
	
}
