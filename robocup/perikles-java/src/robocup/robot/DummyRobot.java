package robocup.robot;

import robocup.robot.interfaces.IRampListener;
import robocup.robot.interfaces.IRobot;
import robocup.robot.interfaces.ITouchListener;
import robocup.robot.interfaces.IUSListener;

public class DummyRobot implements IRobot {

	public void driveCircle(int dir, int rad) {		
	}

	public void driveCircle(int dir, int rad, int speed) {
		
	}

	public void setDefaultSpeed(int speed) {
		
	}

	public void close() {
		
	}

	public void setMotorLeft(boolean dir, int speed) {
		
	}

	public void setMotorRight(boolean dir, int speed) {
		
	}

	public void setup() {
		
	}

	public boolean hasRampSensor() {
		return false;
	}

	public boolean hasTouchSensor() {
		return false;
	}

	public void listen() {
		
	}

	public void setOnRampListener(IRampListener rl) {
		
	}

	public void setTouchListener(ITouchListener tl) {
		
	}

	public void setMotors(boolean dirLeft, int speedLeft, boolean dirRight,
			int speedRight) {
	}

	@Override
	public boolean hasUSSensor() {
		return false;
	}

	@Override
	public void setUSSensorListener(IUSListener ul) {
		
	}		
	

}
