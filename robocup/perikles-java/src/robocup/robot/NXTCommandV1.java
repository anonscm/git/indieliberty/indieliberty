package robocup.robot;

import robocup.robot.interfaces.IMotorControl;


/* Command Formatting:
 * CMD_BEGIN + CMD_TYPE_X + CMD_NEXT_PART + SPEED + CMD_NEXT_PART + CMD_DIR_X + CMD_NEXT_PART + EXTRAS (z.b. radius) + CMD_END + '\n'
 * 
 */

public class NXTCommandV1 {
	
	private static char CMD_BEGIN = 'a';
	private static char CMD_NEXT_PART = 'b';
	private static char CMD_END = 'c';
	
	private static char CMD_TYPE_MOTOR_A = 'k';
	private static char CMD_TYPE_MOTOR_C = 'l';
	private static char CMD_TYPE_CIRCLE = 'm';
	
	public static char FWD = 'u';
	public static char REV = 'v';
	
	public static char LEFT = 'x';
	public static char RIGHT = 'y';
	
	
	public static String createCircleCommand(int radius, int speed, int dir) {
		if(dir == IMotorControl.LEFT) 
			return createCircleCommand(radius, speed, LEFT);
		else 
			return createCircleCommand(radius, speed, RIGHT);
		}
	
	public static String createCircleCommand(int radius, int speed, char dir) {
		if (speed < 0)
			speed *= -1;
		return ("" + CMD_BEGIN + CMD_TYPE_CIRCLE + CMD_NEXT_PART + String.valueOf(speed) + CMD_NEXT_PART + dir + CMD_NEXT_PART + String.valueOf(radius) + CMD_END + '\n');
	}
	
	public static String createMotorACommand(int speed, char dir) {
		if (speed < 0)
			speed *= -1;
		return ("" + CMD_BEGIN + CMD_TYPE_MOTOR_A + CMD_NEXT_PART + String.valueOf(speed) + CMD_NEXT_PART + dir + CMD_END + '\n');
	}
	
	public static String createMotorCCommand(int speed, char dir) {
		if (speed < 0)
			speed *= -1;
		return ("" + CMD_BEGIN + CMD_TYPE_MOTOR_C + CMD_NEXT_PART + String.valueOf(speed) + CMD_NEXT_PART + dir + CMD_END + '\n');
	}
	
	public static String createMotorACommand(int speed, boolean dir) {
		if(dir) 
			return createMotorACommand(speed, FWD);
		else 
			return createMotorACommand(speed, REV);
	}
	
	public static String createMotorCCommand(int speed, boolean dir) {
		if(dir) 
			return createMotorCCommand(speed, FWD);
		else 
			return createMotorCCommand(speed, REV);
	}
}
