package robocup.robot;

import robocup.robot.interfaces.IMotorControl;

public class PriorityWrapper implements IMotorControl {

	public static final int PRIORITY_GAP = -10;
	public static final int PRIORITY_FOLLOWING_LINE = 0;
	public static final int PRIORITY_AVOIDINGWALL = 3;
	public static final int PRIORITY_OBSTACLE = 5;
	public static final int PRIORITY_RAMP = 10;
	public static final int PRIORITY_REMOTE = 100;
	public static final int PRIORITY_REDZONE = 1000;
	
	private static int currentPriority = -100;
	
	private final int priority;
	
	private static IMotorControl targetRobot; 
	
	public static void setRobot(IMotorControl robot) {
		targetRobot = robot;
	}
	
	public PriorityWrapper(int priority) {
		this.priority = priority;
	}
	
	public void driveCircle(int dir, int rad) {
		if(priority >= currentPriority) {
			targetRobot.driveCircle(dir, rad);
		}
	}

	public void driveCircle(int dir, int rad, int speed) {
		if(priority >= currentPriority) {
			targetRobot.driveCircle(dir, rad, speed);
		}
	}

	public void setDefaultSpeed(int speed) {
		if(priority >= currentPriority) {
			targetRobot.setDefaultSpeed(speed);
		}
	}

	public void close() {
		// DO NOTHING, because there might be other clients
		//targetRobot.close();
	}

	public void setMotorLeft(boolean dir, int speed) {
		if(priority >= currentPriority) {
			targetRobot.setMotorLeft(dir, speed);
		}
	}

	public void setMotorRight(boolean dir, int speed) {
		if(priority >= currentPriority) {
			targetRobot.setMotorRight(dir, speed);
		}
	}

	public void setup() {
		// should be done before wrapping
		//		targetRobot.setup();
	}

	public static void setMinimumPriority(int p) {
		if( p > currentPriority) {
			currentPriority = p;
		}
	}
	
	public static void setPriority(int p) { 
		currentPriority = p;
	}

	public void setMotors(boolean dirLeft, int speedLeft, boolean dirRight,
			int speedRight) {
		if(priority >= currentPriority) {
			targetRobot.setMotors(dirLeft, speedLeft, dirRight, speedRight);
		}
	}
}
