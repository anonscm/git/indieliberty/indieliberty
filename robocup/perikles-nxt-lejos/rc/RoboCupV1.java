package rc;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import lejos.navigation.Pilot;
import lejos.navigation.TachoPilot;
import lejos.nxt.Button;
import lejos.nxt.ButtonListener;
import lejos.nxt.Motor;
import lejos.nxt.SensorPort;
import lejos.nxt.TouchSensor;
import lejos.nxt.comm.BTConnection;
import lejos.nxt.comm.Bluetooth;
import lejos.nxt.comm.USB;
import lejos.nxt.comm.USBConnection;

public class RoboCupV1 {
	
	// loggen??
	private static final boolean doLog = false;

	// maximale anzahl der zeichen in 'extras'
	private static final int MAX_EXTRAS_SIZE = 7; // momentan nur Radius, dafuer reichts  (6 ziffern + vorzeichen)
	
	// Laenge eines Befehls:
	private static final int MAX_COMMAND_SIZE = MAX_EXTRAS_SIZE + 11;

	
	/* Roboter Konstanten */
	private static final float wheelDiameter = 6.5f; 	// Die reifen vom blauen LEGO truck
	private static final float trackWidth = 14f; 		
	private static final Motor motorLeft = Motor.C;
	private static final Motor motorRight = Motor.A;
	private static final boolean reversed = true;
	
	
	/* char Konstanten..... */
	
	private static final char CMD_BEGIN = 'a';
	private static final char CMD_NEXT_PART = 'b';
	private static final char CMD_END = 'c'; 
	
	private static final char CMD_TYPE_MOTOR_A = 'k';
	private static final char CMD_TYPE_MOTOR_C = 'l';
	private static final char CMD_TYPE_CIRCLE = 'm';
	
	private static final char FWD = 'u';
	private static final char REV = 'v';
	
	private static final char LEFT = 'x';
	private static final char RIGHT = 'y';
	
	/* Aus Perfomance-Gruenden vom Compiler umwandeln lassen, byte = 1byte, char = 2byte ,ausserdem viele casts */
	
	private static final byte bCMD_BEGIN = (byte) CMD_BEGIN;
	private static final byte bCMD_NEXT_PART = (byte) CMD_NEXT_PART;
	private static final byte bCMD_END = (byte) CMD_END; 
	
	private static final byte bCMD_TYPE_MOTOR_A = (byte) CMD_TYPE_MOTOR_A;
	private static final byte bCMD_TYPE_MOTOR_C = (byte) CMD_TYPE_MOTOR_C;
	private static final byte bCMD_TYPE_CIRCLE = (byte) CMD_TYPE_CIRCLE;
	
	private static final byte bFWD = (byte) FWD;
	private static final byte bREV = (byte) REV;
	
	@SuppressWarnings("unused")
	private static final byte bLEFT = (byte) LEFT;
	private static final byte bRIGHT = (byte) RIGHT;

	
	
	public static boolean run = true;
	public static boolean useUSB = true;
	
	public BTConnection c;
	public USBConnection usb;
	public DataInputStream in;
	public DataOutputStream out;
	
	Pilot pilot = new TachoPilot(wheelDiameter, trackWidth, motorLeft, motorRight, reversed);
	
	private static RoboCupV1 robocup;
	
	private long cmdCount = 0;
	
	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) {
		// automatic restarts:
		
		while(true) {
			run = true;
			robocup = new RoboCupV1();
			robocup.setUpConnection();
			new Thread(new Runnable(){
				public void run() {
					robocup.checkSensors();
				}
			}).start();
			robocup.remoteControl();
			robocup.closeConnection();
		}
	}
		
	public void setUpConnection() {
		Button.ESCAPE.addButtonListener(new ButtonListener(){

			public void buttonPressed(Button arg0) {
				run = false;
				stop();
				System.exit(0);
			}
			public void buttonReleased(Button arg0) {	
			}
			
		});
		
		if(useUSB) {
			log("Waiting for USB Connection...");
			usb = USB.waitForConnection();
			log("Connection established!");
			in = usb.openDataInputStream();
			out = usb.openDataOutputStream();	
		} else {
			log("Waiting for BT Connection...");
			c = Bluetooth.waitForConnection();
			log("Connection established!");
			in = c.openDataInputStream();
			out = c.openDataOutputStream();
		}		
	
		log("waiting for commands");
		

	}
	
	public void remoteControl()  {
		
		while(run){
			try {
				parseCommand();
			} catch (ParserException e) {
				log("ERR: " + e.getMessage());
				
			} catch (IOException e) {
				log("IOException");
				run = false;
			} 
		}
	}
	
	public void checkSensors() {
		System.out.println("setting up sensors...");
		
		TouchSensor touch = new TouchSensor(SensorPort.S1);

		System.out.println("starting sensor loop");
		//TODO: Implement this with listeners
		while(run) {
			if(touch.isPressed()) {
				//TODO: send Command
			}
			try {
				Thread.sleep(30);
			} catch (InterruptedException e) {
				//will not happen
			}
		}
	}

	private void parseCommand() throws IOException, ParserException {
		
		byte bCMD_TYPE;
		int speed;
		byte bDir;
		
		byte tmp = ' ';
	
		log("begin");

		// wenn mehrere Befehle in der Queue sind, die ersten ueberspringen =)
		while(in.available() > MAX_COMMAND_SIZE) {
			if(in.readByte() == bCMD_BEGIN)
				cmdCount++;
		}
		// auf Begin command warten...
		while( in.readByte() != bCMD_BEGIN);
		cmdCount++;
		
		log("type");
		// der naechste CHAR ist "bCMD_TYPE"
		bCMD_TYPE =  in.readByte();
		

		log("next part1");
		// dann kommt ein "bCMD_NEXT_PART"
		if((tmp = in.readByte()) != bCMD_NEXT_PART)
			throw new ParserException("1. NEXT_PART missing: " + tmp);

		
		log("speed");

		// speed
		byte[] tmpBytes = new byte[3];
		int i;
		for(i=0; i < 3; i++) {
			tmp= in.readByte();
			if(tmp == bCMD_NEXT_PART) {
				break;
			} else {
				tmpBytes[i] = tmp;
			}
		}

		log("speed = " + new String(tmpBytes,0,i));
		try {
			speed = Integer.parseInt(new String(tmpBytes,0,i));
		} catch(NumberFormatException e) {
			throw new ParserException("speed is NaN");
		}
		if(speed < 0) {
			throw new ParserException("speed < 0");
		}
		
		// next part ueberprufen
		if(tmp != bCMD_NEXT_PART) {
			tmp =  in.readByte();
			if(tmp != bCMD_NEXT_PART) {
				throw new ParserException("2. NEXT_PART missing: " + tmp);
			}
		}
		

		log("dir");
		// dir auslesen
		bDir =  in.readByte();
		

		log("next part / end (3)");
		// bCMD_NEXT_PART oder bCMD_END
		tmp = in.readByte(); 
		
		if(tmp == bCMD_END) {
			if(!excecuteCommand(bCMD_TYPE, speed, bDir, null)) {
				throw new ParserException("excecuteCommand return false");
			} else {
				return; // Parsen erfolgreich
			}
		}	

		log("next part (3)");
		// falls kein bCMD_END:
		if(tmp != bCMD_NEXT_PART) {
			throw new ParserException("3. NEXT_PART missing: " + tmp);
		}

		log("extras");
		tmpBytes = new byte[MAX_EXTRAS_SIZE]; 
		for(i=0; i < 3; i++) {
			tmp= in.readByte();
			if(tmp == bCMD_END) {
				break;
			} else {
				tmpBytes[i] = tmp;
			}
		}
		String extras = new String(tmpBytes, 0, i);
		
		if(!excecuteCommand(bCMD_TYPE, speed, bDir, extras)) {
			throw new ParserException("excecuteCommand return false");
		} else {
			 return; // Parsen erfolgreich
		}
	}
	
	
	private boolean excecuteCommand(byte bCMD_TYPE, int speed, byte dir, String extras) {
		log("EXE: " + bCMD_TYPE + "," + speed + "," + dir + "," + extras );
		if(bCMD_TYPE == bCMD_TYPE_MOTOR_A) {
			if(dir == bREV) {
				speed *= -1;
			} else if(dir != bFWD) {
				return false; // dir ist weder FWD, noch REV 
			}
			setMotor(Motor.A, speed);
			return true;
		} 
		if(bCMD_TYPE == bCMD_TYPE_MOTOR_C) {
			if(dir == bREV) {
				speed *= -1;
			} else if(dir != bFWD) {
				return false; // dir ist weder FWD, noch REV 
			}
			setMotor(Motor.C, speed);
			return true;
		}
		if(bCMD_TYPE == bCMD_TYPE_CIRCLE) {
			try {
				int radius = Integer.parseInt(extras);
				if(dir == bRIGHT) {// Lejos denkt andersrum als ich ;)
					radius *= -1;
				}
				System.out.println("c: " + cmdCount + " r: " + extras);
				pilot.stop();
				pilot.setSpeed(speed);
				if(radius == 0) {
					pilot.travel(5, true);
				} else {
					pilot.turn(radius, 30, true);
				}
				return true;
			} catch (NumberFormatException e) {
				return false; // einfach diesen Befehl ignorieren
			}
		
		}
		return false;
	}
	
	public void stop(){
		run = false;
	}
	
	public void setMotor(Motor motor, int speed){
		if(speed > 0){
			motor.setSpeed(speed);
			motor.forward();
		}
		else if(speed < 0){
			motor.setSpeed(speed);
			motor.backward();
		}else{
			motor.stop();
		}
	}
	
	public void closeConnection(){
		try {
			in.close();
			out.close();
			c.close();
		} catch (IOException e) {
		} catch (NullPointerException e2) {
		}

		System.exit(0);
	}
	
	private void log(String s) {
		if(doLog) {
			System.out.println(s);
		}
	}
	
	public static int byteArrayToInt(byte [] b, int offset) {
        return (b[offset] << 24)
                + ((b[offset+1] & 0xFF) << 16)
                + ((b[offset+2] & 0xFF) << 8)
                + (b[offset+3] & 0xFF);
	}
	
	class ParserException extends Exception {

		public ParserException(String s) {
			super(s);
		}

	}
}

