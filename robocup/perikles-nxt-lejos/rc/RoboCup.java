/* Version 2 of RoboCup <-> NXT communication spec
 * 
 * 
 * 
 * Command Formatting PC -> NXT:
 * (1Byte) CMD_BEGIN 
 * (2Bytes) CMD_id ([0..255^2]
 * (1Byte) CMD_TYPE_X
 * (1Byte) SPEED [0..255]
 * (1Byte) CMD_DIR_X
 * 
 * (1Byte) CMD_HAS_EXTRAS  			||  (1Byte) CMD_END
 * (1Byte) size of extras (in Byte)
 * (x Bytes) EXTRAS (z.b. radius)
 * (1Byte) CMD_END
 * 
 *  * TODO: ausschreiben:
 * 		byte[] cmd = new byte[11];
		cmd[0] = CMD_BEGIN;
		cmd[1] = UnsignedTypes.intToUnsignedByte(id / 255);
		cmd[2] = UnsignedTypes.intToUnsignedByte(id % 255);
		cmd[3] = CMD_TYPE_MOTOR_BOTH;
		cmd[4] = UnsignedTypes.intToUnsignedByte(speedLeft / 255);
		cmd[5] = UnsignedTypes.intToUnsignedByte(speedLeft % 255);
		cmd[6] = dirLeft ? FWD : REV;
		cmd[7] = UnsignedTypes.intToUnsignedByte(speedRight / 255);
		cmd[8] = UnsignedTypes.intToUnsignedByte(speedRight % 255);
		cmd[9] = dirRight ? FWD : REV;
		cmd[10] = CMD_END;
 * 
 * 
 * Command Formatting NXT -> PC:
 * (1Byte) CMD_BEGIN 
 * (1Byte) CMD_TYPE_X
 * 
 * (1Byte) CMD_HAS_EXTRAS  			||  (1Byte) CMD_END
 * (1Byte) size of extras (in Byte)
 * (x Bytes) EXTRAS (z.b. source-id)
 * (1Byte) CMD_END
 * 
 * 
 */

package rc;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import lejos.navigation.Pilot;
import lejos.navigation.TachoPilot;
import lejos.nxt.Button;
import lejos.nxt.ButtonListener;
import lejos.nxt.Motor;
import lejos.nxt.SensorPort;
import lejos.nxt.TouchSensor;
import lejos.nxt.UltrasonicSensor;
import lejos.nxt.comm.BTConnection;
import lejos.nxt.comm.Bluetooth;
import lejos.nxt.comm.USB;
import lejos.nxt.comm.USBConnection;

public class RoboCup {

	// loggen??
	private static final boolean doLog = false;

	// kommandos ausfuehren?
	private static final boolean doExcecute = true;

	// maximale anzahl der zeichen in 'extras'
	private static final int MAX_EXTRAS_SIZE = 7; // momentan nur Radius, dafuer
	// reichts (6 ziffern +
	// vorzeichen)

	// Laenge eines Befehls:
	private static final int MAX_COMMAND_SIZE = MAX_EXTRAS_SIZE + 11;

	/* Roboter Konstanten */
	private static final float wheelDiameter = 6.5f; // Die reifen vom blauen
	// LEGO truck
	private static final float trackWidth = 14f;
	private static final Motor motorLeft = Motor.C;
	private static final Motor motorRight = Motor.A;
	private static final boolean reversed = true;

	/* CMD Konstanten */// TODO: zusammenfassen mit denen in NXTV2
	private static byte CMD_BEGIN = 'a';
	private static byte CMD_END = 'b';
	private static byte CMD_HAS_EXTRAS = 'c';

	private static byte CMD_TYPE_SENSOR_TOUCH = 'f';
	private static byte CMD_TYPE_SENSOR_US = 'g';

	private static byte CMD_TYPE_MOTOR_BOTH = 'j';
	private static byte CMD_TYPE_MOTOR_A = 'k';
	private static byte CMD_TYPE_MOTOR_C = 'l';
	private static byte CMD_TYPE_CIRCLE = 'm';
	private static byte CMD_TYPE_PING = 'n';

	public static byte FWD = 'u';
	public static byte REV = 'v';

	public static byte LEFT = 'x';
	public static byte RIGHT = 'y';
	public static byte RAMP = 'z';

	private static final byte RELEASED = 0;
	private static final byte PRESSED = 1;

	public static boolean run = true;
	public static boolean useUSB = true;

	public BTConnection c;
	public USBConnection usb;
	public DataInputStream in;
	public DataOutputStream out;

	Pilot pilot = new TachoPilot(wheelDiameter, trackWidth, motorLeft,
			motorRight, reversed);

	private static RoboCup robocup;

	private long cmdCount = 0;

	/**
	 * @param args
	 * @throws IOException
	 */
	public static void main(String[] args) {
		// automatic restarts: TODO: does not work on IOException

		while (run) {
			run = true;
			robocup = new RoboCup();
			robocup.setUpConnection();
			new Thread(new Runnable() {
				public void run() {
					try {
						robocup.checkSensors();
					} catch (IOException e) {
						System.err.println("IOException on writing...");
					}
				}
			}).start();
			robocup.remoteControl();
			robocup.closeConnection();
		}
	}

	public void setUpConnection() {
		Button.ESCAPE.addButtonListener(new ButtonListener() {

			public void buttonPressed(Button arg0) {
				// run = false;
				stop();

				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
				}

				closeConnection();
				System.exit(0);
			}

			public void buttonReleased(Button arg0) {
			}

		});

		if (useUSB) {
			log("Waiting for USB Connection...");
			usb = USB.waitForConnection();
			log("Connection established!");
			in = usb.openDataInputStream();
			out = usb.openDataOutputStream();
		} else {
			log("Waiting for BT Connection...");
			c = Bluetooth.waitForConnection();
			log("Connection established!");
			in = c.openDataInputStream();
			out = c.openDataOutputStream();
		}

		log("waiting for commands");

	}

	public void remoteControl() {

		while (run) {
			try {
				final byte[] id = parseCommand();
				sendPingReply(id);
			} catch (ParserException e) {
				log("ERR: " + e.getMessage());

			} catch (IOException e) {
				log("IOException");
				run = false;
			}
		}
	}

	public void checkSensors() throws IOException {
		System.out.println("setting up sensors...");

		TouchSensor leftTouch = new TouchSensor(SensorPort.S2);
		TouchSensor rightTouch = new TouchSensor(SensorPort.S3);
		// TouchSensor ramp = new TouchSensor(SensorPort.S1);

		UltrasonicSensor usLeft = new UltrasonicSensor(SensorPort.S1);
		// UltrasonicSensor usRight = new UltrasonicSensor(SensorPort.S3);

		System.out.println("starting sensor loop");
		// TODO: Implement this with listeners

		int lastLeftUS = 0;
		// int lastRightUS = 0;
		// boolean lastRamp = false;
		boolean lastLeft = false;
		boolean lastRight = false;
		try {
			while (run) {
				// touchsensor:

				// links
				if (lastLeft != leftTouch.isPressed()) {

					if (leftTouch.isPressed()) {
						sendTouchCommand(PRESSED, LEFT);
					} else {
						sendTouchCommand(RELEASED, LEFT);
					}

					lastLeft = leftTouch.isPressed();
				}
				// rechts
				if (lastRight != rightTouch.isPressed()) {

					if (rightTouch.isPressed()) {
						sendTouchCommand(PRESSED, RIGHT);
					} else {
						sendTouchCommand(RELEASED, RIGHT);
					}

					lastRight = rightTouch.isPressed();
				}

				// rampensensor
				// if(lastRamp != ramp.isPressed()) {
				//				
				// if(ramp.isPressed()) {
				//					
				// sendTouchCommand(PRESSED, RAMP);
				// } else if(!ramp.isPressed()) {
				// sendTouchCommand(RELEASED, RAMP);
				// }
				//				
				// lastRamp = ramp.isPressed();
				// }

				// usSensors:
				int left = usLeft.getDistance();
				// int right = usRight.getDistance();

				int toleranz = 1;

				if ((left > lastLeftUS + toleranz)
						|| (left < lastLeftUS - toleranz)) {

					sendUSChange(LEFT, left);
					lastLeftUS = left;
				}
				// if(right != lastRightUS) {
				//				
				// sendUSChange(RIGHT, right);
				// lastRightUS = right;
				// }
				//			
				Thread.sleep(30);
			}
		} catch (InterruptedException e) {
			// will not happen
		}
	}

	private void sendTouchCommand(byte state, byte location)
			throws IOException, InterruptedException {
		byte[] cmd = new byte[6];
		cmd[0] = CMD_BEGIN;
		cmd[1] = CMD_TYPE_SENSOR_TOUCH;
		cmd[2] = CMD_HAS_EXTRAS;
		cmd[3] = 2;
		cmd[4] = location;
		cmd[5] = state;
		out.write(cmd);
		out.flush();
		Thread.sleep(30); // damit die verbindung nicht ueberlastet wird.
	}

	void sendUSChange(byte location, int distance) throws IOException,
			InterruptedException {
		byte[] cmd = new byte[8];
		cmd[0] = CMD_BEGIN;
		cmd[1] = CMD_TYPE_SENSOR_US;
		cmd[2] = CMD_HAS_EXTRAS;
		cmd[3] = 3;
		cmd[4] = location;
		cmd[5] = intToUnsignedByte(distance / 256);
		cmd[6] = intToUnsignedByte(distance % 256);
		cmd[7] = CMD_END;
		out.write(cmd);
		out.flush();
		Thread.sleep(100); // damit die verbindung nicht ueberlastet wird
	}

	private byte[] parseCommand() throws IOException, ParserException {

		byte tmp = ' ';

		log("begin");

		// wenn mehrere Befehle in der Queue sind, die ersten ueberspringen =)
		while (in.available() > MAX_COMMAND_SIZE) {
			if (in.readByte() == CMD_BEGIN)
				cmdCount++;
		}
		// auf Begin command warten...
		while (in.readByte() != CMD_BEGIN)
			;
		cmdCount++;

		// id ist als zahl uninteressant
		log("id");
		byte[] id = new byte[2];
		id[0] = in.readByte();
		id[1] = in.readByte();

		log("type");
		// das naechste byte ist "CMD_TYPE"
		byte cmdType = in.readByte();

		if (cmdType == CMD_TYPE_MOTOR_BOTH) {
			log("CMD_TYPE_MOTOR_BOTH");

			byte[] speedLeft = new byte[2];
			byte[] speedRight = new byte[2];

			speedLeft[0] = in.readByte();
			speedLeft[1] = in.readByte();
			byte dirLeft = in.readByte();
			speedRight[0] = in.readByte();
			speedRight[1] = in.readByte();
			byte dirRight = in.readByte();

			int iSpeedLeft = unsignedByteToInt(speedLeft[0]) * 255
					+ unsignedByteToInt(speedLeft[1]);
			int iSpeedRight = unsignedByteToInt(speedRight[0]) * 255
					+ unsignedByteToInt(speedRight[1]);

			if (in.readByte() != CMD_END) {
				throw new ParserException("no CMD_END");
			}

			// linken motor
			if (dirLeft == REV) {
				iSpeedLeft *= -1;
			} else if (dirLeft != FWD) {
				throw new ParserException("dirLeft is wrong"); // dir ist weder
																// FWD, noch REV
			}
			setMotor(Motor.A, iSpeedLeft);

			// rechten motor
			if (dirRight == REV) {
				iSpeedRight *= -1;
			} else if (dirRight != FWD) {
				throw new ParserException("dirRight is wrong"); // dir ist weder
																// FWD, noch REV
			}
			setMotor(Motor.C, iSpeedRight);

			return id;
		} else {

			log("speed");

			// speed
			byte bSpeed = in.readByte();
			int iSpeed = unsignedByteToInt(bSpeed);

			log("speed = " + iSpeed);

			if (iSpeed < 0) {
				throw new ParserException("speed < 0");
			}

			log("dir");
			// dir auslesen
			byte dir = in.readByte();

			log("has extras / end (3)");
			// CMD_HAS_EXTRAS oder CMD_END
			tmp = in.readByte();

			if (tmp == CMD_END) {
				if (!excecuteCommand(cmdType, iSpeed, dir, null)) {
					throw new ParserException("excecuteCommand return false");
				} else {
					return id; // Parsen erfolgreich
				}
			}

			log("has extras");
			// falls kein CMD_END:
			if (tmp != CMD_HAS_EXTRAS) {
				throw new ParserException("HAS_EXTRAS missing: " + tmp);
			}

			int extrasSize = Integer.valueOf(in.readByte());
			log("extras-size: " + extrasSize);

			log("extras");
			byte[] extras = new byte[extrasSize];
			int i;
			for (i = 0; i < extrasSize; i++) {
				tmp = in.readByte();
				if (tmp == CMD_END) {
					break;
				} else {
					extras[i] = tmp;
				}
			}

			if (!excecuteCommand(cmdType, iSpeed, dir, extras)) {
				throw new ParserException("excecuteCommand return false");
			} else {
				return id; // Parsen erfolgreich
			}
		}
	}

	private boolean excecuteCommand(byte CMD_TYPE, int speed, byte dir,
			byte[] extras) {
		if (doExcecute) {
			if (extras == null) {
				log("EXE: " + CMD_TYPE + "," + speed + "," + dir + ","
						+ "nix extras");
			} else {
				log("EXE: " + CMD_TYPE + "," + speed + "," + dir + ","
						+ extras.toString());
			}
			if (CMD_TYPE == CMD_TYPE_MOTOR_A) {
				if (dir == REV) {
					speed *= -1;
				} else if (dir != FWD) {
					return false; // dir ist weder FWD, noch REV
				}
				setMotor(Motor.A, speed);
				return true;
			}
			if (CMD_TYPE == CMD_TYPE_MOTOR_C) {
				if (dir == REV) {
					speed *= -1;
				} else if (dir != FWD) {
					return false; // dir ist weder FWD, noch REV
				}
				setMotor(Motor.C, speed);
				return true;
			}
			if (CMD_TYPE == CMD_TYPE_CIRCLE) {
				try {
					int radius = unsignedByteToInt(extras[0]) * 256
							+ unsignedByteToInt(extras[1]);
					if (dir == RIGHT) {// Lejos denkt andersrum als ich ;)
						radius *= -1;
					}
					log("c: " + cmdCount + " r: " + radius);
					pilot.stop();
					// pilot.setSpeed(speed);
					if (radius == 0) {
						pilot.travel(5, true);
					} else {
						pilot.turn(radius, 30, true);
					}
					return true;
				} catch (NumberFormatException e) {
					log("NumberFormatException");
					return false; // einfach diesen Befehl ignorieren
				}

			}
			log("Command unknown");
			return false;
		} else {
			return true; // nicht ausfuehren immer erfolgreich
		}

	}

	/*
	 * Command Formatting NXT -> PC: (1Byte) CMD_BEGIN (1Byte) CMD_TYPE_X
	 * 
	 * (1Byte) CMD_HAS_EXTRAS || (1Byte) CMD_END (1Byte) size of extras (in
	 * Byte) (x Bytes) EXTRAS (z.b. source-id) (1Byte) CMD_END
	 */

	private void sendPingReply(byte[] id) throws IOException {
		byte[] cmd = new byte[7];
		cmd[0] = CMD_BEGIN;
		cmd[1] = CMD_TYPE_PING;
		cmd[2] = CMD_HAS_EXTRAS;
		cmd[3] = 2;
		cmd[4] = id[0];
		cmd[5] = id[1];
		cmd[6] = CMD_END;
		out.write(cmd);
		out.flush();
	}

	public void stop() {
		run = false;
	}

	public void setMotor(Motor motor, int speed) {
		if (speed > 0) {
			motor.setSpeed(speed);
			motor.forward();
		} else if (speed < 0) {
			motor.setSpeed(speed);
			motor.backward();
		} else {
			motor.stop();
		}
	}

	public void closeConnection() {
		try {
			in.close();
			out.close();
			c.close();
		} catch (IOException e) {
		} catch (NullPointerException e2) {
		}

		System.exit(0);
	}

	private void log(String s) {
		if (doLog) {
			System.out.println(s);
		}
	}

	public static int byteArrayToInt(byte[] b, int offset) {
		return (b[offset] << 24) + ((b[offset + 1] & 0xFF) << 16)
				+ ((b[offset + 2] & 0xFF) << 8) + (b[offset + 3] & 0xFF);
	}

	private static int unsignedByteToInt(byte b) {
		return (int) b & 0xFF;
	}

	public static byte intToUnsignedByte(int i) {
		return (byte) (i & 0xFF);
	}

	class ParserException extends Exception {

		public ParserException(String s) {
			super(s);
		}

	}
}
