#include <stdlib.h> 
#include <avr/io.h>
#include <avr/interrupt.h> 
#include "rncontrol.h"
#include "i2cmaster.h"


#define US1  0xFE      
#define US2  0xE0


/*### Variablen ###*/
uint16_t analog; //Variable für jeweils an einem Analogport gemessenen Wert, um nicht für eine Ausgabe mehrere Messungen durchführen zu müssen.
char wort[5];	//Zahlen (Integer und Float) müssen vor der Ausgabe per RS232 in ASCII-Zeichen konvertiert werden, für die ein Speicher benötigt wird.

volatile char inputbuffer[25];
volatile int inputcount = 0;
volatile int inputready = 0; // bool

volatile int vUS1 = 0;
volatile int vUS2 = 0;

volatile int countTimer2;




void init_analog(void)
{
	//Alle internen Pullups an, ausgenommen Port A3 und Batteriespannung/Taster (A6 und A7). Da A3 aber nun auch nicht auf GND liegt, ergibt sich ein "Rauschen", der Wert variiert mit jeder Messung mehr oder weniger stark.
	setportaon(0);
	setportaon(1);
	setportaon(2);
	setportaoff(3);
	setportaon(4);
	setportaon(5);	
}



unsigned char updateUS(int US) {
	 unsigned char b = i2c_start(US+I2C_WRITE);     
     i2c_write(0x00);                      
     i2c_write(0x51);                       
     i2c_stop();
     return b;                             
}

int getUS(int US) {
     unsigned char ret1;
     unsigned char ret2;
	
     i2c_start(US+I2C_WRITE);   
     i2c_write(0x02);                   
     i2c_rep_start(US+I2C_READ);       

	 ret1 = i2c_readAck();
     ret2 = i2c_readNak();                   
     i2c_stop();
          
     
 	 return ( (ret1 * 256) + ret2);    
}

void send_data(int count)
{
	if(count == 1) { 
		for(uint8_t i=0; i<8; i++)	
		{
			utoa(adcwert(i), wort, 10); sendUSART(wort); sendUSART("\n"); 
		}	
	} else if(count == 2) {		
		for(uint8_t i=0; i<8; i++)
		{
			if (PINB & (1<<i)) sendUSART("1");
			else sendUSART("0");
		}
		sendUSART("\n");	
	} else if(count == 3) {
		for(uint8_t i=0; i<8; i++)
		{
			if (PINC & (1<<i)) sendUSART("1");
			else sendUSART("0");
		}
		sendUSART("\n");
	} else if(count == 4) {
		for(uint8_t i=0; i<8; i++)
		{
			if (PIND & (1<<i)) sendUSART("1");
			else sendUSART("0");
		}
		sendUSART("\n");
	} else if(count == 5) {
		utoa(vUS1, wort, 10);
		sendUSART(wort);
		sendUSART("\n");
	} else if(count == 6) {
		utoa(vUS2, wort, 10);
		sendUSART(wort);
		sendUSART("\r\n");	
	}
}

void clear_buffer(void)
{
    while(UCSRA & (1<<RXC)) uart_getc();
}


void obstacle(int v) { // v = 60gradwinkel 
		// Mit voller geschw. fahren
		setPWMlinks(255);
		setPWMrechts(255);
		// links vor
		//Mlinksvor();
		//waitms(3000);
		// rechts vor
		//Mlinksstop();
		//Mrechtsvor();
		//waitms(3000);
		
		//zurueck
		Mlinkszur();
		Mrechtsvor();
		waitms(500);
		//links 60grad
		Mrechtszur();
		waitms(v);
		// vorwaerts
		Mlinksvor();
		waitms(1500);
		//rechts 60grad
		Mrechtsvor();
		waitms(v);
		//vorwaerts
		Mrechtszur();
		waitms(1800);
		// rechts 60grad
		Mrechtsvor();
		waitms(v);
		// stoppen
		Mrechtsstop();
		Mlinksstop();
}

void parseCommand() {
	/* Aufbau der Befehle:
	 * a
	 * Kategorie
	 * Wert
	 * q
	 * Beispiele:
	 * aA1255q : LinkerMotor, vorwärts, 255 Speed
	 * aL41q : Led 4 einschalten
	 * */
	 int i = 0;
	 if(inputbuffer[i++] == 'a') { // richtiger anfang??
	 	if(inputbuffer[i] == 'A') { //Linker Motor
	 		i++;
	 		if(inputbuffer[i++] == '1')
				Mlinksvor();
			else
				Mlinkszur();
			// ascii zu int
			int speed100 = inputbuffer[i++];
			int speed10 = inputbuffer[i++];
			int speed1 = inputbuffer[i++];
			int speed =  (speed100-48) * 100 + (speed10-48) * 10 +(speed1-48);	
			setPWMlinks(speed);	
	 	} else if(inputbuffer[i] == 'B') { // Rechter Motor
	 		i++;
	 		if(inputbuffer[i++] == '1')
				Mrechtsvor();
			else
				Mrechtszur();
			// ascii zu int
			int speed100 = inputbuffer[i++];
			int speed10 = inputbuffer[i++];
			int speed1 = inputbuffer[i++];
			int speed =  (speed100-48) * 100 + (speed10-48) * 10 +(speed1-48);
			setPWMrechts(speed);	
	 	} else if(inputbuffer[i] == 'L') { // LEDs
			i++;
	 		int tmp = (int)inputbuffer[i++] - 48; // direkt zu int konvertieren...
	 		if(inputbuffer[i++] == '1') 
	 			setportcoff(tmp);	 	
	 		else
	 			setportcon(tmp);			
	 	} else if(inputbuffer[i] == 'C') { //Special Commands
	 		i++;
	 		if(inputbuffer[i++] == 'H') {// Hindernis
	 			int speed100 = inputbuffer[i++];
				int speed10 = inputbuffer[i++];
				int speed1 = inputbuffer[i++];
				int speed =  (speed100-48) * 100 + (speed10-48) * 10 +(speed1-48);
	 			obstacle(speed);
	 		} 	
	 	}		
	 }		
	 inputready = 0;
	 inputcount = 0;
}


/*### Hauptschleife ###*/
int main(void)
{

	/*###Initialisierungsphase###*/

	//Pins bzw. Ports als Ein-/Ausgänge konfigurieren
	DDRA |= 0x00;	//00000000 -> alle Analogports als Eingänge
	DDRB |= 0x03;	//00000011 -> PORTB.0 und PORTB.1 sind Kanäle des rechten Motors
	DDRC |= 0xFC;	//11111100 -> PORTC.6 und PORTC.7 sind Kanäle des linken Motors, Rest sind LEDs für Lauflicht
	DDRD |= 0x30;	//00110000 -> PORTD.4 ist PWM-Kanal des linken Motors, PORTD.5 des rechten PORTD.7 ist der Speaker
	
	//Initialisierungen
	setportcon(0); setportcon(1); setportcoff(2); setportcoff(3); setportcoff(4); setportcoff(5); //LEDs ausschalten
	//setportdoff(7);	//Speaker aus
	init_timer1();	//Initialisierung Timer für PWM (motoren)

    i2c_init();                             // initialize I2C library

	init_analog();

	init_USART();	//USART konfigurieren
	
//waitms(3000);



	// Timer-Initialisierung:
//	TCCR2 = (1<<CS22) | (1<<WGM21);	// Prescaler von 1 | CTC-Modus (siehe unten für Beschreibung)
//	OCR2  = 365000;			// Vergleichswert 1ms !?
//	TIMSK |= (1<<OCIE2);		// Interrupts aktivieren und damit Timer starten
	
	UCSRB |= (1 << RXCIE); //enable RX interrupt 
	sei();	// enable interrupts


	Mlinksstop();
	Mrechtsstop();

	setPWMlinks(0);
	setPWMrechts(0);
	
	//Mlinksvor();
	//setPWMlinks(100);
//	sendUSART("anfang while schleife\r\n");
//	waitms(100);
	
	int count = 0;
	
	inputready = 0;
	while(1){
		for(count = 1; count < 7; count++) {
			//Sensordaten senden...
			send_data(count);
			waitms(2);
			
			// check for commands
			if(inputready == 1)
				parseCommand();
			else if(inputcount > 10) // fehler... - neu
				inputcount = 0;
			waitms(2);
			
			// US-Sensoren
			if(countTimer2 > 70) {
				vUS1 = getUS(US1);
				vUS2 = getUS(US2);
				updateUS(US1);
				updateUS(US2);
				countTimer2 = 0;
			}
			waitms(5);
		}
	}
	
	return 0;
}

ISR (TIMER2_COMP_vect) {
	countTimer2++;
}

ISR (USART_RXC_vect) {
	//uart_getc;
	
	inputbuffer[inputcount] = uart_getc();
	if(inputbuffer[inputcount] == 'q') 
		inputready = 1;
	inputcount++;
	
//	waitms(1);
}

/* #######################
 * #### PROGRAMM ENDE ####
 * ####################### 
 * */
	
	/*
	//sound(5,200);	
	//char Line[40];      // String mit maximal 39 zeichen 
	//uart_gets( Line, sizeof( Line ) ); 
	char c_on = 121; // ja = y = 121

	// GET LEDs
	char LED[6];
	for(int i=0; i < 6; i++)
		LED[i] = uart_getc();

	// GET MOTOR A
	int speedA[3];
	for(int i=0; i < 3;i++)
	{	
		speedA[i] = uart_getc(); // get ascii value
		speedA[i] = speedA[i] -48; // convert to int
	}
	char dirA = uart_getc();
	// GET MOTOR B
	int speedB[3];
	for(int i=0; i < 3;i++)
	{	
		speedB[i] = uart_getc(); // get ascii value
		speedB[i] = speedB[i] -48; // convert to int
	}
	char dirB = uart_getc();
	
	uart_getc(); // receive some weird character at the end of every message
	waitms(2);

	// SET LEDs
//	for(int i=0; i < 6; i++)		
//		sendchar(LED[i]);	
	for(int i=0; i < 6; i++)
	{	
		if(LED[i] == c_on)
			setportcoff(i);	
		else
			setportcon(i);
	}
	// SET MOTOR A
	if(dirA == c_on)
		Mlinksvor();
	else
		Mlinkszur();
	int speed = speedA[0]*100+speedA[1]*10+speedA[2];		
	setPWMlinks(speed);
//	utoa(speed, wort, 10); sendUSART(wort);
	// SET MOTOR B
	if(dirB == c_on)
		Mrechtsvor();
	else
		Mrechtszur();
	speed = speedB[0]*100+speedB[1]*10+speedB[2];
	setPWMrechts(speed);
//	utoa(speed, wort, 10); sendUSART(wort);
	send_data();
	waitms(2);
	clear_buffer(); */




/*#####################################################
 * set I2C ADDRESS for US
 * ###################################################### */



 /*    i2c_start_wait(US1+I2C_WRITE);     
     i2c_write(0x00);
     i2c_write(0xA0); 
     i2c_stop();      
     waitms(200);                       
     i2c_start_wait(US1+I2C_WRITE);     
     i2c_write(0x00);                   
     i2c_write(0xAA);                 
     i2c_stop();      
     waitms(200);                       
     i2c_start_wait(US1+I2C_WRITE);     
     i2c_write(0x00);                   
     i2c_write(0xA5);                 
     i2c_stop();      
     waitms(200);                       
     i2c_start_wait(US1+I2C_WRITE);     
     i2c_write(0x00);                   
     i2c_write(0xFE);                
     i2c_stop();                        */
