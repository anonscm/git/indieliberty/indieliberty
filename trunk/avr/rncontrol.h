/*
###################################################
rncontrol.h

Diese Header-Datei stellt grundlegende Funktionen für das RN-Control 1.4 in C zur Verfügung.

Autor: Georg Richter
#######################################################
*/


#include <util/delay.h>

#ifndef F_CPU
#warning "F_CPU war noch nicht definiert, wird nun nachgeholt mit"
#define F_CPU 16000000UL    // Systemtakt in Hz - Definition als unsigned long beachten >> Ohne ergeben Fehler in der Berechnung
#endif
 
#define BAUD 9600UL          // Baudrate
 
// Berechnungen
#define UBRR_VAL ((F_CPU+BAUD*8)/(BAUD*16)-1)   // clever runden
#define BAUD_REAL (F_CPU/(16*(UBRR_VAL+1)))     // Reale Baudrate
#define BAUD_ERROR ((BAUD_REAL*1000)/BAUD) // Fehler in Promille, 1000 = kein Fehler.

/* 
#if ((BAUD_ERROR<990) || (BAUD_ERROR>1010))
  #error Systematischer Fehler der Baudrate grösser 1% und damit zu hoch! 
#endif
*/

#define pressed 1
#define released 0

/*### waitms - Programm pausieren lassen ###*/

/*Die Funktion lässt circa so viele Millisekunden verstreichen, wie angegeben werden.
Angepasst auf das RN-Control 1.4 mit 16 MHz-Quarz!
Vorsicht, Wert ist nur experimentell angenähert, nicht exakt berechnet!*/

void waitms(uint16_t ms)
{
	for(; ms>0; ms--)
	{
		uint16_t __c = 4000;
		__asm__ volatile (
			"1: sbiw %0,1" "\n\t"
			"brne 1b"
			: "=w" (__c)
			: "0" (__c)
		);
	}
}




/*### Ports setzen ###*/

//Ports auf HIGH setzen
static inline void setportaon(const uint8_t n)
{PORTA |= (1<<n);}	//set PORTA.n high

static inline void setportbon(const uint8_t n)
{PORTB |= (1<<n);}	//set PORTB.n high

static inline void setportcon(const uint8_t n)
{PORTC |= (1<<n);}	//set PORTC.n high

static inline void setportdon(const uint8_t n)
{PORTD |= (1<<n);}	//set PORTD.n high



//Ports auf LOW setzen
static inline void setportaoff(const uint8_t n)
{PORTA &= ~(1<<n);}	//set PORTA.n low

static inline void setportboff(const uint8_t n)
{PORTB &= ~(1<<n);}	//set PORTB.n low

static inline void setportcoff(const uint8_t n)
{PORTC &= ~(1<<n);}	//set PORTC.n low

static inline void setportdoff(const uint8_t n)
{PORTD &= ~(1<<n);}	//set PORTD.n low





/*### Senden per USART - RS232-Kommunikation ###*/

/*Zum senden von Zeichen im Hauptprogramm entweder

char irgendwas[] = "meintext";
sendUSART(irgendwas);

oder direkt

sendUSART("meinText");

verwenden.*/ 

void init_USART(void)
{
//  UBRRH = UBRR_VAL >> 8;
//  UBRRL = UBRR_VAL & 0xFF; 
  
  UBRRH = 0;
  UBRRL = 25; // BAUD38400    51; // BAUD19200

  UCSRB |= ( 1 << RXEN ); //UART RX (receive) einschalten

  UCSRB |= (1<<TXEN);   //UART TX (Transmit - senden) einschalten
  UCSRC |= (1<<URSEL)|(3<<UCSZ0);   //Modus Asynchron 8N1 (8 Datenbits, No Parity, 1 Stopbit)
}


void sendchar(unsigned char c)
{
	while(!(UCSRA & (1<<UDRE))) {} //Warten, bis Senden möglich ist
	UDR = c; //schreibt das Zeichen aus 'c' auf die Schnittstelle
} 


void sendUSART(char *s) //*s funktiniert wie eine Art Array - auch bei einem String werden die Zeichen (char) einzeln ausgelesen - und hier dann auf die Sendeschnittstelle übertragen
{
	while(*s)
	{
		sendchar(*s);
		s++;
	}
}

/* Zeichen empfangen */
uint8_t uart_getc(void)
{
    while (!(UCSRA & (1<<RXC)))   // warten bis Zeichen verfuegbar
        ;
    return UDR;                   // Zeichen aus UDR an Aufrufer zurueckgeben
}
 
void uart_gets( char* Buffer, uint8_t MaxLen )
{
  uint8_t NextChar;
  uint8_t StringLen = 0;
 
  NextChar = uart_getc();         // Warte auf und empfange das nächste Zeichen
 
                                  // Sammle solange Zeichen, bis:
                                  // * entweder das String Ende Zeichen kam
                                  // * oder das aufnehmende Array voll ist
  while( NextChar != '\n' && StringLen < MaxLen - 1 ) {
    *Buffer++ = NextChar;
    StringLen++;
    NextChar = uart_getc();
  }
 
                                  // Noch ein '\0' anhängen um einen Standard
                                  // C-String daraus zu machen
  *Buffer = '\0';
}




/*### ADC-Ansteuerung ###*/

uint16_t adcwert(uint8_t kanal)
{
	uint16_t wert = 0; //Variable für Ergebnis deklarieren

	ADCSRA = (1<<ADEN)|(1<<ADPS2)|(1<<ADPS1)|(1<<ADPS0);	//ADEN aktiviert überhaupt erst den internen ADC-Wandler, ADPS2 bis ADPS0 stellen den verwendeten Prescaler ein, denn die Wandlerfrequenz muss immer zwischen 50 und 200 kHz liegen! Der Prescaler muss bei 16MHz also zwischen 80 und 320 eingestellt werden, als einzige Möglichkeit bleibt hier 128 (=alle auf 1).   

	ADMUX = kanal;
	//ADMUX = (1<<REFS1)|(1<<REFS0); //Einstellen der Referenzspannung auf "extern", also REFS1 und REFS0 auf "0" - daher auskommentierte Zeile
	
	ADCSRA |= (1<<ADSC);	//nach Aktivierung des ADC wird ein "Dummy-Readout" empfohlen, man liest also einen Wert und verwirft diesen, um den ADC "warmlaufen zu lassen"      
    while(ADCSRA & (1<<ADSC)) {} //auf Abschluss der Konvertierung warten
	wert = ADCW;	//ADCW muss einmal gelesen werden, sonst wird Ergebnis der nächsten Wandlung nicht übernommen.
 
	/* Eigentliche Messung - Mittelwert aus 4 aufeinanderfolgenden Wandlungen */
	wert = 0; 
	for(uint8_t i=0; i<4; i++)
	{
		ADCSRA |= (1<<ADSC); 	//eine Wandlung "single conversion" starten
		while(ADCSRA & (1<<ADSC)) {} 	//auf Abschluss der Konvertierung warten
		wert = wert + ADCW;	 //Wandlungsergebnisse aufaddieren
    }
	
	ADCSRA &= ~(1<<ADEN);	//ADC deaktivieren
 
	wert = wert/4;		//Durchschnittswert bilden
 
	return wert;
}





/*### Buttonabfrage ###*/

uint8_t button(void)
{
	uint8_t taste = 0; 	//Variable für Nummer des Tasters
	uint16_t analog7 = adcwert(7);	//Wert des Ports
	
	setportaon(7);		//Ohne das hier "flackern" die Werte aus irgend einem Grund -> es werden mitunter Tasten erkannt, die gar nicht gedrückt wurden oder das Programm bleibt für einige Sekunden "hängen"
	waitms(1);
	setportaoff(7);
	
	//Abfrage des gedrückten Tasters - um Störungen zu vermeiden wurden
        //die Bereiche sehr eng gefasst, sollten bei Bedarf an jedes Board extra angepasst werden.
	if((analog7>=337) && (analog7<=343)) {taste = 1;}
	else if((analog7>=268) && (analog7<=274)) {taste = 2;}
	else if((analog7>=200) && (analog7<=206)) {taste = 3;}
	else if((analog7>=132) && (analog7<=138)) {taste = 4;}
	else if((analog7>=64) && (analog7<=70)) {taste = 5;}
	else {}
	
	return taste;
}




/*### Sound durch den Speaker ausgeben ###*/

/*i wird immer um das doppelte der Tonhöhe hochgezählt. Grund: Um so tiefer
der Ton, desto größer ist "hoehe" -> desto länger sind die Pausenzeiten pro
"Tonschwingung". Dadurch würden tiefe Töne immer länger werden, was durch
diese Funktion abgefangen wird. Alle Töne mit der Länge x werden auch
ca x ms lang gespielt. Vernachlässigt wird hierbei jedoch die benötigte Zeit
zum Umschalten der Pins und Hochzählen der Schleife, um so höher der Ton, 
desto länger wird er also real gespielt, weil öfter gezählt und umgeschaltet
werden muss. Bei hoehe=1 benötigt ein Ton etwa das 1,733-fache der Zeit, die
als Dauer angegeben wird; bei hoehe=30 wird sie hingegen beinahe eingehalten.
Der Multiplikator von 15 in der Länge gleicht die seltsame Dauer, die die
Delay-Schleife hat, aus (die kommt aus irgend einem Grund nicht mal annähernd
an Millisekunden heran).*/

void sound(uint8_t hoehe, uint16_t laenge)
{
	for(uint16_t i=0; i<laenge*15; i=i+(2*hoehe))
	{
	setportdon(7);
	_delay_ms(hoehe);
	setportdoff(7);
	_delay_ms(hoehe);
	}
}





/*### PWM-Routinen zur Motoransteuerung ###*/

void init_timer1(void)	//Initialisierung des Timers für Erzeugung des PWM-Signals
{
   /* normale 8-bit PWM aktivieren (nicht invertiert),
   Das Bit WGM10 wird im Datenblatt auch als PWM10 bezeichnet */
   TCCR1A = (1<<COM1A1)|(1<<COM1B1)|(1<<WGM10);

   /* Einstellen der PWM-Frequenz auf 14 kHz ( Prescaler = 1 ) */
   TCCR1B = (1<<CS10);

   /* Interrupts für Timer1 deaktivieren
   Achtung : Auch die Interrupts für die anderen Timer stehen in diesem Register */
   TIMSK &= ~0x3c;
}



void setPWMlinks(uint8_t speed) //Geschwindigkeit linker Motor
{OCR1BL = speed;}

void setPWMrechts(uint8_t speed) //Geschwindigkeit rechter Motor
{OCR1AL = speed;}



void Mlinkszur(void)	//Uhrzeigersinn
{PORTC |= (1<<PC6); PORTC &= ~(1<<PC7);}

void Mlinksvor(void)	//mathematischer Drehsinn
{PORTC &= ~(1<<PC6); PORTC |= (1<<PC7);}

void Mlinksstop(void)	//aus
{ PORTC &= ~(1<<PC6); PORTC &= ~(1<<PC7);}



void Mrechtsvor(void)	//Uhrzeigersinn
{PORTB |= (1<<PB0); PORTB &= ~(1<<PB1);}

void Mrechtszur(void)	//mathematischer Drehsinn
{PORTB &= ~(1<<PB0); PORTB |= (1<<PB1);}

void Mrechtsstop(void)	//aus
{PORTB &= ~(1<<PB0); PORTB &= ~(1<<PB1);}	

