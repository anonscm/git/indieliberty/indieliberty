#include <stdlib.h>
#include <gst/gst.h>
#include <gtk/gtk.h>


static int width = 176;
static int height = 144;
static int bpp = 24;
GtkWidget *image;

#define VIDEO_SRC "v4l2src"

static gboolean buffer_probe_callback(
		GstElement *image_sink,
		GstBuffer *buffer, GstPad *pad, void *selfPointer)
{
  unsigned char *buffer_data = (unsigned char *) GST_BUFFER_DATA(buffer);
  image = (Image)buffer_data;
  printf("%d\n",buffer_data[1]);

  return TRUE;
}


static void bus_callback(GstBus *bus, GstMessage *message, void *appdata)
{
	gchar *message_str;
	GError *error;

	if(GST_MESSAGE_TYPE(message) == GST_MESSAGE_ERROR)
	{
		gst_message_parse_error(message, &error, &message_str);
		g_error("GST error: %s\n", message_str);
		g_free(error);
		g_free(message_str);
	}

	if(GST_MESSAGE_TYPE(message) == GST_MESSAGE_WARNING)
	{
		gst_message_parse_warning(message, &error, &message_str);
		g_warning("GST warning: %s\n", message_str);
		g_free(error);
		g_free(message_str);
	}
}


void init_gstreamer(){
	  GstElement *pipeline, *camera_src, *image_sink;
	  GstElement *csp_filter, *image_filter;
	  GstCaps *caps;
	  GstBus *bus;

	  /* Initialize Gstreamer */
	  gst_init(0, NULL);

	  /* Create pipeline and attach a callback to it's
	   * message bus */
	  pipeline = gst_pipeline_new("grab-camera");

	  bus = gst_pipeline_get_bus(GST_PIPELINE(pipeline));
	  gst_bus_add_watch(bus, (GstBusFunc)bus_callback, NULL);
	  gst_object_unref(GST_OBJECT(bus));

	  /* Create elements */
	  /* Camera video stream comes from a Video4Linux driver */
	  camera_src = gst_element_factory_make(VIDEO_SRC, "camera_src");
	  /* Colorspace filter is needed to make sure that sinks understands
	   * the stream coming from the camera */
	  csp_filter = gst_element_factory_make("ffmpegcolorspace", "csp_filter");

	  /* Filter to convert stream to useable format */
	  image_filter = gst_element_factory_make("ffmpegcolorspace", "image_filter");
	  /* A dummy sink for the image stream. Goes to bitheaven */
	  image_sink = gst_element_factory_make("fakesink", "image_sink");

	  /* Check that elements are correctly initialized */
	  if(!(pipeline && camera_src  && csp_filter
	       && image_filter && image_sink))
	    {
	      g_critical("Couldn't create pipeline elements");
	      //throwException(env, RUNTIME_EX, "Couldn't create pipeline elements");
	      return;
	    }

	  /* Set image sink to emit handoff-signal before throwing away
	   * it's buffer */
	  g_object_set(G_OBJECT(image_sink), "signal-handoffs", TRUE, NULL);

	  /* Add elements to the pipeline. This has to be done prior to
	   * linking them */

	  gst_bin_add_many(GST_BIN(pipeline), camera_src, csp_filter,
			   image_filter, image_sink, NULL);

	  /* Specify what kind of video is wanted from the camera */
	  caps = gst_caps_new_simple("video/x-raw-rgb",
				     "width", G_TYPE_INT, width,
				     "height", G_TYPE_INT, height,
				     NULL);


	  /* Link the camera source and colorspace filter using capabilities
	   * specified */
	  if(!gst_element_link_filtered(camera_src, csp_filter, caps))
	    {
	      //throwException(env, RUNTIME_EX, "Couldn't link elements: camera_src, csp_filter, caps");
	      return;
	    }
	  gst_caps_unref(caps);

	  /* gdkpixbuf requires 8 bits per sample which is 24 bits per
	   * pixel */
	  caps = gst_caps_new_simple("video/x-raw-rgb",
				     "width", G_TYPE_INT, width,
				     "height", G_TYPE_INT, height,
				     "bpp", G_TYPE_INT, bpp,
				     "depth", G_TYPE_INT, bpp,
				     "framerate", GST_TYPE_FRACTION, 15, 1,
				     NULL);

	  /* Link the image-branch of the pipeline. The pipeline is
	   * ready after this */
	  if(!gst_element_link_many(csp_filter, image_filter, NULL)) {
	    //throwException(env, RUNTIME_EX, "Couldn't link elements: csp_filter, image_filter");
	    return;
	  };
	  if(!gst_element_link_filtered(image_filter, image_sink, caps)) {
	    //throwException(env, RUNTIME_EX, "Couldn't link elements: image_filter, image_sink, caps");
	    return;
	  };

	  gst_caps_unref(caps);

	  g_signal_connect(G_OBJECT(image_sink), "handoff", G_CALLBACK(buffer_probe_callback), image_sink);

	  gst_element_set_state(pipeline, GST_STATE_PLAYING);
	  return;
}

int main(int argc, char *argv){
	init_gstreamer();

    GtkWidget *window;


    gtk_init (&argc, &argv);

    window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
    gtk_widget_show  (window);

    gtk_main ();


	return;
}
