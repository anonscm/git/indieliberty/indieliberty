/*=== Thresholds for skin color ====*/
#define MIN_THRESH_r	0.38
#define MAX_THRESH_r	0.48

/*=== for skin map =================*/
#define BLOCK_SIZE		3	/* must be odd nubmer */
#define THRESH_SKIN_PIXEL	50*50

/*=== Definitions for display point by OpenGL ===*/
#define DisplayPoint( x , y ) glBegin(GL_POINTS); glVertex2f( (x) , (y) ); glEnd();

/*=== Prototypes ===*/
void ExtractSkinColorSimpleThresholding( unsigned char *image , int iheight , int iwidth , unsigned char *skin_map );
void ShowSkinRegion( unsigned char *skin_map , int iheight , int iwidth );
