#include <stdio.h> 
#include <stdlib.h>
#include <math.h>
#include <string.h>

#include <GL/gl.h>		/* for GLUT */
#include <GL/glu.h>
#include <GL/glut.h>

#include "capture_v4l.h"	/* for capture by V4L */

/*==========================================================
  Definitions for OpenGL
==========================================================*/
#define DEFAULT_ZOOM_RATE	1
#define INIT_WINDOW_POS_X	200
#define INIT_WINDOW_POS_Y	200

/*==========================================================
  Function Prototypes
==========================================================*/
/*=== show_video_v4l_capture.c ===*/
int ShowVideoInitCaptureDevice( char *device_name , int channel_no );
void ShowVideoCaptureImage();
void ShowVideoDisplayImage();
void ShowVideoMouseCheck( int button , int status , int x , int y );
void ShowVideoChangeOrderImageRow( unsigned char *image , int iwidth , int iheight );
void ShowVideoSavePPMImage( unsigned char *image , int iheight , int iwidth , int frame_no , char *image_prefix );




