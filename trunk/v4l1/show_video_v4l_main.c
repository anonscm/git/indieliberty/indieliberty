#include "show_video_v4l.h"

int extract_skin_color;	/* global variable for enabling image processing */

int main( int argc , char *argv[] )
{

    static char window_title[1024]="Video Capture";
    int video_input;
    double zoom_rate;	/* for zooming */

/*======= Set option =========================================*/
    zoom_rate = DEFAULT_ZOOM_RATE;
    video_input = S_VIDEO;
    extract_skin_color = 1;


/*======= Initialize GLUT ====================================*/
    glutInit( &argc , argv );
    glutInitDisplayMode( GLUT_RGBA | GLUT_DOUBLE ); /* enable double buffering of video board */

/*======= Initialize window ==================================*/
    glutInitWindowPosition( INIT_WINDOW_POS_X , INIT_WINDOW_POS_Y );	/* initial window position */
    glutInitWindowSize( 325 , 120 );
    glutCreateWindow( window_title );	/* creat window with name */

/*======= Initialize projection ==============================*/
    glOrtho( 0.0 , 325-1.0 , 0.0 , 120 , -1.0 , 1.0 );
    glPixelZoom( zoom_rate , zoom_rate );

/*======= Initilize capture device ============================*/
    ShowVideoInitCaptureDevice( DEFAULT_DEVICE_NAME , video_input );

/*======= Initilize buffer ====================================*/
    glClearColor( 0.0 , 0.0 , 0.0 , 0.0 );
    glClear( GL_COLOR_BUFFER_BIT );
    glutSwapBuffers();
    glFlush();

/*======= Register callback functions =========================*/
    glutDisplayFunc( ShowVideoDisplayImage );
    glutMouseFunc( ShowVideoMouseCheck );
    glutIdleFunc( ShowVideoCaptureImage );	/* global idle callback */

/*======= Begin event loop ====================================*/
    glutMainLoop();

    exit(0);

}
