#include "show_video_v4l.h"
#include "extract_skin_color.h"
#include <wand/MagickWand.h>
#include <string.h>

#define PREFIX_IMAGE_FILE 	"video_image"	

/*=== Local global variables ===*/
static unsigned char disp_image[IMAGE_WIDTH_DS*IMAGE_HEIGHT_DS*RGB];/* pointer to image buffer */
static unsigned char resampled_image[IMAGE_WIDTH_DS*2*IMAGE_HEIGHT_DS*2*RGB];/* pointer to image buffer */
static unsigned char tmp[16*12*RGB];
static unsigned char tmp2[160*120*RGB];
static unsigned char test[160*120*RGB];

static int fd;			/* file descriptor for frame buffer */
static int exit_flag;		/* flag to exit program */
static int frame_no=1;		/* frame number */
static int pic_count = 0;

static struct video_mbuf vm; 	/* structure for frame buffer */
static struct video_mmap vmap;	/* structure for memory space for frame buffer */

/*=== global variable ===*/
extern int extract_skin_color;	/* global variable for enabling image processing */

char* Resample(unsigned char *_data, unsigned char *destination, int oldWidth, int oldHeight, int newWidth, int newHeight)
{
        //if(_data == NULL) return false;
        //
        // Get a new buuffer to interpolate into
        unsigned char newData[newWidth * newHeight * 3];// = new unsigned char [newWidth * newHeight * 3];

        double scaleWidth =  (double)newWidth / (double)oldWidth;
        double scaleHeight = (double)newHeight / (double)oldHeight;
	int cy;
	int cx;
        for(cy = 0; cy < newHeight; cy++)
        {
            for(cx = 0; cx < newWidth; cx++)
            {
                int pixel = (cy * (newWidth *3)) + (cx*3);
                int nearestMatch =  (((int)(cy / scaleHeight) * (oldWidth *3)) + ((int)(cx / scaleWidth) *3) );
                
                destination[pixel    ] =  _data[nearestMatch + 2];
                destination[pixel + 1] =  _data[nearestMatch + 1];
                destination[pixel + 2] =  _data[nearestMatch + 0];
            }
        }
	//destination = newData;
        return newData;
}

int TurnImageUpsideDown(unsigned char *image, unsigned char *destination, int width, int height){
	//unsigned char *cache;
	//cache = image;
	int cy;
	int cx;
	int pixel_pos = 0;
	
        for(cy = height; cy > 0; cy--)
        {
            for(cx = 0; cx < width; cx++)
            {
		int pixel = ((cy * (width *3)) + (cx*3));
                destination[pixel    ] =  image[(pixel_pos + cx)*3 + 0];
                destination[pixel + 1] =  image[(pixel_pos + cx)*3 + 1];
                destination[pixel + 2] =  image[(pixel_pos + cx)*3 + 2];
            }
	    pixel_pos += width;
        }
	return 0;

}

/*int TurnImageUpsideDown(unsigned char *image, int width, int height){
	unsigned char cache[width*height*RGB];
	int i;
	for(i = 0; i < (width*height*RGB); i++){
		cache[i] = image[i];
	} 
	int cy;
	int cx;
	int pixel_pos = 0;
	
        for(cy = height; cy > 0; cy--)
        {
	   
            for(cx = 0; cx < width; cx++)
            {
		int pixel = ((cy * (width *3)) + (cx*3));
                image[pixel    ] =  cache[(pixel_pos + cx)*3 + 0];
                image[pixel + 1] =  cache[(pixel_pos + cx)*3 + 1];
                image[pixel + 2] =  cache[(pixel_pos + cx)*3 + 2];
            }
	    pixel_pos += width;
        }
 	printf("Loop %d\n", cy);
	return 0;

}*/


void ShowVideoCaptureImage()
{

    static char image_prefix[1024]=PREFIX_IMAGE_FILE;			/* Prefix of output image file */
    static unsigned char image[IMAGE_WIDTH_DS*IMAGE_HEIGHT_DS*RGB];	/* Captured image */
    static unsigned char skin_map[IMAGE_WIDTH_DS*IMAGE_HEIGHT_DS];	/* Skin color map */

    

/*======= Wait capture for current frame ====================*/
    CaptureV4LDoubleBufferingCaptureWait( fd , &vmap );

/*======= Set data to image array ===========================*/

    CaptureV4LCreateImageArray( vmap , vm , image );
    CaptureV4LSetImageDownSamplingForOpenGL( vmap , vm , DOWN_SAMPLING_RATE , resampled_image , disp_image );
    //CaptureV4LSetImageDownSamplingForOpenGL( vmap , vm , 10 , image , resampled_image );
    //

/*======= Begin capture for next frame ======================*/
    if( CaptureV4LDoubleBufferingCaptureNextFrame( fd , &vmap ) == -1 ) {
	fprintf( stderr , "COuld not capture next frame.\n" );
	exit(-1);
    }

    Resample( image, test, 160, 120, 160, 120);
    Resample( image, tmp, 160, 120, 16, 12);
    Resample( tmp, test, 16, 12, 160, 120);
    Resample( test, test, 160, 120, 160, 120);


/*======= Display image =====================================*/
    ShowVideoDisplayImage();


/*======= Extract skin color by simple thresholding =========*/
    if( extract_skin_color ) {
	//ExtractSkinColorSimpleThresholding( image , IMAGE_HEIGHT_DS , IMAGE_WIDTH_DS , skin_map );
	//ShowSkinRegion( skin_map , IMAGE_HEIGHT_DS , IMAGE_WIDTH_DS );

    }

    TurnImageUpsideDown(test, tmp2, 160, 120);
    //TurnImageUpsideDown(test, 160, 120);
 
/*======= Save image ========================================*/
    //ShowVideoSavePPMImage(test , 160, 120 , 0 , "pic" );

/*======= Swap Buffer: show graphics ========================*/
    glutSwapBuffers();

/*======= Increment frame number ============================*/
    frame_no++;

/*======= Check exit flag ===================================*/
    if( exit_flag ) {
	exit(0);
    }

}

int ShowVideoInitCaptureDevice( char *device_name , int channel_no )
{

    struct video_capability vcap;		/* structure for video capability */
    struct video_channel vch[MAX_NO_CHANNEL]; 	/* structure for video channel */ 
    struct video_picture vp;

/*======= Open video device ================================*/
    if( ( fd = CaptureV4LOpen( device_name ) ) == -1 ) {
	fprintf( stderr, "Could not open device %s.\n" , device_name );
	exit(-1);
    }

/*======= Get information of device =========================*/
    /*=== Get device capabilities ===*/
    if( CaptureV4LGetDeviceCapability( fd , &vcap ) == -1 ) {
	fprintf( stderr , "Could not get capabilities of video device.\n" );
	exit(-1);
    }

    /*=== Get channel information ===*/
    if( CaptureV4LGetChannelInfo( fd , vch , vcap.channels ) == -1 ) {
	fprintf( stderr , "Could not get channel information of video device.\n" );
	exit(-1);
    }

    /*=== Get memory map information ===*/
    if( CaptureV4LGetMemoryMapInfo( fd , &vm ) == -1 ) {
	fprintf( stderr , "Could not get memory map information.\n" );
	exit(-1);
    }

/*======= Show picture information =========================*/
    CaptureV4LGetPictureInfo( fd , &vp );
    CaptureV4LDisplayPictureInfo( vp );

/*======= Select channel ===================================*/
    if( CaptureV4LSelectChannel( fd , vch , channel_no ) == -1 ) {
	fprintf( stderr , "Could not select channel.\n" );
	exit(-1);
    }

/*======= Mapping frame buffer ==============================*/
    if( CaptureV4LMemoryMapping( fd , vm ) == -1 ) {
	fprintf( stdout , "Could not map frame buffer.\n" );
	exit(-1);
    }

/*======= Set image size and formate ========================*/
    vmap.width = CAPTURE_IMAGE_WIDTH;
    vmap.height = CAPTURE_IMAGE_HEIGHT;
    vmap.format = VIDEO_PALETTE_RGB24;
    
/*======= Begin Capture =====================================*/
    CaptureV4LDoubleBufferingInitCapture( fd , &vmap );

    return 0;

}

void ShowVideoDisplayImage()
{
    glRasterPos2i( 0 , 0 );
    glDrawPixels( IMAGE_WIDTH_DS , IMAGE_HEIGHT_DS , GL_BGR , GL_UNSIGNED_BYTE , disp_image ); 

    
    glRasterPos2i( 165 , 0 );
    glDrawPixels( IMAGE_WIDTH_DS , IMAGE_HEIGHT_DS , GL_BGR , GL_UNSIGNED_BYTE , test );

    glFlush();

}

void ShowVideoMouseCheck( int button , int status , int x , int y )
{

    if( button == GLUT_LEFT_BUTTON && status == GLUT_DOWN ) {
	exit_flag = 1;
    }

}

void ShowVideoSavePPMImage( unsigned char *image , int iheight , int iwidth , int frame_no , char *image_prefix )
{

    char file_name[1024];
    FILE *fp;
    sprintf( file_name , "/space/%s_%d.ppm" , image_prefix , frame_no );
    if( ( fp = fopen( file_name , "w" ) ) == NULL ) {
	fprintf( stderr , "Could not open image file: %s\n" , file_name );
	exit(1);
    }
    fprintf( fp , "P6 %d %d 255\n" , iheight , iwidth );
    fwrite( image , iwidth*iheight*RGB , 1 , fp );
    fclose(fp);
} 
