#include "show_video_v4l.h"
#include "extract_skin_color.h"

void ExtractSkinColorSimpleThresholding( unsigned char *image , int iheight , int iwidth , unsigned char *skin_map )
{

    static unsigned char tmp_skin_map[IMAGE_WIDTH_DS*IMAGE_HEIGHT_DS];	/* Skin color map */

    int i,j,k,l;
    int hblock_size;
    int no_skin_pixel;

    double r,g,b;	/* color components */
    double sum_cc;	/* sum of color components */

/*======== Extract candidate pixels =======================================*/
    for( i = 0 ; i < iheight ; i++ ) {
	for( j = 0 ; j < iwidth ; j++ ) {

	    /*=== Calculate color component for skin color ===*/
	    sum_cc = (double)(image[(i*iwidth+j)*RGB]+image[(i*iwidth+j)*RGB+1]+image[(i*iwidth+j)*RGB+2]);
	    r = (double)image[(i*iwidth+j)*RGB]/sum_cc;	/* r=R/(R+G+B) */
	    g = (double)image[(i*iwidth+j)*RGB+1]/sum_cc;	/* g=G/(R+G+B) */
	    b = (double)image[(i*iwidth+j)*RGB+2]/sum_cc;	/* b=B/(R+G+B) */
	    if( r > MIN_THRESH_r && r < MAX_THRESH_r ) {
		tmp_skin_map[i*iwidth+j] = 1;
	    } else {
		tmp_skin_map[i*iwidth+j] = 0;
	    }

	}
    }

/*======== Post processing: dilation by majority in block (3x3) ========*/
    hblock_size = BLOCK_SIZE/2;
    for( i = hblock_size ; i < iheight-hblock_size ; i += BLOCK_SIZE ) {
	for( j = hblock_size ; j < iwidth-hblock_size ; j += BLOCK_SIZE ) {
	    no_skin_pixel = 0;
	    for( k = -hblock_size ; k <= hblock_size ; k++ ) {
		for( l = -hblock_size ; l <= hblock_size ; l++ ) {
		    if( tmp_skin_map[(i+k)*iwidth+(j+l)] ) {
			no_skin_pixel++;
		    }
		}
	    }
	    if( no_skin_pixel > BLOCK_SIZE*BLOCK_SIZE/2 ) { 	/* use majority rule */
		for( k = -hblock_size ; k <= hblock_size ; k++ ) {
		    for( l = -hblock_size ; l <= hblock_size ; l++ ) {
			skin_map[(i+k)*iwidth+(j+l)] = 1; 
		    }
		}
	    } else {
		for( k = -hblock_size ; k <= hblock_size ; k++ ) {
		    for( l = -hblock_size ; l <= hblock_size ; l++ ) {
			skin_map[(i+k)*iwidth+(j+l)] = 0; 
		    }
		}
	    }
	}
    }    

}

void ShowSkinRegion( unsigned char *skin_map , int iheight , int iwidth )
{

    int i,j;

    glColor3f( 0.0 , 1.0 , 0.0 ); /* green */
    glPointSize( 2.0 ); /* point size */
    for( i = 0 ; i < iheight ; i++ ) {
	for( j = 0 ; j < iwidth ; j++ ) {
	    if( skin_map[i*iwidth+j] ) {
		DisplayPoint( (double)j , (double)i );
	    }
	}
    }
    
}
